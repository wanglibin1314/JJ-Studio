package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.UploadHistVo;

import java.util.List;

public interface UploadHistDao {

    List<UploadHistVo> selectUploadHist(String table, UploadHistVo uploadHistVo);

    void addUploadHistVo(String table, UploadHistVo uploadHistVo);

    void addUploadHistList(String table, List<UploadHistVo> uploadHistVoList);
}
