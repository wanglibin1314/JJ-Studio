/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.entities.beans.project.DeviceMapBean;
import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.LoginlogVo;
import cn.easyplatform.studio.vos.ProjectRoleVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IdentityDao {

    LoginUserVo getUser(String userId);

    boolean addUser(String userId, String name, String salt, String password);

    boolean addProject(String userId, String projectId, String roleId);

    void updateState(String userId, String address, int state);

    void updateTryTimes(String userId, int times, String ip);

    void updateTheme(String userId, String theme);

    void updateEditorTheme(String userId, String theme);

    void updatePassword(String userId, String password);

    void updateDefaultProject(String userId, String defaultProject);

    ProjectBean[] getProject(String userId);

    ProjectBean[] getProjectWithID(String projectID, String roleID, String userId);

    ProjectBean[] getAllProject();

    ProjectBean getProjectDetail(String projectId);

    void updateProject(ProjectBean bean);

    String getRolePermissions(String projectID, String roleID);

    String getTopRole(String projectID, String userID);

    List<ProjectRoleVo> getAllProjectRole(String userID);

    List<Object[]> selectList(String sql, FieldVo... params);

    int update(String sql, List<FieldVo> params);

    LoginlogVo getOnlineUser(String projectId,String userId, String roleId);

    long insertDevice(String projectId,String xml);

    void updateDevice(long id,String projectId,String xml);

    boolean deleteDevice(long id);

    String getPageXml(long id);

}
