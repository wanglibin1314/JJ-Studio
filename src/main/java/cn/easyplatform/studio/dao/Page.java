/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Page {

	// 当前页
	private int pageNo = 1;
	// 每页记录数
	private int pageSize = 10;
	// 总记录数
	private int totalCount = 0;
	// 排序
	private String orderBy;
	// 是否需要获取总笔数
	private boolean isGetTotal = true;

	public Page(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 获得当前页的页号,默认为1.
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * 设置当前页的页号,小于1时自动设置为1.
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
		if (pageNo < 1) {
			this.pageNo = 1;
		}
	}

	/**
	 * 获得每页记录数.
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 获得总记录数, 默认值为0.
	 */
	public int getTotalCount() {
		return totalCount < 0 ? 0 : totalCount;
	}

	/**
	 * 设置总记录数.
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean isGetTotal() {
		return isGetTotal;
	}

	public void setGetTotal(boolean isGetTotal) {
		this.isGetTotal = isGetTotal;
	}

	/**
	 * 根据pageSize与totalCount计算总页数, 默认值为-1.
	 */
	public int getTotalPages() {
		if (totalCount < 0) {
			return 0;
		}
		int count = totalCount / pageSize;
		if (totalCount % pageSize > 0) {
			count++;
		}
		return count;
	}
}
