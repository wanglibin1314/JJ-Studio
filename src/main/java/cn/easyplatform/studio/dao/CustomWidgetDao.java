package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.CustomWidgetVo;

import java.util.List;

public interface CustomWidgetDao {

    List<CustomWidgetVo> selectAllPublicList();

    List<CustomWidgetVo> selectAllProductList(String tableName, String userId);

    CustomWidgetVo selectOneProduct(String tableName, String name);

    Boolean addWidget(String tableName, CustomWidgetVo widget);
}
