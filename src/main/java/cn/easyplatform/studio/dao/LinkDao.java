package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.LinkVo;

import java.util.List;

public interface LinkDao {
    List<LinkVo> getLink(String table);

    LinkVo getLinkWithEntityId(String table, String entityId);

    List<LinkVo> getLinkWithChildrenEntityId(String table, String entityId);

    boolean addLink(String table, LinkVo vo);

    boolean deleteLink(String table, String entityId);

    boolean updateLink(String table, LinkVo vo);

    boolean updateLinks(String table, List<LinkVo> voList);
}
