/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.vos.HistoryVo;
import cn.easyplatform.studio.vos.IXVo;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface VersionDao {

    List<EntityInfo> selectList(Page page, String sql, Object... parameters);

    boolean containExport(String txId);

    EntityInfo selectOne(String table, String entityId);

    List<EntityInfo> selectChooseList(String table, List<String> entityIdList);

    void createRepository(String projectId, String userId, String table);

    List<HistoryVo> getHistory(String table, String entityId);

    List<IXVo> selectHistory(String table, String projectId);

    String getContent(String table, long versionNo);

    void save(String table, IXVo vo, String productId);

}
