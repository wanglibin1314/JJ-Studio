package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.DicDetailVo;

import java.util.List;

public interface DicDetailDao {
    List<DicDetailVo> selectAll();
    List<DicDetailVo> selectOne(String dicCode);
    Boolean setDicDetailList(List<DicDetailVo> dicDetailVos);
    Boolean updateDicDetailList(List<DicDetailVo> dicDetailVos);
}
