/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.studio.vos.*;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface BizDao {

	DataSource getDataSource();

	List<MenuVo> getMenus();

	List<AccessVo> getAccess();

	List<RoleAccessVo> getRoleAccess(String roleID);

	ResultVo selectList(String sql, Page page, FieldVo... params);

	List<Object[]> selectList(String sql, FieldVo... params);

	int update(String sql, List<FieldVo> params);

	List<TableBean> getTables();

	TableBean revertTable(String table);

	void createTable(TableBean table);

	void buildIndex(TableBean source, TableBean target);

	void buildForeignKey(TableBean source, TableBean target);

	void buildTable(TableVo tv);

	void buildPrimaryKey(TableBean source, TableBean target);

	void dropTable(TableBean table);

	List<String> getColumnNameList(String table);

	boolean exists(String table);
}
