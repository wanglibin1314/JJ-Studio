/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PsqlBizDao extends AbstractBizDao{

	/**
	 * @param dataSource
	 */
	public PsqlBizDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new PsqlDialect();
	}


	@Override
	public String evalFieldType(TableField field) {
		switch (field.getType()) {
		case INT:
			// 用户自定义了宽度
			if (field.getLength() > 0)
				return "NUMERIC(" + field.getLength() + ")";
			// 用数据库的默认宽度
			return "INT";

		case DATETIME:
			return "TIMESTAMP";
		default:
			return super.evalFieldType(field);
		}
	}
}
