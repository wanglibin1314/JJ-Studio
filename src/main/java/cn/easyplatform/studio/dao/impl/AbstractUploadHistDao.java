package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.UploadHistDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.UploadHistVo;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractUploadHistDao implements UploadHistDao {
    private DataSource ds;

    public AbstractUploadHistDao(DataSource dataSource) {
        this.ds = dataSource;
    }

    protected abstract Dialect getDialect();

    @Override
    public List<UploadHistVo> selectUploadHist(String table, UploadHistVo uploadHistVo) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select id,fileName,uploadData,packName,type,contentType,projectName,uploadUser,createDate from " + table + " where projectName=?"+" and type=?"+" order by createDate desc ");
            pstmt.setString(1,uploadHistVo.getProjectName());
            pstmt.setString(2,uploadHistVo.getType());
            rs = pstmt.executeQuery();
            List<UploadHistVo> list = new ArrayList<>();
            UploadHistVo vo = null;
            while (rs.next()) {
                try {
                    vo = new UploadHistVo(rs.getInt(1),rs.getString(2), new String((byte[]) rs.getObject(3),"UTF-8"),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getTimestamp(9));
                    list.add(vo);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            return list;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public void addUploadHistVo(String table, UploadHistVo uploadHistVo) {
        PreparedStatement pstmt = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("insert into "
                            + table
                            + " (fileName,uploadData,packName,type,contentType,projectName,uploadUser) values(?,?,?,?,?,?,?)");
            pstmt.setString(1,uploadHistVo.getFileName());
            pstmt.setBlob(2, new ByteArrayInputStream(uploadHistVo.getUploadData().getBytes()));
            pstmt.setString(3, uploadHistVo.getPackName());
            pstmt.setString(4, uploadHistVo.getType());
            pstmt.setString(5, uploadHistVo.getContentType());
            pstmt.setString(6, uploadHistVo.getProjectName());
            pstmt.setString(7, uploadHistVo.getUploadUser());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }

    @Override
    public void addUploadHistList(String table, List<UploadHistVo> uploadHistVoList) {
        PreparedStatement pstmt = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("insert into "
                            + table
                            + " (fileName,uploadData,packName,type,contentType,projectName,uploadUser) values(?,?,?,?,?,?,?)");
            for (UploadHistVo histVo : uploadHistVoList) {
                pstmt.setString(1,histVo.getFileName());
                pstmt.setBlob(2, new ByteArrayInputStream(histVo.getUploadData().getBytes()));
                pstmt.setString(3, histVo.getPackName());
                pstmt.setString(4, histVo.getType());
                pstmt.setString(5, histVo.getContentType());
                pstmt.setString(6, histVo.getProjectName());
                pstmt.setString(7, histVo.getUploadUser());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }
}
