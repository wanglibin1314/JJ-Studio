package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDetailDao;

import javax.sql.DataSource;

public class OracleDicDetailDao extends AbstractDicDetailDao {
    /**
     * @param ds
     */
    public OracleDicDetailDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
