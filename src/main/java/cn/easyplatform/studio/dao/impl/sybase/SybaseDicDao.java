package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDao;

import javax.sql.DataSource;

public class SybaseDicDao extends AbstractDicDao {
    /**
     * @param ds
     */
    public SybaseDicDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
