package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDao;

import javax.sql.DataSource;

public class Db2DicDao extends AbstractDicDao {
    /**
     * @param ds
     */
    public Db2DicDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
