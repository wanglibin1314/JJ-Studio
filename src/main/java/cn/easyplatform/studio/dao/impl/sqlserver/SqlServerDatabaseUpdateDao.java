package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDatabaseUpdateDao;

import javax.sql.DataSource;

public class SqlServerDatabaseUpdateDao extends AbstractDatabaseUpdateDao {
    /**
     * @param ds
     */
    public SqlServerDatabaseUpdateDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
