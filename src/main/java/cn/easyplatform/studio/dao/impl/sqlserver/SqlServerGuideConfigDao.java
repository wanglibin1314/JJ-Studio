package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractGuideConfigDao;

import javax.sql.DataSource;

public class SqlServerGuideConfigDao extends AbstractGuideConfigDao {
    /**
     * @param ds
     */
    public SqlServerGuideConfigDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
