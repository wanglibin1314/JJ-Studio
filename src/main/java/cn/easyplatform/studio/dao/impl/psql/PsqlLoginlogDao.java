package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class PsqlLoginlogDao extends AbstractLoginlogDao {

    public PsqlLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
