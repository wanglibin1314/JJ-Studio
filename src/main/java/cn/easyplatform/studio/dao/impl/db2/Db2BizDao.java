/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Db2BizDao extends AbstractBizDao{

	/**
	 * @param dataSource
	 */
	public Db2BizDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new Db2Dialect();
	}


	@Override
	protected String evalFieldType(TableField field) {
		switch (field.getType()) {
		case BOOLEAN:
			return "SMALLINT";
		case INT:
			return "INTEGER";
		case LONG:
			return "decimal(23)";
		default:
			return super.evalFieldType(field);
		}

	}
}
