/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.Page;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class PsqlDialect extends Dialect {

	@Override
	protected String getPageBefore(Page page) {
		return "";
	}

	@Override
	protected String getPageAfter(Page page) {
		int start = (page.getPageNo() - 1) * page.getPageSize();
		StringBuilder sb = new StringBuilder();
		sb.append(" LIMIT ").append(page.getPageSize()).append(" OFFSET ")
				.append(start);
		return sb.toString();
	}

}
