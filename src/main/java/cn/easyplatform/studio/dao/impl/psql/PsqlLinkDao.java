package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLinkDao;

import javax.sql.DataSource;

public class PsqlLinkDao extends AbstractLinkDao {
    public PsqlLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
