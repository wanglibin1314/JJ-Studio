package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractGuideConfigDao;

import javax.sql.DataSource;

public class MySqlGuideConfigDao extends AbstractGuideConfigDao {
    /**
     * @param ds
     */
    public MySqlGuideConfigDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
