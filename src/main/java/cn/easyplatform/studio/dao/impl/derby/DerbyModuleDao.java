package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractModuleDao;

import javax.sql.DataSource;

public class DerbyModuleDao extends AbstractModuleDao {
    public DerbyModuleDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
