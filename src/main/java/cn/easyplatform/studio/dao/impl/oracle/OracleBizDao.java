/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import org.apache.commons.lang3.RandomStringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class OracleBizDao extends AbstractBizDao {

    /**
     * @param dataSource
     */
    public OracleBizDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }

    @Override
    protected String evalFieldType(TableField field) {
        switch (field.getType()) {
            case BOOLEAN:
                return "NUMBER(1)";
            case CLOB:
                return "CLOB";
            case VARCHAR:
                return "VARCHAR2(" + field.getLength() + ")";
            case LONG:
                return "NUMBER(23)";
            case INT:
                // 用户自定义了宽度
                if (field.getLength() > 0)
                    return "NUMBER(" + field.getLength() + ")";
                return "NUMBER";
            case NUMERIC:
                // 用户自定义了精度
                if (field.getLength() > 0) {
                    return "NUMBER(" + field.getLength() + "," + field.getDecimal()
                            + ")";
                } else
                    // 用默认精度
                    return "NUMBER(23,2)";
            case TIME:
            case DATETIME:
            case DATE:
                return "DATE";
            default:
                return super.evalFieldType(field);
        }
    }

    protected void restore(String source, String target) {
        Statement stmt = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE ").append(target).append(" AS SELECT * FROM ").append(source);
            stmt = conn.createStatement();
            stmt.execute(sb.toString());
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(stmt);
        }
    }

    protected String backup(String table/*, List<TableField> fields*/) {
        Statement stmt = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            String tmpTable = table + "_"
                    + RandomStringUtils.randomAlphanumeric(10);
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE ").append(tmpTable).append(" AS SELECT ");
            /*for (TableField tf : fields)
                sb.append(tf.getName()).append(",");
            sb.deleteCharAt(sb.length() - 1);*/
            sb.append("* FROM ").append(table);
            stmt = conn.createStatement();
            stmt.execute(sb.toString());
            return tmpTable;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(stmt);
        }
    }
}
