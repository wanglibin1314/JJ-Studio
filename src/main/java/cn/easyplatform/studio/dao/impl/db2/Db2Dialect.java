/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.Page;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class Db2Dialect extends Dialect{

	@Override
	protected String getPageBefore(Page page) {
		return "SELECT * FROM (SELECT ROW_NUMBER() OVER() AS ROWNUM, T.* FROM (";
	}

	@Override
	protected String getPageAfter(Page page) {
		int start = (page.getPageNo() - 1) * page.getPageSize() + 1;
		return String.format(") T) AS A WHERE ROWNUM BETWEEN %d AND %d", start,
				start + page.getPageSize());
	}
}
