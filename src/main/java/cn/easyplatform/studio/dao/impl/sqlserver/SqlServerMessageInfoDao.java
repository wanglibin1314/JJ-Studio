package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMessageInfoDao;

import javax.sql.DataSource;

public class SqlServerMessageInfoDao extends AbstractMessageInfoDao {
    /**
     * @param ds
     */
    public SqlServerMessageInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
