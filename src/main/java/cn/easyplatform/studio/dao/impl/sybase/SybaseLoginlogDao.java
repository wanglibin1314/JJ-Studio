package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;
import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class SybaseLoginlogDao extends AbstractLoginlogDao {

    public SybaseLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
