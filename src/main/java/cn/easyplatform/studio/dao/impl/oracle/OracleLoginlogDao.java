package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class OracleLoginlogDao extends AbstractLoginlogDao {

    public OracleLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
