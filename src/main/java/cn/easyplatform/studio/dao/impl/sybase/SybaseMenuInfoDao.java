package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMenuInfoDao;

import javax.sql.DataSource;

public class SybaseMenuInfoDao extends AbstractMenuInfoDao {
    /**
     * @param ds
     */
    public SybaseMenuInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
