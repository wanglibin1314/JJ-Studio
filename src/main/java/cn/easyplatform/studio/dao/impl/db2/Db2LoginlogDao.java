package cn.easyplatform.studio.dao.impl.db2;


import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;


/**
 * @Author Zeta
 * @Version 1.0
 */
public class Db2LoginlogDao extends AbstractLoginlogDao {

    public Db2LoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
