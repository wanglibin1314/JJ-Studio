package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractModuleDao;

import javax.sql.DataSource;

public class MySqlModuleDao extends AbstractModuleDao {
    public MySqlModuleDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
