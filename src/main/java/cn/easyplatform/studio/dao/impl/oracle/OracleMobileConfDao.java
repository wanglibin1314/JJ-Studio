package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMobileConfDao;

import javax.sql.DataSource;

public class OracleMobileConfDao extends AbstractMobileConfDao {

    public OracleMobileConfDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
