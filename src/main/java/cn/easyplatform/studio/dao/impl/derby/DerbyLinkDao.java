package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLinkDao;

import javax.sql.DataSource;

public class DerbyLinkDao extends AbstractLinkDao {
    public DerbyLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
