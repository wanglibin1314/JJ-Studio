package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDao;

import javax.sql.DataSource;

public class SqlServerDicDao extends AbstractDicDao {
    /**
     * @param ds
     */
    public SqlServerDicDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
