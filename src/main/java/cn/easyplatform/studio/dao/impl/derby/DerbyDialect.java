/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.Page;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class DerbyDialect extends Dialect {

	@Override
	protected String getPageBefore(Page page) {
		return "";
	}

	@Override
	protected String getPageAfter(Page page) {
		int start = (page.getPageNo() - 1) * page.getPageSize();
		return String.format(" OFFSET %d ROWS FETCH NEXT %d ROW ONLY", start,
				page.getPageSize());
	}
}
