package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMessageInfoDao;

import javax.sql.DataSource;

public class DerbyMessageInfoDao extends AbstractMessageInfoDao {
    /**
     * @param ds
     */
    public DerbyMessageInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
