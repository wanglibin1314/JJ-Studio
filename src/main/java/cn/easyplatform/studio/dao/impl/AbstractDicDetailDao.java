package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.DicDetailDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.DicDetailVo;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDicDetailDao implements DicDetailDao {
    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractDicDetailDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    @Override
    public List<DicDetailVo> selectAll() {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            stmt = conn
                    .prepareStatement("select diccode,detailno,desc1,displayflag,editableflag,detailsort,chosenflag," +
                            "otherflag,dicstatus,simplecode,zh_cn,zh_tw,en_us,ja_jp,ko_kr,de_de,fr_fr,it_it,DETAILID " +
                            "from sys_dic_detail_info order by DICCODE,DETAILNO");
            rs = stmt.executeQuery();
            List<DicDetailVo> data = new ArrayList<>();
            while (rs.next()) {
                DicDetailVo mv = new DicDetailVo();
                mv.setDicCode(rs.getString(1));
                mv.setDetailNO(rs.getString(2));
                mv.setDesc1(rs.getString(3));
                mv.setDisplayFlag(rs.getString(4));
                mv.setEditableFlag(rs.getString(5));
                mv.setDetailSort(rs.getString(6));
                mv.setChosenFlag(rs.getString(7));
                mv.setOtherFlag(rs.getString(8));
                mv.setDicStatus(rs.getString(9));
                mv.setSimpleCode(rs.getString(10));
                mv.setZh_cn(rs.getString(11));
                mv.setZh_tw(rs.getString(12));
                mv.setEn_us(rs.getString(13));
                mv.setJa_jp(rs.getString(14));
                mv.setKo_kr(rs.getString(15));
                mv.setDe_de(rs.getString(16));
                mv.setFr_fr(rs.getString(17));
                mv.setIt_it(rs.getString(18));
                mv.setDetailID(rs.getString(19));
                data.add(mv);
            }
            return data;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(stmt, rs);
        }
    }

    @Override
    public Boolean setDicDetailList(List<DicDetailVo> dicDetailVos)
    {
        if (dicDetailVos == null || dicDetailVos.size() == 0)
        {
            return false;
        }
        synchronized (dicDetailVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("INSERT INTO `sys_dic_detail_info`" +
                                "(`DICCODE`, `DETAILNO`, `DESC1`, `DISPLAYFLAG`, `EDITABLEFLAG`, `DETAILSORT`, `CHOSENFLAG`, " +
                                "`OTHERFLAG`, `DICSTATUS`, `SIMPLECODE`, `ZH_CN`, `ZH_TW`, `EN_US`, `JA_JP`, `KO_KR`, `DE_DE`, " +
                                "`FR_FR`, `IT_IT`, `DETAILID`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                for (DicDetailVo dicVo:dicDetailVos) {
                    pstmt.setString(1, dicVo.getDicCode());
                    pstmt.setString(2, dicVo.getDetailNO());
                    pstmt.setString(3, dicVo.getDesc1());
                    pstmt.setString(4, dicVo.getDisplayFlag());
                    pstmt.setString(5, dicVo.getEditableFlag());
                    pstmt.setString(6, dicVo.getDetailSort());
                    pstmt.setString(7, dicVo.getChosenFlag());
                    pstmt.setString(8, dicVo.getOtherFlag());
                    pstmt.setString(9, dicVo.getDicStatus());
                    pstmt.setString(10, dicVo.getSimpleCode());
                    pstmt.setString(11, dicVo.getZh_cn());
                    pstmt.setString(12, dicVo.getZh_tw());
                    pstmt.setString(13, dicVo.getEn_us());
                    pstmt.setString(14, dicVo.getJa_jp());
                    pstmt.setString(15, dicVo.getKo_kr());
                    pstmt.setString(16, dicVo.getDe_de());
                    pstmt.setString(17, dicVo.getFr_fr());
                    pstmt.setString(18, dicVo.getIt_it());
                    pstmt.setString(19, dicVo.getDetailID());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public Boolean updateDicDetailList(List<DicDetailVo> dicDetailVos)
    {
        if (dicDetailVos == null || dicDetailVos.size() == 0)
        {
            return false;
        }
        synchronized (dicDetailVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("UPDATE `sys_dic_detail_info` SET " +
                                "`DESC1` = ?, `DISPLAYFLAG` = ?, `EDITABLEFLAG` = ?, `DETAILSORT` = ?, `CHOSENFLAG` = ?, " +
                                "`OTHERFLAG` = ?, `DICSTATUS` = ?, `SIMPLECODE` = ?, `ZH_CN` = ?, `ZH_TW` = ?, `EN_US` = ?, " +
                                "`JA_JP` = ?, `KO_KR` = ?, `DE_DE` = ?, `FR_FR` = ?, `IT_IT` = ?, `DETAILID` = ? " +
                                "WHERE `DICCODE` = ? AND `DETAILNO` = ?");
                for (DicDetailVo dicVo:dicDetailVos) {
                    pstmt.setString(1, dicVo.getDesc1());
                    pstmt.setString(2, dicVo.getDisplayFlag());
                    pstmt.setString(3, dicVo.getEditableFlag());
                    pstmt.setString(4, dicVo.getDetailSort());
                    pstmt.setString(5, dicVo.getChosenFlag());
                    pstmt.setString(6, dicVo.getOtherFlag());
                    pstmt.setString(7, dicVo.getDicStatus());
                    pstmt.setString(8, dicVo.getSimpleCode());
                    pstmt.setString(9, dicVo.getZh_cn());
                    pstmt.setString(10, dicVo.getZh_tw());
                    pstmt.setString(11, dicVo.getEn_us());
                    pstmt.setString(12, dicVo.getJa_jp());
                    pstmt.setString(13, dicVo.getKo_kr());
                    pstmt.setString(14, dicVo.getDe_de());
                    pstmt.setString(15, dicVo.getFr_fr());
                    pstmt.setString(16, dicVo.getIt_it());
                    pstmt.setString(17, dicVo.getDetailID());
                    pstmt.setString(18, dicVo.getDicCode());
                    pstmt.setString(19, dicVo.getDetailNO());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public List<DicDetailVo> selectOne(String dicCode) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            stmt = conn
                    .prepareStatement("select diccode,detailno,desc1,displayflag,editableflag,detailsort,chosenflag," +
                            "otherflag,dicstatus,simplecode,zh_cn,zh_tw,en_us,ja_jp,ko_kr,de_de,fr_fr,it_it,DETAILID " +
                            "from sys_dic_detail_info where diccode=?  order by DICCODE,DETAILNO");
            stmt.setString(1, dicCode);
            rs = stmt.executeQuery();
            List<DicDetailVo> data = new ArrayList<>();
            while (rs.next()) {
                DicDetailVo mv = new DicDetailVo();
                mv.setDicCode(rs.getString(1));
                mv.setDetailNO(rs.getString(2));
                mv.setDesc1(rs.getString(3));
                mv.setDisplayFlag(rs.getString(4));
                mv.setEditableFlag(rs.getString(5));
                mv.setDetailSort(rs.getString(6));
                mv.setChosenFlag(rs.getString(7));
                mv.setOtherFlag(rs.getString(8));
                mv.setDicStatus(rs.getString(9));
                mv.setSimpleCode(rs.getString(10));
                mv.setZh_cn(rs.getString(11));
                mv.setZh_tw(rs.getString(12));
                mv.setEn_us(rs.getString(13));
                mv.setJa_jp(rs.getString(14));
                mv.setKo_kr(rs.getString(15));
                mv.setDe_de(rs.getString(16));
                mv.setFr_fr(rs.getString(17));
                mv.setIt_it(rs.getString(18));
                mv.setDetailID(rs.getString(19));
                data.add(mv);
            }
            return data;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(stmt, rs);
        }
    }
}
