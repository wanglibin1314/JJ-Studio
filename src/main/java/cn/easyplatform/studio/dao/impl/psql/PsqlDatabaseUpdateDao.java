package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDatabaseUpdateDao;

import javax.sql.DataSource;

public class PsqlDatabaseUpdateDao extends AbstractDatabaseUpdateDao {
    /**
     * @param ds
     */
    public PsqlDatabaseUpdateDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return null;
    }
}
