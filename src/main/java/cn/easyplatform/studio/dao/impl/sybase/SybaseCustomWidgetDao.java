package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class SybaseCustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public SybaseCustomWidgetDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
