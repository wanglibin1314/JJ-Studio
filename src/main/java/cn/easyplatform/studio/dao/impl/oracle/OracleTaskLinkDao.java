package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractTaskLinkDao;

import javax.sql.DataSource;

public class OracleTaskLinkDao extends AbstractTaskLinkDao {
    public OracleTaskLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
