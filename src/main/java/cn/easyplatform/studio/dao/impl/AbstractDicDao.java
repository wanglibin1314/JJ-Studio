package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.DicDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.DicVo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDicDao implements DicDao {
    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractDicDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();
    @Override
    public List<DicVo> selectAllList() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select diccode,createuser,createdate,updateuser,updatedate,desc1,upcode," +
                    "diclevel,displayflag,editableflag,dicstatus,zh_cn,zh_tw,en_us,ja_jp,ko_kr,de_de,fr_fr,it_it from sys_dic_info where 1=1");
            rs = pstmt.executeQuery();
            List<DicVo> loadedObjects = new ArrayList<DicVo>();
            while (rs.next()) {
                DicVo e = new DicVo();
                e.setDicCode(rs.getString(1));
                e.setCreateUser(rs.getString(2));
                e.setCreateDate(rs.getTimestamp(3));
                e.setUpdateUser(rs.getString(4));
                e.setUpdateDate(rs.getTimestamp(5));
                e.setDesc1(rs.getString(6));
                e.setUpCode(rs.getString(7));
                e.setDicLevel(rs.getString(8));
                e.setDisplayFlag(rs.getString(9));
                e.setEditableFlag(rs.getString(10));
                e.setDicStatus(rs.getString(11));
                e.setZh_cn(rs.getString(12));
                e.setZh_tw(rs.getString(13));
                e.setEn_us(rs.getString(14));
                e.setJa_jp(rs.getString(15));
                e.setKo_kr(rs.getString(16));
                e.setDe_de(rs.getString(17));
                e.setFr_fr(rs.getString(18));
                e.setIt_it(rs.getString(19));
                loadedObjects.add(e);
            }
            return loadedObjects;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public Boolean setDicList(List<DicVo> dicVos)
    {
        if (dicVos == null || dicVos.size() == 0)
        {
            return false;
        }
        synchronized (dicVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("INSERT INTO `sys_dic_info`" +
                                "(`diccode`, `createuser`, `createdate`, `updateuser`, `updatedate`, `desc1`, `upcode`," +
                                " `diclevel`, `displayflag`, `editableflag`, `dicstatus`, `zh_cn`, `zh_tw`, `en_us`, " +
                                "`ja_jp`, `ko_kr`, `de_de`, `fr_fr`, `it_it`) VALUES " +
                                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                for (DicVo dicVo : dicVos) {
                    pstmt.setString(1, dicVo.getDicCode());
                    pstmt.setString(2, dicVo.getCreateUser());
                    pstmt.setTimestamp(3, dicVo.getCreateDate() == null ? null: new Timestamp(dicVo.getCreateDate().getTime()));
                    pstmt.setString(4, dicVo.getUpdateUser());
                    pstmt.setTimestamp(5, dicVo.getUpdateDate() == null ? null: new Timestamp(dicVo.getUpdateDate().getTime()));
                    pstmt.setString(6, dicVo.getDesc1());
                    pstmt.setString(7, dicVo.getUpCode());
                    pstmt.setString(8, dicVo.getDicLevel());
                    pstmt.setString(9, dicVo.getDisplayFlag());
                    pstmt.setString(10, dicVo.getEditableFlag());
                    pstmt.setString(11, dicVo.getDicStatus());
                    pstmt.setString(12, dicVo.getZh_cn());
                    pstmt.setString(13, dicVo.getZh_tw());
                    pstmt.setString(14, dicVo.getEn_us());
                    pstmt.setString(15, dicVo.getJa_jp());
                    pstmt.setString(16, dicVo.getKo_kr());
                    pstmt.setString(17, dicVo.getDe_de());
                    pstmt.setString(18, dicVo.getFr_fr());
                    pstmt.setString(19, dicVo.getIt_it());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public Boolean updateDicList(List<DicVo> dicVos)
    {
        if (dicVos == null || dicVos.size() == 0)
        {
            return false;
        }
        synchronized (dicVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn.prepareStatement("UPDATE `sys_dic_info` SET " +
                        "`createuser` = ?, `createdate` = ?, `updateuser` = ?, `updatedate` = ?, `desc1` = ?, `upcode` = ?, " +
                        "`diclevel` = ?, `displayflag` = ?, `editableflag` = ?, `dicstatus` = ?, `zh_cn` = ?, `zh_tw` = ?, " +
                        "`en_us` = ?, `ja_jp` = ?, `ko_kr` = ?, `de_de` = ?, `fr_fr` = ?, `it_it` = ? WHERE `diccode` = ?");
                for (DicVo dicVo : dicVos) {
                    pstmt.setString(1, dicVo.getCreateUser());
                    pstmt.setTimestamp(2, dicVo.getCreateDate() == null ? null: new Timestamp(dicVo.getCreateDate().getTime()));
                    pstmt.setString(3, dicVo.getUpdateUser());
                    pstmt.setTimestamp(4, dicVo.getUpdateDate() == null ? null: new Timestamp(dicVo.getUpdateDate().getTime()));
                    pstmt.setString(5, dicVo.getDesc1());
                    pstmt.setString(6, dicVo.getUpCode());
                    pstmt.setString(7, dicVo.getDicLevel());
                    pstmt.setString(8, dicVo.getDisplayFlag());
                    pstmt.setString(9, dicVo.getEditableFlag());
                    pstmt.setString(10, dicVo.getDicStatus());
                    pstmt.setString(11, dicVo.getZh_cn());
                    pstmt.setString(12, dicVo.getZh_tw());
                    pstmt.setString(13, dicVo.getEn_us());
                    pstmt.setString(14, dicVo.getJa_jp());
                    pstmt.setString(15, dicVo.getKo_kr());
                    pstmt.setString(16, dicVo.getDe_de());
                    pstmt.setString(17, dicVo.getFr_fr());
                    pstmt.setString(18, dicVo.getIt_it());
                    pstmt.setString(19, dicVo.getDicCode());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public DicVo selectOne(String dicID) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select diccode,createuser,createdate,updateuser,updatedate,desc1,upcode," +
                    "diclevel,displayflag,editableflag,dicstatus,zh_cn,zh_tw,en_us,ja_jp,ko_kr,de_de,fr_fr,it_it from sys_dic_info where diccode=?");
            pstmt.setString(1, dicID);
            rs = pstmt.executeQuery();
            DicVo e = new DicVo();
            while (rs.next()) {
                e.setDicCode(rs.getString(1));
                e.setCreateUser(rs.getString(2));
                e.setCreateDate(rs.getTimestamp(3));
                e.setUpdateUser(rs.getString(4));
                e.setUpdateDate(rs.getTimestamp(5));
                e.setDesc1(rs.getString(6));
                e.setUpCode(rs.getString(7));
                e.setDicLevel(rs.getString(8));
                e.setDisplayFlag(rs.getString(9));
                e.setEditableFlag(rs.getString(10));
                e.setDicStatus(rs.getString(11));
                e.setZh_cn(rs.getString(12));
                e.setZh_tw(rs.getString(13));
                e.setEn_us(rs.getString(14));
                e.setJa_jp(rs.getString(15));
                e.setKo_kr(rs.getString(16));
                e.setDe_de(rs.getString(17));
                e.setFr_fr(rs.getString(18));
                e.setIt_it(rs.getString(19));
            }
            return e;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }
}
