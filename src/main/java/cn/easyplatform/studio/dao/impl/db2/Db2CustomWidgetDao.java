package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class Db2CustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public Db2CustomWidgetDao(DataSource ds) {
        super(ds);
    }
    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
