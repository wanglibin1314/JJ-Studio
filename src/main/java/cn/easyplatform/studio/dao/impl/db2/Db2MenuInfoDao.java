package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMenuInfoDao;

import javax.sql.DataSource;

public class Db2MenuInfoDao extends AbstractMenuInfoDao {
    /**
     * @param ds
     */
    public Db2MenuInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
