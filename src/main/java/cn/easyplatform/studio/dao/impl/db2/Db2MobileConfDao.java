package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMobileConfDao;

import javax.sql.DataSource;

public class Db2MobileConfDao extends AbstractMobileConfDao {

    public Db2MobileConfDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
