/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Nums;
import cn.easyplatform.studio.dao.*;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.vos.*;
import cn.easyplatform.type.FieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractVersionDao implements VersionDao {

    protected final static Logger log = LoggerFactory.getLogger(AbstractVersionDao.class);

    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractVersionDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    public List<EntityInfo> selectList(Page page, String statement, Object... parameters) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            StringBuilder sb = new StringBuilder();
            if (page != null && page.isGetTotal()) {
                sb.append("select count(1) from (").append(statement)
                        .append(") c");
                pstmt = conn.prepareStatement(sb.toString());
                if (parameters.length > 0) {
                    for (int i = 0; i < parameters.length; i++)
                        pstmt.setObject(i + 1, parameters[i]);
                }
                rs = pstmt.executeQuery();
                rs.next();
                page.setTotalCount(Nums.toInt(rs.getObject(1).toString(), 0));
                DaoUtils.closeQuietly(pstmt, rs);
                sb.setLength(0);
            }
            sb.append(statement);
            if (page != null) {
                if (page.isGetTotal() && page.getTotalCount() == 0)
                    return Collections.emptyList();
                if (page.getOrderBy() != null)
                    sb.append(" order by ").append(page.getOrderBy());
                statement = getDialect().getPageSql(sb.toString(), page);
            }
            log.debug("getList->%s", statement);
            pstmt = conn.prepareStatement(statement);
            if (parameters.length > 0) {
                for (int i = 0; i < parameters.length; i++) {
                    FieldType type = FieldType.cast(parameters[i]);
                    FieldVo fieldVo = new FieldVo(type, parameters[i]);
                    DaoUtils.setValue(pstmt, i + 1, fieldVo);
                }
            }
            rs = pstmt.executeQuery();
            List<EntityInfo> loadedObjects = new ArrayList<EntityInfo>();
            while (rs.next()) {
                EntityInfo e = new EntityInfo();
                e.setVersion(rs.getInt(1));
                e.setId(rs.getString(2));
                e.setName(rs.getString(3));
                e.setDescription(rs.getString(4));
                e.setType(rs.getString(5));
                e.setSubType(rs.getString(6));
                e.setContent(rs.getString(7));
                e.setUpdateDate(rs.getTimestamp(8));
                e.setUpdateUser(rs.getString(9));
                e.setStatus(rs.getString(10).charAt(0));
                loadedObjects.add(e);
            }
            return loadedObjects;
        } catch (SQLException ex) {
            log.error("selectList", ex);
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public boolean containExport(String txId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("SELECT * FROM ep_export_detail_info WHERE txId=? AND 1 = 1");
            pstmt.setString(1, txId);
            rs = pstmt.executeQuery();
            while (rs.next())
                return true;
            return false;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public void createRepository(String projectId, String userId, String table) {
        PreparedStatement pstmt = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            // 建版本库表
            StringBuilder sb = new StringBuilder("CREATE TABLE ").append(table)
                    .append("_repo (versionNo ");
            TableField tf = new TableField();
            tf.setType(FieldType.LONG);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("entityId ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(40);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("name ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(100);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("desp ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(200);
            sb.append(getDialect().evalFieldType(tf)).append(",")
                    .append("type ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(10);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("subType ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(40);
            sb.append(getDialect().evalFieldType(tf)).append(",")
                    .append("content ");
            tf = new TableField();
            tf.setType(FieldType.CLOB);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("updateDate ");
            tf = new TableField();
            tf.setType(FieldType.DATETIME);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("updateUser ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(20);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("status ");
            tf = new TableField();
            tf.setType(FieldType.VARCHAR);
            tf.setLength(1);
            sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                    .append("PRIMARY KEY (versionNo))");
            log.debug(sb.toString());
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.execute();
            pstmt.close();
            pstmt = null;

            sb.setLength(0);
            sb.append("CREATE INDEX ").append(table).append("_repo_idx ON ")
                    .append(table).append("_repo(entityId)");
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.execute();
            pstmt.close();
            pstmt = null;
            try {
                //建导出表
                String[] sql = createTable(true);
                for (String s : sql) {
                    log.debug(s);
                    pstmt = conn.prepareStatement(s);
                    pstmt.execute();
                    pstmt.close();
                    pstmt = null;
                }
                sb.setLength(0);
                sb.append("CREATE INDEX ep_export_idx ON ep_export_info(projectId)");
                pstmt = conn.prepareStatement(sb.toString());
                pstmt.execute();
                pstmt.close();
                pstmt = null;
            } catch (SQLException e) {
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
            try {
                //建导入表
                String[] sql = createTable(false);
                for (String s : sql) {
                    log.debug(s);
                    pstmt = conn.prepareStatement(s);
                    pstmt.execute();
                    pstmt.close();
                    pstmt = null;
                }
                sb.setLength(0);
                sb.append("CREATE INDEX ep_import_idx ON ep_import_info(projectId)");
                pstmt = conn.prepareStatement(sb.toString());
                pstmt.execute();
                pstmt.close();
                pstmt = null;
            } catch (SQLException e) {
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
            try {
                pstmt = conn
                        .prepareStatement("DELETE FROM ep_serial_info WHERE keyname='"
                                + projectId + "'");
                pstmt.execute();
            } catch (SQLException ex) {
                DaoUtils.closeQuietly(pstmt);
                sb.setLength(0);
                sb.append("CREATE TABLE ep_serial_info (keyname ");
                tf = new TableField();
                tf.setType(FieldType.VARCHAR);
                tf.setLength(40);
                sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                        .append("keyvalue ");
                tf = new TableField();
                tf.setType(FieldType.LONG);
                sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                        .append("incvalue ");
                tf = new TableField();
                tf.setType(FieldType.INT);
                sb.append(getDialect().evalFieldType(tf)).append(
                        " NOT NULL,PRIMARY KEY(keyname))");
                pstmt = conn.prepareStatement(sb.toString());
                pstmt.execute();
            } finally {
                DaoUtils.closeQuietly(pstmt);
                pstmt = null;
            }
            // 移数据
            pstmt = conn.prepareStatement("SELECT entityId,name,desp,type,subType,content FROM "
                    + table);
            sb.setLength(0);
            sb.append("INSERT INTO ")
                    .append(table)
                    .append("_repo(versionNo,entityId,name,desp,type,subType,content,updateDate,updateUser,status) VALUES(?,?,?,?,?,?,?,?,?,?)");
            stmt = conn.prepareStatement(sb.toString());
            rs = pstmt.executeQuery();
            int count = 1;
            while (rs.next()) {
                stmt.setLong(1, count);
                stmt.setString(2, rs.getString(1));
                stmt.setString(3, rs.getString(2));
                stmt.setString(4, rs.getString(3));
                stmt.setString(5, rs.getString(4));
                stmt.setString(6, rs.getString(5));
                stmt.setString(7, rs.getString(6));
                stmt.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                stmt.setString(9, userId);
                stmt.setString(10, "C");
                stmt.addBatch();
                count++;
            }
            stmt.executeBatch();
            pstmt.close();
            pstmt = null;
            stmt.close();
            stmt = null;

            pstmt = conn
                    .prepareStatement("INSERT INTO ep_serial_info(keyname,keyvalue,incvalue) VALUES(?,?,?)");
            pstmt.setString(1, projectId);
            pstmt.setLong(2, count);
            pstmt.setInt(3, 1);
            pstmt.execute();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    private String[] createTable(boolean isExport) {
        String table = isExport ? "ep_export_info" : "ep_import_info";
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE ").append(table).append("(txId ");
        TableField tf = new TableField();
        tf.setType(FieldType.VARCHAR);
        tf.setLength(80);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("projectId ");
        tf = new TableField();
        tf.setType(FieldType.VARCHAR);
        tf.setLength(40);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("firstNo ");
        tf = new TableField();
        tf.setType(FieldType.INT);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("lastNo ");
        tf = new TableField();
        tf.setType(FieldType.INT);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("txDate ");
        tf = new TableField();
        tf.setType(FieldType.DATETIME);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("txUser ");
        tf = new TableField();
        tf.setType(FieldType.VARCHAR);
        tf.setLength(20);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("desp ");
        tf = new TableField();
        tf.setType(FieldType.VARCHAR);
        tf.setLength(200);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("PRIMARY KEY (txId))");
        String[] result = new String[2];
        result[0] = sb.toString();
        sb.setLength(0);
        table = isExport ? "ep_export_detail_info" : "ep_import_detail_info";
        sb.append("CREATE TABLE ").append(table).append("(txId ");
        tf = new TableField();
        tf.setType(FieldType.VARCHAR);
        tf.setLength(80);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("versionNo ");
        tf = new TableField();
        tf.setType(FieldType.INT);
        sb.append(getDialect().evalFieldType(tf)).append(" NOT NULL,")
                .append("PRIMARY KEY (txId,versionNo))");
        result[1] = sb.toString();
        return result;
    }

    @Override
    public List<HistoryVo> getHistory(String table, String entityId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("SELECT versionNo,name,desp,updateDate,updateUser,status FROM "
                            + table
                            + "_repo WHERE entityId=? order by versionNo DESC");
            pstmt.setString(1, entityId);
            rs = pstmt.executeQuery();
            List<HistoryVo> result = new ArrayList<HistoryVo>();
            while (rs.next())
                result.add(new HistoryVo(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(6), rs
                        .getString(5), rs.getTimestamp(4)));
            return result;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public List<IXVo> selectHistory(String table, String projectId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("SELECT txId,firstNo,lastNo,txDate,txUser,desp FROM "
                            + table
                            + " WHERE projectId=? order by txDate DESC");
            pstmt.setString(1, projectId);
            rs = pstmt.executeQuery();
            List<IXVo> result = new ArrayList<IXVo>();
            while (rs.next()) {
                IXVo vo = new IXVo();
                vo.setId(rs.getString(1));
                vo.setFirstNo(rs.getInt(2));
                vo.setLastNo(rs.getInt(3));
                vo.setDate(rs.getTimestamp(4));
                vo.setUser(rs.getString(5));
                vo.setDesp(rs.getString(6));
                result.add(vo);
            }
            return result;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public EntityInfo selectOne(String table, String entityId) {
        String sql = "SELECT b.versionNo,b.entityId,b.name,b.desp,b.type,b.subType,b.content,b.updateDate,b.updateUser,b.status FROM(SELECT entityId, max(versionNo) as versionNo FROM " + table + "_repo GROUP BY entityId) a," + table + "_repo b WHERE (a.entityId=? AND b.entityId = a.entityId AND b.versionNo = a.versionNo AND b.status<>'D')";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            log.debug("selectOne->%s", sql);
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, entityId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                EntityInfo e = new EntityInfo();
                e.setVersion(rs.getInt(1));
                e.setId(rs.getString(2));
                e.setName(rs.getString(3));
                e.setDescription(rs.getString(4));
                e.setType(rs.getString(5));
                e.setSubType(rs.getString(6));
                e.setContent(rs.getString(7));
                e.setUpdateDate(rs.getTimestamp(8));
                e.setUpdateUser(rs.getString(9));
                e.setStatus(rs.getString(10).charAt(0));
                return e;
            }
            return null;
        } catch (SQLException ex) {
            log.error("selectOne", ex);
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public String getContent(String table, long versionNo) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("SELECT content FROM " + table
                    + "_repo WHERE versionNo=?");
            pstmt.setLong(1, versionNo);
            rs = pstmt.executeQuery();
            if (rs.next())
                return rs.getString(1);
            return null;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public void save(String table, IXVo vo, String productId) {
        PreparedStatement pstmt = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            if (table != null) {//导入处理
                //查看当前版本号是否一致
                pstmt = conn
                        .prepareStatement("SELECT keyvalue FROM ep_serial_info WHERE keyname=?");
                pstmt.setString(1, productId);
                ResultSet rs = pstmt.executeQuery();
                long versionNo = 0;
                if (rs.next())
                    versionNo = rs.getLong(1);
                rs.close();
                pstmt.close();
                rs = null;
                pstmt = null;
                System.out.println("versionNo:" + versionNo);
                for (EntityInfo e : vo.getEntities()) {
                    //先更新主参数表
                    pstmt = conn.prepareStatement("UPDATE " + table + " SET name=?,desp=?,content=?,updateDate=?,updateUser=?,status=? WHERE entityId=?");
                    pstmt.setString(1, e.getName());
                    pstmt.setString(2, e.getDescription());
                    pstmt.setString(3, e.getContent());
                    pstmt.setTimestamp(4, new Timestamp(vo.getDate().getTime()));
                    pstmt.setString(5, vo.getUser());
                    pstmt.setString(6, "U");
                    pstmt.setString(7, e.getId());
                    System.out.println(pstmt.toString());
                    if (pstmt.executeUpdate() == 0) { //如果不存在
                        pstmt.close();
                        pstmt = null;
                        pstmt = conn
                                .prepareStatement("INSERT INTO "
                                        + table
                                        + " (entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
                        pstmt.setString(1, e.getId());
                        pstmt.setString(2, e.getName());
                        pstmt.setString(3, e.getDescription());
                        pstmt.setString(4, e.getType());
                        pstmt.setString(5, e.getSubType());
                        pstmt.setString(6, e.getContent());
                        pstmt.setTimestamp(7, new Timestamp(vo.getDate().getTime()));
                        pstmt.setString(8, vo.getUser());
                        pstmt.setString(9, "C");
                        System.out.println(pstmt.toString());
                        pstmt.execute();
                        e.setStatus('C');
                    } else
                        e.setStatus('U');
                    pstmt.close();
                    pstmt = null;
                    //更新到版本库
                    versionNo ++;
                    pstmt = conn.prepareStatement("UPDATE " + table + "_repo SET entityId=?, name=?,desp=?,type=?,subType=?,content=?,updateDate=?,updateUser=?,status=? WHERE versionNo=?");
                    pstmt.setString(1, e.getId());
                    pstmt.setString(2, e.getName());
                    pstmt.setString(3, e.getDescription());
                    pstmt.setString(4, e.getType());
                    pstmt.setString(5, e.getSubType());
                    pstmt.setString(6, e.getContent());
                    pstmt.setTimestamp(7, new Timestamp(vo.getDate().getTime()));
                    pstmt.setString(8, vo.getUser());
                    pstmt.setString(9, "U");
                    pstmt.setInt(10, (int) versionNo);
                    System.out.println(pstmt.toString());
                    if (pstmt.executeUpdate() == 0) {
                        pstmt.close();
                        pstmt = null;
                        pstmt = conn
                                .prepareStatement("INSERT INTO "
                                        + table
                                        + "_repo(versionNo,entityId,name,desp,type,subType,content,updateDate,updateUser,status) VALUES(?,?,?,?,?,?,?,?,?,?)");
                        pstmt.setLong(1, (int) versionNo);
                        pstmt.setString(2, e.getId());
                        pstmt.setString(3, e.getName());
                        pstmt.setString(4, e.getDescription());
                        pstmt.setString(5, e.getType());
                        pstmt.setString(6, e.getSubType());
                        pstmt.setString(7, e.getContent());
                        pstmt.setTimestamp(8, new Timestamp(vo.getDate().getTime()));
                        pstmt.setString(9, vo.getUser());
                        pstmt.setString(10, e.getStatus() + "");
                        System.out.println(pstmt.toString());
                        pstmt.execute();
                    }
                    pstmt.close();
                    pstmt = null;
                    pstmt = conn.prepareStatement("insert into ep_import_detail_info (txId,versionNo,targetVersionNo) values(?,?,?)");
                    pstmt.setString(1, vo.getId());
                    pstmt.setInt(2, e.getVersion());
                    pstmt.setLong(3, versionNo);
                    System.out.println(pstmt.toString());
                    pstmt.execute();
                    pstmt.close();
                    pstmt = null;
                }
                pstmt = conn
                        .prepareStatement("UPDATE ep_serial_info SET keyvalue=? WHERE keyname=?");
                pstmt.setLong(1, versionNo + 1);
                pstmt.setString(2, productId);
                System.out.println(pstmt.toString());
                pstmt.execute();
                pstmt.close();
                pstmt = null;
            }
            pstmt = conn
                    .prepareStatement("insert into "
                            + (table == null ? "ep_export_info" : "ep_import_info")
                            + " (txId,projectId,firstNo,lastNo,txDate,txUser,desp) values(?,?,?,?,?,?,?)");
            pstmt.setString(1, vo.getId());
            if (table != null) {
                pstmt.setString(2, productId);
            } else {
                pstmt.setString(2, vo.getProjectId());
            }
            pstmt.setInt(3, vo.getFirstNo());
            pstmt.setInt(4, vo.getLastNo());
            pstmt.setTimestamp(5, new Timestamp(vo.getDate().getTime()));
            pstmt.setString(6, vo.getUser());
            pstmt.setString(7, vo.getDesp());
            System.out.println(pstmt.toString());
            pstmt.executeUpdate();
            DaoUtils.closeQuietly(pstmt);
            if (table == null) {
                pstmt = conn.prepareStatement("insert into ep_export_detail_info (txId,versionNo) values(?,?)");
                for (EntityInfo e : vo.getEntities()) {
                    pstmt.setString(1, vo.getId());
                    pstmt.setInt(2, e.getVersion());
                    System.out.println(pstmt.toString());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }

    @Override
    public List<EntityInfo> selectChooseList(String table, List<String> entityIdList) {
        StringBuffer entityStr = new StringBuffer("'");
        entityStr.append(StringUtil.listToString(entityIdList, "','")).append("'");
        String sql = "SELECT b.versionNo,b.entityId,b.name,b.desp,b.type,b.subType,b.content,b.updateDate,b.updateUser,b.status FROM(SELECT entityId, max(versionNo) as versionNo FROM "
                + table + "_repo GROUP BY entityId) a," + table
                + "_repo b WHERE (a.entityId in (" + entityStr + ") AND b.entityId = a.entityId AND b.versionNo = a.versionNo AND b.status<>'D')";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            log.debug("selectOne->%s", sql);
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            List<EntityInfo> list = new ArrayList<>();
            while (rs.next()) {
                EntityInfo e = new EntityInfo();
                e.setVersion(rs.getInt(1));
                e.setId(rs.getString(2));
                e.setName(rs.getString(3));
                e.setDescription(rs.getString(4));
                e.setType(rs.getString(5));
                e.setSubType(rs.getString(6));
                e.setContent(rs.getString(7));
                e.setUpdateDate(rs.getTimestamp(8));
                e.setUpdateUser(rs.getString(9));
                e.setStatus(rs.getString(10).charAt(0));
                list.add(e);
            }
            return list;
        } catch (SQLException ex) {
            log.error("selectOne", ex);
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }
}
