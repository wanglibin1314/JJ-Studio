package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class PsqlCustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public PsqlCustomWidgetDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
