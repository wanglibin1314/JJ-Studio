package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractModuleDao;

import javax.sql.DataSource;

public class PsqlModuleDao extends AbstractModuleDao {
    public PsqlModuleDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
