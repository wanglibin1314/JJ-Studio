package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractGuideConfigDao;

import javax.sql.DataSource;

public class SybaseGuideConfigDao extends AbstractGuideConfigDao {
    /**
     * @param ds
     */
    public SybaseGuideConfigDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
