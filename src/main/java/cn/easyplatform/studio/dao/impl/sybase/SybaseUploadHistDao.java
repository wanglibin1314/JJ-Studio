package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractUploadHistDao;

import javax.sql.DataSource;

public class SybaseUploadHistDao extends AbstractUploadHistDao {
    public SybaseUploadHistDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
