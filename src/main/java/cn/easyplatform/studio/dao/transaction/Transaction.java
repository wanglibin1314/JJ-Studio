package cn.easyplatform.studio.dao.transaction;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class Transaction {

	private int level;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		if (this.level <= 0)
			this.level = level;
	}

	public abstract long getId();

	public abstract void commit() throws SQLException;

	public abstract void rollback();

	public abstract Connection getConnection(DataSource dataSource)
			throws SQLException;

	public abstract void close();

}
