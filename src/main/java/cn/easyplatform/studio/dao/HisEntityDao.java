package cn.easyplatform.studio.dao;

import cn.easyplatform.entities.EntityInfo;

/**
 * @Author Zeta
 * @Version 1.0
 */
public interface HisEntityDao {
    EntityInfo getMaxVsEntry(String table, String id);
    EntityInfo getHisEntry(String table, int versionNo);
}
