package cn.easyplatform.studio.utils;

import cn.easyplatform.studio.StudioApp;

public class StudioServiceFileUtils extends ServiceFileUtil {
    public String imgDirPath_stu = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/img");

    public String cssDirPath_stu = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/css");

    public String jsDirPath_stu = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/js");

    public String audioDirPath_stu = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/audio");

    public String videoDirPath_stu = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/video");

    public String element_app = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/element");

    public String module_app = StudioApp.getServletContext().getRealPath("/apps/" + projectName + "/module");

    public String backup_stu = StudioApp.getServletContext().getRealPath("/backup");
}
