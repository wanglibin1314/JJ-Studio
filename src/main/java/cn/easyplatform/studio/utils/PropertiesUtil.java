package cn.easyplatform.studio.utils;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.UpdateProjectCmd;
import cn.easyplatform.studio.context.Contexts;

import java.util.Map;

import static cn.easyplatform.studio.utils.FileUtil.mkdirForNoExists;

public class PropertiesUtil {
    public void addToolsBackupPath() {
        Map<String, String> map = Contexts.getProject().getProperties();
        boolean isContainKey = false;
        for (String key : map.keySet()) {
            if (key.equals("tools.app.backup.path"))
                isContainKey = true;
        }
        if (isContainKey == false ||
                (isContainKey == true && Strings.isBlank(map.get("tools.app.backup.path")))) {
            mkdirForNoExists(new StudioServiceFileUtils().backup_stu);
            map.put("tools.app.backup.path", new StudioServiceFileUtils().backup_stu);

            Contexts.getProject().setProperties(map);
            StudioApp.execute(new UpdateProjectCmd());
        }
    }
}
