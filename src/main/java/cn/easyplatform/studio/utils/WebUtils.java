/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.utils;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.ScopeType;
import cn.easyplatform.type.SubType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zul.impl.XulElement;

import javax.servlet.http.HttpServletRequest;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebUtils {
    /**
     * 获取客户端ip地址
     *
     * @return
     */
    public static String getRemoteAddr() {
        HttpServletRequest req = (HttpServletRequest) Executions.getCurrent()
                .getNativeRequest();
        // 如果有代理服务器，通过获取头X-Real-IP来获取真实的IP
        String ip = req.getHeader("X-Real-IP");
        if (ip != null)
            return ip;
        ip = req.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getRemoteAddr();
        }
        if (ip == null)
            ip = "";
        return ip;
    }

    public final static String getIconSclass(String type, String subType) {
        if (type.equals("Task"))
            return "z-icon-modx";
        if (type.equals("Page"))
            return "z-icon-newspaper-o";
        if (type.equals("Datalist"))
            return "z-icon-list-alt";
        if (type.equals("Bpm"))
            return "z-icon-share-alt";
        if (type.equals("Logic"))
            return "z-icon-code";
        if (type.equals("Report"))
            return "z-icon-line-chart";
        if (type.equals("Resource"))
            return "z-icon-clock-o";
        if (type.equals("Table"))
            return "z-icon-table";
        if (type.equals("Batch"))
            return "z-icon-gears";
        return "z-icon-cloud";
    }

    public final static String getImage(String type, String subType) {
        if (type.equals("Task"))
            return "~./images/action.gif";
        if (type.equals("Page"))
            return "~./images/application.gif";
        if (type.equals("Datalist"))
            return "~./images/application_view_columns.png";
        if (type.equals("Bpm"))
            return "~./images/arrow_switch.png";
        if (type.equals("Logic"))
            return "~./images/script_code.png";
        if (type.equals("Report"))
            return "~./images/htmleditor.gif";
        if (type.equals("Resource")) {
            if (subType.equals(SubType.JOB.getName()))
                return "~./images/clock.png";
            return "~./images/resource.gif";
        }
        if (type.equals("Table"))
            return "~./images/table_edit.gif";
        if (type.equals("Batch"))
            return "~./images/cog.png";
        return "~./images/grid.gif";
    }

    public final static Combobox createFieldTypeBox() {
        Combobox cbx = new Combobox();
        cbx.setReadonly(true);
        for (FieldType type : FieldType.values()) {
            Comboitem ci = new Comboitem(type.name());
            ci.setParent(cbx);
        }
        return cbx;
    }

    public final static Combobox createOptionTypeBox() {
        Combobox cbx = new Combobox();
        cbx.setReadonly(true);
        String[] list = new String[]{Labels.getLabel("entity.table.none"),
                Labels.getLabel("entity.table.dictionary"), Labels.getLabel("entity.table.table")};
        String[] values = new String[]{"0", "1", "2"};
        for (int index = 0; index < list.length; index++) {
            Comboitem ci = new Comboitem(list[index]);
            ci.setValue(values[index]);
            ci.setParent(cbx);
        }
        return cbx;
    }

    public final static Combobox createScopeTypeBox() {
        Combobox cbx = new Combobox();
        for (ScopeType type : ScopeType.values()) {
            if (type != ScopeType.FIELD) {
                Comboitem ci = new Comboitem(type.name());
                ci.setParent(cbx);
            }
        }
        cbx.setSelectedIndex(2);
        return cbx;
    }

    public final static Combobox createShareTypeBox() {
        Combobox cbx = new Combobox();
        Comboitem ci = new Comboitem(
                Labels.getLabel("entity.task.variables.share.0"));
        ci.setParent(cbx);
        ci = new Comboitem(Labels.getLabel("entity.task.variables.share.1"));
        ci.setParent(cbx);
        cbx.setSelectedIndex(0);
        return cbx;
    }

    public final static void notEmpty(String message) {
        Messagebox.show(
                Labels.getLabel("message.no.empty", new String[]{message}),
                Labels.getLabel("message.title.error"), Messagebox.OK,
                Messagebox.ERROR);
    }

    public final static void notExists(String message) {
        Messagebox.show(
                Labels.getLabel("message.no.exists", new String[]{message}),
                Labels.getLabel("message.title.error"), Messagebox.OK,
                Messagebox.ERROR);
    }

    public final static void showError(String message) {
        Messagebox.show(message, Labels.getLabel("message.title.error"),
                Messagebox.OK, Messagebox.ERROR);
    }

    public final static void showConfirm(String message,
                                         EventListener<Event> evt) {
        Messagebox.show(
                Labels.getLabel("message.confirm", new String[]{message}),
                Labels.getLabel("message.title.confirm"), Messagebox.CANCEL
                        | Messagebox.OK, Messagebox.QUESTION, evt);
    }

    public final static void showInfo(String message) {
        Messagebox.show(message, Labels.getLabel("message.title.info"),
                Messagebox.OK, Messagebox.INFORMATION);
    }

    public final static void showSuccess(String message) {
        Messagebox.show(
                Labels.getLabel("message.success", new String[]{message}),
                Labels.getLabel("message.title.info"), Messagebox.OK,
                Messagebox.INFORMATION);
    }

    public final static XulElement createComponent(TableField tf) {
        switch (tf.getType()) {
            case CHAR:
            case VARCHAR:
            case CLOB:
                return new Textbox();
            case BOOLEAN:
                return new Checkbox();
            case DATETIME:
            case DATE:
                return new Datebox();
            case TIME:
                return new Timebox();
            case INT:
                return new Intbox();
            case LONG:
                return new Longbox();
            case NUMERIC:
                return new Doublebox();
            default:
                return new Textbox();
        }
    }

    public final static Object getComponentValue(Component c) {
        if (c instanceof Combobox)
            return ((Combobox) c).getSelectedItem().getValue();
        else if (c instanceof CMeditor) {
            CMeditor editor = (CMeditor) c;
            return editor.getValue();
        } else if (c instanceof InputElement)
            return ((InputElement) c).getRawValue();
        else if (c instanceof Checkbox)
            return ((Checkbox) c).getValue();
        else if (c instanceof Radiogroup)
            return ((Radiogroup) c).getSelectedItem().getValue();
        return null;
    }
}
