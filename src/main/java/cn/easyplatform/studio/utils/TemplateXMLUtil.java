package cn.easyplatform.studio.utils;


import cn.easyplatform.studio.utils.apkrepackage.util.XMLUtil;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

public class TemplateXMLUtil {

    public static List getListWithXMLFile(String fileStr) {
        Document doc = XMLUtil.getDocument(fileStr);
        Element root = doc.getRootElement();
        if (root.getName().equals("array")) {
            List contentList = new ArrayList();
            getListWithElement(root, contentList);
            return contentList;
        } else {
            throw new RuntimeException("get.templateXML.error");
        }
    }

    public static Map getObjectWithXMLFile(String fileStr) {
        Document doc = XMLUtil.getDocument(fileStr);
        Element root = doc.getRootElement();
        if (root.getName().equals("dic")) {
            Map secondMap = new HashMap();
            getMapWithElement(root, secondMap);
            return secondMap;
        } else {
            throw new RuntimeException("get.templateXML.error");
        }
    }

    private static void getListWithElement(Element content, List contentList) {
        Iterator itt = content.elementIterator();
        while (itt.hasNext()) {
            Element elem = (Element) itt.next();
            if (elem.getName().equals("string")) {
                contentList.add(elem.getStringValue());
            } else if (elem.getName().equals("dic")) {
                Map contentMap = new HashMap();
                getMapWithElement(elem, contentMap);
                if (contentMap.keySet().size() > 0)
                    contentList.add(contentMap);
            } else if (elem.getName().equals("array")) {
                List secondList = new ArrayList();
                getListWithElement(elem, secondList);
                if (secondList.size() > 0)
                    contentList.add(secondList);
            } else {
                throw new RuntimeException("get.templateXML.error");
            }
        }
    }

    private static void getMapWithElement(Element content, Map contentMap) {
        Iterator itt = content.elementIterator();
        String keyStr = null;
        while (itt.hasNext()) {
            Element elem = (Element) itt.next();
            if (elem.getName().equals("key")) {
                keyStr = elem.getStringValue();
            } else {
                if (elem.getName().equals("string")) {
                    contentMap.put(keyStr, elem.getStringValue());
                } else if (elem.getName().equals("dic")) {
                    Map secondMap = new HashMap();
                    getMapWithElement(elem, secondMap);
                    if (secondMap.keySet().size() > 0)
                        contentMap.put(keyStr, secondMap);
                } else if (elem.getName().equals("array")) {
                    List secondList = new ArrayList();
                    getListWithElement(elem, secondList);
                    if (secondList.size() > 0)
                        contentMap.put(keyStr, secondList);
                } else {
                    throw new RuntimeException("get.templateXML.error");
                }
            }
        }
    }

    public static boolean doc2XmlFile(Document document, String filename) {
        boolean flag = true;
        try {
            XMLWriter writer = new XMLWriter(new OutputStreamWriter(
                    new FileOutputStream(filename), "UTF-8"));
            writer.write(document);
            writer.close();
        } catch (Exception ex) {
            flag = false;
            ex.printStackTrace();
        }
        System.out.println(flag);
        return flag;
    }
}
