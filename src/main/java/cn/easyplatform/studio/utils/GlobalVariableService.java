package cn.easyplatform.studio.utils;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.ElementVo;
import cn.easyplatform.studio.vos.LinkVo;
import cn.easyplatform.studio.vos.SMSVo;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalVariableService {
    private static GlobalVariableService globalVariableService;
    private static Map<String, Object> objectMap = new HashMap<>();
    public GlobalVariableService(){}

    private List<LinkVo> linkVoList = new ArrayList<>();

    private List<LinkVo> dataLinkVoList = new ArrayList<>();

    private List<TypeLinkVo> dataTypeLinkVoList = new ArrayList<>();

    private List<ElementVo> appElementList = new ArrayList<>();

    private List<SMSVo> smsVos = new ArrayList<>();

    public static synchronized GlobalVariableService getInstance() {
        String projectName = Contexts.getProject().getId();
        if (objectMap.get(projectName) == null) {
            globalVariableService = new GlobalVariableService();
            objectMap.put(projectName, globalVariableService);
            return globalVariableService;
        } else
            return (GlobalVariableService) objectMap.get(projectName);
    }

    public static synchronized GlobalVariableService getOneInstance() {
        if (globalVariableService == null) {
            globalVariableService = new GlobalVariableService();
            return globalVariableService;
        } else
            return globalVariableService;
    }

    public synchronized void setAllDataLinkVoMap(Map<String, List<LinkVo>> listMap) {
        for (String projectName : listMap.keySet()) {
            GlobalVariableService service = null;
            if (objectMap.get(projectName) == null) {
                service = new GlobalVariableService();
            } else
                service = (GlobalVariableService) objectMap.get(projectName);
            service.setDataLinkVoList(listMap.get(projectName));
            objectMap.put(projectName, service);
        }
    }

    public synchronized void setAllDataTypeLinkVoMap(Map<String, List<TypeLinkVo>> listMap) {
        for (String projectName : listMap.keySet()) {
            GlobalVariableService service = null;
            if (objectMap.get(projectName) == null) {
                service = new GlobalVariableService();
            } else
                service = (GlobalVariableService) objectMap.get(projectName);
            service.setDataTypeLinkVoList(listMap.get(projectName));
            objectMap.put(projectName, service);
        }
    }

    public synchronized List<LinkVo> getLinkVoList() {
        return linkVoList;
    }

    public synchronized void setLinkVoList(List<LinkVo> linkVoList) {
        this.linkVoList = linkVoList;
    }

    public synchronized List<LinkVo> getDataLinkVoList() {
        return dataLinkVoList;
    }

    public synchronized void setDataLinkVoList(List<LinkVo> dataLinkVoList) {
        this.dataLinkVoList = dataLinkVoList;
    }

    public synchronized List<TypeLinkVo> getDataTypeLinkVoList() {
        return dataTypeLinkVoList;
    }

    public synchronized void  setDataTypeLinkVoList(List<TypeLinkVo> dataTypeLinkVoList) {
        this.dataTypeLinkVoList = dataTypeLinkVoList;
    }

    public synchronized List<ElementVo> getAppElementList() {
        return appElementList;
    }

    public synchronized void setAppElementList(List<ElementVo> appElementList) {
        this.appElementList = appElementList;
    }

    public List<SMSVo> getSmsVos() {
        return smsVos;
    }

    public void setSmsVos(List<SMSVo> smsVos) {
        this.smsVos = smsVos;
    }
}
