package cn.easyplatform.studio.utils;

import cn.easyplatform.lang.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class StringUtil {
    public enum RegexType {
        RegexIsMobile,
        RegexIsVerification,
        RegexIsName,
        RegexIsPassword
    }

    public static String listToString(List list, String separator) {
        StringBuilder sb = new StringBuilder();
        if (list.size() == 0)
            return null;
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i)).append(separator);
        }
        return sb.toString().substring(0, sb.toString().length() - separator.length());
    }

    public static String addStringToListString(String listString, String separator, String addString) {
        List<String> list = new ArrayList<>();
        if (Strings.isBlank(listString) == false) {
            List<String> result = Arrays.asList(listString.split(separator));
            list = new ArrayList<>(result);
        }
        list.add(addString);
        return StringUtil.listToString(list, separator);
    }

    public static String removeStringToListString(String listString, String separator, String removeString) {
        List<String> list = new ArrayList<>();
        if (Strings.isBlank(listString) == false) {
            List<String> result = Arrays.asList(listString.split(separator));
            list = new ArrayList<>(result);
        }
        if (list.size() > 0 && list.contains(removeString))
            list.remove(removeString);
        return StringUtil.listToString(list, separator);
    }

    public static boolean isChangeString(String originalStr, String resultStr) {
        if (Strings.isBlank(originalStr) && Strings.isBlank(resultStr) == false)
            return true;
        else if (Strings.isBlank(resultStr) && Strings.isBlank(originalStr) == false)
            return true;
        else if (Strings.isBlank(resultStr) == false && Strings.isBlank(originalStr) == false &&
                originalStr.equals(resultStr) == false)
            return true;
        return false;
    }

    public static String truncatesLastLine(String originalStr, int reserveLength) {
        if (Strings.isBlank(originalStr))
            return originalStr;
        else if (originalStr.length() < reserveLength)
            return originalStr;
        else
            return originalStr.substring(0, reserveLength - 3) + "...";
    }

    public static String[] addStringList(String[] originalList, String[] addList) {
        int size = originalList.length;
        int addSize = addList.length;
        String[] tmp = new String[size + addSize];
        for (int i = 0; i < size; i++){
            tmp[i] = originalList[i];
        }
        for (int i = size; i < size + addSize; i++){
            tmp[i] = addList[i - size];
        }
        return tmp;
    }

    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }


    public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
            "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z" };

    public static String getShortUUID() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();
    }

    public static boolean regexString(String content, RegexType regexType) {
        String pattern = null;
        switch (regexType) {
            case RegexIsMobile:
                pattern = "^1(3|4|5|7|8)[0-9]\\d{8}$";
                break;
            case RegexIsVerification:
                pattern = "^[\\S]{6}$";
                break;
            case RegexIsName:
                pattern = "^[\\u4e00-\\u9fa5\\S]{1,20}$";
                break;
            case RegexIsPassword:
                pattern = "^\\s*(?=.*[a-z])(?=.*[0-9])[a-zA-Z0-9]{6,16}]*$";
                break;
        }
        if (pattern == null || Strings.isBlank(content))
            return false;
        boolean isMatch = Pattern.matches(pattern, content);
        return isMatch;
    }

    public static String random() {
        return String.valueOf((int)((Math.random()*9+1)*100000));
    }
}
