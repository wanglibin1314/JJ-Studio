/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StudioException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 * @param cause
	 */
	public StudioException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public StudioException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public StudioException(Throwable cause) {
		super(cause);
	}

}
