/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.interceptor;

import cn.easyplatform.studio.context.Contexts;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommandInvoker extends AbstractCommandInterceptor {

	public <T> T execute(Command<T> command) {
		return command.execute(Contexts.getCommandContext());
	}

	@Override
	public CommandInterceptor getNext() {
		return null;
	}

	@Override
	public void setNext(CommandInterceptor next) {
		throw new UnsupportedOperationException(
				"CommandInvoker must be the last interceptor in the chain");
	}
}
