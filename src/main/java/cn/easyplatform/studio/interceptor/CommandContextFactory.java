/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.interceptor;

import cn.easyplatform.studio.cfg.EngineConfiguration;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommandContextFactory {

	/**
	 * @param engineConfiguration
	 * @return
	 */
	public CommandContext createCommandContext(
			EngineConfiguration engineConfiguration) {
		return new CommandContext(engineConfiguration);
	}
}
