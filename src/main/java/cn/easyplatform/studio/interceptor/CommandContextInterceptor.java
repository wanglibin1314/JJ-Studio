/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.interceptor;

import cn.easyplatform.studio.cfg.EngineConfiguration;
import cn.easyplatform.studio.context.Contexts;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommandContextInterceptor extends AbstractCommandInterceptor {

	protected CommandContextFactory commandContextFactory;

	protected EngineConfiguration engineConfiguration;

	/**
	 * @param commandContextFactory
	 * @param engineConfiguration
	 */
	public CommandContextInterceptor(
			CommandContextFactory commandContextFactory,
			EngineConfiguration engineConfiguration) {
		this.commandContextFactory = commandContextFactory;
		this.engineConfiguration = engineConfiguration;
	}

	/*
	 * 
	 * 设置当前执行动作的上下文环境
	 */
	@Override
	public <T> T execute(Command<T> command) {
		try {
			CommandContext cc = commandContextFactory
					.createCommandContext(engineConfiguration);
			Contexts.set(CommandContext.class, cc);
			return next.execute(command);
		} finally {
			Contexts.clear();
		}
	}

}
