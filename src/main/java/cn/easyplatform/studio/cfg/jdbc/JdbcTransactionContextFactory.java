/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.jdbc;

import cn.easyplatform.studio.cfg.TransactionContext;
import cn.easyplatform.studio.cfg.TransactionContextFactory;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JdbcTransactionContextFactory implements TransactionContextFactory {

	@Override
	public TransactionContext openTransactionContext(
			CommandContext commandContext) {
		return new JdbcTransactionContext();
	}

}
