/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.jdbc;

import cn.easyplatform.studio.cfg.TransactionContext;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JdbcTransactionContext implements TransactionContext {

	/**
	 * 
	 */
	JdbcTransactionContext() {
		try {
			JdbcTransactions.begin();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.begin", ex);
		}
	}

	@Override
	public void commit() {
		try {
			JdbcTransactions.commit();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.commit", ex);
		}
	}

	@Override
	public void rollback() {
		try {
			JdbcTransactions.rollback();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.rollback", ex);
		}
	}

	@Override
	public void close() {
		JdbcTransactions.close();
	}

}
