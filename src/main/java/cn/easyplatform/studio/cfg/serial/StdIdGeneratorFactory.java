/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.serial;

import cn.easyplatform.studio.cfg.IdGenerator;
import cn.easyplatform.studio.cfg.IdGeneratorFactory;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StdIdGeneratorFactory implements IdGeneratorFactory {

	@Override
	public IdGenerator getIdGenerator(CommandContext cc) {
		return new StdIdGenerator(cc);
	}

}
