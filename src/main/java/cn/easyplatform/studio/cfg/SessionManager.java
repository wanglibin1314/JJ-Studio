/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg;

import cn.easyplatform.studio.vos.LoginUserVo;

import java.util.Collection;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface SessionManager {

	void setUser(LoginUserVo user);

	boolean removeUser(LoginUserVo user);

	Collection<LoginUserVo> getUsers();

	int checkUser(LoginUserVo user);

	void setState(String userId, int state);

	void validateSessions();

	LoginUserVo getUser(String id);
}
