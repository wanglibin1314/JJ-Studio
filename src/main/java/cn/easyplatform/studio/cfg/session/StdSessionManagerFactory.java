/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.session;

import cn.easyplatform.studio.cfg.SessionManager;
import cn.easyplatform.studio.cfg.SessionManagerFactory;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StdSessionManagerFactory implements SessionManagerFactory {

	@Override
	public SessionManager getSessionManager(CommandContext cc) {
		return new StdSessionManager(cc);
	}

}
