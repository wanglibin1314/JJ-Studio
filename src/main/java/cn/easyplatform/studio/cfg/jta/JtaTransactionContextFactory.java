/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.jta;

import cn.easyplatform.studio.cfg.TransactionContext;
import cn.easyplatform.studio.cfg.TransactionContextFactory;
import cn.easyplatform.studio.interceptor.CommandContext;

import javax.transaction.TransactionManager;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JtaTransactionContextFactory implements TransactionContextFactory {

	protected final TransactionManager transactionManager;

	public JtaTransactionContextFactory(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public TransactionContext openTransactionContext(
			CommandContext commandContext) {
		return new JtaTransactionContext(transactionManager);
	}

}