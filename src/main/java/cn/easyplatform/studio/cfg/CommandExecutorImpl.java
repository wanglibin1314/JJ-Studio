/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandExecutor;
import cn.easyplatform.studio.interceptor.CommandInterceptor;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommandExecutorImpl implements CommandExecutor {

	private final CommandInterceptor first;

	public CommandExecutorImpl(CommandInterceptor first) {
		this.first = first;
	}

	public CommandInterceptor getFirst() {
		return first;
	}

	@Override
	public <T> T execute(Command<T> command) {
		return first.execute(command);
	}

}
