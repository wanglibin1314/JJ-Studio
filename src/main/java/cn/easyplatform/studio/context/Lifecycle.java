/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.context;

import cn.easyplatform.studio.StudioApp;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WebApp;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class Lifecycle {

	public static void beginApp(WebApp app) throws Exception {
		StudioApp.me().start(app);
	}

	public static void endApp() {
		StudioApp.me().stop();
	}
	
	public static void beginSession(Session session){
		
	}

	public static void endSession(Session session){
		
	}
	
}
