/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import cn.easyplatform.entities.BaseEntity;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetEntityVo {

	private BaseEntity entity;
	
	private String lockUser;

	/**
	 * @param entity
	 * @param lockUser
	 */
	public GetEntityVo(BaseEntity entity, String lockUser) {
		this.entity = entity;
		this.lockUser = lockUser;
	}

	public BaseEntity getEntity() {
		return entity;
	}

	public String getLockUser() {
		return lockUser;
	}
	
	
}
