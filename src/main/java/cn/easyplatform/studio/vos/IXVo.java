/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import cn.easyplatform.entities.EntityInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class IXVo implements Serializable,Cloneable{

	private static final long serialVersionUID = 1L;

	private String id;

	private String projectId;

	private int firstNo;

	private int lastNo;

	private String desp;

	private String user;

	private Date date;

	private List<EntityInfo> entities;

	private List<DicVo> dicVos;

	private List<MessageVo> messageVos;

	private List<MenuInfoVo> menuInfoVos;

	private List<UploadHistVo> fileVos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public int getFirstNo() {
		return firstNo;
	}

	public void setFirstNo(int firstNo) {
		this.firstNo = firstNo;
	}

	public int getLastNo() {
		return lastNo;
	}

	public void setLastNo(int lastNo) {
		this.lastNo = lastNo;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<EntityInfo> getEntities() {
		return entities;
	}

	public void setEntities(List<EntityInfo> entities) {
		this.entities = entities;
	}

	public List<DicVo> getDicVos() {
		return dicVos;
	}

	public void setDicVos(List<DicVo> dicVos) {
		this.dicVos = dicVos;
	}

	public List<MessageVo> getMessageVos() {
		return messageVos;
	}

	public void setMessageVos(List<MessageVo> messageVos) {
		this.messageVos = messageVos;
	}

	public List<MenuInfoVo> getMenuInfoVos() {
		return menuInfoVos;
	}

	public void setMenuInfoVos(List<MenuInfoVo> menuInfoVos) {
		this.menuInfoVos = menuInfoVos;
	}

	public List<UploadHistVo> getFileVos() {
		return fileVos;
	}

	public void setFileVos(List<UploadHistVo> fileVos) {
		this.fileVos = fileVos;
	}

	public Object clone() {
		IXVo o = null;
		try {
			o = (IXVo) super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println(e.toString());
		}
		return o;
	}
}
