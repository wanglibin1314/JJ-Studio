package cn.easyplatform.studio.vos;


import java.io.Serializable;

public class DicDetailVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String dicCode;
    private String detailNO;
    private String desc1;
    private String displayFlag;
    private String editableFlag;
    private String detailSort;
    private String chosenFlag;
    private String otherFlag;
    private String dicStatus;
    private String simpleCode;
    private String zh_cn;
    private String zh_tw;
    private String en_us;
    private String ja_jp;
    private String ko_kr;
    private String de_de;
    private String fr_fr;
    private String it_it;
    private String detailID;

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDetailNO() {
        return detailNO;
    }

    public void setDetailNO(String detailNO) {
        this.detailNO = detailNO;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getDisplayFlag() {
        return displayFlag;
    }

    public void setDisplayFlag(String displayFlag) {
        this.displayFlag = displayFlag;
    }

    public String getEditableFlag() {
        return editableFlag;
    }

    public void setEditableFlag(String editableFlag) {
        this.editableFlag = editableFlag;
    }

    public String getDetailSort() {
        return detailSort;
    }

    public void setDetailSort(String detailSort) {
        this.detailSort = detailSort;
    }

    public String getChosenFlag() {
        return chosenFlag;
    }

    public void setChosenFlag(String chosenFlag) {
        this.chosenFlag = chosenFlag;
    }

    public String getOtherFlag() {
        return otherFlag;
    }

    public void setOtherFlag(String otherFlag) {
        this.otherFlag = otherFlag;
    }

    public String getDicStatus() {
        return dicStatus;
    }

    public void setDicStatus(String dicStatus) {
        this.dicStatus = dicStatus;
    }

    public String getSimpleCode() {
        return simpleCode;
    }

    public void setSimpleCode(String simpleCode) {
        this.simpleCode = simpleCode;
    }

    public String getZh_cn() {
        return zh_cn;
    }

    public void setZh_cn(String zh_cn) {
        this.zh_cn = zh_cn;
    }

    public String getZh_tw() {
        return zh_tw;
    }

    public void setZh_tw(String zh_tw) {
        this.zh_tw = zh_tw;
    }

    public String getEn_us() {
        return en_us;
    }

    public void setEn_us(String en_us) {
        this.en_us = en_us;
    }

    public String getJa_jp() {
        return ja_jp;
    }

    public void setJa_jp(String ja_jp) {
        this.ja_jp = ja_jp;
    }

    public String getKo_kr() {
        return ko_kr;
    }

    public void setKo_kr(String ko_kr) {
        this.ko_kr = ko_kr;
    }

    public String getDe_de() {
        return de_de;
    }

    public void setDe_de(String de_de) {
        this.de_de = de_de;
    }

    public String getFr_fr() {
        return fr_fr;
    }

    public void setFr_fr(String fr_fr) {
        this.fr_fr = fr_fr;
    }

    public String getIt_it() {
        return it_it;
    }

    public void setIt_it(String it_it) {
        this.it_it = it_it;
    }

    public String getDetailID() {
        return detailID;
    }

    public void setDetailID(String detailID) {
        this.detailID = detailID;
    }
}
