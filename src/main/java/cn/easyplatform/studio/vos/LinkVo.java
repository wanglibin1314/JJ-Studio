package cn.easyplatform.studio.vos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LinkVo implements Cloneable {
    public final static String ENTITYSTATUS = "E";
    public final static String VIRTUALSTATUS = "V";
    public final static String UNCERTAINSTATUS = "UN";

    private String entityId;

    private String childrenEntityId;

    private String entityName;

    private String entityType;

    private String entitySubType;

    private String entityDesp;

    private Date createDate;

    private Date updateDate;

    private String createUser;

    private String updateUser;

    private String status;

    private boolean needRefresh;

    private List<LinkVo> childrenList = new ArrayList<>();

    private boolean canAdd = true;

    public LinkVo()
    {

    }

    public LinkVo(String entityId, String childrenEntityId, String entityName, String entityType,
                  String entitySubType, String entityDesp, Date createDate, Date updateDate, String createUser,
                  String updateUser,String status)
    {
        this.entityId = entityId;
        this.childrenEntityId = childrenEntityId;
        this.entityName = entityName;
        this.entityType = entityType;
        this.entitySubType = entitySubType;
        this.entityDesp = entityDesp;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.createUser = createUser;
        this.updateUser = updateUser;
        this.status = status;
        this.needRefresh = true;
    }

    public Object clone() {
        LinkVo o = null;
        try {
            o = (LinkVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        o.childrenList = new ArrayList<>();
        return o;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityDesp() {
        return entityDesp;
    }

    public void setEntityDesp(String entityDesp) {
        this.entityDesp = entityDesp;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<LinkVo> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<LinkVo> childrenList) {
        this.childrenList = childrenList;
    }

    public String getEntitySubType() {
        return entitySubType;
    }

    public void setEntitySubType(String entitySubType) {
        this.entitySubType = entitySubType;
    }

    public String getChildrenEntityId() {
        return childrenEntityId;
    }

    public void setChildrenEntityId(String childrenEntityId) {
        this.childrenEntityId = childrenEntityId;
    }

    public boolean isNeedRefresh() {
        return needRefresh;
    }

    public void setNeedRefresh(boolean needRefresh) {
        this.needRefresh = needRefresh;
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public void setCanAdd(boolean canAdd) {
        this.canAdd = canAdd;
    }
}
