package cn.easyplatform.studio.vos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DicVo implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    private String dicCode;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateDate;
    private String desc1;
    private String upCode;
    private String dicLevel;
    private String displayFlag;
    private String editableFlag;
    private String dicStatus;
    private String zh_cn;
    private String zh_tw;
    private String en_us;
    private String ja_jp;
    private String ko_kr;
    private String de_de;
    private String fr_fr;
    private String it_it;
    private Boolean selected;
    private List<DicVo> childrenList = new ArrayList<>();
    private List<DicDetailVo> dicDetailVoList = new ArrayList<>();

    public Object clone() {
        DicVo o = null;
        try {
            o = (DicVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        o.childrenList = new ArrayList<>();
        return o;
    }

    public DicVo(){}
    public DicVo(String dicCode, String createUser, String desc1, String upCode, String dicLevel, String zh_cn) {
        this.dicCode = dicCode;
        this.createUser = createUser;
        this.desc1 = desc1;
        this.upCode = upCode;
        this.dicLevel = dicLevel;
        this.zh_cn = zh_cn;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getUpCode() {
        return upCode;
    }

    public void setUpCode(String upCode) {
        this.upCode = upCode;
    }

    public String getDicLevel() {
        return dicLevel;
    }

    public void setDicLevel(String dicLevel) {
        this.dicLevel = dicLevel;
    }

    public String getDisplayFlag() {
        return displayFlag;
    }

    public void setDisplayFlag(String displayFlag) {
        this.displayFlag = displayFlag;
    }

    public String getEditableFlag() {
        return editableFlag;
    }

    public void setEditableFlag(String editableFlag) {
        this.editableFlag = editableFlag;
    }

    public String getDicStatus() {
        return dicStatus;
    }

    public void setDicStatus(String dicStatus) {
        this.dicStatus = dicStatus;
    }

    public String getZh_cn() {
        return zh_cn;
    }

    public void setZh_cn(String zh_cn) {
        this.zh_cn = zh_cn;
    }

    public String getZh_tw() {
        return zh_tw;
    }

    public void setZh_tw(String zh_tw) {
        this.zh_tw = zh_tw;
    }

    public String getEn_us() {
        return en_us;
    }

    public void setEn_us(String en_us) {
        this.en_us = en_us;
    }

    public String getJa_jp() {
        return ja_jp;
    }

    public void setJa_jp(String ja_jp) {
        this.ja_jp = ja_jp;
    }

    public String getKo_kr() {
        return ko_kr;
    }

    public void setKo_kr(String ko_kr) {
        this.ko_kr = ko_kr;
    }

    public String getDe_de() {
        return de_de;
    }

    public void setDe_de(String de_de) {
        this.de_de = de_de;
    }

    public String getFr_fr() {
        return fr_fr;
    }

    public void setFr_fr(String fr_fr) {
        this.fr_fr = fr_fr;
    }

    public String getIt_it() {
        return it_it;
    }

    public void setIt_it(String it_it) {
        this.it_it = it_it;
    }

    public List<DicDetailVo> getDicDetailVoList() {
        return dicDetailVoList;
    }

    public void setDicDetailVoList(List<DicDetailVo> dicDetailVoList) {
        this.dicDetailVoList = dicDetailVoList;
    }

    public List<DicVo> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<DicVo> childrenList) {
        this.childrenList = childrenList;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
