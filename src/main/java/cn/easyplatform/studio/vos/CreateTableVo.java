package cn.easyplatform.studio.vos;

import cn.easyplatform.type.FieldType;

import java.util.List;

public class CreateTableVo {
    private String tableName;
    private List<TableField> tfs;

    public CreateTableVo(String tableName, List<TableField> tfs) {
        this.tableName = tableName;
        this.tfs = tfs;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<TableField> getTfs() {
        return tfs;
    }

    public void setTfs(List<TableField> tfs) {
        this.tfs = tfs;
    }
    public static class TableField {
        private String tfName;
        private FieldType type;
        private int length = 0;
        private boolean isKey = false;
        private boolean isNull = true;
        private boolean isAutoIncrement = false;
        private boolean defaultCurrentTime = false;
        private boolean updateCurrentTime = false;

        public TableField(String tfName, FieldType type, int length, boolean isKey, boolean isNull,
                          boolean isAutoIncrement, boolean defaultCurrentTime, boolean updateCurrentTime) {
            this.tfName = tfName;
            this.type = type;
            this.length = length;
            this.isKey = isKey;
            this.isNull = isNull;
            this.isAutoIncrement = isAutoIncrement;
            this.defaultCurrentTime = defaultCurrentTime;
            this.updateCurrentTime = updateCurrentTime;
        }

        public String getTfName() {
            return tfName;
        }

        public void setTfName(String tfName) {
            this.tfName = tfName;
        }

        public FieldType getType() {
            return type;
        }

        public void setType(FieldType type) {
            this.type = type;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public boolean isKey() {
            return isKey;
        }

        public void setKey(boolean key) {
            isKey = key;
        }

        public boolean isNull() {
            return isNull;
        }

        public void setNull(boolean aNull) {
            isNull = aNull;
        }

        public boolean isAutoIncrement() {
            return isAutoIncrement;
        }

        public void setAutoIncrement(boolean autoIncrement) {
            isAutoIncrement = autoIncrement;
        }

        public boolean isDefaultCurrentTime() {
            return defaultCurrentTime;
        }

        public void setDefaultCurrentTime(boolean defaultCurrentTime) {
            this.defaultCurrentTime = defaultCurrentTime;
        }

        public boolean isUpdateCurrentTime() {
            return updateCurrentTime;
        }

        public void setUpdateCurrentTime(boolean updateCurrentTime) {
            this.updateCurrentTime = updateCurrentTime;
        }
    }
}


