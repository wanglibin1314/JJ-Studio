/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BizQueryResultVo {
	
	private int totalSize;

	private ResultVo result;

	/**
	 * @param totalSize
	 * @param data
	 */
	public BizQueryResultVo(int totalSize, ResultVo data) {
		this.totalSize = totalSize;
		this.result = data;
	}

	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @return the data
	 */
	public ResultVo getResult() {
		return result;
	}

}
