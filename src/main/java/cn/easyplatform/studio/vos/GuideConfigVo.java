package cn.easyplatform.studio.vos;

import cn.easyplatform.web.ext.introJs.Step;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GuideConfigVo implements Cloneable  {

    public enum EditStatus {
        EditUpdate, EditDelete, EditAdd, EDITNone
    }
    private int guideID;
    private String guideName;
    private String guideStep;
    private String guideNextID;
    private String guideNextName;
    private String isRoot;
    private Date createDate;
    private Date updateDate;
    private String createUser;
    private String updateUser;
    private List<Step> stepList = new ArrayList<>();
    private boolean selected = false;
    private EditStatus editStatus = EditStatus.EDITNone;

    public GuideConfigVo() {

    }

    public GuideConfigVo(int guideID, String guideName, String guideStep, String guideNextID, String isRoot,
                         Date createDate, Date updateDate, String createUser, String updateUser) {
        this.guideID = guideID;
        this.guideName = guideName;
        this.guideStep = guideStep;
        this.guideNextID = guideNextID;
        this.isRoot = isRoot;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.createUser = createUser;
        this.updateUser = updateUser;
    }

    public Object clone() {
        GuideConfigVo o = null;
        try {
            o = (GuideConfigVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        return o;
    }

    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuideStep() {
        return guideStep;
    }

    public void setGuideStep(String guideStep) {
        this.guideStep = guideStep;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public List<Step> getStepList() {
        return stepList;
    }

    public void setStepList(List<Step> stepList) {
        this.stepList = stepList;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public EditStatus getEditStatus() {
        return editStatus;
    }

    public void setEditStatus(EditStatus editStatus) {
        this.editStatus = editStatus;
    }

    public String getGuideNextID() {
        return guideNextID;
    }

    public void setGuideNextID(String guideNextID) {
        this.guideNextID = guideNextID;
    }

    public String getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(String isRoot) {
        this.isRoot = isRoot;
    }

    public String getGuideNextName() {
        return guideNextName;
    }

    public void setGuideNextName(String guideNextName) {
        this.guideNextName = guideNextName;
    }
}
