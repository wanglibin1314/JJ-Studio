/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class QueryResultVo {

	private int totalSize;
	
	private List<EntityVo> entities;

	/**
	 * @param totalSize
	 * @param entities
	 */
	public QueryResultVo(int totalSize, List<EntityVo> entities) {
		this.totalSize = totalSize;
		this.entities = entities;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public List<EntityVo> getEntities() {
		return entities;
	}
	
	
}
