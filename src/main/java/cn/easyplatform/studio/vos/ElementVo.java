package cn.easyplatform.studio.vos;


public class ElementVo implements Cloneable {

    private String entityId;

    private String elementId;

    private String elementPath;

    private String elementType;

    private String action;

    private boolean isDoubleId;

    private boolean isZKComponent;

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getElementPath() {
        return elementPath;
    }

    public void setElementPath(String elementPath) {
        this.elementPath = elementPath;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String actionList) {
        this.action = actionList;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Object clone() {
        ElementVo o = null;
        try {
            o = (ElementVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        return o;
    }

    public boolean isDoubleId() {
        return isDoubleId;
    }

    public void setDoubleId(boolean doubleId) {
        isDoubleId = doubleId;
    }
}
