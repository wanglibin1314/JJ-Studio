package cn.easyplatform.studio.vos;

import java.util.List;

public class QueryMenuInfoVo {
    private List<MenuInfoVo> menuVoList;

    private int totalSize = 1;

    public QueryMenuInfoVo(int totalSize,List<MenuInfoVo> menuVoList)
    {
        this.totalSize = totalSize;
        this.menuVoList = menuVoList;
    }

    public List<MenuInfoVo> getMenuVoList() {
        return menuVoList;
    }

    public void setMenuVoList(List<MenuInfoVo> menuVoList) {
        this.menuVoList = menuVoList;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }
}
