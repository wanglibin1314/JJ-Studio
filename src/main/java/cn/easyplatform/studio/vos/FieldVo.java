/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import cn.easyplatform.type.FieldType;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FieldVo {
	
	private FieldType type;
	
	private Object value;

	private String name;
	
	/**
	 * @param type
	 */
	public FieldVo(FieldType type) {
		this.type = type;
	}

	/**
	 * @param type
	 * @param value
	 */
	public FieldVo(FieldType type, Object value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * @param name
	 * @param type
	 * @param value
	 */
	public FieldVo(String name, FieldType type, Object value) {
		this.name = name;
		this.type = type;
		this.value = value;
	}

	public FieldType getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
