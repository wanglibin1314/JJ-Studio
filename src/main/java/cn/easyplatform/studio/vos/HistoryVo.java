/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.Date;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class HistoryVo {

	private long version;

	private String name;

	private String desp;

	private String status;
	
	private String committer;
	
	private Date committedDate;

	/**
	 * @param version
	 * @param name
	 * @param committer
	 * @param committedDate
	 */
	public HistoryVo(long version,String name,String desp, String status, String committer,
			Date committedDate) {
		this.version = version;
		this.name = name;
		this.desp = desp;
		this.status = status;
		this.committer = committer;
		this.committedDate = committedDate;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the committer
	 */
	public String getCommitter() {
		return committer;
	}

	/**
	 * @return the committedDate
	 */
	public Date getCommittedDate() {
		return committedDate;
	}

	public String getName() {
		return name;
	}

	public String getDesp() {
		return desp;
	}
}
