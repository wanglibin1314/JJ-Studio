/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.Date;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RepoVo {

	private String id;

	private String name;

	private String desp;

	private String type;

	private String content;

	private Date beginDate;

	private String user;

	private Date endDate;

	private boolean lastVersion;

	private int beginVersion, endVersion;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isLastVersion() {
		return lastVersion;
	}

	public void setLastVersion(boolean lastVersion) {
		this.lastVersion = lastVersion;
	}

	public int getBeginVersion() {
		return beginVersion;
	}

	public void setBeginVersion(int beginVersion) {
		this.beginVersion = beginVersion;
	}

	public int getEndVersion() {
		return endVersion;
	}

	public void setEndVersion(int endVersion) {
		this.endVersion = endVersion;
	}
}
