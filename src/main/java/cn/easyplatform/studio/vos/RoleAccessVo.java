package cn.easyplatform.studio.vos;

public class RoleAccessVo {
    private String roleId;
    private String taskId;
    private String type;
    private String access;

    public RoleAccessVo() {

    }
    public RoleAccessVo(String roleId, String taskId, String type, String access) {
        this.roleId = roleId;
        this.taskId = taskId;
        this.type = type;
        this.access = access;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}
