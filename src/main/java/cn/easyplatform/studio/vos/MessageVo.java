/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;


import java.io.Serializable;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String code;

	private String status;

	private String MSG_TYPE;

	private String zh_cn;

	private String zh_tw;

	private String en_us;

	private String ja_jp;

	private String ko_kr;

	private String de_de;

	private String fr_fr;

	private String it_it;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMSG_TYPE() {
		return MSG_TYPE;
	}

	public void setMSG_TYPE(String MSG_TYPE) {
		this.MSG_TYPE = MSG_TYPE;
	}

	public String getZh_cn() {
		return zh_cn;
	}

	public void setZh_cn(String zh_cn) {
		this.zh_cn = zh_cn;
	}

	public String getZh_tw() {
		return zh_tw;
	}

	public void setZh_tw(String zh_tw) {
		this.zh_tw = zh_tw;
	}

	public String getEn_us() {
		return en_us;
	}

	public void setEn_us(String en_us) {
		this.en_us = en_us;
	}

	public String getJa_jp() {
		return ja_jp;
	}

	public void setJa_jp(String ja_jp) {
		this.ja_jp = ja_jp;
	}

	public String getKo_kr() {
		return ko_kr;
	}

	public void setKo_kr(String ko_kr) {
		this.ko_kr = ko_kr;
	}

	public String getDe_de() {
		return de_de;
	}

	public void setDe_de(String de_de) {
		this.de_de = de_de;
	}

	public String getFr_fr() {
		return fr_fr;
	}

	public void setFr_fr(String fr_fr) {
		this.fr_fr = fr_fr;
	}

	public String getIt_it() {
		return it_it;
	}

	public void setIt_it(String it_it) {
		this.it_it = it_it;
	}

}
