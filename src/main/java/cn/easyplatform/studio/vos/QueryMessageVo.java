package cn.easyplatform.studio.vos;

import java.util.List;

public class QueryMessageVo {
    private List<MessageVo> messageVoList;

    private int totalSize = 1;

    public QueryMessageVo(int totalSize,List<MessageVo> messageVoList)
    {
        this.totalSize = totalSize;
        this.messageVoList = messageVoList;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public List<MessageVo> getMessageVoList() {
        return messageVoList;
    }

    public void setMessageVoList(List<MessageVo> messageVoList) {
        this.messageVoList = messageVoList;
    }
}
