package cn.easyplatform.studio.vos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TypeLinkVo implements Cloneable{
    public final static String ROOTENTITYID = "R";
    public final static String NODEENTITYID = "N";

    private String entityId;

    private String childrenEntityId;

    private String entityName;

    private String entityType;

    private String entitySubType;

    private String entityDesp;

    private Date updateDate;

    private String updateUser;

    private String isRoot;

    private String status;

    private boolean needRefresh;

    private List<TypeLinkVo> childrenList = new ArrayList<>();
    public TypeLinkVo(String entityId, String childrenEntityId, String entityName, String entityType,
                      String entitySubType, String entityDesp, Date updateDate, String updateUser, String isRoot, String status)
    {

        this.entityId = entityId;
        this.childrenEntityId = childrenEntityId;
        this.entityName = entityName;
        this.entityType = entityType;
        this.entityDesp = entityDesp;
        this.entitySubType = entitySubType;
        this.updateDate = updateDate;
        this.updateUser = updateUser;
        this.isRoot = isRoot;
        this.status = status;
        this.needRefresh = true;
    }

    public TypeLinkVo(String entityId, String childrenEntityId, String isRoot)
    {
        this.entityId = entityId;
        this.childrenEntityId = childrenEntityId;
        this.isRoot = isRoot;
        this.needRefresh = true;
    }

    public Object clone() {
        TypeLinkVo o = null;
        try {
            o = (TypeLinkVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        o.childrenList = new ArrayList<>();
        return o;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public List<TypeLinkVo> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<TypeLinkVo> childrenList) {
        this.childrenList = childrenList;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityDesp() {
        return entityDesp;
    }

    public void setEntityDesp(String entityDesp) {
        this.entityDesp = entityDesp;
    }

    public String getEntitySubType() {
        return entitySubType;
    }

    public void setEntitySubType(String entitySubType) {
        this.entitySubType = entitySubType;
    }

    public String isRoot() {
        return isRoot;
    }

    public void setRoot(String root) {
        isRoot = root;
    }

    public String getChildrenEntityId() {
        return childrenEntityId;
    }

    public void setChildrenEntityId(String childrenEntityId) {
        this.childrenEntityId = childrenEntityId;
    }

    public boolean isNeedRefresh() {
        return needRefresh;
    }

    public void setNeedRefresh(boolean needRefresh) {
        this.needRefresh = needRefresh;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
