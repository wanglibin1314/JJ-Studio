package cn.easyplatform.studio.vos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomWidgetGroupVo implements Cloneable {
    private String groupName;
    private List<CustomWidgetVo> customWidgetVoList;

    public CustomWidgetGroupVo(String groupName) {
        this.groupName = groupName;
    }

    public Object clone() {
        CustomWidgetGroupVo o = null;
        try {
            o = (CustomWidgetGroupVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        o.customWidgetVoList = new ArrayList<>();
        return o;
    }

    public CustomWidgetGroupVo() {}

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<CustomWidgetVo> getCustomWidgetVoList() {
        return customWidgetVoList;
    }

    public void setCustomWidgetVoList(List<CustomWidgetVo> customWidgetVoList) {
        this.customWidgetVoList = customWidgetVoList;
    }

}
