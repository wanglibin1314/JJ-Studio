package cn.easyplatform.studio.vos;

import java.util.Date;

public class MobileConfVo {
    public final static String iOSCONF = "1";
    public final static String ANDROIDCONF = "2";
    public final static String WXCONF = "3";
    private int confId;

    private String projectId;

    //1为安卓2为iOS3为微信小程序
    private String confType;

    private String bundleId;

    private String displayName;

    private String mobileVersion;

    private String icon;

    private String launch;

    private String url;

    private String createUserId;

    private Date createDate;

    private String updateUserId;

    private Date updateDate;


    public MobileConfVo(int confId, String projectId, String confType, String bundleId, String displayName, String mobileVersion,
                        String icon, String launch, String url, String createUserId, Date createDate, String updateUserId,
                        Date updateDate) {
        this.confId = confId;
        this.projectId = projectId;
        this.confType = confType;
        this.bundleId = bundleId;
        this.displayName = displayName;
        this.mobileVersion = mobileVersion;
        this.icon = icon;
        this.launch = launch;
        this.url = url;
        this.createUserId = createUserId;
        this.createDate = createDate;
        this.updateUserId = updateUserId;
        this.updateDate = updateDate;
    }

    public MobileConfVo(int confId, String confType, String bundleId, String displayName, String mobileVersion,
                        String icon, String launch, String url, String createUserId, Date createDate) {
        this.confId = confId;
        this.confType = confType;
        this.bundleId = bundleId;
        this.displayName = displayName;
        this.mobileVersion = mobileVersion;
        this.icon = icon;
        this.launch = launch;
        this.url = url;
        this.createUserId = createUserId;
        this.createDate = createDate;
    }

    public String getConfType() {
        return confType;
    }

    public void setConfType(String confType) {
        this.confType = confType;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLaunch() {
        return launch;
    }

    public void setLaunch(String launch) {
        this.launch = launch;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMobileVersion() {
        return mobileVersion;
    }

    public void setMobileVersion(String mobileVersion) {
        this.mobileVersion = mobileVersion;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getConfId() {
        return confId;
    }

    public void setConfId(int confId) {
        this.confId = confId;
    }
}
