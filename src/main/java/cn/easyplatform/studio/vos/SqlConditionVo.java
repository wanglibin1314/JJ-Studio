package cn.easyplatform.studio.vos;

public class SqlConditionVo {
    public enum ConditionType {
        EQUAL("Equal"),
        NOEQUAL("NoEqual"),
        LESS("Less"),
        LESSOREQUAL("LessOrEqual"),
        MORE("More"),
        MOREOREQUAL("MoreOrEqual"),
        CONTAIN("Contain"),
        NOCONTAIN("NoContain"),
        START("Start"),
        NOSTART("NoStart"),
        END("End"),
        NOEND("NoEnd"),
        NULL("Null"),
        NONULL("NoNull"),
        EMPTY("Empty"),
        NOEMPTY("NoEmpty"),
        BETWEEN("Between"),
        NOBETWEEN("NoBetween"),
        IN("In"),
        NOIN("NoIn");

        String name;

        private ConditionType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return this.name;
        }
    }

    public enum LinkType {
        AND("And"),
        OR("Or");

        String name;

        private LinkType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return this.name;
        }
    }

    private String keyContent;
    private String conditionType;
    private String valueContent;
    private String valueSecondContent;
    private String linkType;

    public String getKeyContent() {
        return keyContent;
    }

    public void setKeyContent(String keyContent) {
        this.keyContent = keyContent;
    }

    public String getConditionType() {
        return conditionType;
    }

    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getValueContent() {
        return valueContent;
    }

    public void setValueContent(String valueContent) {
        this.valueContent = valueContent;
    }

    public String getValueSecondContent() {
        return valueSecondContent;
    }

    public void setValueSecondContent(String valueSecondContent) {
        this.valueSecondContent = valueSecondContent;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }
}
