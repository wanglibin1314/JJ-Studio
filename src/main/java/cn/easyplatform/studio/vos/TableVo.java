/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import cn.easyplatform.entities.beans.table.TableBean;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TableVo {

	public static final int NORMAL = 0;

	public static final int DIFF = 1;

	public static final int NEW = 2;

	public static final int REVERT = 3;

	private String id;

	private String name;

	/**
	 * 0：正常，1：不一致，2：有参数表，没有真实表，3：有真实表，没有参数表
	 */
	private int state;

	private TableBean source;

	private TableBean target;

	private List<String> diffFields;

	private List<String> sourceNeFields;

	private List<String> targetNewFields;

	private List<String> autoIncrementFields;

	private boolean diffByField;
	
	private boolean diffByIndex;
	
	private boolean diffByKey;
	
	private boolean diffByFk;
	
	private boolean diffByAutoIncrement;
	/**
	 * @param id
	 * @param name
	 */
	public TableVo(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return the diffFields
	 */
	public List<String> getDiffFields() {
		return diffFields;
	}

	/**
	 * @param diffFields the diffFields to set
	 */
	public void setDiffFields(List<String> diffFields) {
		this.diffFields = diffFields;
	}

	/**
	 * @return the sourceNeFields
	 */
	public List<String> getSourceNeFields() {
		return sourceNeFields;
	}

	/**
	 * @param sourceNeFields the sourceNeFields to set
	 */
	public void setSourceNeFields(List<String> sourceNeFields) {
		this.sourceNeFields = sourceNeFields;
	}

	/**
	 * @return the targetNewFields
	 */
	public List<String> getTargetNewFields() {
		return targetNewFields;
	}

	/**
	 * @param targetNewFields the targetNewFields to set
	 */
	public void setTargetNewFields(List<String> targetNewFields) {
		this.targetNewFields = targetNewFields;
	}

	public TableBean getSource() {
		return source;
	}

	public void setSource(TableBean source) {
		this.source = source;
	}

	public TableBean getTarget() {
		return target;
	}

	public void setTarget(TableBean target) {
		this.target = target;
	}

	/**
	 * @return the diffByField
	 */
	public boolean isDiffByField() {
		return diffByField;
	}

	/**
	 * @param diffByField the diffByField to set
	 */
	public void setDiffByField(boolean diffByField) {
		this.diffByField = diffByField;
	}

	/**
	 * @return the diffByIndex
	 */
	public boolean isDiffByIndex() {
		return diffByIndex;
	}

	/**
	 * @param diffByIndex the diffByIndex to set
	 */
	public void setDiffByIndex(boolean diffByIndex) {
		this.diffByIndex = diffByIndex;
	}

	/**
	 * @return the diffByKey
	 */
	public boolean isDiffByKey() {
		return diffByKey;
	}

	/**
	 * @param diffByKey the diffByKey to set
	 */
	public void setDiffByKey(boolean diffByKey) {
		this.diffByKey = diffByKey;
	}

	/**
	 * @return the diffByFk
	 */
	public boolean isDiffByFk() {
		return diffByFk;
	}

	/**
	 * @param diffByFk the diffByFk to set
	 */
	public void setDiffByFk(boolean diffByFk) {
		this.diffByFk = diffByFk;
	}

	public boolean isDiffByAutoIncrement() {
		return diffByAutoIncrement;
	}

	public void setDiffByAutoIncrement(boolean diffByAutoIncrement) {
		this.diffByAutoIncrement = diffByAutoIncrement;
	}
}
