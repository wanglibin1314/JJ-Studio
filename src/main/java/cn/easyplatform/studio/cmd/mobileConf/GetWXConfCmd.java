package cn.easyplatform.studio.cmd.mobileConf;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MobileConfVo;

import static cn.easyplatform.studio.vos.MobileConfVo.WXCONF;

public class GetWXConfCmd implements Command<MobileConfVo> {
    @Override
    public MobileConfVo execute(CommandContext cc) {
        MobileConfVo vo = cc.getMobileConf().getConf(WXCONF, cc.getProject().getId());
        return vo;
    }
}
