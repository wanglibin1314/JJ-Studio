/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RevertTableCmd implements Command<TableBean> {

	private String dbId;

	private String table;

	public RevertTableCmd(String dbId, String table) {
		this.dbId = dbId;
		this.table = table;
	}

	@Override
	public TableBean execute(CommandContext cc) {
		return cc.getBizDao(dbId).revertTable(table);
	}

}
