/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.studio.dao.BizDao;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CreateTableCmd implements Command<Boolean> {

	private TableBean table;

	public CreateTableCmd(TableBean table) {
		this.table = table;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		BizDao dao = cc.getBizDao(table.getSubType());
		try {
			cc.beginTx();
			dao.createTable(table);
			cc.commitTx();
			return true;
		} catch (Exception ex) {
			cc.rollbackTx();
			throw (RuntimeException) ex;
		} finally {
			cc.closeTx();
		}
	}

}
