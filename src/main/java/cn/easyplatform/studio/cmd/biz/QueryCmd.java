/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.BizQueryResultVo;
import cn.easyplatform.studio.vos.BizQueryVo;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.ResultVo;
import cn.easyplatform.type.FieldType;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class QueryCmd implements Command<BizQueryResultVo> {

	private int pageSize;

	private BizQueryVo vo;

	/**
	 * @param vo
	 * @param pageSize
	 */
	public QueryCmd(BizQueryVo vo, int pageSize) {
		this.vo = vo;
		this.pageSize = pageSize;
	}

	@Override
	public BizQueryResultVo execute(CommandContext cc) {
		String sql = vo.getSql();
		FieldVo[] params = vo.getParams();
		StringBuilder sb = new StringBuilder(vo.getSql());
		if (vo.getSearchValue() != null) {
			if (vo.getParams() == null || vo.getParams().length == 0) {// 表示没有where从句
				sb.append(" WHERE (");
				String[] fields = vo.getSearchField().split(",");
				params = new FieldVo[fields.length];
				for (int i = 0; i < fields.length; i++) {
					sb.append(fields[i]).append(" LIKE ?");
					params[i] = new FieldVo(FieldType.VARCHAR, "%"
							+ vo.getSearchValue() + "%");
					if (i < fields.length - 1)
						sb.append(" OR ");
				}
			} else {
				sb.append(" AND (");
				String[] fields = vo.getSearchField().split(",");
				FieldVo[] tmp = new FieldVo[vo.getParams().length
						+ fields.length];
				System.arraycopy(vo.getParams(), 0, tmp, 0,
						vo.getParams().length);
				int len = vo.getParams().length;
				params = tmp;
				for (int i = 0; i < fields.length; i++) {
					sb.append(fields[i]).append(" LIKE ?");
					params[len + i] = new FieldVo(FieldType.VARCHAR, "%"
							+ vo.getSearchValue() + "%");
					if (i < fields.length - 1)
						sb.append(" OR ");
				}
			}
			sb.append(")");
			//sql = sb.toString();
		}
		if (Strings.isBlank(vo.getGroupBy()) == false) {
			sb.append(" GROUP BY ").append(vo.getGroupBy());
		}
		Page page = new Page(pageSize);
		page.setGetTotal(true);
		page.setOrderBy(vo.getOrderBy());
		ResultVo result = cc.getBizDao(vo.getId())
				.selectList(sb.toString(), page, params);
		return new BizQueryResultVo(page.getTotalCount(), result);
	}
}
