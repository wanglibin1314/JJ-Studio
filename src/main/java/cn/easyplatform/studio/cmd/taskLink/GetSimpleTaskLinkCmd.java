package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.TypeLinkVo;


public class GetSimpleTaskLinkCmd implements Command<TypeLinkVo> {
    private String entityId;
    public GetSimpleTaskLinkCmd(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public TypeLinkVo execute(CommandContext cc) {
        TypeLinkVo vo = cc.getTaskLink().getTaskLink("ep_"+cc.getProject().getId()+"_link",
                "ep_"+cc.getProject().getId()+"_task_link", entityId);
        return vo;
    }
}
