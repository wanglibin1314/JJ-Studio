package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.QueryTypeLinkVo;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.ArrayList;
import java.util.List;

public class PagingTaskLinkCmd extends TaskLinkCmd implements Command<QueryTypeLinkVo> {

    private String field;

    private String value;

    private int pageSize;

    private int pageNo;

    public PagingTaskLinkCmd(String field, String value, int pageSize, int pageNo) {
        this.field = field;
        this.value = value;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    @Override
    public QueryTypeLinkVo execute(CommandContext cc) {
        List<TypeLinkVo> changeList = new ArrayList<>();
        if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() == 0)
            return new QueryTypeLinkVo(1, changeList);
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            return parentTree(GlobalVariableService.getInstance().getDataTypeLinkVoList(), field, value, pageSize, pageNo);
        }
    }
}
