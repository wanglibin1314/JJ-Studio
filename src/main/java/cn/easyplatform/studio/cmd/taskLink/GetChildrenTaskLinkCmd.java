package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.List;

public class GetChildrenTaskLinkCmd extends TaskLinkCmd implements Command<List<TypeLinkVo>> {
    private String entityID;
    public GetChildrenTaskLinkCmd(String entityID)
    {
        this.entityID = entityID;
    }
    @Override
    public List<TypeLinkVo> execute(CommandContext cc) {
        if (Strings.isBlank(entityID) == false) {
            List<TypeLinkVo> list = cc.getTaskLink().getLinkWithChildrenEntityId("ep_"+cc.getProject().getId()+"_link",
                    "ep_"+cc.getProject().getId()+"_task_link", entityID);
            return list;
        }
        return null;
    }
}
