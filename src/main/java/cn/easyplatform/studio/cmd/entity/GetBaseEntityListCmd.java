package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.List;

public class GetBaseEntityListCmd implements Command<List<BaseEntity>> {
    private List<String> idList;


    public GetBaseEntityListCmd(List<String> idList) {
        this.idList = idList;
    }

    @Override
    public List<BaseEntity> execute(CommandContext cc) {
        if (idList != null && idList.size() > 0) {
            return cc.getEntityDao().getEntityList(cc.getProject().getEntityTableName(), idList);
        }
        return null;
    }
}
