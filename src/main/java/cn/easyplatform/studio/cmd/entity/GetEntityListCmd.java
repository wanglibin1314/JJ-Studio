package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.EntityVo;

import java.util.List;

public class GetEntityListCmd implements Command<List<EntityVo>> {
    private List<String> idsList;

    public GetEntityListCmd(List<String> idsList) {
        this.idsList = idsList;
    }
    @Override
    public List<EntityVo> execute(CommandContext cc) {
        if (idsList != null && idsList.size() > 0) {
            return cc.getEntityDao().selectListWithID(cc.getProject().getEntityTableName(), idsList);
        }
        return null;
    }
}
