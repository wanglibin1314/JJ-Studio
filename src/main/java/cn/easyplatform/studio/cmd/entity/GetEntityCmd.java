/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetEntityCmd implements Command<BaseEntity> {

	private String id;

	/**
	 * @param id
	 */
	public GetEntityCmd(String id) {
		this.id = id;
	}

	@Override
	public BaseEntity execute(CommandContext cc) {
		return cc.getEntityDao().getEntity(cc.getProject().getEntityTableName(), id);
	}

}
