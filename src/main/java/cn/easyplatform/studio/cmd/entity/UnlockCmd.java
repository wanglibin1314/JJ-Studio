/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UnlockCmd implements Command<Boolean> {

	private String id;

	/**
	 * @param id
	 */
	public UnlockCmd(String id) {
		this.id = id;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		cc.getEntityLockProvider().unlock(cc.getProject().getId(), id);
		return true;
	}

}
