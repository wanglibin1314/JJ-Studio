/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SaveCmd implements Command<Boolean> {

	private EntityInfo entity;

	/**
	 * @param entity
	 */
	public SaveCmd(EntityInfo entity) {
		this.entity = entity;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		entity.setUpdateUser(cc.getUser().getName());
		try {
			cc.beginTx();
			cc.getEntityDao().save(cc.getProject().getId(),
					cc.getProject().getEntityTableName(), entity);
			cc.commitTx();
			return true;
		} catch (Exception e) {
			cc.rollbackTx();
			throw (RuntimeException) e;
		} finally {
			cc.closeTx();
		}
	}
}
