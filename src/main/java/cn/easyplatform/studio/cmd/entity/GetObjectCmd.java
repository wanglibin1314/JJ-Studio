/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetObjectCmd implements Command<Object> {

    private String query;

    private Object[] params;

    public GetObjectCmd(String query, Object[] params) {
        this.query = query;
        this.params = params;
    }

    @Override
    public Object execute(CommandContext cc) {
        return cc.getEntityDao().selectObject(query, params);
    }

}
