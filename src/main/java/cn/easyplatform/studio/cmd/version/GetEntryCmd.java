/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetEntryCmd implements Command<EntityInfo> {

    private String entityId;

    /**
     * @param entityId
     */
    public GetEntryCmd(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public EntityInfo execute(CommandContext cc) {
        return cc.getVersionDao().selectOne(cc.getProject().getEntityTableName(),
                entityId);
    }

}
