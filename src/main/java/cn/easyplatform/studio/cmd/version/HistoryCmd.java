/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.HistoryVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class HistoryCmd implements Command<List<HistoryVo>> {

	private String entityId;

	/**
	 * @param entityId
	 */
	public HistoryCmd(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public List<HistoryVo> execute(CommandContext cc) {
		return cc.getVersionDao().getHistory(
				cc.getProject().getEntityTableName(), entityId);
	}

}
