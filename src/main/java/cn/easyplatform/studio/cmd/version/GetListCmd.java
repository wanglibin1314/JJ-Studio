/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.RepoVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetListCmd implements Command<List<EntityInfo>> {

    private RepoVo vo;

    private Page page;

    public GetListCmd(RepoVo vo) {
        this.vo = vo;
    }

    public GetListCmd(RepoVo vo, Page page) {
        this.vo = vo;
        this.page = page;
    }

    @Override
    public List<EntityInfo> execute(CommandContext cc) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT b.versionNo,b.entityId,b.name,b.desp,b.type,b.subType,b.content,b.updateDate,b.updateUser,b.status FROM ");
        if (vo.isLastVersion()) {
            sb.append("(SELECT entityId, max(versionNo) as versionNo FROM ").append(cc.getProject().getEntityTableName()).append("_repo GROUP BY entityId) a,");
            sb.append(cc.getProject().getEntityTableName()).append("_repo b").append(" WHERE (b.entityId = a.entityId AND b.versionNo = a.versionNo AND b.status<>'D')");
        } else {
            sb.append(cc.getProject().getEntityTableName()).append("_repo b WHERE (1=1)");
        }
        List<Object> params = new ArrayList<Object>();
        if (!Strings.isBlank(vo.getId())) {
            sb.append(" AND b.entityId LIKE ?");
            params.add("%" + vo.getId() + "%");
        }
        if (!Strings.isBlank(vo.getName())) {
            sb.append(" AND b.name LIKE ?");
            params.add("%" + vo.getName() + "%");
        }
        if (!Strings.isBlank(vo.getDesp())) {
            sb.append(" AND b.desp LIKE ?");
            params.add("%" + vo.getDesp() + "%");
        }
        if (!Strings.isBlank(vo.getType())) {
            if (vo.getType().equals("Job")) {
                sb.append(" AND b.type='Resource' AND b.subType='Job'");
            } else {
                sb.append(" AND b.type = ?");
                params.add(vo.getType());
            }
        }
        if (!Strings.isBlank(vo.getContent())) {
            sb.append(" AND b.content LIKE ?");
            params.add("%" + vo.getContent() + "%");
        }
        if (vo.getBeginDate() != null) {
            params.add(vo.getBeginDate());
            if (vo.getEndDate() != null) {
                sb.append(" AND (b.updateDate BETWEEN ? AND ?)");
                params.add(vo.getEndDate());
            } else
                sb.append(" AND b.updateDate >=?");
        } else if (vo.getEndDate() != null) {
            sb.append(" AND b.updateDate<=?");
            params.add(vo.getEndDate());
        }
        if (vo.getBeginVersion() > 0) {
            params.add(vo.getBeginVersion());
            if (vo.getEndVersion() > 0) {
                sb.append(" AND (b.versionNo BETWEEN ? AND ?)");
                params.add(vo.getEndVersion());
            } else
                sb.append(" AND b.versionNo >=?");
        } else if (vo.getEndVersion() > 0) {
            sb.append(" AND b.versionNo<=?");
            params.add(vo.getEndVersion());
        }
        if (!Strings.isBlank(vo.getUser())) {
            sb.append(" AND b.updateUser = ?");
            params.add(vo.getUser());
        }
        String sql = sb.toString();
        if (page != null) {
            sb.setLength(0);
            sb.append("b.entityId");
            if (!vo.isLastVersion())
                sb.append(",b.versionNo DESC");
            page.setOrderBy(sb.toString());
        } else {
            sb.append(" ORDER BY b.entityId");
            if (!vo.isLastVersion())
                sb.append(",b.versionNo DESC");
            sql = sb.toString();
        }
        return cc.getVersionDao().selectList(page, sql, params.toArray());
    }

}
