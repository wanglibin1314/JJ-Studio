/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CheckCmd implements Command<List<EntityInfo>> {

    private List<EntityInfo> entites;

    private String txId;

    public CheckCmd(List<EntityInfo> entites, String txId) {
        this.entites = entites;
        this.txId = txId;
    }

    @Override
    public List<EntityInfo> execute(CommandContext cc) {
        List<EntityInfo> result = new ArrayList<EntityInfo>();
        for (EntityInfo e : entites) {
            Object o = cc.getEntityDao().selectObject("SELECT COUNT(*) FROM " +
                    cc.getProject().getEntityTableName() + "_repo WHERE versionNo=(SELECT targetVersionNo FROM ep_import_detail_info " +
                    "WHERE txId=? AND versionNo=?)", txId + "_" + cc.getProject().getId(), e.getVersion());
            if (Integer.parseInt(o + "") != 0)
                result.add(e);
        }
        return result;
    }

}
