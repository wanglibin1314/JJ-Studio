/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.IXVo;

import java.util.Date;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ImportCmd implements Command<Boolean> {

    private IXVo entity;

    /**
     * @param entity
     */
    public ImportCmd(IXVo entity) {
        this.entity = (IXVo) entity.clone();
    }

    @Override
    public Boolean execute(CommandContext cc) {
        entity.setId(entity.getId() + "_" + cc.getProject().getId());
        entity.setUser(cc.getUser().getUserId());
        entity.setDate(new Date());
        try {
            cc.beginTx();
            cc.getVersionDao().save(cc.getProject().getEntityTableName(), entity, cc.getProject().getId());
            cc.commitTx();
        } catch (Exception ex) {
            cc.rollbackTx();
            if (ex instanceof DaoException)
                throw (DaoException) ex;
            throw new StudioException("ExportCmd:" + ex.getMessage());
        } finally {
            cc.closeTx();
        }
        return Boolean.TRUE;
    }

}
