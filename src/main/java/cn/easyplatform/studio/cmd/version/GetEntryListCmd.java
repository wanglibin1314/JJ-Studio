package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.List;

public class GetEntryListCmd implements Command<List<EntityInfo>> {

    private List<String> entityStr;

    /**
     * @param entityStr
     */
    public GetEntryListCmd(List<String> entityStr) {
        this.entityStr = entityStr;
    }

    @Override
    public List<EntityInfo> execute(CommandContext cc) {
        return cc.getVersionDao().selectChooseList(cc.getProject().getEntityTableName(),
                entityStr);
    }
}
