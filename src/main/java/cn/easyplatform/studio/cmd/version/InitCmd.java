/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.entities.beans.table.TableIndex;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class InitCmd implements Command<Boolean> {

    @Override
    public Boolean execute(CommandContext cc) {
        //第一个版本
        if (!cc.getEntityDao().exists(cc.getProject().getEntityTableName() + "_repo")) {
            try {
                cc.beginTx();
                cc.getVersionDao().createRepository(cc.getProject().getId(), Contexts
                        .getUser().getUserId(), cc.getProject()
                        .getEntityTableName());
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
            return Boolean.FALSE;
        }
        //第二个版本
        if (!cc.getEntityDao().exists("ep_upload_file_hist")) {
            try {
                cc.beginTx();

                cc.getDatabaseUpdateDao().createSecondVersion(cc.getProject()
                        .getEntityTableName());

                TableBean tableBean = new TableBean();
                tableBean.setId("sys_field_detail_info");
                List<TableField> tableFieldList = new ArrayList<>();
                String[] nameList = new String[]{"ENTITYID", "disptype", "field1orderno", "field1label", "field1id",
                        "field1type", "field1disptype", "field1width", "field1constraint", "dictionarydetailid",
                        "field1labeldesp", "field1query"};
                FieldType[] typeList = new FieldType[]{FieldType.VARCHAR, FieldType.VARCHAR, FieldType.VARCHAR,
                        FieldType.VARCHAR, FieldType.VARCHAR, FieldType.VARCHAR, FieldType.VARCHAR, FieldType.VARCHAR,
                        FieldType.VARCHAR, FieldType.VARCHAR, FieldType.VARCHAR, FieldType.INT};
                Boolean[] boolList = new Boolean[]{Boolean.TRUE, Boolean.FALSE, Boolean.FALSE,
                        Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE,
                        Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE};
                int[] lengthList = new int[]{80, 20, 3, 150, 80, 20, 20, 10, 100, 30, 150, 1};
                for (int i = 0; i < 12; i++) {
                    TableField field = new TableField();
                    field.setName(nameList[i]);
                    field.setType(typeList[i]);
                    field.setNotNull(boolList[i]);
                    field.setLength(lengthList[i]);
                    tableFieldList.add(field);
                }
                List<String> keyList = new ArrayList<>();
                keyList.add("ENTITYID");
                keyList.add("field1id");
                tableBean.setKey(keyList);
                TableIndex tableIndex = new TableIndex();
                tableIndex.setName("sys_field_detail_info_idx");
                tableIndex.setFields("ENTITYID,field1id");
                List<TableIndex> indexList = new ArrayList<>();
                indexList.add(tableIndex);
                tableBean.setIndexes(indexList);
                tableBean.setFields(tableFieldList);
                cc.getBizDao().createTable(tableBean);

                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

}
