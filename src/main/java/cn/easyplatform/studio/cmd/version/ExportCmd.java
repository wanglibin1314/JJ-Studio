/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.IXVo;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ExportCmd implements Command<Boolean> {

    private IXVo vo;

    public ExportCmd(IXVo vo) {
        this.vo = vo;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        try {
            cc.beginTx();
            cc.getVersionDao().save(null, vo, cc.getProject().getId());
            cc.commitTx();
        } catch (Exception ex) {
            cc.rollbackTx();
            if (ex instanceof DaoException)
                throw (DaoException) ex;
            throw new StudioException("ExportCmd:" + ex.getMessage());
        } finally {
            cc.closeTx();
        }
        return Boolean.TRUE;
    }

}
