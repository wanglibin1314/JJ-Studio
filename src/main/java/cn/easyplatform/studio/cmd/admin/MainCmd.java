package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import javax.sql.DataSource;
/**
 * @Author Zeta
 * @Version 1.0
 */

public class MainCmd implements Command<DataSource> {

    public MainCmd() {
    }

    @Override
    public DataSource execute(CommandContext cc) {
        return cc.getDataSource();
    }
}
