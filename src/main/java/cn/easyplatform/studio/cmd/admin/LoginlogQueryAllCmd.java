package cn.easyplatform.studio.cmd.admin;

/**
 * @Author Zeta
 * @Version 1.0
 */

import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.Map;

public class LoginlogQueryAllCmd implements Command<Map> {

    private Page page;

    public LoginlogQueryAllCmd(Page page) {
        this.page = page;
    }

    @Override
    public Map<String, Object> execute(CommandContext cc) {
        return cc.getLoginlogDao().getLoginlogAll(cc.getProject().getEntityTableName()+"_login_log", page);
    }
}
