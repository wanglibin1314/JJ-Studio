package cn.easyplatform.studio.cmd.admin;

/**
 * @Author Zeta
 * @Version 1.0
 */

import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginlogVo;

import java.util.Map;

public class LoginlogQueryCmd implements Command<Map> {
    private LoginlogVo vo;
    private Page page;

    public LoginlogQueryCmd(LoginlogVo vo, Page page) {
        this.vo = vo;
        this.page = page;
    }

    @Override
    public Map<String, Object> execute(CommandContext cc) {
        return cc.getLoginlogDao().getLoginlogByTermpage(cc.getProject().getEntityTableName()+"_login_log", vo, page);
    }
}
