package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginlogVo;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class LoginlogAddCmd implements Command<Boolean> {

    private LoginlogVo vo;

    public LoginlogAddCmd(LoginlogVo vo) {
        this.vo = vo;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getLoginlogDao().addLoginlog(cc.getProject().getEntityTableName()+"_login_log",vo);
    }
}
