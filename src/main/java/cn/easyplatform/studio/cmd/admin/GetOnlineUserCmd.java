package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginlogVo;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class GetOnlineUserCmd implements Command<LoginlogVo>{

    private String userid;

    private String roleId;

    public GetOnlineUserCmd(String userid, String roleId) {
        this.userid = userid;
        this.roleId = roleId;
    }

    @Override
    public LoginlogVo execute(CommandContext cc) {
        return cc.getIdentityDao().getOnlineUser(Contexts.getProject().getId(), userid, roleId);
    }
}
