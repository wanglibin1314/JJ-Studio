package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.cfg.SessionManager;
import cn.easyplatform.studio.dos.LockDo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.Map;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class GetLockMapCmd implements Command<Map<String, LockDo>> {

    private String name;

    public  GetLockMapCmd(String name) {
        this.name = name;
    }

    @Override
    public Map<String, LockDo> execute(CommandContext cc) {
        return (Map<String, LockDo>)cc.getAppAttribute(name);
    }
}
