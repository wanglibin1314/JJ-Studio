package cn.easyplatform.studio.cmd.help;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;

import java.util.ArrayList;
import java.util.List;

public class DeleteGuideConfigCmd implements Command<Boolean> {
    private GuideConfigVo configVo;
    private GuideConfigEditor.GuideConfigType guideConfigType;
    public DeleteGuideConfigCmd(GuideConfigVo configVo, GuideConfigEditor.GuideConfigType guideConfigType) {
        this.configVo = configVo;
        this.guideConfigType = guideConfigType;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (configVo == null)
            return false;
        List<GuideConfigVo> vos = new ArrayList<>();
        vos.add(configVo);
        boolean deleteSuccess = cc.getGuideConfig(guideConfigType).deleteGuideConfig(guideConfigType, vos);
        return deleteSuccess;
    }
}
