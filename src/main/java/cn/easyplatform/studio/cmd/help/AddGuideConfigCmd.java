package cn.easyplatform.studio.cmd.help;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;

import java.util.List;

public class AddGuideConfigCmd implements Command<Boolean> {
    private List<GuideConfigVo> configVos;
    private GuideConfigEditor.GuideConfigType guideConfigType;
    public AddGuideConfigCmd(List<GuideConfigVo> configVos, GuideConfigEditor.GuideConfigType guideConfigType) {
        this.configVos = configVos;
        this.guideConfigType = guideConfigType;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (configVos == null || configVos.size() == 0)
            return false;
        boolean addSuccess = cc.getGuideConfig(guideConfigType).addGuideConfig(guideConfigType, configVos);
        return addSuccess;
    }
}
