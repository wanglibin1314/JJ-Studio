package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LinkVo;

import java.util.List;

public class GetChildrenLinkCmd extends LinkCmd implements Command<List<LinkVo>> {
    private String entityID;
    public GetChildrenLinkCmd(String entityID)
    {
        this.entityID = entityID;
    }
    @Override
    public List<LinkVo> execute(CommandContext cc) {
        if (Strings.isBlank(entityID) == false) {
            List<LinkVo> list = cc.getLink().getLinkWithChildrenEntityId("ep_"+cc.getProject().getId()+"_link", entityID);
            return list;
        }
        return null;
    }
}
