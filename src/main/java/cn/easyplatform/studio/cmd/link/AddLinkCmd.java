package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.LinkVo;


public class AddLinkCmd extends LinkCmd implements Command<Boolean> {
    private LinkVo linkVo;
    public AddLinkCmd(LinkVo linkVo)
    {
        this.linkVo = linkVo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (linkVo == null)
            return false;
        //判断节点存在
        LinkVo selectVo = cc.getLink().getLinkWithEntityId("ep_"+cc.getProject().getId()+"_link", linkVo.getEntityId());
        if (selectVo == null) {
            //添加节点
            linkVo.setCreateUser(cc.getUser().getUserId());
            boolean success = cc.getLink().addLink("ep_"+cc.getProject().getId()+"_link", linkVo);
            if (success == false)
                return false;
            return addValidation(cc);
        }
        return true;
    }

    protected boolean addValidation(CommandContext cc) {
        LinkVo updateVo = cc.getLink().getLinkWithEntityId("ep_"+cc.getProject().getId()+"_link", linkVo.getEntityId());
        if (updateVo == null) {
            return false;
        }
        synchronized (GlobalVariableService.getInstance().getDataLinkVoList()) {
            //List<LinkVo> linkVos = GlobalVariableService.getInstance().getDataLinkVoList();
            //List<LinkVo> treeVoList = GlobalVariableService.getInstance().getLinkVoList();
            GlobalVariableService.getInstance().getDataLinkVoList().add(updateVo);
            GlobalVariableService.getInstance().getLinkVoList().clear();
        }
        return true;
    }
}
