package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.LinkVo;


public class DeleteLinkCmd extends LinkCmd implements Command<Boolean> {
    private LinkVo linkVo;
    public DeleteLinkCmd(LinkVo linkVo)
    {
        this.linkVo = linkVo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (linkVo == null)
            return false;
        boolean deleteSuccess = cc.getLink().deleteLink("ep_" + cc.getProject().getId() + "_link", linkVo.getEntityId());
        if (deleteSuccess == false)
            return false;
        return deleteValidation(cc);
    }

    protected Boolean deleteValidation(CommandContext cc) {
        LinkVo updateVo = cc.getLink().getLinkWithEntityId("ep_"+cc.getProject().getId()+"_link", linkVo.getEntityId());
        if (updateVo != null)
            return false;
        //更换数据表缓存
        synchronized (GlobalVariableService.getInstance().getDataLinkVoList()) {
            //List<LinkVo> linkVos = GlobalVariableService.getInstance().getDataLinkVoList();
            //List<LinkVo> treeVoList = GlobalVariableService.getInstance().getLinkVoList();
            int index = -1;
            for (int i = 0; i < GlobalVariableService.getInstance().getDataLinkVoList().size(); i++) {
                LinkVo vo = GlobalVariableService.getInstance().getDataLinkVoList().get(i);
                if (vo.getEntityId().equals(linkVo.getEntityId())) {
                    index = i;
                    break;
                }
            }
            if (index >= 0)
            {
                GlobalVariableService.getInstance().getDataLinkVoList().remove(index);
                GlobalVariableService.getInstance().getLinkVoList().clear();
            }
            return true;
        }
    }
}
