package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LinkVo;


public class GetSimpleLinkCmd implements Command<LinkVo> {
    private String entityID;
    public GetSimpleLinkCmd(String entityID) {
        this.entityID = entityID;
    }

    @Override
    public LinkVo execute(CommandContext cc) {
        LinkVo updateVo = cc.getLink().getLinkWithEntityId("ep_"+cc.getProject().getId()+"_link", entityID);
        return updateVo;
    }
}
