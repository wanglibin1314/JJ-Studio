package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.LinkVo;

import java.util.ArrayList;
import java.util.List;

public class GetLinkCmd extends LinkCmd implements Command<List<LinkVo>> {
    private String entityID;
    private boolean isTask;
    private boolean isTable;
    private List<String> entityIDList;
    public GetLinkCmd(String entityID) {
        this.entityID = entityID;
        isTask = true;
    }
    public GetLinkCmd(BaseEntity entity) {
        this.entityID = entity.getId();
        isTask = false;
    }
    public GetLinkCmd(String entityID, boolean isTable) {
        this.entityID = entityID;
        this.isTable = isTable;
        isTask = true;
    }
    public GetLinkCmd(List<String> entityIDList, boolean isTable) {
        this.entityIDList = entityIDList;
        this.isTable = isTable;
        isTask = true;
    }
    @Override
    public List<LinkVo> execute(CommandContext cc) {
        synchronized (GlobalVariableService.getInstance().getDataLinkVoList()) {
            //List<LinkVo> linkVos = GlobalVariableService.getInstance().getDataLinkVoList();
            //List<LinkVo> treeVoList = GlobalVariableService.getInstance().getLinkVoList();
            if (Strings.isBlank(entityID) && GlobalVariableService.getInstance().getDataLinkVoList().size() == 0 &&
                    entityIDList == null && isTask == true)
            {
                List<LinkVo> list = cc.getLink().getLink("ep_"+cc.getProject().getId()+"_link");
                if (list.size() == 0)
                    return list;
                GlobalVariableService.getInstance().getDataLinkVoList().addAll(list);
                return list;
            } else if ((Strings.isBlank(entityID) == false || entityIDList != null) && isTask == true) {
                //获取单个entityID的关系
                if (entityIDList == null) {
                    return getEntityInDatasource(entityID, cc);
                } else {
                    //获取多个entityID的关系
                    return getEntityListInDatasource(entityIDList);
                }
            } else if (Strings.isBlank(entityID) == false && isTask == false) {
                return getEntityInDatasource(entityID, cc);
            } else
                return null;
        }
    }

    private List<LinkVo> getEntityInDatasource(String entityID, CommandContext cc) {
        LinkVo updateVo = cc.getLink().getLinkWithEntityId("ep_"+cc.getProject().getId()+"_link", entityID);
        boolean isNeedRefresh = false;
        int updateIndex = -1;
        //查询数据库数据在缓存的表结构是否需要更新
        if (GlobalVariableService.getInstance().getDataLinkVoList().size() > 0) {
            for (int i = 0; i < GlobalVariableService.getInstance().getDataLinkVoList().size(); i++) {
                LinkVo parentVo = GlobalVariableService.getInstance().getDataLinkVoList().get(i);
                if (parentVo.getEntityId().equals(entityID)) {
                    updateIndex = i;
                    Boolean isBlack = Strings.isBlank(parentVo.getChildrenEntityId()) ||
                            Strings.isBlank(updateVo.getChildrenEntityId());
                    if (isBlack == true)
                        isNeedRefresh = true;
                    else if (parentVo.getChildrenEntityId().equals(updateVo.getChildrenEntityId()) == false)
                        isNeedRefresh = true;
                    break;
                }
            }
        }
        //修改缓存的表数据
        if (updateVo == null) {
            if (updateIndex >= 0) {
                GlobalVariableService.getInstance().getDataLinkVoList().remove(updateIndex);
                return new ArrayList<>();
            } else {
                return new ArrayList<>();
            }
        } else {
            if (updateIndex == -1) {
                isNeedRefresh = true;
                GlobalVariableService.getInstance().getDataLinkVoList().add(updateVo);
            } else {
                GlobalVariableService.getInstance().getDataLinkVoList().set(updateIndex, updateVo);
            }
        }
        //查询缓存的树数据
        List<LinkVo> changeList = new ArrayList<>();
        LinkVo treeVo = null;
        int updateTreeLinkVoIndex = -1;
        for (int i = 0; i < GlobalVariableService.getInstance().getLinkVoList().size(); i++) {
            LinkVo parentVo = GlobalVariableService.getInstance().getLinkVoList().get(i);
            if (parentVo.getEntityId().equals(entityID)) {
                updateTreeLinkVoIndex = i;
                treeVo = parentVo;
                break;
            }
        }
        //表数据未变化情况下
        if (isNeedRefresh == false && updateTreeLinkVoIndex > -1) {
            changeList.add(treeVo);
            if (isTable == true) {
                List<LinkVo> tableVoList = new ArrayList<>();
                tableVoList = getTableListWithTree(changeList.get(0), tableVoList);
                return tableVoList;
            }
            return changeList;
        } else {
            //表数据变化情况下
            if (GlobalVariableService.getInstance().getDataLinkVoList().size() == 0)
                return changeList;
            else {
                if (isTask) {
                    List<LinkVo> list = parseTree(GlobalVariableService.getInstance().getDataLinkVoList(), entityID);
                    if (list.size() == 0)
                        return changeList;
                    //跟新显示数据
                    if (updateTreeLinkVoIndex == -1)
                        GlobalVariableService.getInstance().getLinkVoList().add(list.get(0));
                    else
                        GlobalVariableService.getInstance().getLinkVoList().set(updateTreeLinkVoIndex, list.get(0));
                    //生成这个树的所有节点
                    if (isTable == true) {
                        List<LinkVo> tableVoList = new ArrayList<>();
                        tableVoList = getTableListWithTree(list.get(0), tableVoList);
                        return tableVoList;
                    }
                    return list;
                } else {
                    return changeList;
                }
            }
        }
    }
    private List<LinkVo> getEntityListInDatasource(List<String> entityIDList) {
        //查询
        List<LinkVo> changeList = new ArrayList<>();
        List<Boolean> needGetLinkList = new ArrayList<>();
        //获取缓存表数据，根据是否存在缓存表数据，中判断是否需要更新
        if (GlobalVariableService.getInstance().getLinkVoList().size() > 0) {
            for (int i = 0; i < entityIDList.size(); i++) {
                String entityIDStr = entityIDList.get(i);
                boolean isContain = false;
                for (int j = 0; j < GlobalVariableService.getInstance().getDataLinkVoList().size(); j++) {
                    LinkVo parentVo = GlobalVariableService.getInstance().getDataLinkVoList().get(j);
                    if (parentVo.getEntityId().equals(entityIDStr)) {
                        needGetLinkList.add(parentVo.isNeedRefresh());
                        isContain = true;
                        break;
                    }
                }
                if (isContain == false) {
                    needGetLinkList.add(true);
                }
            }
        } else {
            for (String idString : entityIDList) {
                needGetLinkList.add(true);
            }
        }
        //是否需要更新
        boolean isAllFalse = true;
        for (Boolean isNeed: needGetLinkList) {
            if (isNeed == true) {
                isAllFalse = false;
                break;
            }
        }
        //全部都不需要更新
        if (isAllFalse == true) {
            List<LinkVo> allLinkVoList = new ArrayList<>();
            for (String searchEntityId: entityIDList) {
                for (LinkVo linkVo: GlobalVariableService.getInstance().getLinkVoList()) {
                    if (linkVo.getEntityId().equals(searchEntityId)) {
                        changeList.add(linkVo);
                        if (isTable == true)
                            allLinkVoList = getTableListWithTree(linkVo, allLinkVoList);
                        break;
                    }
                }
            }

            if (isTable == true) {
                return allLinkVoList;
            }
            return changeList;
        } else {
            //需要更新
            if (GlobalVariableService.getInstance().getDataLinkVoList().size() == 0)
                return changeList;
            else {
                List<LinkVo> allLinkVoList = new ArrayList<>();
                for (int index = 0; index < entityIDList.size(); index++) {
                    if (needGetLinkList.get(index) == true) {
                        List<LinkVo> list = parseTree(GlobalVariableService.getInstance().getDataLinkVoList(), entityIDList.get(index));
                        changeList.add(list.get(0));
                        if (list.size() == 0)
                            return changeList;
                        //跟新显示数据
                        int updateLinkVoIndex = -1;
                        for (int i = 0; i < GlobalVariableService.getInstance().getLinkVoList().size(); i++) {
                            LinkVo parentVo = GlobalVariableService.getInstance().getLinkVoList().get(i);
                            if (parentVo.getEntityId().equals(entityID)) {
                                updateLinkVoIndex = i;
                                break;
                            }
                        }
                        if (updateLinkVoIndex == -1)
                            GlobalVariableService.getInstance().getLinkVoList().add(list.get(0));
                        else
                            GlobalVariableService.getInstance().getLinkVoList().set(updateLinkVoIndex, list.get(0));
                        //生成这个树的所有节点
                        if (isTable == true)
                            allLinkVoList = getTableListWithTree(list.get(0), allLinkVoList);
                    }
                }
                if (isTable == true) {
                    return allLinkVoList;
                }
                return changeList;
            }
        }
    }
}
