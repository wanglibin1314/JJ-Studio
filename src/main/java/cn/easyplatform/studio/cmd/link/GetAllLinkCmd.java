package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.LinkVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAllLinkCmd extends LinkCmd implements Command<Boolean> {
    private String beanId;

    public GetAllLinkCmd(String beanId) {
        this.beanId = beanId;
    }

    public GetAllLinkCmd() {

    }

    @Override
    public Boolean execute(CommandContext cc) {
        Map<String, List<LinkVo>> lists = new HashMap<>();
        if (Strings.isBlank(beanId)) {
            ProjectBean[] projects = cc.getIdentityDao().getAllProject();
            for (ProjectBean bean : projects) {
                if (cc.getEntityDao().exists("ep_"+bean.getId()+"_link")) {
                    List<LinkVo> list = cc.getLink().getLink("ep_"+bean.getId()+"_link");
                    lists.put(bean.getId(), list);
                }
            }

        } else {
            List<LinkVo> list = cc.getLink().getLink("ep_"+beanId+"_link");
            lists.put(beanId, list);
        }
        GlobalVariableService.getOneInstance().setAllDataLinkVoMap(lists);
        return true;
    }
}
