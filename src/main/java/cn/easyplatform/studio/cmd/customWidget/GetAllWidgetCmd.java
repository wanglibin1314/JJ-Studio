package cn.easyplatform.studio.cmd.customWidget;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.CustomWidgetGroupVo;
import org.zkoss.util.resource.Labels;

import java.util.*;

public class GetAllWidgetCmd implements Command<List<CustomWidgetGroupVo>> {

    @Override
    public List<CustomWidgetGroupVo> execute(CommandContext cc) {
        List<CustomWidgetGroupVo> customWidgetGroupVos = new ArrayList<>();
        CustomWidgetGroupVo jjGroupVo = new CustomWidgetGroupVo();
        jjGroupVo.setGroupName(Labels.getLabel("custom.tab.jj"));
        jjGroupVo.setCustomWidgetVoList(cc.getCustomWidgetDao().selectAllPublicList());
        customWidgetGroupVos.add(jjGroupVo);

        CustomWidgetGroupVo productGroupVo = new CustomWidgetGroupVo();
        productGroupVo.setGroupName(Labels.getLabel("menu.project"));
        productGroupVo.setCustomWidgetVoList(cc.getCustomWidgetDao().selectAllProductList(
                cc.getProject().getEntityTableName() + "_widget_info", null));
        customWidgetGroupVos.add(productGroupVo);

        CustomWidgetGroupVo personGroupVo = new CustomWidgetGroupVo();
        personGroupVo.setGroupName(Labels.getLabel("custom.tab.person"));
        personGroupVo.setCustomWidgetVoList(cc.getCustomWidgetDao().selectAllProductList(
                cc.getProject().getEntityTableName() + "_widget_info", cc.getUser().getUserId()));
        customWidgetGroupVos.add(personGroupVo);
        return customWidgetGroupVos;
    }
}
