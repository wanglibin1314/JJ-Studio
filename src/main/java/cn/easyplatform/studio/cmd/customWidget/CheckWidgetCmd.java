package cn.easyplatform.studio.cmd.customWidget;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.CustomWidgetVo;
import org.zkoss.util.resource.Labels;


public class CheckWidgetCmd implements Command<String> {
    private CustomWidgetVo customWidgetVo;

    public CheckWidgetCmd(CustomWidgetVo customWidgetVo) {
        this.customWidgetVo = customWidgetVo;
    }
    @Override
    public String execute(CommandContext cc) {
        CustomWidgetVo widgetVo = cc.getCustomWidgetDao().selectOneProduct(
                cc.getProject().getEntityTableName() + "_widget_info", customWidgetVo.getWidgetName());
        if (customWidgetVo.getWidgetId() == 0 && widgetVo != null)
            return Labels.getLabel("custom.error.double.name");
        if (widgetVo != null && customWidgetVo.getWidgetId() != 0 && customWidgetVo.getWidgetId() != widgetVo.getWidgetId())
            return Labels.getLabel("custom.error.double.name");
        return null;
    }
}
