package cn.easyplatform.studio.cmd.module;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.ModuleVo;

import java.util.Date;

public class UpdateModuleCmd implements Command<Boolean> {
    private ModuleVo moduleVo;

    public UpdateModuleCmd(ModuleVo moduleVo) {
        this.moduleVo = moduleVo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (moduleVo != null && Strings.isBlank(moduleVo.getModuleId()) == false) {
            moduleVo.setUpdateUser(cc.getUser().getUserId());
            moduleVo.setUpdateDate(new Date());
            return cc.getModule().updateModule("ep_" + cc.getProject().getId() + "_module", moduleVo);
        }
        else
            return false;
    }
}
