package cn.easyplatform.studio.cmd.module;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.ModuleVo;

import java.util.List;

public class GetModuleCmd implements Command<List<ModuleVo>> {
    private String searchStr;
    public GetModuleCmd(String searchStr) {
        this.searchStr = searchStr;
    }
    @Override
    public List<ModuleVo> execute(CommandContext cc) {
        List<ModuleVo> vos = cc.getModule().getModule("ep_"+cc.getProject().getId()+"_module", searchStr);
        return vos;
    }
}
