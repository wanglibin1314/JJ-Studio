package cn.easyplatform.studio.cmd.module;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.ModuleVo;

public class DeleteModuleCmd implements Command<Boolean> {
    private ModuleVo moduleVo;

    public DeleteModuleCmd(ModuleVo moduleVo) {
        this.moduleVo = moduleVo;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        if (moduleVo != null && Strings.isBlank(moduleVo.getModuleId()) == false)
            return cc.getModule().deleteModule("ep_"+cc.getProject().getId()+"_module", moduleVo);
        else
            return false;
    }
}
