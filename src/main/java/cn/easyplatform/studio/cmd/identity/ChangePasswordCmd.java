/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.dao.IdentityDao;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.SecurityUtils;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ChangePasswordCmd implements Command<String> {

    private String oldPassword;

    private String newPassword;

    public ChangePasswordCmd(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    @Override
    public String execute(CommandContext cc) {
        String pwd = SecurityUtils.getSecurePassword(oldPassword, cc.getUser().getSalt());
        if (!pwd.equals(cc.getUser().getPassword())) //旧密码不匹配
            return "user.password.error";
        pwd = SecurityUtils.getSecurePassword(newPassword, cc.getUser().getSalt());
        IdentityDao dao = cc.getIdentityDao();
        dao.updatePassword(cc.getUser().getUserId(), pwd);
        cc.getUser().setPassword(pwd);
        return null;
    }

}
