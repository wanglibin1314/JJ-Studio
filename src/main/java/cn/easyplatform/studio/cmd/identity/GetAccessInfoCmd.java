package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.AccessVo;

import java.util.List;

public class GetAccessInfoCmd implements Command<List<AccessVo>> {
    @Override
    public List<AccessVo> execute(CommandContext cc) {
        return cc.getBizDao().getAccess();
    }
}
