/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.UserVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetStudioUsersCmd implements Command<List<UserVo>> {

    @Override
    public List<UserVo> execute(CommandContext cc) {
        FieldVo params = new FieldVo(FieldType.VARCHAR, cc.getProject().getId());
        List<Object[]> result = cc.getIdentityDao()
                .selectList(
                        "select a.userId,a.name,a.password,a.validDate,a.validDays,a.state,a.theme,a.editorTheme,a.salt,b.roleId from ep_user_info a,ep_user_project_info b " +
                                "where a.userId=b.userId and b.projectId=? order by a.userId", params);
        List<UserVo> users = new ArrayList<UserVo>(result.size());
        for (Object[] objs : result) {
            UserVo vo = new UserVo();
            vo.setId((String) objs[0]);
            vo.setName((String) objs[1]);
            vo.setPassword((String) objs[2]);
            vo.setValidDate((Date) objs[3]);
            vo.setValidDays(objs[4] == null ? 0 : ((Number) objs[4]).intValue());
            vo.setState(objs[5] == null ? 0 : ((Number) objs[5]).intValue());
            vo.setTheme(objs[6] == null ? "default" : objs[6].toString());
            vo.setEditorTheme(objs[7] == null ? "default" : objs[7].toString());
            vo.setSalt(objs[8].toString());
            vo.setRoleId(objs[9] == null ? "default" : objs[9].toString());
            users.add(vo);
        }
        return users;
    }
}
