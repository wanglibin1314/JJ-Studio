/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SetThemeCmd implements Command<Boolean> {

	private String theme;

	private boolean isEditor;

	/**
	 * @param theme
	 * @param isEditor
	 */
	public SetThemeCmd(String theme, boolean isEditor) {
		this.theme = theme;
		this.isEditor = isEditor;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		if (isEditor) {
			cc.getIdentityDao().updateEditorTheme(cc.getUser().getUserId(),
					theme);
			cc.getUser().setEditorTheme(theme);
		} else {
			cc.getIdentityDao().updateTheme(cc.getUser().getUserId(), theme);
			cc.getUser().setTheme(theme);
		}
		return true;
	}

}
