/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.UserVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UpdateStudioUserCmd implements Command<Boolean> {

    private UserVo user;

    /**
     * @param user
     */
    public UpdateStudioUserCmd(UserVo user) {
        this.user = user;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        List<FieldVo> params = new ArrayList<FieldVo>();
        try {
            cc.beginTx();
            if (user.getCode() == 'D') {
                params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
                //params.add(new FieldVo(FieldType.VARCHAR, cc.getProject().getId()));
                cc.getIdentityDao()
                        .update("delete from ep_user_project_info where userId=?",
                                params);
                cc.getIdentityDao().update(
                        "delete from ep_user_info where userId=?", params);
            } else if (user.getCode() == 'C') {
                params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getName()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getPassword()));
                //params.add(new FieldVo(FieldType.INT, user.getType()));
                params.add(new FieldVo(FieldType.DATE, user.getValidDate()));
                params.add(new FieldVo(FieldType.INT, user.getValidDays()));
                params.add(new FieldVo(FieldType.INT, user.getState()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getSalt()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getTheme()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getEditorTheme()));
                //params.add(new FieldVo(FieldType.VARCHAR, user.getRoleId()));
                cc.getIdentityDao()
                        .update("insert into ep_user_info(userId,name,password,validDate,validDays,state,salt,theme,editorTheme) values (?,?,?,?,?,?,?,?,?)",
                                params);
                params.clear();
                params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
                params.add(new FieldVo(FieldType.VARCHAR, cc.getProject().getId()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getRoleId()));
                cc.getIdentityDao()
                        .update("insert into ep_user_project_info(userId,projectId,roleId) values (?,?,?)",
                                params);
            } else {
                params.add(new FieldVo(FieldType.VARCHAR, user.getName()));
                //params.add(new FieldVo(FieldType.INT, user.getType()));
                params.add(new FieldVo(FieldType.DATE, user.getValidDate()));
                params.add(new FieldVo(FieldType.INT, user.getValidDays()));
                params.add(new FieldVo(FieldType.INT, user.getState()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getTheme()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getEditorTheme()));
                //params.add(new FieldVo(FieldType.VARCHAR, user.getRoleId()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
                cc.getIdentityDao()
                        .update("update ep_user_info set name=?,validDate=?,validDays=?,state=?,theme=?,editorTheme=? where userId=?",
                                params);
                params.clear();
                params.add(new FieldVo(FieldType.VARCHAR, user.getRoleId()));
                params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
                params.add(new FieldVo(FieldType.VARCHAR, cc.getProject().getId()));
                cc.getIdentityDao()
                        .update("update ep_user_project_info set roleId=? where userId=? and projectId=?",
                                params);
            }
            cc.commitTx();
            return true;
        } catch (Exception ex) {
            cc.rollbackTx();
            if (ex instanceof DaoException)
                throw (DaoException) ex;
            throw new StudioException("UpdateStudioUserCmd:" + ex.getMessage());
        } finally {
            cc.closeTx();
        }
    }

}
