/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.UserVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UpdateUserCmd implements Command<Boolean> {

	private UserVo user;

	/**
	 * @param user
	 */
	public UpdateUserCmd(UserVo user) {
		this.user = user;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		List<FieldVo> params = new ArrayList<FieldVo>();
		try {
			cc.beginTx();
			if (user.getCode() == 'D') {
				params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
				cc.getBizDao()
						.update("delete from sys_user_role_info where userId=?",
								params);
				cc.getBizDao().update(
						"delete from sys_user_org_info where userId=?", params);
				cc.getBizDao().update(
						"delete from sys_user_info where userId=?", params);
			} else if (user.getCode() == 'C') {
				params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
				params.add(new FieldVo(FieldType.VARCHAR, user.getName()));
				params.add(new FieldVo(FieldType.VARCHAR, user.getPassword()));
				params.add(new FieldVo(FieldType.INT, user.getType()));
				params.add(new FieldVo(FieldType.DATE, user.getValidDate()));
				params.add(new FieldVo(FieldType.INT, user.getValidDays()));
				params.add(new FieldVo(FieldType.INT, user.getState()));
				cc.getBizDao()
						.update("insert into sys_user_info(userId,name,password,type,validDate,validDays,state) values (?,?,?,?,?,?,?)",
								params);
				if (!user.getRoles().isEmpty()) {
					for (int i = 0; i < user.getRoles().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, user
								.getRoles().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						cc.getBizDao()
								.update("insert into sys_user_role_info(userId,roleId,orderNo) values (?,?,?)",
										params);
					}
				}
				if (!user.getOrgs().isEmpty()) {
					for (int i = 0; i < user.getOrgs().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, user
								.getOrgs().get(i)));
						cc.getBizDao()
								.update("insert into sys_user_org_info(userId,orgId) values (?,?)",
										params);
					}
				}
			} else {
				params.add(new FieldVo(FieldType.VARCHAR, user.getName()));
				params.add(new FieldVo(FieldType.INT, user.getType()));
				params.add(new FieldVo(FieldType.DATE, user.getValidDate()));
				params.add(new FieldVo(FieldType.INT, user.getValidDays()));
				params.add(new FieldVo(FieldType.INT, user.getState()));
				params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
				cc.getBizDao()
						.update("update sys_user_info set name=?,type=?,validDate=?,validDays=?,state=? where userId=?",
								params);
				params.clear();
				params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
				cc.getBizDao()
						.update("delete from sys_user_role_info where userId=?",
								params);
				if (!user.getRoles().isEmpty()) {
					for (int i = 0; i < user.getRoles().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, user
								.getRoles().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						cc.getBizDao()
								.update("insert into sys_user_role_info(userId,roleId,orderNo) values (?,?,?)",
										params);
					}
				}
				params.clear();
				params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
				cc.getBizDao().update(
						"delete from sys_user_org_info where userId=?", params);
				if (!user.getOrgs().isEmpty()) {
					for (int i = 0; i < user.getOrgs().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, user.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, user
								.getOrgs().get(i)));
						cc.getBizDao()
								.update("insert into sys_user_org_info(userId,orgId) values (?,?)",
										params);
					}
				}
			}
			cc.commitTx();
			return true;
		} catch (Exception ex) {
			cc.rollbackTx();
			if (ex instanceof DaoException)
				throw (DaoException) ex;
			throw new StudioException("UpdateUserCmd:" + ex.getMessage());
		} finally {
			cc.closeTx();
		}
	}

}
