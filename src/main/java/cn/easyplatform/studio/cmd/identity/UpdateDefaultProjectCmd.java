package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.cfg.SessionManager;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginUserVo;
import org.zkoss.zk.ui.Sessions;

public class UpdateDefaultProjectCmd implements Command<Boolean> {

    private String userId;
    private String defaultProject;

    public UpdateDefaultProjectCmd(String userId, String defaultProject) {
        this.userId = userId;
        this.defaultProject = defaultProject;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        cc.getIdentityDao().updateDefaultProject(userId, defaultProject);
        LoginUserVo userVo = cc.getUser();
        userVo.setDefaultProject(defaultProject);
        Contexts.setUser(userVo);
        return true;
    }
}
