/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.SecurityUtils;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.type.FieldType;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PasswordResetCmd implements Command<String> {

	private String userId;

	/**
	 * @param userId
	 */
	public PasswordResetCmd(String userId) {
		this.userId = userId;
	}

	@Override
	public String execute(CommandContext cc) {
		String password = RandomStringUtils.randomAlphanumeric(6);
		List<FieldVo> params = new ArrayList<FieldVo>();
		params.add(new FieldVo(FieldType.VARCHAR, SecurityUtils
				.encodeBizPassword(password)));
		params.add(new FieldVo(FieldType.VARCHAR, userId));
		cc.getBizDao().update(
				"update sys_user_info set password=? where userId=?", params);
		return password;
	}

}
