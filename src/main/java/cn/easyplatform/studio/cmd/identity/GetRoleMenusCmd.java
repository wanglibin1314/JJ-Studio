/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetRoleMenusCmd implements Command<List<String>> {

	public enum RoleType {
		DESKTOPTYPE,
		MOBILETYPE
	}
	private String roleId;
	private RoleType roleType;

	/**
	 * @param roleId
	 */
	public GetRoleMenusCmd(String roleId, RoleType roleType) {
		this.roleId = roleId;
		this.roleType = roleType;
	}

	@Override
	public List<String> execute(CommandContext cc) {
		List<Object[]> result = null;
		if (roleType == RoleType.DESKTOPTYPE) {
			result = cc.getBizDao().selectList(
							"select menuId from sys_role_menu_info where roleId=? and `type`='ajax' order by orderNo",
							new FieldVo(FieldType.VARCHAR, roleId));
		} else {
			result = cc.getBizDao().selectList(
					"select menuId from sys_role_menu_info where roleId=? and `type`='mil' order by orderNo",
					new FieldVo(FieldType.VARCHAR, roleId));
		}

		List<String> menus = new ArrayList<String>(result.size());
		for (Object[] objs : result) {
			menus.add((String) objs[0]);
		}
		return menus;
	}

}
