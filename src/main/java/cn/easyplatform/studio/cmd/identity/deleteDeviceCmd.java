/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class deleteDeviceCmd implements Command<Boolean> {

    private long id;

    public deleteDeviceCmd(long id ) {
        this.id = id;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getIdentityDao().deleteDevice(id);
    }

}
