package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.dao.IdentityDao;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginUserVo;

public class InviteLoginCmd implements Command<Object> {
    private String userId;
    private String projectID;
    private String roleID;
    public InviteLoginCmd(String userId, String projectID, String roleID) {
        this.userId = userId;
        this.projectID = projectID;
        this.roleID = roleID;
    }
    @Override
    public Object execute(CommandContext cc) {
        IdentityDao dao = cc.getIdentityDao();
        LoginUserVo user = dao.getUser(userId);
        ProjectBean[] projects = cc.getIdentityDao().getProjectWithID(projectID, roleID, userId);
        if (projects.length > 0) {
            if (projects.length > 1)
                return projects;
            Contexts.setProject(projects[0]);
        }
        return user;
    }
}
