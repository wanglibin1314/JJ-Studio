/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.RoleVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UpdateRoleCmd implements Command<Boolean> {

	private RoleVo role;

	/**
	 * @param role
	 */
	public UpdateRoleCmd(RoleVo role) {
		this.role = role;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		List<FieldVo> params = new ArrayList<FieldVo>();
		try {
			cc.beginTx();
			if (role.getCode() == 'D') {
				params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
				cc.getBizDao()
						.update("delete from sys_role_menu_info where roleId=?",
								params);
				cc.getBizDao().update(
						"delete from sys_role_info where roleId=?", params);
				cc.getBizDao().update(
						"delete from sys_role_menu_info where roleId=?", params);
			} else if (role.getCode() == 'C') {
				params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getName()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getDesp()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getImage()));
				cc.getBizDao()
						.update("insert into sys_role_info(roleId,name,desp,image) values (?,?,?,?)",
								params);
				if ((role.getDeskTopMenus() != null && role.getMobileMenus() != null) &&
                        !role.getDeskTopMenus().isEmpty() && !role.getMobileMenus().isEmpty()) {
					for (int i = 0; i < role.getDeskTopMenus().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getDeskTopMenus().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						params.add(new FieldVo(FieldType.VARCHAR, "ajax"));
						cc.getBizDao()
								.update("insert into sys_role_menu_info(roleId,menuId,orderNo,`type`) values (?,?,?,?)",
										params);
					}
					for (int i = 0; i < role.getMobileMenus().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getMobileMenus().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						params.add(new FieldVo(FieldType.VARCHAR, "mil"));
						cc.getBizDao()
								.update("insert into sys_role_menu_info(roleId,menuId,orderNo,`type`) values (?,?,?,?)",
										params);
					}
				}

				if (role.getRoleAccessVos() != null && !role.getRoleAccessVos().isEmpty()) {
					for (int i = 0; i < role.getRoleAccessVos().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getTaskId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getType()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getAccess()));
						cc.getBizDao()
								.update("insert into sys_role_access_info(roleId,taskId,`type`,access) values (?,?,?,?)",
										params);
					}
				}
			} else {
				params.add(new FieldVo(FieldType.VARCHAR, role.getName()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getDesp()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getImage()));
				params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
				cc.getBizDao()
						.update("update sys_role_info set name=?,desp=?,image=? where roleId=?",
								params);
				params.clear();
				params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
				cc.getBizDao()
						.update("delete from sys_role_menu_info where roleId=?",
								params);
				if ((role.getDeskTopMenus() != null && role.getMobileMenus() != null) &&
						!role.getDeskTopMenus().isEmpty() || !role.getMobileMenus().isEmpty()) {
					for (int i = 0; i < role.getDeskTopMenus().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getDeskTopMenus().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						params.add(new FieldVo(FieldType.VARCHAR, "ajax"));
						cc.getBizDao()
								.update("insert into sys_role_menu_info(roleId,menuId,orderNo,`type`) values (?,?,?,?)",
										params);
					}
					for (int i = 0; i < role.getMobileMenus().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getMobileMenus().get(i)));
						params.add(new FieldVo(FieldType.INT, i + 1));
						params.add(new FieldVo(FieldType.VARCHAR, "mil"));
						cc.getBizDao()
								.update("insert into sys_role_menu_info(roleId,menuId,orderNo,`type`) values (?,?,?,?)",
										params);
					}
				}
				params.clear();
				params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
				cc.getBizDao()
						.update("delete from sys_role_access_info where roleId=?",
								params);
				if (role.getRoleAccessVos() != null && !role.getRoleAccessVos().isEmpty()) {
					for (int i = 0; i < role.getRoleAccessVos().size(); i++) {
						params.clear();
						params.add(new FieldVo(FieldType.VARCHAR, role.getId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getTaskId()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getType()));
						params.add(new FieldVo(FieldType.VARCHAR, role
								.getRoleAccessVos().get(i).getAccess()));
						cc.getBizDao()
								.update("insert into sys_role_access_info(roleId,taskId,`type`,access) values (?,?,?,?)",
										params);
					}
				}
			}
			cc.commitTx();
			return true;
		} catch (Exception ex) {
			cc.rollbackTx();
			if (ex instanceof DaoException)
				throw (DaoException) ex;
			throw new StudioException("BatchUpdateCmd:" + ex.getMessage());
		} finally {
			cc.closeTx();
		}
	}
}
