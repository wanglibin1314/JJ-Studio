/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginlogVo;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class AddDeviceCmd implements Command<Long> {

    private String xml;

    public AddDeviceCmd(String xml) {
        this.xml = xml;
    }

    @Override
    public Long execute(CommandContext cc) {
        return cc.getIdentityDao().insertDevice(Contexts.getProject().getId(),xml);
    }

}
