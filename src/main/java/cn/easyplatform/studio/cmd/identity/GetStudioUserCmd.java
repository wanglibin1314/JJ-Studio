package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.LoginUserVo;

public class GetStudioUserCmd implements Command<LoginUserVo> {

    private String userId;
    public GetStudioUserCmd(String userId) {
        this.userId = userId;
    }

    @Override
    public LoginUserVo execute(CommandContext cc) {
        return cc.getIdentityDao().getUser(userId);
    }
}
