package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.IdentityDao;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.utils.SecurityUtils;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.SMSVo;
import org.zkoss.util.resource.Labels;

import java.util.Date;
import java.util.List;

public class RegisterCmd implements Command<Boolean> {
    private String mobile;
    private String name;
    private String password;
    private String code;

    public RegisterCmd(String mobile, String name, String password, String code) {
        this.mobile = mobile;
        this.name = name;
        this.password = password;
        this.code = code;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        IdentityDao dao = cc.getIdentityDao();
        LoginUserVo user = dao.getUser(mobile);
        String salt = SecurityUtils.getSalt();
        if (user != null)
            throw new StudioException(Labels.getLabel("register.mobile.register"));
        //获取手机号获取验证码信息
        List<SMSVo> smsVoList = GlobalVariableService.getOneInstance().getSmsVos();
        long diff = 600000;
        SMSVo currentSMSVo = null;
        for (SMSVo smsVo : smsVoList) {
            if (mobile.equals(smsVo.getPhoneNO()) && SMSVo.SMSType.SMSRegister.getName().equals(smsVo.getType())) {
                currentSMSVo = smsVo;
                diff = new Date().getTime() -  currentSMSVo.getCreateDate().getTime();
            }
        }
        if (currentSMSVo == null || diff > 600000)
            throw new StudioException(Labels.getLabel("sms.set.error"));
        if (code.equals(currentSMSVo.getContent()) == false)
            throw new StudioException(Labels.getLabel("sms.input.error"));
        return dao.addUser(mobile, name, salt, SecurityUtils.getSecurePassword(password, salt));
    }
}
