/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class GetPageXmlCmd implements Command<String> {

    private long id;

    public GetPageXmlCmd(long id ) {
        this.id = id;
    }

    @Override
    public String execute(CommandContext cc) {
        return cc.getIdentityDao().getPageXml(id);
    }

}
