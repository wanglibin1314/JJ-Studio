package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.List;

public class DatabaseCompleteCmd implements Command<Boolean> {
    @Override
    public Boolean execute(CommandContext cc) {
        //第3.8版本
        List<String> projectList = cc.getEntityDao().getColumnNameList("ep_user_project_info");
        if (projectList != null && projectList.size() == 2 && "userId".equals(projectList.get(0)) &&
                "projectId".equals(projectList.get(1))) {
            cc.getDatabaseUpdateDao().createThreeEightVersion();
            cc.getDatabaseUpdateDao().createThreeNineVersion();
        }
        return true;
    }
}
