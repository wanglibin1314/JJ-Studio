/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.OrgVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetOrgsCmd implements Command<List<OrgVo>> {

	@Override
	public List<OrgVo> execute(CommandContext cc) {
		List<Object[]> result = cc.getBizDao().selectList(
				"select orgId,name from sys_org_info order by orgId");
		List<OrgVo> orgs = new ArrayList<OrgVo>(result.size());
		for (Object[] objs : result) {
			OrgVo vo = new OrgVo();
			vo.setId((String) objs[0]);
			vo.setName((String) objs[1]);
			orgs.add(vo);
		}
		return orgs;
	}
}
