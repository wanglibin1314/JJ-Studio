package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MenuInfoVo;
import cn.easyplatform.studio.vos.QueryMenuInfoVo;

import java.util.ArrayList;
import java.util.List;

public class PagingMenuInfoCmd  extends MenuInfoCmd implements Command<QueryMenuInfoVo> {
    private MenuInfoVo value;

    private int pageSize;

    private int pageNo;

    private List<MenuInfoVo> allList;

    public PagingMenuInfoCmd(MenuInfoVo value, int pageSize, int pageNo, List<MenuInfoVo> allList) {
        this.value = value;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.allList = allList;
    }

    @Override
    public QueryMenuInfoVo execute(CommandContext cc) {
        List<MenuInfoVo> changeList = new ArrayList<>();
        if (allList.size() == 0)
            return new QueryMenuInfoVo(1, changeList);
        return parentTree(allList, value, pageSize, pageNo);
    }
}
