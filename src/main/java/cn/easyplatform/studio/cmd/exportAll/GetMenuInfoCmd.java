package cn.easyplatform.studio.cmd.exportAll;


import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MenuInfoVo;

import java.util.ArrayList;
import java.util.List;

public class GetMenuInfoCmd extends MenuInfoCmd implements Command<List<MenuInfoVo>> {
    private String menuIdCode;
    private List<MenuInfoVo> menuList;
    public  GetMenuInfoCmd(String menuIdCode, List<MenuInfoVo> menuList)
    {
        this.menuIdCode = menuIdCode;
        this.menuList = menuList;
    }
    @Override
    public List<MenuInfoVo> execute(CommandContext cc) {
        if (Strings.isBlank(menuIdCode) && (menuList == null || menuList.size() == 0)) {
            List<MenuInfoVo> list = cc.getMenuInfoDao(null).selectAllList();
            return list;
        } else {
            List<MenuInfoVo> changeList = new ArrayList<>();

            MenuInfoVo vo = null;
            if (menuList.size() > 0) {
                for (int i = 0; i < menuList.size(); i++) {
                    MenuInfoVo parentVo = menuList.get(i);
                    if (parentVo.getMenuId().equals(menuIdCode)) {
                        vo = parentVo;
                        break;
                    }
                }
            }
            changeList.add(vo);
            return changeList;
        }
    }
}
