package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicDetailVo;

import java.util.List;

public class GetDicDetailCmd implements Command<List<DicDetailVo>> {
    private String dicCode;
    public  GetDicDetailCmd(String dicCode)
    {
        this.dicCode = dicCode;
    }

    @Override
    public List<DicDetailVo> execute(CommandContext cc) {
        List<DicDetailVo> detailList = cc.getDicDetailDao(null).selectOne(dicCode);
        return detailList;
    }
}
