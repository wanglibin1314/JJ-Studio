package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicVo;
import cn.easyplatform.studio.vos.QueryDicVo;

import java.util.ArrayList;
import java.util.List;

public class PagingDicCmd extends DicCmd implements Command<QueryDicVo> {

    private DicVo value;

    private int pageSize;

    private int pageNo;

    private List<DicVo> allList;

    public PagingDicCmd(DicVo value, int pageSize, int pageNo, List<DicVo> allList) {
        this.value = value;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.allList = allList;
    }

    @Override
    public QueryDicVo execute(CommandContext cc) {
        List<DicVo> changeList = new ArrayList<>();
        if (allList.size() == 0)
            return new QueryDicVo(1, changeList);
        return parentTree(allList, value, pageSize, pageNo);
    }
}
