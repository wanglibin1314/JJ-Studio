package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.cmd.taskLink.TaskLinkCmd;
import cn.easyplatform.studio.vos.MenuInfoVo;
import cn.easyplatform.studio.vos.QueryMenuInfoVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuInfoCmd {
    protected QueryMenuInfoVo parentTree(List<MenuInfoVo> linkList, MenuInfoVo value, int pageSize, int pageNo)
    {
        List<MenuInfoVo> entityList = new ArrayList<>();

        // 1、获取第一级节点
        int fromIndex = (pageNo - 1) * pageSize;
        int toIndex = pageNo * pageSize;
        int addIndex = 0;
        if (linkList.size() <= (pageNo - 1) * pageSize) {
            return new QueryMenuInfoVo(1, entityList);
        }

        for (MenuInfoVo linkVo: linkList) {
            //分页
            boolean pageAdd = false;
            if (addIndex >= fromIndex && addIndex < toIndex)
                pageAdd = true;
            if (pageSize == TaskLinkCmd.NOLIMITPAGESIZE)
                pageAdd = true;
            //搜索
            boolean searchAdd = false;
            if (Strings.isBlank(linkVo.getParentMenuId()) || linkVo.getParentMenuId().equals(" "))
            {
                if (value == null || (Strings.isBlank(value.getMenuId()) && Strings.isBlank(value.getName())
                        && Strings.isBlank(value.getParentMenuId()) && Strings.isBlank(value.getTasks()))) {
                    searchAdd = true;
                } else {
                    if (linkVo.getMenuId().contains(value.getMenuId()) && linkVo.getName().contains(value.getName())
                            && linkVo.getParentMenuId().contains(value.getParentMenuId()) && linkVo.getTasks().contains(value.getTasks()))
                        searchAdd = true;
                }
            }

            if (searchAdd == true)
                addIndex = addIndex + 1;
            if (searchAdd == true && pageAdd == true)
                entityList.add(linkVo);
        }
        return new QueryMenuInfoVo(addIndex, entityList);
    }

    protected QueryMenuInfoVo parseTree(List<MenuInfoVo> linkList, MenuInfoVo parentVo, QueryMenuInfoVo queryDicVo) {
        parentVo.getChildrenList().clear();
        // 缓存成字典
        HashMap<String, MenuInfoVo> entityDic = new HashMap<>();
        for (MenuInfoVo vo: linkList) {
            entityDic.put(vo.getMenuId(), (MenuInfoVo)vo.clone());
        }
        if (parentVo != null) {
            recursiveTree(parentVo, linkList);
            System.out.println(parentVo);
        }

        return queryDicVo;
    }

    private MenuInfoVo recursiveTree(MenuInfoVo parent, List<MenuInfoVo> linkList) {
        for (MenuInfoVo vo: linkList) {
            if (Strings.isBlank(vo.getParentMenuId()) == false) {
                if (vo.getParentMenuId().equals(parent.getMenuId())) {
                    parent.getChildrenList().add(vo);
                }
            }
        }
        for (MenuInfoVo vo: parent.getChildrenList()) {
            recursiveTree(vo,linkList);
        }
        return parent;
    }
}
