/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.uploadHist;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.UploadHistVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SetUploadHistCmd implements Command<Boolean> {

	private UploadHistVo vo;

	private List<UploadHistVo> voList;

	/**
	 * @param vo
	 */
	public SetUploadHistCmd(UploadHistVo vo) {
		this.vo = vo;
	}

	public SetUploadHistCmd(List<UploadHistVo> voList) {
		this.voList = voList;
	}
	@Override
	public Boolean execute(CommandContext cc) {
		if (vo == null && voList == null)
			return false;
		if (vo != null) {
			cc.getUploadHistDao().addUploadHistVo("ep_upload_file_hist", vo);
			return true;
		} else if (voList != null) {
			try {
				cc.beginTx();
				cc.getUploadHistDao().addUploadHistList("ep_upload_file_hist", voList);
				cc.commitTx();
			} catch (Exception ex) {
				cc.rollbackTx();
				if (ex instanceof DaoException)
					throw (DaoException) ex;
				throw new StudioException("ExportCmd:" + ex.getMessage());
			} finally {
				cc.closeTx();
			}
			return true;
		}
		return false;
	}

}
