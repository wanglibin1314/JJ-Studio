package cn.easyplatform.studio.web.editors.page;

import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.entities.beans.report.JxlsReport;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetEntityCmd;
import cn.easyplatform.web.ext.zul.JReport;
import org.zkoss.util.media.AMedia;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReportBuilder extends ComponentBuilder<JReport> {

    @Override
    protected void build(JReport report) {
        report.setSclass("epeditor-canvas-report");
        if (!Strings.isBlank(report.getEntity())
                && Character.isJavaIdentifierPart(report.getEntity().charAt(0))) {
            Object object = StudioApp.execute(report, new GetEntityCmd(report.getEntity()));
            String entityName = null;
            if (object instanceof JasperReportBean) {
                JasperReportBean entity = (JasperReportBean) object;
                entityName = entity.getName();
            } else if (object instanceof JxlsReport) {
                JxlsReport entity = (JxlsReport) object;
                entityName = entity.getName();
            }
            if (entityName != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("<h2 style=\"text-align: center;\">")
                        .append(entityName).append("</h2>");
                report.setContent(new AMedia("", "html", "text/html", sb
                        .toString()));
            }
        }
    }
}
