/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import org.zkoss.zk.ui.Page;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EditorCallback<T> {

	T getValue();

	void setValue(T value);
	
	String getTitle();
	
	Page getPage();
	
	Editor getEditor();
}
