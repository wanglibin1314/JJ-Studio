/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface Editor {

    public final static String DEFAULT_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String DESKTOP_LOGIN_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String DESKTOP_MAIN_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MOBILE_LOGIN_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MOBILE_MAIN_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MENU_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String ROLE_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String USER_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String ACCESS_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String TABLE_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String FILE_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String PRODUCT_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String DB_VIEW_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String SQL_WIZARD_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String I18n_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String HISTORY_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String STUDIO_ROLE_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String STUDIO_USER_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String EXPORT_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String IMPORT_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MANAGE_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String EXPORT_HIST_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String IMPORT_HIST_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String iOS_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Andrpid_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String WX_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String ROOTTASK_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String DICT_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String LABEL_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String INFORM_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MENU_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String FILE_EXPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String ROOTTASK_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String DICT_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String LABEL_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String INFORM_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String MENU_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String FILE_IMPORT_REPO_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String APP_GUIDE_CONFIG_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String STUDIO_GUIDE_CONFIG_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String STUDIO_GUIDE_ABOUT_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String APP_GUIDE_SET_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Module_EDITOR_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Login_Log_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Online_User_Management_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Operation_Log_ID = RandomStringUtils
            .randomAlphanumeric(20);

    public final static String Template_Configuration_ID = RandomStringUtils
            .randomAlphanumeric(20);

    /**
     * 在工作区新建编辑器
     *
     * @param args
     */
    void create(Object... args);

    /**
     * 快捷键
     *
     * @param keyCode
     */
    void onCtrlKeys(int keyCode);

    /**
     * @return
     */
    String getId();


    /**
     * @param theme
     */
    void setTheme(String theme);

    /**
     * 关闭编辑器
     */
    void close();
}
