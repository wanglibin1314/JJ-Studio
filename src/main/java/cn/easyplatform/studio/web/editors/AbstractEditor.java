/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.Executions;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractEditor {

    protected WorkbenchController workbench;

    public AbstractEditor(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    public void onCtrlKeys(int keyCode) {
        // 根据不同的工具打开不同的窗口
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("editor", this);
        data.put("code", keyCode);
        Executions.createComponents("~./include/entitybox.zul", null, data);
    }

    public WorkbenchController getWorkbench() {
        return workbench;
    }

    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }
}
