/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.entities.beans.task.TransitionBean;
import cn.easyplatform.lang.Strings;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TransitionEditorCallback implements EditorCallback<String>, Tablable {

    private Editor editor;

    private TransitionBean bean;

    private Button btn;

    private String table;

    /**
     * @param editor
     * @param bean
     * @param btn
     */
    public TransitionEditorCallback(Editor editor, TransitionBean bean,
                                    Button btn, String table) {
        this.editor = editor;
        this.bean = bean;
        this.btn = btn;
        this.table = table;
    }

    public void setValue(String value) {
        bean.setMappings(value);
        if (Strings.isBlank(value))
            btn.setSclass("epeditor-btn");
        else
            btn.setSclass("epeditor-btn-mark");
        ((AbstractEntityEditor<?>) editor).setDirty();
    }

    public String getTitle() {
        Label label = null;
        if (btn.getPreviousSibling() instanceof Bandbox)
            label = (Label) btn.getParent().getParent().getFirstChild();
        else
            label = (Label) btn.getPreviousSibling();
        return label.getValue();
    }

    public String getValue() {
        return bean.getMappings();
    }

    public Page getPage() {
        return btn.getPage();
    }

    @Override
    public Editor getEditor() {
        return editor;
    }

    @Override
    public String getTable() {
        return table;
    }
}
