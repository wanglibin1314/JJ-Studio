package cn.easyplatform.studio.web.editors.view;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.vos.ArgsVo;
import cn.easyplatform.studio.vos.FunctionVo;
import cn.easyplatform.studio.web.editors.AbstractEntityEditor;
import cn.easyplatform.studio.web.editors.EntityEditor;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import cn.easyplatform.web.ext.cmez.DropEvent;
import cn.easyplatform.web.ext.zul.Include;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Funcs implements EventListener<Event>  {

    private Include funcs;
    private Bandbox method_search;

    private Listbox listbox_catalog;

    private Scrollview scrollview_content;

    private Combobox combobox_catalog;

    private Comboitem comboitem_all;

    private Map<String, FunctionVo> funcMap;

    private List<FunctionVo> nameList;

    public Funcs(Include comp, String scrollviewHeight) {
        funcs = comp;
        comboitem_all = (Comboitem)funcs.getFellow("logic_comboitem_all");
        scrollview_content = (Scrollview)funcs.getFellow("logic_scrollview_method_content");
        scrollview_content.setHeight(scrollviewHeight);
        method_search = (Bandbox)funcs.getFellow("logic_bandbox_method_search");
        method_search.addEventListener(Events.ON_CHANGE, this);
        method_search.addEventListener(Events.ON_OK, this);
        method_search.addEventListener(Events.ON_OPEN, this);
        listbox_catalog = (Listbox) funcs.getFellow("logic_listbox_method_catalog");
        listbox_catalog.addEventListener(Events.ON_CLICK, this);
        combobox_catalog = (Combobox)funcs.getFellow("logic_combobox_catalog");
        combobox_catalog.addEventListener(Events.ON_CHANGE, this);
        listbox_catalog.setStyle("background:#FFFFFF;border: 1px solid #D9D9D9;overflow: hidden;zoom:1;");
        listbox_catalog.setZclass("ui-list-cell-div");
        nameList= new ArrayList<FunctionVo>();
        funcMap = StudioApp.getAllFunction();
        showCatalog("");
        refreshListbox();


    }

    //查找方法内容
    public void showContent(){
        String methodName = listbox_catalog.getSelectedItem().getValue();
        FunctionVo vo = StudioApp.getFunctionDesc(methodName);
        StringBuilder sb = new StringBuilder();
        if(vo!=null) {
            if (vo.getArgs() != null) {
                for (ArgsVo av : vo.getArgs()) {
                    sb.append(vo.getName() + "." + av.getName() + "\n");
                    sb.append(av.getDesc() + "\n");
                }
            }
        }
        if (vo.getExamples() != null) {
            for (String s : vo.getExamples()) {
                sb.append(s);
            }
        }
        scrollview_content.getChildren().clear();
        Label hintLab = new Label();
        hintLab.setValue(sb.toString());
        hintLab.setStyle("font-size:14px;color:#333333;white-space:pre-wrap");
        Div hintDiv = new Div();
        hintDiv.appendChild(hintLab);
        scrollview_content.appendChild(hintDiv);

    }

    //listbox刷新方法
    public void refreshListbox(){
        listbox_catalog.getChildren().clear();
        Listcell cell = null, cell1 = null;
        for(FunctionVo n:nameList){
            Listitem row = new Listitem();
            row.setValue(n.getName());
            cell = new Listcell(n.getName());
            cell1 = new Listcell(n.getcName());
            row.appendChild(cell);
            row.appendChild(cell1);
            row.setDraggable(EntityType.TABLE.getName());
            listbox_catalog.appendChild(row);
        }
    }

    //查找方法目录
    public void showCatalog(String str){
        if(null!=nameList){
            nameList.clear();
        }
        funcMap.values().stream().forEach(i->{
            String name = i.getName();
            String cname = i.getcName();
            if("other".equals(str)){
                if(!name.contains(".")){
                    nameList.add(i);
                }
            } else if(name.contains(str) || cname.contains(str)){
                nameList.add(i);
            }
        });
    }

    @Override
    public void onEvent(Event evt) throws Exception {
        if(evt.getTarget() == method_search){
            if (evt.getName().equals(Events.ON_OPEN)||evt instanceof KeyEvent) {
                if (evt instanceof OpenEvent){
                    method_search.setValue(((String)((OpenEvent)evt).getValue()).trim());
                }
                combobox_catalog.setSelectedItem(comboitem_all);
                showCatalog(method_search.getValue());
                refreshListbox();
            }
        } else if(evt.getTarget() == listbox_catalog){
            showContent();
        }else if (evt.getName().equals(Events.ON_CHANGE)
                || evt.getName().equals(Events.ON_CHANGING)) {
            Component c = evt.getTarget();
            if (c == combobox_catalog) {
                showCatalog((String) combobox_catalog.getSelectedItem().getValue());
                refreshListbox();
            }
        }
    }

    public void updateScroll(Div hintDiv) {
        scrollview_content.getChildren().clear();
        scrollview_content.appendChild(hintDiv);
    }

    public void updateScroll(StringBuilder sb, FunctionVo vo) {
        StringBuffer newsb = new StringBuffer();
        sb.append("<ul class=\"list-group\">");
        if (vo.getArgs() != null) {
            for (ArgsVo av : vo.getArgs()) {
                sb.append("<li class=\"list-group-item\">");
                sb.append("<h4 class=\"list-group-item-heading\">")
                        .append(vo.getName()).append(av.getName())
                        .append("</h4>");
                sb.append("<p class=\"list-group-item-text\">")
                        .append(av.getDesc()).append("</p>");
                sb.append("</li>");
                newsb.append(vo.getName()+"."+av.getName()+ "\n");
                newsb.append(av.getDesc()+ "\n"+ "\n");
            }
        }
        if (vo.getExamples() != null) {
            for (String s : vo.getExamples()) {
                sb.append("<li class=\"list-group-item\">");
                String[] cs = s.split("\n");
                for (int i = 0; i < cs.length; i++) {
                    if (!cs[i].trim().equals("")) {
                        cs[i] = cs[i].replaceFirst("\t", "");
                        sb.append(cs[i].replaceAll("\t", "&emsp;"));
                        if (i < cs.length - 1)
                            sb.append("</br>");
                    }
                }
                sb.append("</li>");
                newsb.append(s);
            }
        }
        sb.append("</ul>");

        Label hintLab = new Label();
        hintLab.setValue(newsb.toString());
        hintLab.setStyle("font-size:14px;color:#333333;white-space:pre-wrap");
        Div hintDiv = new Div();
        hintDiv.appendChild(hintLab);

        updateScroll(hintDiv);
    }
}
