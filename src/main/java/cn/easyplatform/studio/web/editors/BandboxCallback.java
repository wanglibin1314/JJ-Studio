/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BandboxCallback implements EditorCallback<String> {

	private Editor editor;
	
	private Bandbox input;

	private String title;

	public BandboxCallback(Editor editor, Bandbox input) {
		this.editor = editor;
		this.input = input;
	}

	/**
	 * @param input
	 * @param title
	 */
	public BandboxCallback(Bandbox input, String title) {
		this.input = input;
		this.title = title;
	}

	@Override
	public String getValue() {
		return input.getValue();
	}

	@Override
	public void setValue(String value) {
		input.setValue(value);
		Events.sendEvent(Events.ON_CHANGE, input, null);
	}

	@Override
	public String getTitle() {
		if (title != null)
			return title;
		Label label = null;
		if (input.getPreviousSibling() == null)
			label = (Label) input.getParent().getPreviousSibling();
		else
			label = (Label) input.getPreviousSibling();
		return label.getValue();
	}

	@Override
	public Page getPage() {
		return input.getPage();
	}

	@Override
	public Editor getEditor() {
		return editor;
	}

}
