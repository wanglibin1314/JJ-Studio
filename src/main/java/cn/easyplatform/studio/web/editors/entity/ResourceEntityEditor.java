/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.entity;

import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.AbstractEntityEditor;
import cn.easyplatform.studio.web.editors.BandboxCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.SubType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResourceEntityEditor extends AbstractEntityEditor<ResourceBean> {

    private Tabbox tabbox;

    private Textbox entityName;

    private Textbox entityDesp;

    private Combobox type;

    private Include include;

    /**
     * @param workbench
     * @param entity
     * @param code
     */
    public ResourceEntityEditor(WorkbenchController workbench, ResourceBean entity,
                                char code) {
        super(workbench, entity, code);
    }

    @Override
    public void create(Object... args) {
        super.create(args);
        Tabpanel tabpanel = new Tabpanel();
        Idspace is = new Idspace();
        is.setHflex("1");
        is.setVflex("1");
        is.setParent(tabpanel);
        Executions.createComponents("~./include/editor/entity/resource.zul", is,
                null);
        for (Component comp : is.getFellows()) {
            if (comp.getId().equals("resource_tabbox_main")) {
                tabbox = (Tabbox) comp;
                tabbox.addEventListener(Events.ON_SELECT, this);
            } else if (comp.getId().equals("resource_textbox_name")) {
                entityName = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityName.setValue(entity.getName());
            } else if (comp.getId().equals("resource_textbox_desp")) {
                entityDesp = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityDesp.setValue(entity.getDescription());
            } else if (comp.getId().equals("resource_cmeditor_source")) {
                source = (CMeditor) comp;
                source.setTheme(Contexts.getUser().getEditorTheme());
            } else if (comp.getId().equals("resource_combobox_type")) {
                type = (Combobox) comp;
                if (code == 'C')
                    type.addEventListener(Events.ON_SELECT, this);
                else
                    type.setDisabled(true);
                int sel = 0;
                if (entity.getSubType().equals(SubType.PRINTER.getName()))
                    sel = 1;
                type.setSelectedIndex(sel);
            } else if (comp.getId().equals("resource_include_content")) {
                include = (Include) comp;
            }
        }
        redraw();
        workbench.addEditor(tab, tabpanel);
    }

    @Override
    protected void redraw() {
        Map<String, String> props = entity.getProps();
        if (type.getSelectedIndex() == 0) {
            include.setSrc("~./include/editor/entity/cache.zul");
            for (Component c : include.getSpaceOwner().getFellows()) {
                if (c.getId().equals("cache_radiogroup_scope")) {
                    Radiogroup scope = (Radiogroup) c;
                    String val = props.get("scope");
                    scope.setSelectedIndex(val == null || val.equals("session") ? 1 : 0);
                    scope.addEventListener(Events.ON_CHECK, this);
                } else if (c.getId().equals("cache_bandbox_db")) {
                    Bandbox db = (Bandbox) c;
                    db.addEventListener(Events.ON_CHANGE, this);
                    db.addEventListener(Events.ON_OPEN, this);
                    db.setText(props.get("db"));
                } else if (c.getId().equals("cache_cmeditor_sql")) {
                    CMeditor sql = (CMeditor) c;
                    sql.addEventListener(Events.ON_CHANGE, this);
                    sql.setValue(props.get("sql"));
                }
            }
        } else if (type.getSelectedIndex() == 1) {
            include.setSrc("~./include/editor/entity/printer.zul");
        }
    }

    private void setProperty(Component c) {
        if (entity.getProps() == null)
            entity.setProps(new HashMap<String, String>());
        Object val = WebUtils.getComponentValue(c);
        if (val != null)
            entity.getProps().put(c.getId(), val.toString());
        setDirty();
    }

    @Override
    protected void dispatch(Event evt) {
        Component c = evt.getTarget();
        if (c instanceof Tab) {
            if (tabbox.getSelectedIndex() == 1 && validate())
                refreshSource();
        } else if (evt.getName().equals(Events.ON_CHANGE) || evt.getName().equals(Events.ON_CHECK)) {
            if (c instanceof Radio)
                c = ((Radio) c).getRadiogroup();
            setProperty(c);
        } else if (evt.getName().equals(Events.ON_OPEN)) {
            if (c.getId().equals("cache_bandbox_db"))
                AbstractView.createDatasourceView(
                        new BandboxCallback((Bandbox) c, Labels
                                .getLabel("entity.table.db")), evt.getTarget()).doOverlapped();

        } else if (evt.getName().equals(Events.ON_SELECT)) {
            if (entity.getProps() != null)
                entity.getProps().clear();
            redraw();
        }
    }

    @Override
    protected boolean validate() {
        if (Strings.isBlank(entity.getName())) {
            WebUtils.notEmpty(Labels.getLabel("entity.name"));
            return false;
        }
        Map<String, String> props = entity.getProps();
        if (type.getSelectedIndex() == 0) {
            if (Strings.isBlank(props.get("sql"))) {
                WebUtils.notEmpty(Labels.getLabel("entity.list.query"));
                return false;
            }
            entity.setSubType(SubType.CACHE.getName());
        }
        return true;
    }
}
