package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.event.Event;

public class RootTaskImportEditor extends AbstractPanelEditor {
    public RootTaskImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {

    }

    @Override
    public void create(Object... args) {

    }
}
