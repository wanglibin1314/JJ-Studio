/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface Savable {

    /**
     * 是否已变更
     *
     * @return
     */
    boolean isDirty();

    /**
     * 保存
     */
    boolean save();
}
