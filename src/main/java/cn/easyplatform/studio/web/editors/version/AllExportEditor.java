package cn.easyplatform.studio.web.editors.version;

import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;

public class AllExportEditor extends AbstractPanelEditor {
    protected Textbox id;

    protected Textbox name;

    protected Textbox desp;

    protected Listbox targets;

    protected Tree sources;

    protected Textbox comment;

    protected Checkbox allListCheck;

    protected Checkbox pageListCheck;

    protected Checkbox revListCheck;

    public AllExportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {

    }

    @Override
    public void create(Object... args) {

    }
}
