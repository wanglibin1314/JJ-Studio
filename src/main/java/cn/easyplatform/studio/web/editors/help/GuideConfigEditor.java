package cn.easyplatform.studio.web.editors.help;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.help.AddGuideConfigCmd;
import cn.easyplatform.studio.cmd.help.DeleteGuideConfigCmd;
import cn.easyplatform.studio.cmd.help.GetGuideConfigCmd;
import cn.easyplatform.studio.cmd.help.UpdateGuideConfigCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.ElementVo;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.web.ext.introJs.Action;
import cn.easyplatform.web.ext.introJs.Step;
import com.alibaba.fastjson.JSON;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

public class GuideConfigEditor extends AbstractPanelEditor {
    public enum GuideConfigType {
        GuideConfigApp, GuideConfigStudio
    }
    private enum EditStatus {
        EDIT_CREATE, EDIT_CREATE_CHANGE, EDIT_SELECT, EDIT_SELECT_CHANGE
    }

    private Listbox guides;
    private Listbox fields;
    private Bandbox searchBandbox;
    private Textbox groupName;
    private Bandbox nextStep;
    private Checkbox rootBox;
    private Toolbarbutton groupSave;
    private Toolbarbutton groupDelete;
    private Toolbarbutton groupInsert;

    private GuideConfigVo selectedVo = new GuideConfigVo();
    private GuideConfigType configType;
    private List<GuideConfigVo> allGuideConfigVos;
    private EditStatus editStatus = EditStatus.EDIT_CREATE;
    public GuideConfigEditor(WorkbenchController workbench, String id, int type) {
        super(workbench, id);
        if (type == 1)
            configType = GuideConfigType.GuideConfigApp;
        else
            configType = GuideConfigType.GuideConfigStudio;
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_DOUBLE_CLICK) && evt.getTarget() instanceof  Listitem) {
            Listitem selectedItem = (Listitem)evt.getTarget();
            final GuideConfigVo selectedItemVo = selectedItem.getValue();

            if (editStatus == EditStatus.EDIT_SELECT_CHANGE || editStatus == EditStatus.EDIT_CREATE_CHANGE)
                WebUtils.showConfirm(Labels.getLabel("button.save"), new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (event.getName().equals(Events.ON_OK))
                            Events.postEvent(new Event(Events.ON_CLICK, groupSave));
                        else {
                            selectedVo = (GuideConfigVo) selectedItemVo.clone();
                            editStatus = EditStatus.EDIT_SELECT;
                            showHeaderBtns();
                            setValue();
                        }
                    }
                });
            else {
                selectedVo = (GuideConfigVo) selectedItemVo.clone();
                editStatus = EditStatus.EDIT_SELECT;
                showHeaderBtns();
                setValue();
            }
        } else if (evt.getTarget().getId().equals("search") && evt.getName().equals(Events.ON_OPEN)) {
            OpenEvent event = (OpenEvent) evt;
            List<GuideConfigVo> currentGuideConfigVos = searchName(event.getValue().toString().trim());
            redrawGuides(currentGuideConfigVos);
        } else if (evt.getTarget().equals(groupName)) {
            edit();
            selectedVo.setGuideName(groupName.getValue());
        } else if (evt.getTarget().equals(nextStep)) {
            AbstractView.createGuideView(new EditorCallback<GuideConfigVo>() {
                @Override
                public GuideConfigVo getValue() {
                    return null;
                }

                @Override
                public void setValue(GuideConfigVo value) {
                    if (value.getGuideID() == selectedVo.getGuideID()) {
                        WebUtils.showError(Labels.getLabel("guide.add.next.error"));
                        return;
                    } else if (isContain(selectedVo.getGuideID(), value.getGuideID())) {
                        WebUtils.showError(Labels.getLabel("guide.add.next.error"));
                        return;
                    }
                    nextStep.setValue(value.getGuideName());
                    selectedVo.setGuideNextID(Integer.toString(value.getGuideID()));
                    edit();
                }

                @Override
                public String getTitle() {
                    return Labels.getLabel("group.next");
                }

                @Override
                public Page getPage() {
                    return nextStep.getPage();
                }

                @Override
                public Editor getEditor() {
                    return null;
                }
            }, allGuideConfigVos, evt.getTarget()).doOverlapped();
        } else if (evt.getTarget().equals(rootBox)) {
            selectedVo.setIsRoot(rootBox.isChecked() == true ? "R": "N");
            edit();
        } else if (evt.getTarget().getId().equals("addStep")) {
            Step step = new Step();
            selectedVo.getStepList().add(step);
            Listitem row = createStep(step, -1);
            fields.setActivePage(row);
            edit();
        } else if (evt.getTarget().getId().equals("insert")) {
            if (fields.getSelectedItem() == null) {
                WebUtils.showError(Labels.getLabel("step.selected.error"));
                return;
            }
            int pos = fields.getSelectedIndex();
            Step step = new Step();
            if (pos >= 0) {
                pos++;
                selectedVo.getStepList().add(pos, step);
            } else
                selectedVo.getStepList().add(step);
            createStep(step, pos);
            edit();
        } else if (evt.getTarget().getId().equals("delete")) {
            if (fields.getSelectedItem() != null) {
                WebUtils.showConfirm(Labels.getLabel("button.delete"), this);
                edit();
            } else
                WebUtils.showError(Labels.getLabel("step.selected.error"));
        } else if (evt.getTarget().getId().equals("moveUp")) {
            if (fields.getSelectedItem() != null) {
                moveStep(false);
                edit();
            } else
                WebUtils.showError(Labels.getLabel("step.selected.error"));
        } else if (evt.getTarget().getId().equals("moveDown")) {
            if (fields.getSelectedItem() != null) {
                moveStep(true);
                edit();
            } else
                WebUtils.showError(Labels.getLabel("step.selected.error"));
        } else if (evt.getTarget().getId().equals("groupInsert")) {
            if (editStatus == EditStatus.EDIT_SELECT_CHANGE || editStatus == EditStatus.EDIT_CREATE_CHANGE)
                WebUtils.showConfirm(Labels.getLabel("button.save"), new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (event.getName().equals(Events.ON_OK))
                            Events.postEvent(new Event(Events.ON_CLICK, groupSave));
                        else {
                            editStatus = EditStatus.EDIT_CREATE;
                            showHeaderBtns();
                            selectedVo = new GuideConfigVo();
                            setValue();
                        }
                    }
                });
            else {
                editStatus = EditStatus.EDIT_CREATE;
                showHeaderBtns();
                selectedVo = new GuideConfigVo();
                setValue();
            }
        } else if (evt.getTarget().getId().equals("groupDelete")) {
            if (selectedVo.getGuideName() == null) {
                WebUtils.showError(Labels.getLabel("step.selected.error"));
                return;
            }
            WebUtils.showConfirm(Labels.getLabel("button.delete"), new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    if (event.getName().equals(Events.ON_OK)) {
                        Boolean deleteSuccess = StudioApp.execute(new DeleteGuideConfigCmd(selectedVo, configType));
                        List<GuideConfigVo> updateVos = new ArrayList<>();
                        for (GuideConfigVo guideConfigVo: allGuideConfigVos) {
                            if (Strings.isBlank(guideConfigVo.getGuideNextID()) == false &&
                                    Integer.valueOf(guideConfigVo.getGuideNextID()) == selectedVo.getGuideID()) {
                                guideConfigVo.setUpdateUser(Contexts.getUser().getUserId());
                                guideConfigVo.setGuideNextID(null);
                                updateVos.add(guideConfigVo);
                            }
                        }
                        Boolean updateList = true;
                        if (updateVos.size() != 0)
                            updateList = StudioApp.execute(new UpdateGuideConfigCmd(updateVos, configType));

                        if (deleteSuccess == true && updateList == true) {
                            if (guides.getSelectedItem() != null)
                                guides.getSelectedItem().detach();
                            allGuideConfigVos = StudioApp.execute(new GetGuideConfigCmd(configType));
                            List<GuideConfigVo> currentGuideConfigVos = searchName(searchBandbox.getValue().trim());
                            redrawGuides(currentGuideConfigVos);
                            selectedVo = new GuideConfigVo();
                            setValue();
                            editStatus = EditStatus.EDIT_CREATE;
                            showHeaderBtns();
                            workbench.getFunctionBar().refreshGuide();
                        }
                    }
                }
            });
        } else if (evt.getTarget().getId().equals("groupSave")) {
            if (Strings.isBlank(selectedVo.getGuideName())) {
                WebUtils.showError(Labels.getLabel("message.no.empty", new Object[]{Labels.getLabel("entity.name")}));
                return;
            }  else if (selectedVo.getGuideID() <= 0) {
                List<GuideConfigVo> addvos = new ArrayList<>();
                String stepStr = JSON.toJSONString(selectedVo.getStepList());
                selectedVo.setCreateUser(Contexts.getUser().getUserId());
                selectedVo.setIsRoot(rootBox.isChecked() ? "R": "N" );
                selectedVo.setGuideStep(stepStr);
                addvos.add(selectedVo);
                Boolean addflag = StudioApp.execute(new AddGuideConfigCmd(addvos, configType));
                if (addflag) {
                    WebUtils.showSuccess(Labels.getLabel("button.save"));
                    allGuideConfigVos = StudioApp.execute(new GetGuideConfigCmd(configType));
                    selectedVo.setGuideID(allGuideConfigVos.get(allGuideConfigVos.size() - 1).getGuideID());
                    List<GuideConfigVo> currentGuideConfigVos = searchName(searchBandbox.getValue().trim());
                    redrawGuides(currentGuideConfigVos);
                    editStatus = EditStatus.EDIT_CREATE;
                    showHeaderBtns();
                    workbench.getFunctionBar().refreshGuide();
                } else {
                    selectedVo = new GuideConfigVo();
                    WebUtils.showError(Labels.getLabel("message.error", new String[]{Labels.getLabel("button.save")}));
                }
            } else {
                List<GuideConfigVo> vos = new ArrayList<>();
                String stepStr = JSON.toJSONString(selectedVo.getStepList());
                selectedVo.setUpdateUser(Contexts.getUser().getUserId());
                selectedVo.setIsRoot(rootBox.isChecked() ? "R": "N" );
                selectedVo.setGuideStep(stepStr);
                vos.add(selectedVo);
                Boolean upflag = StudioApp.execute(new UpdateGuideConfigCmd(vos, configType));
                if (upflag) {
                    WebUtils.showSuccess(Labels.getLabel("button.save"));
                    allGuideConfigVos = StudioApp.execute(new GetGuideConfigCmd(configType));
                    List<GuideConfigVo> currentGuideConfigVos = searchName(searchBandbox.getValue().trim());
                    redrawGuides(currentGuideConfigVos);
                    editStatus = EditStatus.EDIT_SELECT;
                    showHeaderBtns();
                    workbench.getFunctionBar().refreshGuide();
                } else {
                    WebUtils.showError(Labels.getLabel("message.error", new String[]{Labels.getLabel("button.save")}));
                }
            }
        } else if (evt.getName().equals(Events.ON_OK)) {// 删除栏位
            edit();
            Step step = fields.getSelectedItem().getValue();
            selectedVo.getStepList().remove(step);
            fields.getSelectedItem().detach();
        }
    }

    @Override
    public void create(Object... args) {
        String titleStr;
        if (configType == GuideConfigType.GuideConfigApp)
            titleStr = Labels.getLabel("app.guide.config");
        else
            titleStr = Labels.getLabel("studio.guide.config");
        Idspace is = createPanel(titleStr, "~./images/config.png",
                "~./include/help/GuideConfig.zul");
        Toolbarbutton stepCreate = (Toolbarbutton) is.getFellow("addStep");
        stepCreate.addEventListener(Events.ON_CLICK, this);
        Toolbarbutton insert = (Toolbarbutton) is.getFellow("insert");
        insert.addEventListener(Events.ON_CLICK, this);
        Toolbarbutton delete = (Toolbarbutton) is.getFellow("delete");
        delete.addEventListener(Events.ON_CLICK, this);
        Toolbarbutton moveUp = (Toolbarbutton) is.getFellow("moveUp");
        moveUp.addEventListener(Events.ON_CLICK, this);
        Toolbarbutton moveDown = (Toolbarbutton) is.getFellow("moveDown");
        moveDown.addEventListener(Events.ON_CLICK, this);
        groupInsert = (Toolbarbutton) is.getFellow("groupInsert");
        groupInsert.addEventListener(Events.ON_CLICK, this);
        groupDelete = (Toolbarbutton) is.getFellow("groupDelete");
        groupDelete.addEventListener(Events.ON_CLICK, this);
        groupSave = (Toolbarbutton) is.getFellow("groupSave");
        groupSave.addEventListener(Events.ON_CLICK, this);
        searchBandbox = (Bandbox) is.getFellow("search");
        searchBandbox.addEventListener(Events.ON_CHANGE, this);
        searchBandbox.addEventListener(Events.ON_OPEN, this);
        groupName = (Textbox) is.getFellow("name");
        groupName.addEventListener(Events.ON_CHANGE, this);
        nextStep = (Bandbox) is.getFellow("nextStep");
        nextStep.addEventListener(Events.ON_CHANGE, this);
        nextStep.addEventListener(Events.ON_OPEN, this);
        rootBox = (Checkbox) is.getFellow("rootBox");
        rootBox.addEventListener(Events.ON_CHECK, this);

        guides = (Listbox) is.getFellow("guides");
        fields = (Listbox) is.getFellow("fields");

        allGuideConfigVos = StudioApp.execute(new GetGuideConfigCmd(configType));
        redrawGuides(allGuideConfigVos);
    }

    private void redrawGuides(List<GuideConfigVo> currentGuideConfigVos) {
        guides.getItems().clear();
        if (currentGuideConfigVos != null && currentGuideConfigVos.size() > 0) {
            for (GuideConfigVo vo : currentGuideConfigVos) {
                Listitem li = new Listitem();
                li.appendChild(new Listcell(Integer.toString(vo.getGuideID())));
                li.appendChild(new Listcell(vo.getGuideName()));
                li.appendChild(new Listcell(vo.getGuideNextName()));
                Listcell listcell = new Listcell();
                if (Strings.isBlank(vo.getIsRoot()) == false && vo.getIsRoot().equals("R")) {
                    listcell.setIconSclass("z-icon-check");
                }
                li.appendChild(listcell);
                li.setValue(vo);
                li.addEventListener(Events.ON_DOUBLE_CLICK, this);
                guides.appendChild(li);
            }
        }
    }

    private void setValue() {
        fields.getItems().clear();
        for (int index = 0; index < selectedVo.getStepList().size(); index++) {
            createStep(selectedVo.getStepList().get(index), index);
        }

        groupName.setValue(selectedVo.getGuideName());
        if (Strings.isBlank(selectedVo.getGuideNextName()) == false)
            nextStep.setValue(selectedVo.getGuideNextName());
        else
            nextStep.setValue(null);
        boolean isRoot = false;
        if (Strings.isBlank(selectedVo.getIsRoot()) == false && "R".equals(selectedVo.getIsRoot()))
            isRoot = true;
        rootBox.setChecked(isRoot);
    }

    private boolean isContain(Integer cuarrentId, Integer nextId) {
        List<List<GuideConfigVo>> allPathList = GetGuideConfigCmd.allTree(allGuideConfigVos);
        boolean isContain = false;
        for (List<GuideConfigVo> indexs: allPathList) {
            for (GuideConfigVo index: indexs) {
                if (index.getGuideID() == cuarrentId) {
                    for (GuideConfigVo sIndex: indexs) {
                        if (sIndex.getGuideID() == nextId) {
                            isContain = true;
                            break;
                        }
                    }
                }
            }
        }

        return isContain;
    }

    private Listitem createStep(Step step, int pos) {
        Listitem li = new Listitem();

        Listcell selectedCell = new Listcell();
        li.appendChild(selectedCell);

        Textbox element;
        element = new Bandbox();
        element.setHflex("1");
        element.setReadonly(true);
        element.setValue(step.getElement());
        Listcell elementCell = new Listcell();
        elementCell.appendChild(element);
        li.appendChild(elementCell);

        Textbox intro = new Textbox(step.getIntro());
        intro.setRows(2);
        intro.setHflex("1");
        Listcell introCell = new Listcell();
        introCell.appendChild(intro);
        li.appendChild(introCell);

        Combobox positionBox = new Combobox();
        positionBox.setHflex("1");
        String[] positionTypeList = new String[]{"top", "top-right-aligned", "left", "left-bottom-aligned", "right",
                "right-bottom-aligned", "bottom", "bottom-left-aligned", "center"};
        String[] positionNameList = new String[]{Labels.getLabel("guide.position.top"),
                Labels.getLabel("guide.position.top.right"), Labels.getLabel("guide.position.left"),
                Labels.getLabel("guide.position.left.bottom"), Labels.getLabel("guide.position.right"),
                Labels.getLabel("guide.position.right.bottom"), Labels.getLabel("guide.position.bottom"),
                Labels.getLabel("guide.position.bottom.left"), Labels.getLabel("guide.position.center")};
        for (int index = 0; index < positionTypeList.length; index++) {
            Comboitem ci = new Comboitem();
            ci.setLabel(positionNameList[index]);
            ci.setValue(positionTypeList[index]);
            ci.setParent(positionBox);
            if (Strings.isBlank(step.getPosition()))
                step.setPosition("right");
            if (step.getPosition().equals(positionTypeList[index]))
                positionBox.setSelectedIndex(index);
        }
        positionBox.addEventListener(Events.ON_CHANGE, this);
        Listcell positionCell = new Listcell();
        positionCell.appendChild(positionBox);
        li.appendChild(positionCell);

        Intbox delayTimeBox = new Intbox(step.getDelayTime());
        delayTimeBox.setHflex("1");
        delayTimeBox.addEventListener(Events.ON_CHANGE, this);
        Listcell delayTimeCell = new Listcell();
        delayTimeCell.appendChild(delayTimeBox);
        li.appendChild(delayTimeCell);

        Checkbox autoSwitchbox = new Checkbox();
        autoSwitchbox.setHflex("1");
        autoSwitchbox.setChecked(step.getAuto());
        autoSwitchbox.addEventListener(Events.ON_CHECK, this);
        Listcell autoCell = new Listcell();
        autoCell.appendChild(autoSwitchbox);
        li.appendChild(autoCell);

        Intbox waitTimeBox = new Intbox(step.getWaitTime());
        waitTimeBox.setHflex("1");
        waitTimeBox.addEventListener(Events.ON_CHANGE, this);
        Listcell waitTimeCell = new Listcell();
        waitTimeCell.appendChild(waitTimeBox);
        li.appendChild(waitTimeCell);

        Button showBtn = new Button();
        showBtn.setHflex("1");
        showBtn.setLabel(Labels.getLabel("menu.view"));
        Listcell showCell = new Listcell();
        showCell.appendChild(showBtn);
        li.appendChild(showCell);

        li.setValue(step);
        new StepCell(element, intro, positionBox, delayTimeBox, autoSwitchbox, waitTimeBox, showBtn);
        if (pos >= 0) {
            fields.getItems().add(pos, li);
        } else
            li.setParent(fields);

        return li;
    }

    private void moveStep(boolean down) {
        Listitem item = fields.getSelectedItem();
        Step step = item.getValue();
        int pos = fields.getSelectedIndex();
        if (down)
            pos++;
        else
            pos--;
        if (pos < 0)
            pos = 0;
        else if (pos >= fields.getItemCount())
            pos = fields.getItemCount() - 1;
        item.detach();
        fields.getItems().add(pos, item);
        selectedVo.getStepList().remove(step);
        selectedVo.getStepList().add(pos, step);
    }

    private List<GuideConfigVo> searchName(String searchStr) {
        if (Strings.isBlank(searchStr)) {
            return allGuideConfigVos;
        } else {
            List<GuideConfigVo> currentGuideConfigVos = new ArrayList<>();
            for (GuideConfigVo vo : allGuideConfigVos) {
                if (vo.getGuideName().contains(searchStr))
                    currentGuideConfigVos.add(vo);
            }
            return currentGuideConfigVos;
        }
    }

    private void showHeaderBtns() {
        if (editStatus == EditStatus.EDIT_CREATE) {
            groupSave.setDisabled(true);
            groupDelete.setDisabled(true);
        } else if (editStatus == EditStatus.EDIT_CREATE_CHANGE || editStatus == EditStatus.EDIT_SELECT_CHANGE) {
            groupSave.setDisabled(false);
            groupDelete.setDisabled(true);
        } else if (editStatus == EditStatus.EDIT_SELECT) {
            groupSave.setDisabled(true);
            groupDelete.setDisabled(false);
        }
    }

    private void edit() {
        if (editStatus == EditStatus.EDIT_SELECT)
            editStatus = EditStatus.EDIT_SELECT_CHANGE;
        else if (editStatus == EditStatus.EDIT_CREATE)
            editStatus = EditStatus.EDIT_CREATE_CHANGE;
        showHeaderBtns();
    }
    private class StepCell implements EventListener<Event> {

        private Bandbox element;
        private Textbox intro;
        private Combobox positionBox;
        private Intbox delayTimeBox;
        private Checkbox autoSwitchbox;
        private Intbox waitTimeBox;
        private Button showBtn;

        public StepCell(Textbox element, Textbox intro, Combobox positionBox, Intbox delayTimeBox,
                        Checkbox autoSwitchbox, Intbox waitTimeBox, Button showBtn) {
            this.element = (Bandbox) element;
            this.element.addEventListener(Events.ON_OK, this);
            this.element.addEventListener(Events.ON_CHANGE, this);
            this.element.addEventListener(Events.ON_OPEN, this);
            this.intro = intro;
            this.intro.addEventListener(Events.ON_CHANGE, this);
            this.positionBox = positionBox;
            this.positionBox.addEventListener(Events.ON_CHANGE, this);
            this.delayTimeBox = delayTimeBox;
            this.delayTimeBox.addEventListener(Events.ON_CHANGE, this);
            this.autoSwitchbox = autoSwitchbox;
            this.autoSwitchbox.addEventListener(Events.ON_CHECK, this);
            this.waitTimeBox = waitTimeBox;
            this.waitTimeBox.addEventListener(Events.ON_CHANGE, this);
            this.showBtn = showBtn;
            this.showBtn.addEventListener(Events.ON_CLICK, this);
        }

        @Override
        public void onEvent(Event event) throws Exception {
            Listitem listitem = (Listitem) event.getTarget().getParent().getParent();
            final Step index = listitem.getValue();
            edit();
            if (event.getTarget().equals(element)) {
                AbstractView.createElementView(new EditorCallback<ElementVo>() {
                    @Override
                    public ElementVo getValue() {
                        return null;
                    }

                    @Override
                    public void setValue(ElementVo value) {
                        element.setValue(value.getElementId());
                        index.setElement(value.getElementId());
                    }

                    @Override
                    public String getTitle() {
                        return Labels.getLabel("guide.step.element");
                    }

                    @Override
                    public Page getPage() {
                        return element.getPage();
                    }

                    @Override
                    public Editor getEditor() {
                        return null;
                    }
                }, configType, element).doOverlapped();
            }
            else if (event.getTarget().equals(intro)) {
                index.setIntro(intro.getValue());
            } else if (event.getTarget().equals(positionBox)) {
                index.setPosition((String) positionBox.getSelectedItem().getValue());
            } else if (event.getTarget().equals(delayTimeBox)) {
                if (delayTimeBox.getValue() == null)
                    delayTimeBox.setValue(0);
                index.setDelayTime(delayTimeBox.getValue());
            } else if (event.getTarget().equals(autoSwitchbox)) {
                index.setAuto(autoSwitchbox.isChecked());
            } else if (event.getTarget().equals(waitTimeBox)) {
                if (waitTimeBox.getValue() == null)
                    waitTimeBox.setValue(0);
                index.setWaitTime(waitTimeBox.getValue());
            } else if (event.getTarget().equals(showBtn)) {
                AbstractView.createActionView(new EditorCallback<List<Action>>() {
                    @Override
                    public List<Action> getValue() {
                        return null;
                    }

                    @Override
                    public void setValue(List<Action> value) {
                        index.setActionList(value);
                    }

                    @Override
                    public String getTitle() {
                        return Labels.getLabel("guide.step.action");
                    }

                    @Override
                    public Page getPage() {
                        return guides.getPage();
                    }

                    @Override
                    public Editor getEditor() {
                        return null;
                    }
                }, index.getActionList(), configType, showBtn).doOverlapped();
            }
        }
    }
}


