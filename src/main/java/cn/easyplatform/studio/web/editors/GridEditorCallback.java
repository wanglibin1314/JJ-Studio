/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Grid;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GridEditorCallback implements EditorCallback<TableInfo> {

	private Grid grid;

	private String title;

	public GridEditorCallback(Grid grid, String title) {
		this.grid = grid;
		this.title = title;
	}

	@Override
	public TableInfo getValue() {
		return (TableInfo) grid.getAttribute("");
	}

	@Override
	public void setValue(TableInfo table) {
		Events.postEvent(new Event(Events.ON_CHANGE, grid, table));
		Object obj = grid.removeAttribute("");
		obj = null;
		obj = table;
		grid.setAttribute("", obj);
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public Page getPage() {
		return grid.getPage();
	}

	@Override
	public Editor getEditor() {
		return null;
	}

}
