package cn.easyplatform.studio.web.editors.page;

import cn.easyplatform.entities.beans.list.AuxHead;
import cn.easyplatform.entities.beans.list.AuxHeader;
import cn.easyplatform.entities.beans.list.Header;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetEntityCmd;
import cn.easyplatform.web.ext.zul.Datalist;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.*;

import java.util.Arrays;
import java.util.List;
/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataListBuilder extends ComponentBuilder<Datalist> {

    @Override
    protected void build(Datalist list) {
        Grid grid = new Grid();
        grid.setSizedByContent(list.isSizedByContent());
        grid.setSpan(list.isSpan());
        grid.setSclass(list.getSclass());
        grid.setHflex("1");
        grid.setVflex("1");
        grid.setVisible(list.isVisible());
        grid.setOddRowSclass(list.getOddRowSclass());
        grid.setStyle(list.getStyle());
        grid.setTooltiptext(list.getTooltiptext());
        grid.setTooltip(list.getTooltip());
        grid.setTop(list.getTop());
        grid.setLeft(list.getLeft());
        grid.setMold(list.getMold());
        if (!Strings.isBlank(list.getEntity())
                && Character.isJavaIdentifierPart(list.getEntity().charAt(0))) {
            ListBean entity = (ListBean) StudioApp.execute(list,
                    new GetEntityCmd(list.getEntity()));
            if (entity != null) {
                grid.setTooltiptext(Labels.getLabel("entity.page.component"));
                if (entity.getAuxHeads() != null) {
                    for (AuxHead head : entity.getAuxHeads()) {
                        Auxhead ah = new Auxhead();
                        for (AuxHeader header : head.getAuxHeaders()) {
                            Auxheader ahd = new Auxheader(header.getTitle());
                            ahd.setStyle(header.getStyle());
                            ahd.setIconSclass(header.getIconSclass());
                            ahd.setImage(header.getImage());
                            ahd.setAlign(header.getAlign());
                            ahd.setHoverImage(header.getHoverimg());
                            ahd.setValign(header.getValign());
                            ahd.setColspan(header.getColspan() <= 0 ? 1
                                    : header.getColspan());
                            ahd.setRowspan(header.getRowspan() <= 0 ? 1
                                    : header.getRowspan());
                            ahd.setParent(ah);
                        }
                        ah.setParent(grid);
                    }
                }
                if (entity.getHeaders() != null) {
                    Columns columns = new Columns();
                    columns.setSizable(list.isSizable());
                    List<String> titleList = null;
                    if (Strings.isBlank(list.getInvisibleColumns()) == false)
                        titleList = Arrays.asList(list.getInvisibleColumns().split(","));
                    for (Header header : entity.getHeaders()) {
                        boolean isContain = titleList != null && titleList.size() > 0 &&
                                titleList.contains(header.getTitle());
                        if (isContain == false) {
                            Column ahd = new Column(header.getTitle());
                            ahd.setStyle(header.getStyle());
                            ahd.setIconSclass(header.getIconSclass());
                            ahd.setImage(header.getImage());
                            ahd.setAlign(header.getAlign());
                            ahd.setHoverImage(header.getHoverimg());
                            ahd.setValign(header.getValign());
                            ahd.setParent(columns);
                        }
                    }
                    columns.setParent(grid);
                }
            } else
                createEmptyGrid(grid, list.isSizable());
        } else
            createEmptyGrid(grid, list.isSizable());
        list.appendChild(grid);
    }

    private void createEmptyGrid(Grid grid, boolean sizable) {
        Columns columns = new Columns();
        columns.setSizable(sizable);
        for (int i = 0; i < 10; i++) {
            Column ahd = new Column("Field" + i);
            ahd.setParent(columns);
        }
        columns.setParent(grid);
    }
}
