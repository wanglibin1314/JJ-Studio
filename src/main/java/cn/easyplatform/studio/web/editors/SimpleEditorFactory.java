/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.LogicBean;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.beans.api.ApiBean;
import cn.easyplatform.entities.beans.batch.BatchBean;
import cn.easyplatform.entities.beans.bpm.BpmBean;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.entities.beans.report.JxlsReport;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.studio.web.editors.admin.LoginLogEditor;
import cn.easyplatform.studio.web.editors.admin.OperationLogEditor;
import cn.easyplatform.studio.web.editors.admin.TemplateCfgEditor;
import cn.easyplatform.studio.web.editors.admin.UserManageEditor;
import cn.easyplatform.studio.web.editors.entity.*;
import cn.easyplatform.studio.web.editors.help.AboutEditor;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;
import cn.easyplatform.studio.web.editors.help.ProductEditor;
import cn.easyplatform.studio.web.editors.help.WidgetIDEditor;
import cn.easyplatform.studio.web.editors.project.*;
import cn.easyplatform.studio.web.editors.version.ExtHistEditor;
import cn.easyplatform.studio.web.editors.version.ImtHistEditor;
import cn.easyplatform.studio.web.editors.version.ManageEditor;
import cn.easyplatform.studio.web.editors.workbench.*;
import cn.easyplatform.studio.web.editors.workbench.RoleEditor;
import cn.easyplatform.studio.web.editors.workbench.UserEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.type.SubType;

import java.util.HashMap;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class SimpleEditorFactory {

    @SuppressWarnings("unchecked")
    public final static <T extends BaseEntity> EntityEditor<T> createEditor(
            WorkbenchController workbench, T entity, char code) {
        if (entity instanceof TaskBean)
            return (EntityEditor<T>) new TaskEntityEditor(workbench,
                    (TaskBean) entity, code);
        if (entity instanceof PageBean)
            return (EntityEditor<T>) new PageEntityEditor(workbench,
                    (PageBean) entity, code);
        if (entity instanceof TableBean)
            return (EntityEditor<T>) new TableEntityEditor(workbench,
                    (TableBean) entity, code);
        if (entity instanceof ListBean)
            return (EntityEditor<T>) new ListEntityEditor(workbench,
                    (ListBean) entity, code);
        if (entity instanceof LogicBean)
            return (EntityEditor<T>) new LogicEntityEditor(workbench,
                    (LogicBean) entity, code);
        if (entity instanceof JasperReportBean)
            return (EntityEditor<T>) new ReportEntityEditor(workbench,
                    (JasperReportBean) entity, code);
        if (entity instanceof JxlsReport)
            return (EntityEditor<T>) new JxlsReportEntityEditor(workbench,
                    (JxlsReport) entity, code);
        if (entity instanceof BpmBean)
            return (EntityEditor<T>) new BpmEntityEditor(workbench,
                    (BpmBean) entity, code);
        if (entity instanceof BatchBean)
            return (EntityEditor<T>) new BatchEntityEditor(workbench,
                    (BatchBean) entity, code);
        if (entity instanceof ApiBean)
            return (EntityEditor<T>) new ApiEntityEditor(workbench,
                    (ApiBean) entity, code);
        if (entity instanceof ResourceBean) {
            ResourceBean rb = (ResourceBean) entity;
            if (SubType.JOB.getName().equals(rb.getSubType())) {
                if (rb.getProps() == null) {
                    rb.setProps(new HashMap<String, String>());
                    rb.getProps().put("expression", "* * * * * ?");
                }
                return (EntityEditor<T>) new JobEntityEditor(workbench, rb,
                        code);
            } else {
                if (rb.getProps() == null) {
                    rb.setProps(new HashMap<String, String>());
                    rb.getProps().put("scope", "application");
                }
                return (EntityEditor<T>) new ResourceEntityEditor(workbench, rb, code);
            }
        }
        return null;
    }

    public final static Editor createEditor(WorkbenchController workbench,
                                            String id) {
        if (id.equals(Editor.DESKTOP_LOGIN_EDITOR_ID))
            return new AppPageEditor(workbench, id, 1);
        if (id.equals(Editor.DESKTOP_MAIN_EDITOR_ID))
            return new AppPageEditor(workbench, id, 2);
        if (id.equals(Editor.MOBILE_LOGIN_EDITOR_ID))
            return new AppPageEditor(workbench, id, 3);
        if (id.equals(Editor.MOBILE_MAIN_EDITOR_ID))
            return new AppPageEditor(workbench, id, 4);
        if (id.equals(Editor.MENU_EDITOR_ID))
            return new MenuEditor(workbench, id);
        if (id.equals(Editor.ACCESS_EDITOR_ID))
            return new AccessEditor(workbench, id);
        if (id.equals(Editor.ROLE_EDITOR_ID))
            return new cn.easyplatform.studio.web.editors.project.RoleEditor(workbench, id);
        if (id.equals(Editor.USER_EDITOR_ID))
            return new cn.easyplatform.studio.web.editors.project.UserEditor(workbench, id);
        if (id.equals(Editor.TABLE_EDITOR_ID))
            return new TableEditor(workbench, id);
        if (id.equals(Editor.FILE_EDITOR_ID))
            return new FileEditor(workbench, id);
        if (id.equals(Editor.PRODUCT_EDITOR_ID))
            return new ProductEditor(workbench, id);
        if (id.equals(Editor.DB_VIEW_EDITOR_ID))
            return new DbViewEditor(workbench, id);
        if (id.equals(Editor.SQL_WIZARD_EDITOR_ID))
            return new SqlWizardEditor(workbench, id);
        if (id.equals(Editor.I18n_EDITOR_ID))
            return new I18nEditor(workbench, id);
        if (id.equals(Editor.HISTORY_EDITOR_ID))
            return new HistoryEditor(workbench, id);
        if (id.equals(Editor.STUDIO_USER_EDITOR_ID))
            return new UserEditor(workbench, id);
        if (id.equals(Editor.STUDIO_ROLE_EDITOR_ID))
            return new RoleEditor(workbench, id);
        if (id.equals(Editor.EXPORT_EDITOR_ID))
            return new ExportEditor(workbench, id);
        if (id.equals(Editor.IMPORT_EDITOR_ID))
            return new ImportEditor(workbench, id);
        if (id.equals(Editor.MANAGE_REPO_ID))
            return new ManageEditor(workbench, id);
        if (id.equals(Editor.EXPORT_HIST_REPO_ID))
            return new ExtHistEditor(workbench, id);
        if (id.equals(Editor.IMPORT_HIST_REPO_ID))
            return new ImtHistEditor(workbench, id);
        if (id.equals(Editor.EXPORT_REPO_ID))
            return new cn.easyplatform.studio.web.editors.version.ExportEditor(workbench, id);
        if (id.equals(Editor.IMPORT_REPO_ID))
            return new cn.easyplatform.studio.web.editors.version.ImportEditor(workbench, id);
        if (id.equals(Editor.ROOTTASK_EXPORT_REPO_ID))
            return new RootTaskExportEditor(workbench, id);
        if (id.equals(Editor.DICT_EXPORT_REPO_ID))
            return new DictExportEditor(workbench, id);
        if (id.equals(Editor.LABEL_EXPORT_REPO_ID))
            return new LabelExportEditor(workbench, id);
        if (id.equals(Editor.INFORM_EXPORT_REPO_ID))
            return new InformExportEditor(workbench, id);
        if (id.equals(Editor.MENU_EXPORT_REPO_ID))
            return new MenuExportEditor(workbench, id);
        if (id.equals(Editor.FILE_EXPORT_REPO_ID))
            return new FileExportEditor(workbench, id);
        if (id.equals(Editor.DICT_IMPORT_REPO_ID))
            return new DictImportEditor(workbench, id);
        if (id.equals(Editor.LABEL_IMPORT_REPO_ID))
            return new LabelImportEditor(workbench, id);
        if (id.equals(Editor.INFORM_IMPORT_REPO_ID))
            return new InformImportEditor(workbench, id);
        if (id.equals(Editor.MENU_IMPORT_REPO_ID))
            return new MenuImportEditor(workbench, id);
        if (id.equals(Editor.FILE_IMPORT_REPO_ID))
            return new FileImportEditor(workbench, id);
        if (id.equals(Editor.ROOTTASK_IMPORT_REPO_ID))
            return new cn.easyplatform.studio.web.editors.version.ImportEditor(workbench, id);
        if (id.equals(Editor.APP_GUIDE_CONFIG_ID))
            return new GuideConfigEditor(workbench, id, 1);
        if (id.equals(Editor.STUDIO_GUIDE_CONFIG_ID))
            return new GuideConfigEditor(workbench, id, 2);
        if (id.equals(Editor.APP_GUIDE_SET_ID))
            return new WidgetIDEditor(workbench, id);
        if (id.equals(Editor.STUDIO_GUIDE_ABOUT_ID))
            return new AboutEditor(workbench, id);
        if (id.equals(Editor.Module_EDITOR_ID))
            return new ModuleEditor(workbench, id);
        if (id.equals(Editor.Login_Log_ID))
            return new LoginLogEditor(workbench, id);
        if (id.equals(Editor.Online_User_Management_ID))
            return new UserManageEditor(workbench, id);
        if (id.equals(Editor.Operation_Log_ID))
            return new OperationLogEditor(workbench, id);
        if (id.equals(Editor.DEFAULT_EDITOR_ID))
            return new DefaultEditor(workbench, id);
        if (id.equals(Editor.Template_Configuration_ID))
            return new TemplateCfgEditor(workbench, id);
        return null;
    }
}
