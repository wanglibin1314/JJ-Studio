/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.support;


import cn.easyplatform.studio.utils.StudioUtil;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class NodeFactory extends nu.xom.NodeFactory {
    public NodeFactory() {
        try {
            m.invoke(cache, "web");
            m.invoke(cache, "zul");
            m.invoke(cache, "native");
            m.invoke(cache, "client");
            m.invoke(cache, "client/attribute");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final Object cache;
    private static final Method m;

    static {
        try {
            Class<?> v = Class.forName("nu.xom.Verifier");
            Field f = FieldUtils.getField(v, "cache", true);
            cache = f.get(null);
            m = StudioUtil.findMethod(cache.getClass(), "put", String.class);
            m.setAccessible(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Element startMakingElement(String name, String namespace) {
        return new Element(name, namespace.intern());
    }
}
