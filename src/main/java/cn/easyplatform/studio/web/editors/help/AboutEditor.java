package cn.easyplatform.studio.web.editors.help;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.Div;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Label;


/**
 * @Author Zeta
 * @Version 1.0
 */
public class AboutEditor extends AbstractPanelEditor {

    public AboutEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    private Scrollview scrollview_updateContent;

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.about"), "~./images/dictionary.png",
                "~./include/help/About.zul");

        scrollview_updateContent = (Scrollview) is.getFellow("about_scrollview_updateContent");

        String jsonPath = StudioApp.getServletContext().getRealPath("/WEB-INF/about/about.json");
        String jsonResult = null;
        try {
            jsonResult = FileUtil.getStreamWithFile(jsonPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONArray jsonObject = JSON.parseArray(jsonResult);
        if(null!=jsonObject&&!"".equals(jsonObject)){
            for (int i = 0; i < jsonObject.size(); i++) {
                String versions = jsonObject.getJSONObject(i).getString("versions");
                String releaseDate = jsonObject.getJSONObject(i).getString("releaseDate");
                String updateContent = jsonObject.getJSONObject(i).getString("updateContent");
                Label versionsLab = new Label();
                versionsLab.setValue(Labels.getLabel("guide.about.versions")+"："+versions);
                versionsLab.setStyle("font-size:14px;color:#333333");
                Label releaseDateLab = new Label();
                releaseDateLab.setValue(Labels.getLabel("guide.about.releaseDate")+"："+releaseDate);
                releaseDateLab.setStyle("font-size:14px;color:#333333");
                Label updateContentLab = new Label();
                updateContentLab.setValue(Labels.getLabel("guide.about.updateContent")+"：");
                updateContentLab.setStyle("font-size:14px;color:#333333");
                Div div = new Div();
                div.setStyle("margin:25px 0px");
                div.appendChild(updateContentLab);
                scrollview_updateContent.appendChild(versionsLab);
                scrollview_updateContent.appendChild(releaseDateLab);
                scrollview_updateContent.appendChild(div);
                JSONArray updateList = JSON.parseArray(updateContent);
                for(int j = 0; j < updateList.size() ;j++){
                    String content = updateList.getString(j);
                    Label contentlab = new Label();
                    contentlab.setValue((j+1)+"、"+content);
                    contentlab.setStyle("font-size:14px;color:#333333");
                    scrollview_updateContent.appendChild(contentlab);
                }
                if(i==0){
                    Label hintLab = new Label();
                    hintLab.setValue(Labels.getLabel("guide.about.hint"));
                    hintLab.setStyle("font-size:14px;color:#333333");
                    Div hintDiv = new Div();
                    hintDiv.setStyle("margin:25px 0px");
                    hintDiv.appendChild(hintLab);
                    scrollview_updateContent.appendChild(hintDiv);
                }

            }
        }else{
            WebUtils.showInfo(Labels.getLabel("guide.about.warning"));
        }

    }

    @Override
    protected void dispatch(Event evt) {

    }
}
