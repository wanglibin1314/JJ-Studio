/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractPanelEditor extends AbstractEditor implements
        Editor, EventListener<Event> {

    private String id;

    protected Tab tab;

    public AbstractPanelEditor(WorkbenchController workbench, String id) {
        super(workbench);
        this.id = id;
    }

    /**
     * @param title
     * @param image
     * @param zul
     * @return
     */
    protected Idspace createPanel(String title, String image, String zul) {
        tab = new Tab(title);
        tab.setValue(this);
        tab.setClosable(true);
        tab.setSelected(true);
        //tab.setImage(image);
        Tabpanel tabpanel = new Tabpanel();
        Idspace is = new Idspace();
        is.setHflex("1");
        is.setVflex("1");
        is.setParent(tabpanel);
        Executions.createComponents
                (zul, is,
                        null);
        workbench.addEditor(tab, tabpanel);
        workbench.setDisabled("save", true);
        if(LoginUserVo.UserType.DEV.getName().equals(Contexts.getUser().getRoleId())){
            workbench.getHeader().setMenuitem(0);
            workbench.getShortcutKeyBar().setMenuitem(0);
        }
        return is;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        dispatch(event);
    }

    @Override
    public void close() {
        tab.close();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setTheme(String theme) {
    }

    /**
     * 分发事件
     *
     * @param evt
     */
    protected abstract void dispatch(Event evt);
}
