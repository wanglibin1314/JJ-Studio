/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.lang.Mirror;
import cn.easyplatform.lang.Strings;
import nu.xom.Element;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SimpleSqlEditorCallback implements EditorCallback<String> {

	private Editor editor;

	private Button btn;

	private String name;

	private boolean redraw;

	private Element each;

	//private Object entity;

	/**
	 * @param editor
	 * @param btn
	 * @param name
	 */
	public SimpleSqlEditorCallback(Editor editor, Button btn,
								   String name, boolean redraw) {
		this.editor = editor;
		this.btn = btn;
		this.name = Strings.upperFirst(name);
		this.redraw = redraw;
	}

	/**
	 * @param editor
	 * @param btn
	 * @param redraw
	 */
	public SimpleSqlEditorCallback(EntityEditor<?> editor, Element each,
								   Button btn, String attribute, boolean redraw) {
		this.editor = editor;

		this.btn = btn;
		this.name = attribute;
		this.redraw = redraw;
		this.each = each;
	}

	@Override
	public String getValue() {
		return each.getAttribute(name).getValue();

	}

	@Override
	public void setValue(String value) {
		if (Strings.isBlank(value)) {
			btn.setSclass("epeditor-btn");
			each.getAttribute(name).setValue("");
		} else {
			btn.setSclass("epeditor-btn-mark");
			each.getAttribute(name).setValue(value);
		}
		((AbstractEntityEditor<?>) editor).setDirty();
		if (redraw)
			((AbstractEntityEditor<?>) editor).redraw();
	}

	@Override
	public String getTitle() {
		Label label = null;
		if (btn.getPreviousSibling() instanceof Bandbox)
			label = (Label) btn.getParent().getParent().getPreviousSibling();
		else
			label = (Label) btn.getPreviousSibling();
		return label.getValue().trim();
	}

	@Override
	public Page getPage() {
		return btn.getPage();
	}
	
	@Override
	public Editor getEditor() {
		return editor;
	}
}
