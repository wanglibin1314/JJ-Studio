package cn.easyplatform.studio.web.editors;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;

public class ComponentCallback implements EditorCallback<String> {

    private Component component;

    private String title;


    public ComponentCallback(Component component, String title) {
        this.component = component;
        this.title = title;
    }
    @Override
    public String getValue() {
        return null;
    }

    @Override
    public void setValue(String value) {

    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Page getPage() {
        return component.getPage();
    }

    @Override
    public Editor getEditor() {
        return null;
    }
}
