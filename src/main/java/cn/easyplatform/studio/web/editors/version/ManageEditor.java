/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetEntryCmd;
import cn.easyplatform.studio.cmd.entity.GetHisEntryCmd;
import cn.easyplatform.studio.cmd.entity.GetMaxVsEntryCmd;
import cn.easyplatform.studio.cmd.entity.SaveCmd;
import cn.easyplatform.studio.cmd.version.GetListCmd;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.RepoVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.EntityType;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ManageEditor extends AbstractPanelEditor {

    private Textbox id;

    private Textbox name;

    private Textbox desp;

    private Textbox content;

    private Combobox type;

    private Textbox user;

    private Datebox beginDate;

    private Datebox endDate;

    private Intbox beginVersion;

    private Intbox endVersion;

    private Listbox detail;

    private Paging paging;

    public ManageEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }
    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.hist"), "~./images/svn/synch.gif", "~./include/editor/version/manage.zul");
        for (Component comp : is.getFellows()) {
            String id = comp.getId();
            if (id.equals("manage_textbox_id")) {
                this.id = (Textbox) comp;
            } else if (id.equals("manage_textbox_name")) {
                this.name = (Textbox) comp;
            } else if (id.equals("manage_textbox_desp")) {
                this.desp = (Textbox) comp;
            } else if (id.equals("manage_textbox_content")) {
                this.content = (Textbox) comp;
            } else if (id.equals("manage_combobox_type")) {
                this.type = (Combobox) comp;
            } else if (id.equals("manage_datebox_beginDate")) {
                this.beginDate = (Datebox) comp;
            } else if (id.equals("manage_datebox_endDate")) {
                this.endDate = (Datebox) comp;
            } else if (id.equals("manage_intbox_beginVersion")) {
                this.beginVersion = (Intbox) comp;
            } else if (id.equals("manage_intbox_endVersion")) {
                this.endVersion = (Intbox) comp;
            } else if (id.equals("manage_textbox_user")) {
                this.user = (Textbox) comp;
            } else if (id.equals("manage_paging_paging")) {
                this.paging = (Paging) comp;
                this.paging.addEventListener(ZulEvents.ON_PAGING, this);
            } else if (id.equals("manage_listbox_detail")) {
                this.detail = (Listbox) comp;
            } else if (comp instanceof Button) {
                comp.addEventListener(Events.ON_CLICK, this);
            }
        }
    }

    private void redraw(Component btn, List<EntityInfo> data) {
        detail.getItems().clear();
        for (EntityInfo e : data) {
            Listitem li = new Listitem();
            li.appendChild(new Listcell(e.getId()));
            li.appendChild(new Listcell(e.getName()));
            li.appendChild(new Listcell(e.getDescription()));
            li.appendChild(new Listcell(e.getType()));
            li.appendChild(new Listcell(e.getUpdateDate().toString()));
            li.appendChild(new Listcell(e.getUpdateUser()));
            li.appendChild(new Listcell(e.getStatus() + ""));
            li.appendChild(new Listcell(e.getVersion() + ""));
            li.addEventListener(Events.ON_DOUBLE_CLICK, this);
            li.setValue(e);
            detail.appendChild(li);
        }
    }

    private RepoVo createQuery() {
        String tp = type.getSelectedIndex() == -1 ? "" : type.getSelectedItem().getValue().toString();
        RepoVo vo = new RepoVo();
        vo.setId(id.getValue());
        vo.setName(name.getValue());
        vo.setDesp(desp.getValue());
        vo.setContent(content.getValue());
        vo.setType(tp);
        vo.setBeginDate(beginDate.getValue());
        vo.setEndDate(endDate.getValue());
        vo.setBeginVersion(beginVersion.getValue());
        vo.setEndVersion(endVersion.getValue());
        vo.setUser(user.getValue());
        return vo;
    }

    @Override
    public void dispatch(Event event) {
        if (event.getName().equals(Events.ON_CLICK)) {
            if(event.getTarget().getId().equals("manage_button_query")){
                cn.easyplatform.studio.dao.Page page = new cn.easyplatform.studio.dao.Page(paging.getPageSize());
                page.setGetTotal(true);
                page.setPageNo(0);
                List<EntityInfo> data = StudioApp.execute(event.getTarget(), new GetListCmd(createQuery(), page));
                paging.setTotalSize(page.getTotalCount());
                paging.setActivePage(0);
                redraw(event.getTarget(), data);
            }else if(event.getTarget().getId().equals("manage_button_getBack")){
                if(null!=detail.getSelectedItem()){
                    final EntityInfo sel = detail.getSelectedItem().getValue();
                    final EntityInfo max = StudioApp.execute( new GetMaxVsEntryCmd(sel.getId()));
                    if((max.getStatus())=='D'){
                        WebUtils.showConfirm(Labels.getLabel("menu.repo.back"), new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (event.getName().equals(Events.ON_OK)) {
                                        EntityInfo his = StudioApp.execute(new GetHisEntryCmd(sel.getVersion()));
                                        his.setStatus('C');
                                        if(StudioApp.execute(new SaveCmd(his))){
                                            WebUtils.showSuccess(Labels.getLabel("menu.repo.back"));
                                        }
                                        workbench.getEntityBar().refreshEntity();
                                }
                            }
                        });
                    }else{
                        WebUtils.showConfirm(Labels.getLabel("menu.replace"), new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (event.getName().equals(Events.ON_OK)) {
                                    EntityInfo his = StudioApp.execute(new GetHisEntryCmd(sel.getVersion()));
                                    his.setStatus('U');
                                    if(StudioApp.execute(new SaveCmd(his))) {
                                        WebUtils.showSuccess(Labels.getLabel("menu.replace"));
                                    }
                                    workbench.getEntityBar().refreshEntity();
                                }
                            }
                        });
                    }
                }
            }
        } else if (event.getName().equals(ZulEvents.ON_PAGING)) {
            PagingEvent pe = (PagingEvent) event;
            cn.easyplatform.studio.dao.Page page = new cn.easyplatform.studio.dao.Page(paging.getPageSize());
            page.setGetTotal(false);
            page.setPageNo(pe.getActivePage());
            List<EntityInfo> data = StudioApp.execute(event.getTarget(), new GetListCmd(createQuery(), page));
            redraw(event.getTarget(), data);
        } else if (event.getName().equals(Events.ON_DOUBLE_CLICK)) {
            final EntityInfo e = detail.getSelectedItem().getValue();
            EntityInfo orig = StudioApp.execute(detail, new GetEntryCmd(e.getId()));
            if (orig != null) {
                AbstractView.createVersionView(new EditorCallback<String>() {
                    @Override
                    public String getValue() {
                        if (e.getType().equals(EntityType.REPORT.getName()))
                            return StudioUtil.convertReportContent(e.getContent());
                        return e.getContent();
                    }

                    @Override
                    public void setValue(String value) {
                    }

                    @Override
                    public String getTitle() {
                        return Labels.getLabel("entity.content.compare", new String[]{e.getId()});
                    }

                    @Override
                    public Page getPage() {
                        return detail.getPage();
                    }

                    @Override
                    public Editor getEditor() {
                        return ManageEditor.this;
                    }
                }, orig.getType().equals(EntityType.REPORT.getName()) ? StudioUtil.convertReportContent(orig.getContent()) :
                        orig.getContent()).doOverlapped();
            }
        }
    }


}
