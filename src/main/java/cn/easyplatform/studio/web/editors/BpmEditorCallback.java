/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.web.ext.bpm.Bpmdesigner;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Page;

import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BpmEditorCallback implements EditorCallback<String> {

	private Editor editor;

	private Bpmdesigner designer;

	private Map<String, String> data;

	/**
	 * @param editor
	 * @param designer
	 * @param data
	 */
	public BpmEditorCallback(Editor editor, Bpmdesigner designer,
			Map<String, String> data) {
		this.editor = editor;
		this.designer = designer;
		this.data = data;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public void setValue(String value) {
		designer.setInputValue(value);
	}

	@Override
	public String getTitle() {
		return Labels.getLabel("editor.bpm." + data.get("type"));
	}

	@Override
	public Page getPage() {
		return designer.getPage();
	}

	@Override
	public Editor getEditor() {
		return editor;
	}

}
