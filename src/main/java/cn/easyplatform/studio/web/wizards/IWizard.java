/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.wizards;

import cn.easyplatform.entities.BaseEntity;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IWizard<T extends BaseEntity> {

	T getEntity();

}
