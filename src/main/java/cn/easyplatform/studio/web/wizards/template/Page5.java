/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.wizards.template;

import cn.easyplatform.entities.beans.list.Header;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.*;
import cn.easyplatform.studio.web.editors.support.CodeFormatter;
import cn.easyplatform.studio.web.editors.support.ZulXsdUtil;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.EntityType;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Page5 extends Page1 {

	private Grid list0;

	private TableInfo selectedTable0;

	private String selectedList0;

	private Checkbox toolbar0;

	@Override
	protected void initComponents(Component root) {
		super.initComponents(root);
		list0 = (Grid) root.getFellow("list0");
		toolbar0 = (Checkbox) root.getFellow("toolbar0");
		//list0.addEventListener(Events.ON_CHANGE, this);
		//root.getFellow("selectList0").addEventListener(Events.ON_CLICK, this);
	}

	@Override
	protected void dispatch(final Event evt) {
		super.dispatch(evt);
		final Component c = evt.getTarget();
		if (c == list0) {
			selectedTable0 = (TableInfo) evt.getData();
			onCreate(list0, selectedTable0);
            System.out.println(selectedTable0);
		} else if (c.getId().equals("selectList0")) {
			AbstractView.createComponentView(new EditorCallback<String>() {
				@Override
				public String getValue() {
					return selectedList0;
				}

				@Override
				public void setValue(String value) {
					selectedList0 = value;
					onSelect(list0, selectedList0);
				}

				@Override
				public String getTitle() {
					return ((Button) c).getLabel();
				}

				@Override
				public Page getPage() {
					return c.getPage();
				}

				@Override
				public Editor getEditor() {
					return null;
				}
			},EntityType.DATALIST.getName(), c).doHighlighted();
		} else if (c.getId().equals("addList0")) {
			AbstractView.createTableView(new EditorCallback<TableInfo>() {
				@Override
				public TableInfo getValue() {
					return selectedTable0;
				}

				@Override
				public void setValue(TableInfo value) {
					selectedTable0 = value;
					onCreate(list0, selectedTable0);
				}

				@Override
				public String getTitle() {
					return Labels.getLabel("wizard.table");
				}

				@Override
				public Page getPage() {
					return list0.getPage();
				}

				@Override
				public Editor getEditor() {
					return null;
				}
			}, true, evt.getTarget()).doOverlapped("right,center");
		}
	}

	@Override
	protected void save() {
		super.save();
		if (selectedTable0 != null) {
			ListBean bean = new ListBean();
			bean.setTable(selectedTable0.getTable());
			bean.setId(fetchId());
			bean.setType(EntityType.DATALIST.getName());
			bean.setName((String) list0.getRoot().getAttribute("name"));
			List<Header> headers = new ArrayList<Header>();
			for (TableField tf : selectedTable0.getFields()) {
				Header header = new Header();
				header.setField(tf.getName());
				header.setName(tf.getName());
				header.setTitle(tf.getDescription());
				header.setType(tf.getType());
				headers.add(header);
			}
			bean.setHeaders(headers);
			StudioUtil.save(bean, 'C');
			this.selectedList0 = bean.getId();
		}
	}

	@Override
	protected void buildList(StringBuilder sb) {
		sb.append("<datalist entity=\"").append(selectedList0).append("\"")
				.append(" id=\"detailList\"");
		sb.append(" hflex=\"1\" vflex=\"1\" pageSize=\"20\"/>");
	}

	@Override
	protected String getContent() {
		StringBuilder sb = new StringBuilder();
		sb.append("<borderlayout>");
		sb.append("<west border=\"none\" vflex=\"1\" size=\"300px\">");
		if (Strings.isBlank(selectedList) == false) {
			sb.append("<datalist entity=\"").append(selectedList).append("\"")
					.append(" id=\"pageList\"");
			sb.append(
					" hflex=\"1\" vflex=\"1\" durable=\"true\" event=\"open('U')\"")
					.append(" showRowNumbers=\"true\" showPanel=\"true\" pageSize=\"20\"")
					.append(" sizedByContent=\"true\" span=\"true\"/>");
		}
		sb.append("</west>");
		sb.append("<center border=\"none\">");

		/*if (Strings.isBlank(selectedList0) == false)
			buildList(sb);*/
		sb.append(super.getContent());
		sb.append("</center>");
		sb.append("</borderlayout>");
		return CodeFormatter.formatXML(ZulXsdUtil.buildDocument(sb.toString()));
	}

	protected void buildListToolbar(StringBuilder sb) {
		if (toolbar0.isChecked()) {
			sb.append("<caption>");
			sb.append("<button label=\"").append(Labels.getLabel("button.add"))
					.append("\" event=\"detailList.create()\"")
					.append(" iconSclass=\"z-icon-plus\"/>");
			sb.append("<button label=\"")
					.append(Labels.getLabel("button.delete"))
					.append("\" event=\"detailList.remove()\"")
					.append(" iconSclass=\"z-icon-minus\"/>");
			sb.append("</caption>");
		}
	}

}
