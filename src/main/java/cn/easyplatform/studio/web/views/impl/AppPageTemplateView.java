package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.utils.AppServiceFileUtils;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.TemplateUtil;
import cn.easyplatform.studio.vos.TemplateVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.image.AImage;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class AppPageTemplateView extends AbstractView<String> {

    private Grid chooseTmpGrid;

    private boolean isDeskTop;

    private String pathName;

    private List<TemplateVo> list;

    private TemplateUtil.TemplateType templateType;


    protected AppPageTemplateView(EditorCallback<String> cb, String pathName, TemplateUtil.TemplateType templateType,
                                  Component component) {
        super(component, cb);
        this.templateType = templateType;
        this.pathName = pathName;
    }

    @Override
    protected void createContent(Component parent) {

        list = new TemplateUtil().getTemplateWithType(templateType);

        if (templateType == TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_LOGIN || templateType == TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_MAIN) {
            isDeskTop = true;
        } else {
            isDeskTop = false;
        }
        Window window = (Window) parent.getParent().getParent();
        window.setTitle(Labels.getLabel("wiard.template"));
        window.setBorder("normal");
        window.setPosition("right,bottom");
        window.setWidth("79.5%");
        window.setHeight("95%");
        window.setMaximizable(true);
        window.setSizable(true);
        window.setClosable(true);
        window.setVisible(true);

        chooseTmpGrid = new Grid();
        chooseTmpGrid.setVflex("1");
        chooseTmpGrid.setHflex("1");
        chooseTmpGrid.setStyle("overflow:auto");
        Columns columns = new Columns();
        columns.appendChild(new Column());
        columns.appendChild(new Column());
        if (isDeskTop == false) {
            columns.appendChild(new Column());
            columns.appendChild(new Column());
        }
        chooseTmpGrid.appendChild(columns);
        chooseTmpGrid.appendChild(new Rows());
        Row row = null;
        int size = list.size();
        if (isDeskTop == false) {
            //mobile
            if (list.size() % 4 != 0)
                size += (4 - list.size() % 4);
            for (int index = 0; index < size; index++) {
                if (index % 4 == 0) {
                    if (row != null)
                        chooseTmpGrid.getRows().appendChild(row);
                    row = new Row();
                    row.setStyle("background:#e8e8e8");
                }
                Image image = new Image();
                if (list.size() - 1 >= index) {
                    TemplateVo vo = list.get(index);
                    image.setWidth("80%");
                    image.setId(vo.getProductIdentifier());
                    image.setStyle("margin:10% 10%");
                    if (vo.getProductPreview() != null && vo.getProductPreview().size() > 0) {
                        try {
                            InputStream inputStream = FileUtil.getJarImageWithFileName((new AppServiceFileUtils().getJarFilePath()
                                    + "/" + vo.getProductIdentifier()), vo.getProductPreview().get(0));
                            if(null!=inputStream){
                                image.setContent(new AImage(vo.getProductPreview().get(0), inputStream));
                            }else{
                                org.zkoss.image.Image nullimage=null;
                                image.setContent(nullimage);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    image.addEventListener(Events.ON_CLICK, this);
                }
                row.appendChild(image);
            }
        } else {
            //desktop
            if (list.size() % 2 == 1)
                size++;
            for (int index = 0; index < size; index++) {
                if (index % 2 == 0) {
                    if (row != null)
                        chooseTmpGrid.getRows().appendChild(row);
                    row = new Row();
                    row.setStyle("background:#e8e8e8");
                }
                Image image = new Image();
                if (list.size() - 1 >= index) {
                    TemplateVo vo = list.get(index);
                    image.setWidth("96%");
                    image.setId(vo.getProductIdentifier());
                    image.setStyle("margin:2%");
                    if (vo.getProductPreview() != null && vo.getProductPreview().size() > 0) {
                        try {
                            InputStream inputStream = FileUtil.getJarImageWithFileName((new AppServiceFileUtils().getJarFilePath()
                                    + "/" + vo.getProductIdentifier()), vo.getProductPreview().get(0));
                            if(null!=inputStream){
                                image.setContent(new AImage(vo.getProductPreview().get(0), inputStream));
                            }else{
                                org.zkoss.image.Image nullimage=null;
                                image.setContent(nullimage);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    image.addEventListener(Events.ON_CLICK, this);
                }
                row.appendChild(image);
            }
        }
        if (row != null) {
            chooseTmpGrid.getRows().appendChild(row);
        }
        chooseTmpGrid.setParent(parent);
    }

    @Override
    public void onEvent(Event evt) throws Exception {
        if (evt.getTarget() instanceof Image) {
            if ("default".equals(pathName)) {
                pathName = "";
            }
            new TemplateUtil().copyTemplate(pathName, evt.getTarget().getId(), templateType);
            String path = null;
            String page = evt.getTarget().getId();
            for (TemplateVo vo : list) {
                if (vo.getProductIdentifier().equals(page)) {
                    path =  new AppServiceFileUtils().getFilePath(pathName,templateType) + "/" + vo.getZULFiles().get(0);
                }
            }
            if (cb != null) {
                cb.setValue(path);
            }
            chooseTmpGrid.getRoot().detach();
            onCloseClick(win);
        }
    }
}
