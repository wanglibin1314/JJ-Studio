package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.SetThemeCmd;
import cn.easyplatform.studio.vos.ThemeVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.theme.Themes;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThemeContentView extends AbstractView<String> {
    private Grid grid;
    /**
     * @param cb
     */
    protected ThemeContentView(EditorCallback<String> cb) {
        super(cb);
    }

    @Override
    protected void createContent(Component parent) {
        grid = new Grid();
        grid.setId("themeContentView_grid_result");
        grid.setHflex("1");
        grid.setVflex("1");
        List<ThemeVo> voList = new ArrayList<>();

        Rows rows = new Rows();
        String theme = Themes.getCurrentTheme();
        for (String themesStr : Themes.getThemes()) {
            if (themesStr.equals("zen") || themesStr.equals("iceblue"))
                continue;
            voList.add(new ThemeVo(themesStr, themesStr, themesStr + ".jpg"));
        }
        for (int i = 0; i < voList.size(); i = i + 2) {
            Row row = new Row();
            row.setHeight("50%");
            int maxJ = i + 1 < voList.size()? 2: 1;
            for (int j = 0; j < maxJ; j++) {
                ThemeVo themeVo = voList.get(i + j);
                Vlayout vlayout = new Vlayout();
                if (theme.equals(themeVo.getThemeId()))
                    vlayout.setStyle("text-align: center;border: 3px solid #ffb529;");
                else
                    vlayout.setStyle("text-align: center;");
                Image themePic = new Image();
                themePic.setHflex("1");
                try {
                    themePic.setContent(new AImage(themeVo.getThemePic(), new FileInputStream(
                            StudioApp.getServletContext().getRealPath("/WEB-INF/themePic/" + themeVo.getThemePic()))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                themePic.setParent(vlayout);
                Label themeNameLabel = new Label();
                themeNameLabel.setValue(themeVo.getThemeName());
                themeNameLabel.setParent(vlayout);
                vlayout.setParent(row);
                vlayout.setAttribute("tag",themeVo);
                vlayout.addEventListener(Events.ON_CLICK, this);
            }
            row.setParent(rows);
        }
        grid.appendChild(rows);
        grid.setParent(parent);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Vlayout vlayout = (Vlayout) event.getTarget();
        ThemeVo theme = (ThemeVo) vlayout.getAttribute("tag");
        if (!theme.getThemeId().equals(Themes.getCurrentTheme())) {
            StudioApp.execute(new SetThemeCmd(theme.getThemeId(), false));
            Themes.setTheme(Executions.getCurrent(), theme.getThemeId());
            Executions.getCurrent().sendRedirect("");
        }
    }
}
