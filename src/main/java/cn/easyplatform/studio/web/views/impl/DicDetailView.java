package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.biz.QueryCmd;
import cn.easyplatform.studio.vos.BizQueryResultVo;
import cn.easyplatform.studio.vos.BizQueryVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.util.List;

public class DicDetailView extends AbstractView<String> {
    private Grid grid;
    private Paging paging;
    private Bandbox searchbox;

    private BizQueryVo vo;

    protected DicDetailView(Component component, EditorCallback<String> cb) {
        super(component, cb);
        vo = new BizQueryVo();
        vo.setSql("SELECT diccode,desc1 FROM sys_dic_info ");
        vo.setSearchField("diccode,desc1");
    }

    @Override
    protected void createHeader(Component parent) {
        // 搜索栏
        North searchbar = new North();
        searchbar.setBorder("none");
        searchbar.setSize("30px");
        searchbar.setHflex("1");
        Div bar = new Div();
        Label label = new Label(Labels.getLabel("menu.code") + ":");
        label.setParent(bar);
        searchbox = new Bandbox();
        searchbox.setPlaceholder(Labels.getLabel("menu.code"));
        searchbox.addEventListener(Events.ON_OK, this);
        searchbox.addEventListener(Events.ON_OPEN, this);
        searchbox.setParent(bar);
        bar.setParent(searchbar);
        searchbar.setParent(parent);
    }

    @Override
    protected void createContent(Component parent) {
        // 内容
        grid = new Grid();
        grid.setHflex("1");
        grid.setVflex("1");
        Columns columns = new Columns();
        columns.setSizable(true);
        Column column = new Column(Labels.getLabel("menu.code"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.desp"));
        column.setHflex("1");
        column.setParent(columns);

        columns.setParent(grid);
        grid.appendChild(new Rows());
        grid.setParent(parent);

        Window window = (Window) parent.getParent().getParent();
    }

    @Override
    protected void createFooter(Component parent) {
        paging = new Paging();
        paging.setPageSize(20);
        paging.setDetailed(true);
        paging.addEventListener(ZulEvents.ON_PAGING, this);
        South south = new South();
        south.setBorder("none");
        south.setHflex("1");
        south.setSize("30px");
        south.appendChild(paging);
        south.setParent(parent);

        BizQueryResultVo result = StudioApp.execute(grid, new QueryCmd(vo,
                paging.getPageSize()));
        paging.setTotalSize(result.getTotalSize());
        redraw(result.getResult().getData());
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getName().equals(ZulEvents.ON_PAGING)) {
            PagingEvent pe = (PagingEvent) event;
            BizQueryResultVo result = StudioApp.execute(grid, new QueryCmd(vo,
                    paging.getPageSize()));
            grid.getRows().getChildren().clear();
            redraw(result.getResult().getData());
        } else if (event.getTarget() == searchbox) {
            String val = null;
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                val = (String) evt.getValue();
                searchbox.setValue(val);
            } else
                val = searchbox.getValue();
            vo.setSearchValue(val);
            BizQueryResultVo result = StudioApp.execute(grid, new QueryCmd(
                    vo, paging.getPageSize()));
            paging.setTotalSize(result.getTotalSize());
            grid.getRows().getChildren().clear();
            redraw(result.getResult().getData());
        } else if (event.getTarget() instanceof Row) {
            Row row = (Row) event.getTarget();
            Object[] vo = row.getValue();
            grid.getRoot().detach();
            onCloseClick(win);
            if (null != cb) {
                cb.setValue((String) vo[0]);
            }
        }
    }

    private void redraw(List<Object[]> data) {
        for (Object[] vo : data) {
            Row row = new Row();
            row.setSclass("epstudio-link");
            row.setValue(vo);
            row.appendChild(new Label((String) vo[0]));
            row.appendChild(new Label((String) vo[1]));
            row.setParent(grid.getRows());
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }
}
