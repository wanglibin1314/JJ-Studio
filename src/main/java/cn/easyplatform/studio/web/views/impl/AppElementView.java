package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.utils.StudioServiceFileUtils;
import cn.easyplatform.studio.vos.ElementVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import com.alibaba.fastjson.JSON;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static cn.easyplatform.studio.utils.FileUtil.mkdirForNoExists;

public class AppElementView extends ElementView implements ElementViewIml {

    /**
     * @param cb
     */
    protected AppElementView(EditorCallback<ElementVo> cb, Component component) {
        super(cb, component);
    }

    @Override
    protected void createContent(Component parent) {
        setUpView(parent);
        listbox.getListhead().getChildren().clear();

        String[] labels = new String[]{Labels.getLabel("navi.entity"), Labels.getLabel("guide.step.element"),
                Labels.getLabel("guide.element.path")};
        String[] widths = new String[]{"100px", "250px", "250px"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(labels[index]);
            listheader.setWidth(widths[index]);
            listheader.setHeight("38px");
            listheader.setParent(listbox.getListhead());
        }
        if (GlobalVariableService.getInstance().getAppElementList() == null ||
                GlobalVariableService.getInstance().getAppElementList().size() == 0)
            getNewElementVos();
        redraw(GlobalVariableService.getInstance().getAppElementList());
        Window window = (Window) parent.getParent().getParent();
        window.setWidth("610px");
        window.setHeight("644px");
    }

    @Override
    public void redraw(List<ElementVo> data) {
        listbox.getItems().clear();
        for (ElementVo vo : data) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getEntityId()));
            row.appendChild(new Listcell(vo.getElementId()));
            row.appendChild(new Listcell(vo.getElementPath()));
            row.appendChild(new Listcell(vo.getAction()));
            row.setParent(listbox);
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    public static void getNewElementVos() {
        mkdirForNoExists(new StudioServiceFileUtils().element_app);
        String elementPath = new StudioServiceFileUtils().element_app + File.separator + "element.json";
        if (new File(elementPath).exists() == true) {
            try {
                String content = FileUtil.getStreamWithFile(elementPath);
                GlobalVariableService.getInstance().setAppElementList(JSON.parseArray(content, ElementVo.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<ElementVo> searchName(String searchStr) {
        if (Strings.isBlank(searchStr)) {
            return GlobalVariableService.getInstance().getAppElementList();
        } else {
            List<ElementVo> currentGuideConfigVos = new ArrayList<>();
            for (ElementVo vo : GlobalVariableService.getInstance().getAppElementList()) {
                if (vo.getElementId().contains(searchStr))
                    currentGuideConfigVos.add(vo);
                else if (vo.getElementPath().contains(searchStr))
                    currentGuideConfigVos.add(vo);
                else if (Strings.isBlank(vo.getEntityId()) == false && vo.getEntityId().contains(searchStr))
                    currentGuideConfigVos.add(vo);
            }
            return currentGuideConfigVos;
        }
    }
}
