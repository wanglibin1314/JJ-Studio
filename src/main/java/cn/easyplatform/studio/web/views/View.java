/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface View {

	void doOverlapped();

	void doHighlighted();

	void doOverlapped(String position);

}
