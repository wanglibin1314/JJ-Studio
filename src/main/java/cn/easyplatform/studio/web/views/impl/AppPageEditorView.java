package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class AppPageEditorView extends AbstractView<String>{
    private CMeditor editor;

    private Button format;

    private Button reset;

    private Button close;

    private String xml;


    protected AppPageEditorView(EditorCallback<String> cb, String xml,Component component) {
        super(component, cb);
        this.xml = xml;
    }

    @Override
    protected void createContent(Component parent) {

        Window window = (Window) parent.getParent().getParent();
        window.setTitle(Labels.getLabel("admin.api.edit"));
        window.setBorder("normal");
        window.setPosition("right,bottom");
        window.setWidth("950px");
        window.setHeight("650px");
        window.setMaximizable(true);
        window.setSizable(true);
        window.setClosable(true);
        window.setMaximizable(true);
        window.setPosition("center,top");

        Borderlayout layout = new Borderlayout();
        Center container = new Center();
        container.setHflex("1");
        container.setVflex("1");
        editor = new CMeditor();
        editor.setValue(xml);
        editor.setHflex("1");
        editor.setVflex("1");
        editor.setLineNumbers(true);
        editor.addEventListener(Events.ON_CHANGING, this);
        editor.setAutoCloseTags(true);
        editor.setFoldGutter(true);
        editor.setParent(container);
        container.setParent(layout);

        South south = new South();
        south.setSize("35px");
        south.setBorder("none");
        Div divs = new Div();
        divs.setZclass("text-center mt-2");
        divs.setParent(south);
        format = new Button(Labels.getLabel("button.beautify"));
        format.setSclass("ml-2");
        format.addEventListener(Events.ON_CLICK,this);
        format.setParent(divs);
        reset = new Button(Labels.getLabel("button.reset"));
        reset.setSclass("ml-2");
        reset.setParent(divs);
        reset.addEventListener(Events.ON_CLICK,this);
        close = new Button(Labels.getLabel("button.close"));
        close.setSclass("ml-2");
        close.addEventListener(Events.ON_CLICK, this);
        close.setParent(divs);
        south.setParent(layout);
        layout.setParent(parent);
    }

    @Override
    public void onEvent(Event evt) throws Exception {
        if (evt.getTarget() == editor){
            if(cb!=null){
                cb.setValue((String)editor.getValue());
            }
        }
        if(evt.getTarget() == format){
            editor.formatAll();
        }
        if(evt.getTarget() == reset){
            editor.setValue(xml);
            if(cb!=null){
                cb.setValue((String)editor.getValue());
            }
        }
        if(evt.getTarget() == close){
            editor.getRoot().detach();
            onCloseClick(win);
        }
    }
}
