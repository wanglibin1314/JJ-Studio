package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.AccessVo;
import cn.easyplatform.studio.vos.RoleAccessVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectedMenuAccessView extends AbstractView<List<RoleAccessVo>> {
    Borderlayout borderLayout;
    private Listbox listbox;

    private List<RoleAccessVo> roleAccessVos;
    private List<BaseEntity> entities;
    private List<AccessVo> accessVos;

    protected SelectedMenuAccessView(Component component, EditorCallback<List<RoleAccessVo>> cb,
                                     List<BaseEntity> entities, List<AccessVo> accessVos) {
        super(component, cb);
        this.accessVos = accessVos;
        if (cb != null) {
            roleAccessVos = cb.getValue();
        }
        this.entities = entities;
    }

    @Override
    protected void createContent(Component parent) {
        borderLayout = new Borderlayout();
        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        center.setParent(borderLayout);

        listbox = new Listbox();
        listbox.setHflex("1");
        listbox.setVflex("1");
        listbox.setOddRowSclass("none");
        listbox.setMold("paging");
        listbox.getPagingChild().setMold("os");
        listbox.setParent(parent);

        Listhead listhead = new Listhead();
        listhead.setSizable(true);
        listhead.setParent(listbox);

        String[] labels = new String[]{"entity.id", "entity.name", "entity.desp", "editor.access.title"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(Labels.getLabel(labels[index]));
            listheader.setHflex("1");
            listheader.setParent(listhead);
        }

        if (entities.size() > 0) {
            for (int index = 0; index < entities.size(); index++) {
                RoleAccessVo currentAccessVo = null;
                for (RoleAccessVo roleAccessVo: roleAccessVos) {
                    if (entities.get(index).getId().equals(roleAccessVo.getTaskId())) {
                        currentAccessVo = roleAccessVo;
                        break;
                    }
                }
                createRow(entities.get(index),currentAccessVo);
            }
        }
        center.appendChild(listbox);

        South south = new South();
        south.setSize("44px");
        south.setHflex("1");
        south.setBorder("none");
        south.setParent(borderLayout);

        Div southDiv = new Div();
        southDiv.setParent(south);

        Button save = new Button(Labels.getLabel("button.save"));
        save.setIconSclass("z-icon-save");
        save.setSclass("pull-right epstudio-top");
        save.setId("save");
        save.addEventListener(Events.ON_CLICK, this);
        save.setParent(southDiv);
        borderLayout.setParent(parent);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if ("save".equals(event.getTarget().getId())) {
            List<RoleAccessVo> roleAccessVoList = new ArrayList<>();
            if (listbox.getItems().size() > 0) {
                for (Listitem li : listbox.getItems()) {
                    Label idLabel = (Label)li.getChildren().get(0).getChildren().get(0);
                    Chosenbox Chosenbox = (Chosenbox)li.getChildren().get(3).getChildren().get(0);

                    String accessString = "";
                    List<String> accessList = new ArrayList<>();
                    if (Chosenbox.getSelectedObjects() != null) {
                        for (Object obj:Chosenbox.getSelectedObjects()){
                            accessList.add(obj+"");
                        }
                    }
                    if (accessList.size() > 0) {
                        accessString = StringUtil.listToString(accessList, ",");
                    }
                    RoleAccessVo roleAccessVo = new RoleAccessVo("", idLabel.getValue(), "", accessString);
                    roleAccessVoList.add(roleAccessVo);
                }
            }
            if (cb != null) {
                cb.setValue(roleAccessVoList);
                borderLayout.getRoot().detach();
                onCloseClick(win);
            }
        }
    }

    private void createRow(BaseEntity vo, RoleAccessVo accessVo) {
        Listitem li = new Listitem();

        Listcell elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getId()));
        li.appendChild(elementCell);

        elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getName()));
        li.appendChild(elementCell);

        elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getDescription()));
        li.appendChild(elementCell);

        Chosenbox chosenbox = new Chosenbox();
        chosenbox.setId(vo.getId());
        chosenbox.setHflex("1");

        List<String> accessList = new ArrayList<>();
        if (vo != null) {
            String accessString = ((TaskBean)vo).getAccess();
            if (Strings.isBlank(accessString) == false) {
                accessList = Arrays.asList(((TaskBean)vo).getAccess().split(","));
                ListModelList<String> model = new ListModelList<>(accessList.size());
                List<String> noContainList = new ArrayList<>();
                for (String field : accessList) {
                    for (AccessVo currentAccessvo : accessVos) {
                        StringBuffer sb = new StringBuffer(currentAccessvo.getCode());
                        if (Strings.isBlank(currentAccessvo.getControlType()) == false)
                            sb.append("_").append(currentAccessvo.getControlType());
                        if (sb.toString().equals(field)) {
                            model.add(field);
                        } else
                            noContainList.add(field);
                    }
                }
                chosenbox.setModel(model);
            }
        }

        if (accessVo != null) {
            String accessString = accessVo.getAccess();
            if (Strings.isBlank(accessString) == false) {
                List<String> selectedAccessList = Arrays.asList(accessString.split(","));
                List<String> noContainList = new ArrayList<>();
                List<String> containList = new ArrayList<>();
                if (selectedAccessList != null) {
                    for (String accessId: selectedAccessList) {
                        if (accessList.contains(accessId)) {
                            containList.add(accessId);
                        } else {
                            noContainList.add(accessId);
                        }
                    }
                    if (noContainList.size() > 0) {
                        WebUtils.showInfo(Labels.getLabel("menu.access.lose", new Object[] {noContainList.get(0)}));
                    }
                    chosenbox.setSelectedObjects(containList);
                }
            }
        }
        elementCell = new Listcell();
        elementCell.appendChild(chosenbox);

        li.appendChild(elementCell);
        li.setParent(listbox);
    }
}
