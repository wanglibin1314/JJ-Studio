package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.vos.CustomWidgetGroupVo;
import cn.easyplatform.studio.vos.CustomWidgetVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.type.EntityType;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.*;
import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class CustomWidgetView extends AbstractView<List<CustomWidgetGroupVo>> {
    BASE64Decoder decoder = new BASE64Decoder();//解码

    private List<CustomWidgetGroupVo> customWidgetVos;

    private Tabbox tabbox;

    protected CustomWidgetView(EditorCallback<List<CustomWidgetGroupVo>> cb) {
        super(cb);
        if (cb != null)
            customWidgetVos = cb.getValue();
    }

    @Override
    protected void createContent(Component parent) {
        tabbox = new Tabbox();
        tabbox.setHflex("1");
        tabbox.setVflex("1");
        //tabbox.setMold("accordion");
        parent.appendChild(tabbox);
        Tabs tabs = new Tabs();
        tabbox.appendChild(tabs);
        Tabpanels tabpanels = new Tabpanels();
        tabbox.appendChild(tabpanels);

        for (CustomWidgetGroupVo customWidgetGroupVo: customWidgetVos) {
            Tab tab = new Tab();
            tab.setHflex("1");
            tab.setLabel(customWidgetGroupVo.getGroupName());
            tabs.appendChild(tab);

            Tabpanel tabpanel = new Tabpanel();
            Vlayout layout = new Vlayout();
            layout.setHflex("1");
            layout.setVflex("1");
            layout.setSpacing("0");
            layout.setStyle("background: #FAFAFA;");

            Scrollview centerScrollview= new Scrollview();
            centerScrollview.setHflex("1");
            centerScrollview.setVflex("1");
            layout.appendChild(centerScrollview);
            if (customWidgetGroupVo.getCustomWidgetVoList() != null && customWidgetGroupVo.getCustomWidgetVoList().size() > 0) {
                //每一行最外层控件
                Div contentDiv = new Div();
                contentDiv.setHflex("1");
                contentDiv.setStyle("display: flex;flex-direction: row;flex-wrap: wrap;min-width:164px;justify-content: center;");
                centerScrollview.appendChild(contentDiv);
                for (int index = 0; index < customWidgetGroupVo.getCustomWidgetVoList().size(); index++) {
                    new CustomWidgetView.TemplateCell(customWidgetGroupVo.getCustomWidgetVoList().get(index), contentDiv);
                }
            }
            tabpanel.appendChild(layout);
            tabpanels.appendChild(tabpanel);
        }

        Window window = (Window) parent.getParent().getParent();
        window.setWidth("600px");
        window.setHeight("500px");
    }

    @Override
    public void onEvent(Event event) throws Exception {

    }

    private class TemplateCell implements EventListener<Event> {

        public TemplateCell(CustomWidgetVo customWidgetVo, Component parentLayout) {
            if (customWidgetVo == null) {
                Div div = new Div();
                div.setHflex("1");
                parentLayout.appendChild(div);
            } else  {
                Popup popup = new Popup();
                popup.setWidth("300px");
                popup.setHeight("80px");

                Vlayout popVlayout = new Vlayout();
                popVlayout.setHflex("1");
                popVlayout.setVflex("1");
                popVlayout.setStyle("overflow-y: auto;");

                Label name = new Label(customWidgetVo.getWidgetName());
                name.setStyle("font-weight:bold;font-size:18px;");
                popVlayout.appendChild(name);

                Label desp = new Label(customWidgetVo.getWidgetDesp());
                desp.setStyle("font-size:14px;white-space:pre-wrap;");
                popVlayout.appendChild(desp);

                Div popupDiv = new Div();
                popupDiv.setHflex("1");
                popVlayout.appendChild(popupDiv);
                Label create = new Label(customWidgetVo.getCreateUser());
                popupDiv.appendChild(create);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Label data = new Label(sdf.format(customWidgetVo.getCreateDate()));
                data.setStyle("font-size:14px;");
                data.setSclass("pull-right");
                popupDiv.appendChild(data);
                popup.appendChild(popVlayout);

                Vlayout contentLayout = new Vlayout();
                contentLayout.setSpacing("0");
                contentLayout.setHeight("200px");
                contentLayout.setWidth("164px");
                contentLayout.setStyle("text-align: center;#195B40 solid 2px;");
                contentLayout.setSclass("ui-div-normal");
                contentLayout.addEventListener(Events.ON_CLICK, this);
                contentLayout.setDraggable(EntityType.TABLE.getName());
                contentLayout.setAttribute("t", customWidgetVo.getContent());
                contentLayout.setAttribute("s", customWidgetVo.getDevice());
                //contentLayout.setId(customWidgetVo.getWidgetName());
                parentLayout.appendChild(contentLayout);

                Div oneDiv = new Div();
                oneDiv.setHeight("15px");
                contentLayout.appendChild(oneDiv);

                Image contentImage = new Image();
                if (Strings.isBlank(customWidgetVo.getWidgetPic()) == false) {
                    try {
                        contentImage.setContent(new AImage("logo.png", decoder.decodeBuffer(customWidgetVo.getWidgetPic())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                contentImage.setHeight("137px");
                contentImage.setWidth("137px");
                contentImage.setTooltip(popup);
                contentLayout.appendChild(contentImage);

                Div secondDiv = new Div();
                secondDiv.setHeight("8px");
                contentLayout.appendChild(secondDiv);

                Label contentLabel = new Label();
                contentLabel.setValue(customWidgetVo.getWidgetName());
                contentLabel.setStyle("font-size:12px;border-radius: 14px;padding: 2px 8px;");
                contentLayout.appendChild(contentLabel);

                Div lastDiv = new Div();
                lastDiv.setHeight("8px");
                contentLayout.appendChild(lastDiv);
            }
        }
        @Override
        public void onEvent(Event event) throws Exception {
        }
    }
}
