package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.vos.AccessVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.*;

import java.util.Arrays;
import java.util.List;

public class MenuAccessView extends AbstractView<List<BaseEntity>> {

    private Listbox listbox;

    private List<BaseEntity> taskBeans;
    private List<AccessVo> accessVos;
    protected MenuAccessView(Component component, EditorCallback<List<BaseEntity>> cb, List<AccessVo> accessVos) {
        super(component, cb);
        taskBeans = cb.getValue();
        this.accessVos = accessVos;
    }

    @Override
    protected void createContent(Component parent) {
        listbox = new Listbox();
        listbox.setHflex("1");
        listbox.setVflex("1");
        listbox.setOddRowSclass("none");
        listbox.setMold("paging");
        listbox.getPagingChild().setMold("os");
        listbox.setParent(parent);

        Listhead listhead = new Listhead();
        listhead.setSizable(true);
        listhead.setParent(listbox);

        String[] labels = new String[]{"entity.id", "entity.name", "entity.desp", "editor.access.title"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(Labels.getLabel(labels[index]));
            listheader.setHflex("1");
            listheader.setParent(listhead);
        }

        if (taskBeans.size() > 0) {
            for (int index = 0; index < taskBeans.size(); index++) {
                createRow(taskBeans.get(index) );
            }
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {

    }

    private void createRow(BaseEntity vo) {
        Listitem li = new Listitem();

        Listcell elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getId()));
        li.appendChild(elementCell);

        elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getName()));
        li.appendChild(elementCell);

        elementCell = new Listcell();
        elementCell.appendChild(new Label(vo.getDescription()));
        li.appendChild(elementCell);

        Div accessDiv = new Div();
        accessDiv.setStyle("display: flex;flex-direction: row;flex-wrap: wrap;min-width:10px;justify-content: center;");
        String accessString = ((TaskBean) vo).getAccess();
        if (Strings.isBlank(accessString) == false) {
            List<String> accessList = Arrays.asList(accessString.split(","));
            if (accessList != null && accessList.size() > 0) {
                for (String accessCode: accessList) {
                    String popContent = null;
                    for (AccessVo accessVo: accessVos) {
                        StringBuffer codeString = new StringBuffer(accessVo.getCode());
                        if (Strings.isBlank(accessVo.getControlType()) == false)
                            codeString.append("_").append(accessVo.getControlType());
                        if (accessCode.equals(codeString.toString())) {
                            popContent = accessVo.getName();
                            break;
                        }
                    }
                    Div labelDiv = new Div();
                    labelDiv.setStyle("padding-left: 8px;padding-right: 8px;");
                    labelDiv.setSclass("ui-div-center-parent");
                    labelDiv.setTooltip(accessCode + ", position=after_start, delay=300");
                    Label showLabel = new Label(accessCode);
                    labelDiv.appendChild(showLabel);
                    accessDiv.appendChild(labelDiv);

                    Popup popup = new Popup();
                    popup.setId(accessCode);
                    Label popupLabel = new Label(popContent);
                    popup.appendChild(popupLabel);
                    accessDiv.appendChild(popup);
                }
            }
        }

        elementCell = new Listcell();
        elementCell.appendChild(accessDiv);

        li.appendChild(elementCell);
        li.setParent(listbox);
    }
}
