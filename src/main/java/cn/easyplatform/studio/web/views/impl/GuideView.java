package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.List;

public class GuideView extends AbstractView<GuideConfigVo> {
    private Listbox listbox;

    private List<GuideConfigVo> voList;
    /**
     * @param cb
     */
    protected GuideView(EditorCallback<GuideConfigVo> cb, List<GuideConfigVo> voList, Component component) {
        super(component, cb);
        this.voList = voList;
    }

    @Override
    protected void createContent(Component parent) {
        listbox = new Listbox();
        listbox.setVflex("1");
        listbox.setHflex("1");
        Listhead listhead = new Listhead();
        listhead.setSizable(true);
        listhead.setParent(listbox);
        String[] labels = new String[]{"entity.name", "group.next", "group.isRoot"};
        String[] widths = new String[]{"120px", "120px", "62px"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(Labels.getLabel(labels[index]));
            listheader.setWidth(widths[index]);
            listheader.setParent(listhead);
        }

        listbox.setParent(parent);
        redraw();

        Window window = (Window) parent.getParent().getParent();
        window.setWidth("314px");
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Listitem) {
            Listitem row = (Listitem) event.getTarget();
            cb.setValue((GuideConfigVo)row.getValue());
            listbox.getRoot().detach();
        }
    }

    private void redraw() {
        listbox.getItems().clear();
        if (voList != null && voList.size() != 0) {
            for (GuideConfigVo configVo : voList) {
                Listitem row = new Listitem();
                row.setValue(configVo);
                row.appendChild(new Listcell(configVo.getGuideName()));
                row.appendChild(new Listcell(configVo.getGuideNextName()));
                Listcell listcell = new Listcell();
                if (Strings.isBlank(configVo.getIsRoot()) == false && configVo.getIsRoot().equals("R")) {
                    listcell.setIconSclass("z-icon-check");
                }
                row.appendChild(listcell);
                row.setParent(listbox);
                row.addEventListener(Events.ON_DOUBLE_CLICK, this);
            }
        }
    }
}
