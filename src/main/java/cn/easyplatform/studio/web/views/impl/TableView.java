/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetEntityCmd;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.BandboxCallback;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.editors.TableInfo;
import cn.easyplatform.type.EntityType;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class TableView extends AbstractView<TableInfo> {

	private Bandbox searchbox;

	private Listbox listbox;

	private TableBean selected;

	private boolean closable;

	/**
	 * @param cb
	 */
	protected TableView(EditorCallback<TableInfo> cb, boolean closable, Component component) {
		super(component, cb);
		this.closable = closable;
	}

	@Override
	protected void createHeader(Component parent) {
		Window window = (Window) parent.getParent();
		window.setId("tableView_window_main");
		// 搜索栏
		North searchbar = new North();
		searchbar.setBorder("none");
		searchbar.setSize("28px");
		searchbar.setHflex("1");
		Div bar = new Div();
		bar.setHflex("1");
		Label label = new Label(Labels.getLabel("entity.search.key") + ":");
		label.setParent(bar);
		searchbox = new Bandbox();
		searchbox.setId("tableView_bandbox_searchbox");
		searchbox.setPlaceholder(Labels.getLabel("wizard.table"));
		searchbox.addEventListener(Events.ON_OK, this);
		searchbox.addEventListener(Events.ON_CHANGE, this);
		searchbox.addEventListener(Events.ON_OPEN, this);
		searchbox.setParent(bar);
		Button btn = new Button(Labels.getLabel("button.ok"));
		btn.setSclass("pull-right");
		btn.setId("tableView_button_searchbtn");
		btn.setIconSclass("z-icon-check");
		btn.addEventListener(Events.ON_CLICK, this);
		btn.setParent(bar);
		bar.setParent(searchbar);
		searchbar.setParent(parent);
	}

	@Override
	protected void createContent(Component parent) {
		listbox = new Listbox();
		listbox.setId("tableView_listbox_resultlist");
		listbox.setHflex("1");
		listbox.setVflex("1");
		listbox.setSizedByContent(true);
		listbox.setSpan(true);
		listbox.setMultiple(true);
		listbox.setCheckmark(true);
		Listhead columns = new Listhead();
		columns.setSizable(true);
		Listheader header = new Listheader(Labels.getLabel("entity.serial"));
		header.setId("tableView_listheader_serial");
		header.setWidth("50px");
		header.setParent(columns);
		header = new Listheader(Labels.getLabel("entity.table.field"));
		header.setId("tableView_listheader_field");
		header.setHflex("2");
		header.setParent(columns);
		header = new Listheader(Labels.getLabel("entity.name"));
		header.setId("tableView_listheader_name");
		header.setHflex("4");
		header.setParent(columns);
		columns.setParent(listbox);
		TableInfo table = cb.getValue();
		if (table != null){
			searchbox.setValue(table.getTable());
			redraw(table.getTable(), table);
		}
		listbox.setParent(parent);
		((Window) parent.getRoot()).setWidth("350px");
	}

	private void redraw(String id, TableInfo table) {
		selected = (TableBean) StudioApp.execute(null, new GetEntityCmd(id));
		listbox.getItems().clear();
		if (selected != null && selected.getFields() != null) {
			for (int i = 0; i < selected.getFields().size(); i++) {
				TableField tf = selected.getFields().get(i);
				Listitem li = new Listitem();
				li.setValue(tf);
				li.appendChild(new Listcell((i + 1) + ""));
				li.appendChild(new Listcell(tf.getName()));
				li.appendChild(new Listcell(tf.getDescription()));
				if (table != null
						&& table.getTable().equalsIgnoreCase(selected.getId())) {
					for (TableField f : table.getFields()) {
						if (tf.getName().equalsIgnoreCase(f.getName())) {
							li.setSelected(true);
							break;
						}
					}
				}
				listbox.appendChild(li);
			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getTarget() == searchbox) {
			if (event.getName().equals(Events.ON_OPEN)) {
				searchbox.setValue(((OpenEvent)event).getValue()+"");
				AbstractView.createEntityView(
						new BandboxCallback(searchbox,
								Labels.getLabel("wizard.table")),
						EntityType.TABLE.getName(), searchbox).doOverlapped();
			} else {
				if (Strings.isBlank(searchbox.getValue()))
					return;
				redraw(searchbox.getValue(), cb.getValue());
			}
		} else {
			if (listbox.getSelectedIndex() < 0) {
				WebUtils.showError(Labels.getLabel("editor.select",
						new Object[] { Labels.getLabel("editor.bpm.record") }));
			} else {
				List<TableField> result = new ArrayList<TableField>();
				for (Listitem li : listbox.getSelectedItems()) {
					TableField tf = li.getValue();
					result.add(tf);
				}
				TableInfo table = new TableInfo(selected.getId(), result);
				if (cb != null)
					cb.setValue(table);
				if (closable) {
					listbox.getRoot().detach();
					onCloseClick(win);
				}
			}
		}
	}
}
