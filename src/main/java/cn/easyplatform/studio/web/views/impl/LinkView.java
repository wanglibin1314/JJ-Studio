package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.RoleVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Cardlayout;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class LinkView extends AbstractView<List<RoleVo>> {
    private Cardlayout cardlayout;
    private Button nextBtn;
    private Button closeBtn;
    private Button backBtn;
    private Button finishBtn;
    private Chosenbox chosenbox;
    private Textbox textbox;

    private List<RoleVo> roleVos;
    private List<String> accessList;
    private String projectID;
    private String userID;
    protected LinkView(Component component, EditorCallback<List<RoleVo>> cb, String projectID, String userID) {
        super(component, cb);
        this.projectID = projectID;
        this.userID = userID;
        if (cb != null) {
            roleVos = cb.getValue();
        }
    }

    @Override
    protected void createContent(Component parent) {

        cardlayout = new Cardlayout();
        cardlayout.setHflex("1");
        cardlayout.setVflex("1");
        parent.appendChild(cardlayout);

        //第一个页面
        Vlayout layout = new Vlayout();
        layout.setHflex("1");
        layout.setVflex("1");
        layout.setSpacing("0");
        layout.setStyle("background: #FAFAFA;");

        Div titleDiv = new Div();
        titleDiv.setHflex("1");
        titleDiv.setHeight("28px");
        titleDiv.setStyle("padding-top: 90px;padding-left: 60px;padding-right: 60px;text-align: center;");
        layout.appendChild(titleDiv);

        Label title = new Label(Labels.getLabel("product.choose.role.title"));
        title.setStyle("font-size:20px;");
        titleDiv.appendChild(title);

        Div contentDiv = new Div();
        contentDiv.setHflex("1");
        contentDiv.setVflex("1");
        contentDiv.setStyle("padding-top: 78px;padding-left: 60px;padding-right: 60px;");
        layout.appendChild(contentDiv);

        chosenbox = new Chosenbox();
        chosenbox.setHflex("1");
        chosenbox.setStyle("white-space:normal");
        contentDiv.appendChild(chosenbox);
        ListModelList<String> model = new ListModelList<>();
        for (RoleVo field : roleVos) {
            model.add(field.getId());
        }
        chosenbox.setModel(model);
        chosenbox.addEventListener(Events.ON_SELECT,this);

        Div buttonDiv = new Div();
        buttonDiv.setHflex("1");
        buttonDiv.setHeight("50px");
        buttonDiv.setStyle("background: white;");
        layout.appendChild(buttonDiv);

        closeBtn = new Button(Labels.getLabel("button.close"));
        closeBtn.setStyle("left: 20px;position: absolute;margin-top: 13px;");
        closeBtn.addEventListener(Events.ON_CLICK, this);
        buttonDiv.appendChild(closeBtn);

        nextBtn = new Button(Labels.getLabel("button.next"));
        nextBtn.setStyle("right: 20px;position: absolute;margin-top: 13px;");
        nextBtn.addEventListener(Events.ON_CLICK, this);
        buttonDiv.appendChild(nextBtn);

        layout.setParent(cardlayout);

        //第二个页面

        Vlayout secondLayout = new Vlayout();
        secondLayout.setHflex("1");
        secondLayout.setVflex("1");
        secondLayout.setSpacing("0");
        secondLayout.setStyle("background: #FAFAFA;");

        Div secondTitleDiv = new Div();
        secondTitleDiv.setHflex("1");
        secondTitleDiv.setHeight("28px");
        secondTitleDiv.setStyle("padding-top: 90px;padding-left: 60px;padding-right: 60px;text-align: center;");
        secondLayout.appendChild(secondTitleDiv);

        Label secondTitle = new Label(Labels.getLabel("product.send.link.title"));
        secondTitle.setStyle("font-size:20px;");
        secondTitleDiv.appendChild(secondTitle);

        Div secondContentDiv = new Div();
        secondContentDiv.setHflex("1");
        secondContentDiv.setHeight("24px");
        secondContentDiv.setStyle("padding-top: 45px;padding-left: 65px;text-align: left;");
        secondLayout.appendChild(secondContentDiv);

        Label smallTitle = new Label(Labels.getLabel("product.send.link.placeholder"));
        smallTitle.setStyle("font-size:14px;color:rgba(144,147,153,1);");
        secondContentDiv.appendChild(smallTitle);

        Div thirdContentDiv = new Div();
        thirdContentDiv.setHflex("1");
        thirdContentDiv.setVflex("1");
        thirdContentDiv.setStyle("padding-top: 28px;padding-left: 65px;padding-right: 65px;");
        secondLayout.appendChild(thirdContentDiv);

        textbox = new Textbox();
        textbox.setReadonly(true);
        textbox.setHflex("1");
        textbox.setHeight("30px");
        textbox.addEventListener(Events.ON_DOUBLE_CLICK, this);
        thirdContentDiv.appendChild(textbox);

        Div secondButtonDiv = new Div();
        secondButtonDiv.setHflex("1");
        secondButtonDiv.setHeight("50px");
        secondButtonDiv.setStyle("background: white;");
        secondLayout.appendChild(secondButtonDiv);

        backBtn = new Button(Labels.getLabel("button.back"));
        backBtn.setStyle("left: 20px;position: absolute;margin-top: 13px;");
        backBtn.addEventListener(Events.ON_CLICK, this);
        secondButtonDiv.appendChild(backBtn);

        finishBtn = new Button(Labels.getLabel("button.ok"));
        finishBtn.setStyle("right: 20px;position: absolute;margin-top: 13px;");
        finishBtn.addEventListener(Events.ON_CLICK, this);
        secondButtonDiv.appendChild(finishBtn);

        secondLayout.setParent(cardlayout);

        Window window = (Window) parent.getParent().getParent();
        window.setWidth("580px");
        window.setHeight("414px");
    }
    @Override
    public void onEvent(Event event) throws Exception {
        if (nextBtn.equals(event.getTarget())) {
            accessList = new ArrayList<>();
            if (chosenbox.getSelectedObjects() != null) {
                for (Object obj:chosenbox.getSelectedObjects()){
                    accessList.add(obj+"");
                }
            }
            if (accessList.size() > 0) {
                String roleId = StringUtil.listToString(accessList, ",");
                cardlayout.next();
                HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
                String url = request.getHeader("referer");
                url = url+ "s/m?u="+userID+"&p="+projectID+"&r="+roleId+"&t="+ StudioUtil.getLinkToken(userID, projectID, roleId);
                System.out.println(url);
                textbox.setValue(url);
            } else
                WebUtils.showInfo(Labels.getLabel("product.choose.role"));
        } else if (backBtn.equals(event.getTarget())) {
            cardlayout.previous();
        } else if (closeBtn.equals(event.getTarget())) {
            cardlayout.getRoot().detach();
            onCloseClick(win);
        } else if (finishBtn.equals(event.getTarget())) {
            //Clients.evalJavaScript("copyUrl(" + textbox.getUuid() + ")");
            cardlayout.getRoot().detach();
            onCloseClick(win);
        } else if (textbox.equals(event.getTarget())) {
            Clients.evalJavaScript("copyUrl(" + textbox.getUuid() + ")");
        }
    }
}
