/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.type.EntityType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Row;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class SelectEchartView extends AbstractView<String> {

    private String scope;

    private Grid grid;

    /**
     * @param cb
     */
    protected SelectEchartView(EditorCallback<String> cb,String scope) {
        super(cb);
        this.scope = scope;
    }

    @Override
    protected void createContent(final Component parent) {
        Component c = Executions.createComponents(
                "~./include/views/selectEcharts.zul", parent, null);
        grid = (Grid) c.query("grid");
        for (Component comp : c.getFellows()) {
            if (comp instanceof Image){
                comp.addEventListener(Events.ON_CLICK,this);
            }
        }
        redraw("default");//初始化图片
    }

    private void redraw(String type){
        grid.getRows().getChildren().clear();
        Row row = new Row();
        Image image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/line-stack_"+type+".png");
        image.setAttribute("name","line-stack_"+type);
        image.setParent(row);
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/pie-nest_"+type+".png");
        image.setAttribute("name","pie-nest_"+type);
        image.setParent(row);
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/gauge_"+type+".png");
        image.setAttribute("name","gauge_"+type);
        image.setParent(row);
        row.setParent(grid.getRows());
        row = new Row();
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/bar1_"+type+".png");
        image.setAttribute("name","bar1_"+type);
        image.setParent(row);
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/bar-stack_"+type+".png");
        image.setAttribute("name","bar-stack_"+type);
        image.setParent(row);
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/funnel_"+type+".png");
        image.setAttribute("name","funnel_"+type);
        image.setParent(row);
        row.setParent(grid.getRows());
        row = new Row();
        image = new Image();
        if ("drag".equals(scope))
            image.setDraggable(EntityType.TABLE.getName());
        else if ("click".equals(scope))
            image.addEventListener(Events.ON_CLICK,this);
        image.setHflex("1");
        image.setSrc("~./images/echart/bar-waterfall2_"+type+".png");
        image.setAttribute("name","bar-waterfall2_"+type);
        image.setParent(row);
        row.setParent(grid.getRows());
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget().getId().equals("default_img")){
            redraw("default");
        }else if (event.getTarget().getId().equals("light_img")){
            redraw("light");
        }else if (event.getTarget().getId().equals("dark_img")){
            redraw("dark");
        }else if (event.getTarget().getId().equals("vintage_img")){
            redraw("vintage");
        }else if (event.getTarget().getId().equals("infographic_img")){
            redraw("infographic");
        }else if (event.getTarget().getId().equals("macarons_img")){
            redraw("macarons");
        }else if (event.getTarget().getId().equals("roma_img")){
            redraw("roma");
        }else if (event.getTarget().getId().equals("shine_img")){
            redraw("shine");
        }
        if ("click".equals(scope)){
            if (event.getTarget().getAttribute("name")!=null){
                cb.setValue(event.getTarget().getAttribute("name")+"");
                event.getTarget().getRoot().detach();
            }
        }
    }

}
