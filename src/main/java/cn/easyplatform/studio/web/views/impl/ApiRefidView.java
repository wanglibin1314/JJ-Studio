/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.vos.QueryResultVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ApiRefidView extends AbstractView<String> {

    private Grid grid;

    private Bandbox bandbox;

    /**
     * @param cb
     */
    protected ApiRefidView(EditorCallback<String> cb, Component component) {
        super(component, cb);
    }

    @Override
    protected void createHeader(Component parent) {
        North north = new North();
        north.setHflex("1");
        north.setBorder("none");
        north.setParent(parent);
        bandbox = new Bandbox();
        bandbox.setId("apiRefidView_bandbox_search");
        bandbox.setHflex("1");
        bandbox.setPlaceholder(Labels.getLabel("navi.search.placeholder"));
        bandbox.setParent(north);
        bandbox.addEventListener(Events.ON_OK, this);
        bandbox.addEventListener(Events.ON_OPEN, this);
    }

    @Override
    protected void createContent(Component parent) {
        // 内容
        grid = new Grid();
        grid.setId("apiRefidView_grid_result");
        grid.setHflex("1");
        grid.setVflex("1");
        Columns columns = new Columns();
        columns.setSizable(true);
        Column column = new Column(Labels.getLabel("entity.id"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.name"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.type"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.desp"));
        column.setHflex("1");
        column.setParent(columns);

        columns.setParent(grid);
        grid.appendChild(new Rows());
        grid.setParent(parent);
        QueryResultVo result = StudioApp.execute(grid, new QueryCmd("Logic,Task,Datalist,Report", 0));
        List<EntityVo> data = result.getEntities();
        if (data != null)
            redraw(data);
    }

    private void redraw(List<EntityVo> data) {
        for (EntityVo vo : data) {
            Row row = new Row();
            row.setSclass("epstudio-link");
            row.setValue(vo);
            row.appendChild(new Label(vo.getId()));
            row.appendChild(new Label(vo.getName()));
            row.appendChild(new Label(vo.getType()));
            row.appendChild(new Label(vo.getDesp()));
            row.setParent(grid.getRows());
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Bandbox) {
            String val = null;
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                val = (String) evt.getValue();
                bandbox.setValue(val);
            } else
                val = bandbox.getValue();
            QueryResultVo result = StudioApp.execute(bandbox, new QueryCmd("Logic,Task,Datalist,Report", "*", val.trim(), 0));
            List<EntityVo> data = result.getEntities();
            Components.removeAllChildren(grid.getRows());
            if (data != null){
                redraw(data);
            }
        }
        if (event.getTarget() instanceof Row) {
            Row row = (Row) event.getTarget();
            EntityVo vo = row.getValue();
            cb.setValue(vo.getId());
            grid.getRoot().detach();
            onCloseClick(win);
        }
    }
}
