/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.web.layout.FunctionController;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.NavbarController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.layout.impl.*;
import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.util.SimpleResolver;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zk.ui.util.ComposerExt;
import org.zkoss.zul.*;



import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MainLayoutController implements Composer<Component>,
        ComposerExt<Component> {
    private HeaderController headerController;
    private Include funcInclude;
    private Include entityInclude;
    private NavbarController entityController;
    private NavbarController entityWindowController;
    private FunctionController functionBar;

    private EntityType entityType;
    private enum EntityType {
        EntityTypeNormal, EntityTypeWindow, EntityTypeNone
    }
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
                                         ComponentInfo compInfo) throws Exception {
        Map<String, Object> variables = new HashMap<String, Object>();
        VariableResolver variableResolver = new SimpleResolver(variables);
        headerController = new HeaderBarControllerImpl();
        HeaderController shortcutKeyBar = new ShortcutKeyControllerImpl();
        entityController = new EntityBarControllerImpl(
                EntityBarControllerImpl.FunctionType.FunctionNone, false);
        entityWindowController = new EntityBarControllerImpl(
                EntityBarControllerImpl.FunctionType.FunctionNone, true);
        functionBar = new FunctionControllerImpl();
        variables.put("$headerBar", headerController);
        variables.put("$toolbar", functionBar);
        variables.put("$entityBar", entityController);
        variables.put("$entityBarWindow", entityWindowController);
        variables.put("$shortcutKeyBar", shortcutKeyBar);
        final WorkbenchController wc = new WorkbenchControllerImpl(
                headerController, entityController, entityWindowController, page,
                shortcutKeyBar, this, functionBar);
        variables.put("$workbench", wc);
        LoginUserVo user = Contexts.getUser();
        variables.put("$user", user);
        variables.put("$project", Contexts.getProject());
        page.addVariableResolver(variableResolver);
        page.setTitle(StudioApp.getAppName() + "-"
                + Contexts.getProject().getName());
        parent.addEventListener(Events.ON_CTRL_KEY,
                new EventListener<KeyEvent>() {
                    @Override
                    public void onEvent(KeyEvent evt) throws Exception {
                        wc.onCtrlKeys(evt.getKeyCode());
                    }

                });
        //Clients.confirmClose(Labels.getLabel("app.close.error"));

        return compInfo;
    }
    @Override
    public void doBeforeComposeChildren(Component comp) throws Exception {
    }

    @Override
    public boolean doCatch(Throwable ex) throws Exception {
        return false;
    }

    @Override
    public void doFinally() throws Exception {

    }

    @Override
    public void doAfterCompose(final Component comp) throws Exception {
        entityType = EntityType.EntityTypeNone;
        ((EntityBarControllerImpl) entityController).showFunctionType(EntityBarControllerImpl.FunctionType.FunctionNone);
        funcInclude = (Include) comp.getFellow("normal_main_include_func");
        entityInclude = (Include) comp.getFellow("normal_main_include_entity");
        entityInclude.setVisible(false);
        Clients.evalJavaScript("move()");
        //Clients.evalJavaScript("unloadChange(" + Boolean.TRUE + ")");
        ((Tabbox)comp.getFellow("normal_main_tabbox")).getTabpanels().addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (entityType == EntityType.EntityTypeWindow) {
                    entityInclude.setVisible(false);
                    ((EntityBarControllerImpl) entityController).changePegDiv();
                    entityType = EntityType.EntityTypeNone;
                }
            }
        });
        ((HeaderBarControllerImpl) headerController).clickListener = new HeaderBarControllerImpl.OnClickListener() {
            @Override
            public void onShowFunctionBar() {
                showFunc(true);
                Clients.evalJavaScript("funBarShow()");
            }

            @Override
            public void onSearchEntityBar(String searchStr) {
                if (entityType == EntityType.EntityTypeNone) {
                    entityInclude.setVisible(true);
                    entityType = EntityType.EntityTypeNormal;
                    entityInclude.setStyle("");
                    Clients.resize(comp);
                }
                ((EntityBarControllerImpl) entityController).addFunctionEntity(
                        EntityBarControllerImpl.FunctionType.FunctionEntity);
                if (Strings.isBlank(searchStr) == false)
                    ((EntityBarControllerImpl) entityController).setSearchStr(searchStr);
            }
        };
        ((EntityBarControllerImpl) entityController).clickListener = new EntityBarControllerImpl.OnClickListener() {
            @Override
            public void onShowEntityBar() {
                if (entityType == EntityType.EntityTypeNormal) {
                    entityType = EntityType.EntityTypeWindow;
                    entityInclude.setStyle("position: absolute;z-index: 1802;top: 0px;right: 0px;bottom: 0px;left: 0px");

                } else {
                    entityInclude.setStyle("");
                    entityType = EntityType.EntityTypeNormal;
                }
                Clients.resize(comp);
            }

            @Override
            public void onVisible() {
                entityInclude.setVisible(false);
                ((EntityBarControllerImpl) entityController).changePegDiv();
            }

            @Override
            public void onShowNewEntity() {
                if (entityType == EntityType.EntityTypeWindow) {
                    entityInclude.setVisible(false);
                    ((EntityBarControllerImpl) entityController).changePegDiv();
                    entityType = EntityType.EntityTypeNone;
                }
            }
        };
        ((FunctionControllerImpl) functionBar).clickListener = new FunctionControllerImpl.OnClickListener() {
            @Override
            public void onShowEntity() {
                if (entityType == EntityType.EntityTypeNone) {
                    entityInclude.setVisible(true);
                    entityType = EntityType.EntityTypeNormal;
                    entityInclude.setStyle("");
                    Clients.resize(comp);
                }
                ((EntityBarControllerImpl) entityController).addFunctionEntity(
                        EntityBarControllerImpl.FunctionType.FunctionModule);
            }
        };
    }

    //ui
    public void showFunc(Boolean isVisible) {
        funcInclude.setVisible(isVisible);
    }
}
