/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.LogoutCmd;
import cn.easyplatform.studio.cmd.identity.SelectProjectCmd;
import cn.easyplatform.studio.web.controller.admin.LoginLogListener;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectProjectController extends SelectorComposer<Window> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Wire("listbox")
	private Listbox listbox;

	@Listen("onClick=#ok;onOK=window")
	public void submit() {
		if (listbox.getSelectedItem() == null)
			listbox.setSelectedIndex(0);
		ProjectBean pb = listbox.getSelectedItem().getValue();
		StudioApp.execute(listbox, new SelectProjectCmd(pb));
		Executions.sendRedirect("");
	}

	@Listen("onClick=#cancel;onCancel=window")
	public void cancel() {
		LoginLogListener.loginOutLog();//记录用户退出
		StudioApp.execute(new LogoutCmd(false));
		this.getSelf().detach();
	}
}
