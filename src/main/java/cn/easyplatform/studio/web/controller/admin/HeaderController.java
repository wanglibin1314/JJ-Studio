/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller.admin;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class HeaderController extends SelectorComposer<Component> implements EventListener<Event> {

    private Component comp;

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.comp=comp;
        initView();
        initEvent();
    }

    private void initView() {

    }

    public void initEvent(){

    }
    @Override
    public void onEvent(Event event) throws Exception {
        String id = event.getTarget().getId();

        switch (id){
            case "":

        }
    }
}
