/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller.admin;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.web.layout.FunctionController;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.NavbarController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.layout.impl.*;
import org.zkoss.util.resource.Labels;
import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.util.SimpleResolver;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zk.ui.util.ComposerExt;

import java.util.HashMap;
import java.util.Map;

/**
 * @since 2.0.0 <br/>
 */
public class AdminMainController implements Composer<Component>,
        ComposerExt<Component> {
    private HeaderController headerController;
    private NavbarController entityController;
    private NavbarController entityWindowController;
    private FunctionController functionBar;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
                                         ComponentInfo compInfo) throws Exception {
        Map<String, Object> variables = new HashMap<String, Object>();
        VariableResolver variableResolver = new SimpleResolver(variables);
        headerController = new HeaderBarControllerImpl();
        HeaderController shortcutKeyBar = new ShortcutKeyControllerImpl();
        entityController = new EntityBarControllerImpl(
                EntityBarControllerImpl.FunctionType.FunctionNone, false);
        entityWindowController = new EntityBarControllerImpl(
                EntityBarControllerImpl.FunctionType.FunctionNone, true);
        functionBar = new FunctionControllerImpl();
        variables.put("$headerBar", headerController);
        variables.put("$toolbar", functionBar);
        variables.put("$entityBar", entityController);
        variables.put("$entityBarWindow", entityWindowController);
        variables.put("$shortcutKeyBar", shortcutKeyBar);
        final WorkbenchController wc = new WorkbenchControllerImpl(
                headerController, entityController, entityWindowController, page,
                shortcutKeyBar, this, functionBar);
        variables.put("$workbench", wc);
        LoginUserVo user = Contexts.getUser();
        variables.put("$user", user);
        variables.put("$project", Contexts.getProject());
        page.addVariableResolver(variableResolver);
        page.setTitle(StudioApp.getAppName() + "-"
                + Contexts.getProject().getName());
        parent.addEventListener(Events.ON_CTRL_KEY,
                new EventListener<KeyEvent>() {
                    @Override
                    public void onEvent(KeyEvent evt) throws Exception {
                        wc.onCtrlKeys(evt.getKeyCode());
                    }

                });
        //Clients.confirmClose(Labels.getLabel("app.close.error"));
        return compInfo;
    }
    @Override
    public void doBeforeComposeChildren(Component comp) throws Exception {
    }

    @Override
    public boolean doCatch(Throwable ex) throws Exception {
        return false;
    }

    @Override
    public void doFinally() throws Exception {

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
//        showNavDiv = (Div) comp.getFellow("normal_main_div_navDraggable");
//
//        funcInclude = (Include) comp.getFellow("normal_main_include_func");
//        showFuncDiv = (Div) comp.getFellow("normal_main_div_funcDraggable");
        Clients.evalJavaScript("move()");
    }
}
