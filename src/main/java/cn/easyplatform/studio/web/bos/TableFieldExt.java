package cn.easyplatform.studio.web.bos;

import cn.easyplatform.entities.beans.table.TableField;
import org.zkoss.bind.annotation.NotifyChange;

public class TableFieldExt {
    //左侧列表是否选中
    private boolean selected = false;
    //表单/是否可编辑是否选中
    private boolean rightSelected = false;
    //是否关联字段
    private boolean relSelectted = false;
    private String name;
    private String desc;
    private String type;
    private boolean isKey;
    private Integer optionType;
    private Boolean leftAll;
    private Boolean northAll;

    public TableFieldExt(String name, String desc, String type, Integer optionType, Boolean leftAll, Boolean northAll) {
        this.type = type;
        this.name = name;
        this.desc = desc;
        this.optionType = optionType;
        this.leftAll = leftAll;
        this.northAll = northAll;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if(!selected) {
            this.leftAll = selected;
        }
    }
    @NotifyChange("selected")
    public void setSelectedWithoutAll(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isRightSelected() {
        return rightSelected;
    }
    @NotifyChange("northAll")
    public void setRightSelected(boolean rightSelected) {
        this.rightSelected = rightSelected;
        if(!rightSelected) {
            this.northAll = rightSelected;
        }
    }

    public void setRightSelectedWithoutAll(boolean rightSelected) {
        this.rightSelected = rightSelected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isKey() {
        return isKey;
    }

    public void setKey(boolean key) {
        isKey = key;
    }

    public Integer getOptionType() {
        return optionType;
    }

    public void setOptionType(Integer optionType) {
        this.optionType = optionType;
    }

    public boolean isRelSelectted() {
        return relSelectted;
    }

    public void setRelSelectted(boolean relSelectted) {
        this.relSelectted = relSelectted;
    }
}
