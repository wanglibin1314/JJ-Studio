package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.LogoutCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.ProjectRoleVo;
import cn.easyplatform.studio.web.controller.InviteController;
import cn.easyplatform.studio.web.controller.admin.LoginLogListener;
import cn.easyplatform.studio.web.editors.ComponentCallback;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zul.*;
import org.zkoss.zul.Timer;
import org.zkoss.zul.theme.Themes;

import java.util.*;

public class HeaderBarControllerImpl  implements HeaderController,
        EventListener<Event> {
    private WorkbenchController workbench;
    private IdSpace idspace;
    private Textbox searchTextbox;
    private Menupopup projectMenupopup;

    private Map<String, Disable> actionComponents;
    public OnClickListener clickListener;
    public interface OnClickListener {
        void onShowFunctionBar();
        void onSearchEntityBar(String searchStr);
    }

    //对外调用方法
    public HeaderBarControllerImpl() {
        actionComponents = new HashMap<String, Disable>();
    }

    @Override
    public void setDisabled(String name, boolean disabled) {

    }

    @Override
    public void setMenuitem(int state) {
        actionComponents.get("headerBar_a_backout").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_backout");
        actionComponents.get("headerBar_a_reform").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_reform");
        actionComponents.get("headerBar_a_delete").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_delete");
        actionComponents.get("headerBar_a_checkAll").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_checkAll");

        actionComponents.get("headerBar_a_codeSearch").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_codeSearch");
        /*actionComponents.get("headerBar_a_todo").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_todo")；*/
        actionComponents.get("headerBar_a_annotation").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_annotation");
        /*actionComponents.get("headerBar_a_left").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_left");
        actionComponents.get("headerBar_a_right").setDisabled(state < 2);
        setFunctionStatus(state < 2, "headerBar_a_right");*/
    }

    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget().equals(searchTextbox) ||
                event.getTarget().getId().equals("headerBar_div_search")) {
            if (clickListener != null) {
                clickListener.onSearchEntityBar(searchTextbox.getValue());
                searchTextbox.setValue(null);
            }
        }/* else if (event.getTarget().getId().equals("headerBar_div_fullScreen")) {
            Clients.evalJavaScript("fullScreenClick()");
        }*/ else if ("headerBar_hlayout_productCenter".equals(event.getTarget().getId())) {
            Contexts.setIntoProjectCenter(true);
            Executions.getCurrent().sendRedirect("/", "_black");
        } else if (event.getTarget() instanceof Menuitem) {
            String role = ((Menuitem) event.getTarget()).getValue();
            String[] projectRole = role.split(",");
            if (Strings.isBlank(role) == false && projectRole.length == 2) {
                InviteController inviteController = new InviteController();
                inviteController.changeProject(projectRole[0], projectRole[1], searchTextbox);
            }
        }
    }

    //ui
    //导航栏初始化方法
    public void bindComponents(Component root) {
        idspace = root.getSpaceOwner();
        for (Component c : idspace.getFellows()) {
            if (c instanceof Disable) {
                actionComponents.put(c.getId(), (Disable) c);
            }
        }
        if (idspace.getFellow("headerBar_timer_heartbeat") != null) {
            // 设置心跳
            Component c = idspace.getFellow("headerBar_timer_heartbeat");
            if (StudioApp.me().getConfigAsBoolean("app.sessionKeepAlive")) {
                Timer timer = (Timer) c;
                final int interval = StudioApp.me().getConfigAsInt(
                        "app.sessionValidationInterval") * 1000;
                timer.setDelay(interval);
                timer.setRepeats(true);
                c.addEventListener(Events.ON_TIMER,
                        new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event)
                                    throws Exception {
                                LoginUserVo user = null;
                                if (Sessions.getCurrent() != null) {
                                    user = (LoginUserVo) Sessions.getCurrent().getAttribute(Contexts.EP_USER);
                                }
                                if (user != null)
                                    user.setLastAccessTime(new Date());
                            }
                        });
                timer.setRunning(true);
            }
        }
        searchTextbox = (Textbox) idspace.getFellow("headerBar_textbox_search");
        searchTextbox.addEventListener(Events.ON_OK, this);
        idspace.getFellow("headerBar_div_search").addEventListener(Events.ON_CLICK, this);
        idspace.getFellow("headerBar_div_fullScreen").addEventListener(Events.ON_CLICK, this);
        idspace.getFellow("headerBar_hlayout_productCenter").addEventListener(Events.ON_CLICK, this);
        projectMenupopup = (Menupopup) idspace.getFellow("header_popup_product");
        setValue();
        setMenuitem(0);
    }
    private void setFunctionStatus(boolean disable, String id) {
        A aButton = (A) idspace.getFellow(id);
        Div bgDiv = (Div) idspace.getFellow(id).getParent();
        if (bgDiv != null) {
            if (disable == true) {
                bgDiv.setSclass("ui-icon-div-disable");
            } else {
                bgDiv.setSclass("ui-icon-div");
            }
        }
    }

    private void setValue() {
        List<ProjectRoleVo> projectRoleVoList = Contexts.getUser().getRoleVoList();
        for (ProjectRoleVo projectRoleVo : projectRoleVoList) {
            String[] roles = null;
            if (Strings.isBlank(projectRoleVo.getRoleIDs()) == false) {
                roles = projectRoleVo.getRoleIDs().split(",");
            }
            if (roles == null) {
                Menuitem item = new Menuitem(projectRoleVo.getProjectName());
                item.addEventListener(Events.ON_CLICK, this);
                item.setValue(projectRoleVo.getProjectId());
                projectMenupopup.appendChild(item);
            } else {
                Menu menu= new Menu(projectRoleVo.getProjectName());
                Menupopup pop = new Menupopup();
                for (String roleId : roles) {
                    Menuitem item = new Menuitem(roleId);
                    pop.appendChild(item);
                    item.setValue(projectRoleVo.getProjectId()+ ","+roleId);
                    item.addEventListener(Events.ON_CLICK, this);
                }
                menu.appendChild(pop);
                projectMenupopup.appendChild(menu);
            }
        }
    }

    //功能
    public void changeTheme(Component c) {
        AbstractView.createThemeView(new ComponentCallback(c, Labels.getLabel("menu.theme")))
                .doOverlapped();
    }

    public void changeCodeTheme(final Component c) {
        AbstractView.createCodeThemeContentView(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return null;
            }

            @Override
            public void setValue(String value) {
                ((WorkbenchControllerImpl)workbench).setTheme(value);
            }

            @Override
            public String getTitle() {
                return Labels.getLabel("menu.codeditor");
            }

            @Override
            public Page getPage() {
                return c.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }

    public void logout(final Component c) {
        WebUtils.showConfirm(Labels.getLabel("user.logout"), new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (event.getName().equals(Events.ON_OK)) {
                    //Clients.evalJavaScript("unloadChange(" + Boolean.FALSE + ")");
                    LoginLogListener.loginOutLog();//记录用户退出
                    StudioApp.execute(c, new LogoutCmd(true));
                    Themes.setTheme(Executions.getCurrent(), "atrovirens_c");
                    Executions.sendRedirect("");
                }
            }
        });
    }

    public void showFunc(Component c) {
        if (clickListener != null) {
            clickListener.onShowFunctionBar();
        }
    }

    public void changeUserPassword(Component c) {
        Executions.createComponents("~./include/normal/password.zul", null, null);
    }
}
