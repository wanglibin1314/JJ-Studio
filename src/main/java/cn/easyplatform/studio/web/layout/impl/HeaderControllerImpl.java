/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.entities.beans.api.ApiBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.cmd.help.GetGuideConfigCmd;
import cn.easyplatform.studio.cmd.identity.LogoutCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.AppServiceFileUtils;
import cn.easyplatform.studio.utils.StringToDoc;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.QueryResultVo;
import cn.easyplatform.studio.web.action.CreateAction;
import cn.easyplatform.studio.web.action.MobileExport.AndroidExportAction;
import cn.easyplatform.studio.web.action.MobileExport.WXExportAction;
import cn.easyplatform.studio.web.action.MobileExport.iOSExportAction;
import cn.easyplatform.studio.web.controller.admin.LoginLogListener;
import cn.easyplatform.studio.web.editors.ComponentCallback;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.editors.SimpleEditorFactory;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.web.ext.introJs.*;
import cn.easyplatform.web.ext.zul.BandboxExt;
import cn.easyplatform.web.ext.zul.ComboboxExt;
import cn.easyplatform.web.ext.zul.GridExt;
import cn.easyplatform.web.ext.zul.ListboxExt;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.Timer;
import org.zkoss.zul.theme.Themes;

import java.util.*;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class HeaderControllerImpl implements HeaderController,
        EventListener<Event> {
    public enum CompType {
        CompOnlyClick, CompOnlyValue, CompClickAndValue, CompNoClickAndValue, CompSpecial
    }

    private WorkbenchController workbench;
    private Menu guideMenu;

    private IntroJs introJs;
    private List<List<GuideConfigVo>> paths;
    private Map<String, Disable> actionComponents;

    /**
     *
     */
    public HeaderControllerImpl() {
        actionComponents = new HashMap<String, Disable>();
    }

    public Map<String, Disable> getMenuitem() {
        return actionComponents;
    }

    public void createEditor() {
        workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",true);
        new CreateAction(workbench, new CreateAction.OnCreateListener() {
            @Override
            public void closeWindow() {
                workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",false);
            }
        }).on();
    }

    public void createPageTemplate(int type) {
        if (type == 1) {
            if (workbench.checkEditor(Editor.DESKTOP_LOGIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.DESKTOP_LOGIN_EDITOR_ID)
                        .create();
        } else if (type == 2) {
            if (workbench.checkEditor(Editor.DESKTOP_MAIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.DESKTOP_MAIN_EDITOR_ID)
                        .create();
        } else if (type == 3) {
            if (workbench.checkEditor(Editor.MOBILE_LOGIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.MOBILE_LOGIN_EDITOR_ID)
                        .create();
        } else if (type == 4) {
            if (workbench.checkEditor(Editor.MOBILE_MAIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.MOBILE_MAIN_EDITOR_ID)
                        .create();
        }
    }

    public void createMenuEditor() {
        if (workbench.checkEditor(Editor.MENU_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.MENU_EDITOR_ID)
                    .create();
    }

    public void createRoleEditor(int type) {
        String editorType = type == 0 ? Editor.ROLE_EDITOR_ID : Editor.STUDIO_ROLE_EDITOR_ID;
        if (workbench.checkEditor(editorType) == null)
            SimpleEditorFactory.createEditor(workbench, editorType)
                    .create();
    }

    public void createUserEditor(int type) {
        String editorType = type == 0 ? Editor.USER_EDITOR_ID : Editor.STUDIO_USER_EDITOR_ID;
        if (workbench.checkEditor(editorType) == null)
            SimpleEditorFactory.createEditor(workbench, editorType)
                    .create();
    }

    public void createExportEditor(int type) {
        String id = Editor.ROOTTASK_EXPORT_REPO_ID;
        switch (type) {
            case 0:
                id = Editor.ROOTTASK_EXPORT_REPO_ID;
                break;
            case 1:
                id = Editor.DICT_EXPORT_REPO_ID;
                break;
            case 2:
                id = Editor.LABEL_EXPORT_REPO_ID;
                break;
            case 3:
                id = Editor.INFORM_EXPORT_REPO_ID;
                break;
            case 4:
                id = Editor.MENU_EXPORT_REPO_ID;
                break;
            case 5:
                id = Editor.FILE_EXPORT_REPO_ID;
                break;
            case 6:
                id = Editor.EXPORT_REPO_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    public void createImportEditor(int type) {
        String id = Editor.ROOTTASK_IMPORT_REPO_ID;
        switch (type) {
            case 0:
                id = Editor.ROOTTASK_IMPORT_REPO_ID;
                break;
            case 1:
                id = Editor.DICT_IMPORT_REPO_ID;
                break;
            case 2:
                id = Editor.LABEL_IMPORT_REPO_ID;
                break;
            case 3:
                id = Editor.INFORM_IMPORT_REPO_ID;
                break;
            case 4:
                id = Editor.MENU_IMPORT_REPO_ID;
                break;
            case 5:
                id = Editor.FILE_IMPORT_REPO_ID;
                break;
            case 6:
                id = Editor.IMPORT_REPO_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    public void createVersionManageEditor() {
        if (workbench.checkEditor(Editor.MANAGE_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.MANAGE_REPO_ID)
                    .create();
    }

    public void createExportHistEditor() {
        if (workbench.checkEditor(Editor.EXPORT_HIST_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.EXPORT_HIST_REPO_ID)
                    .create();
    }

    public void createImportHistEditor() {
        if (workbench.checkEditor(Editor.IMPORT_HIST_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.IMPORT_HIST_REPO_ID)
                    .create();
    }

    public void createTableEditor() {
        if (workbench.checkEditor(Editor.TABLE_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.TABLE_EDITOR_ID)
                    .create();
    }

    public void createFileEditor() {
        if (new AppServiceFileUtils().webContainerIsLegal() == false) {
            WebUtils.showInfo(Labels.getLabel("editor.appPath.error"));
            return;
        }
        if (workbench.checkEditor(Editor.FILE_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.FILE_EDITOR_ID)
                    .create();
    }

    public void createUploadProduct() {
        if (workbench.checkEditor(Editor.PRODUCT_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.PRODUCT_EDITOR_ID)
                    .create();
    }

    public void createDbViewEditor() {
        if (workbench.checkEditor(Editor.DB_VIEW_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.DB_VIEW_EDITOR_ID).create();
    }

    public void createSqlWizardEditor() {
        if (workbench.checkEditor(Editor.SQL_WIZARD_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.SQL_WIZARD_EDITOR_ID).create();
    }

    public void createI18nEditor() {
        if (workbench.checkEditor(Editor.I18n_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.I18n_EDITOR_ID).create();
    }

    public void createMobileExportEditor(int type) {
        String editorType = Editor.iOS_EXPORT_REPO_ID;
        switch (type) {
            case 0:
                new iOSExportAction(workbench).on();
                break;
            case 1:
                new AndroidExportAction(workbench).on();
                break;
            case 2:
                new WXExportAction(workbench).on();
                break;
        }
    }

    public void changeTheme(Component c) {
        AbstractView.createThemeView(new ComponentCallback(c, Labels.getLabel("menu.theme")))
                .doOverlapped();
    }

    public void changeProperties(final Component c) {
        AbstractView.createPropertiesView(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return null;
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("menu.settings");
            }

            @Override
            public Page getPage() {
                return c.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }

    public void apiTest(final Component c) {
        AbstractView.createApiTestView(new EditorCallback<ApiBean>() {
            @Override
            public ApiBean getValue() {
                return null;
            }

            @Override
            public void setValue(ApiBean value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("entity.api.apitest");
            }

            @Override
            public Page getPage() {
                return c.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }, "more").doHighlighted();
    }

    public void apiDoc() {
        try {
            QueryResultVo result = StudioApp.execute(null, new QueryCmd("Api", "*", "", 0));
            //如果不存在,提示用户添加
            if (result.getEntities() == null) {
                WebUtils.showError("未创建API");
                return;
            }
            Filedownload.save(StringToDoc.apiToDoc(result).getBytes("UTF-8"), "application/msword", "EP接口文档");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void apiRAR() {
        try {
            Filedownload.save("~./word/api/APIFile.zip", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createGuideConfig(int type) {
        String id = Editor.APP_GUIDE_CONFIG_ID;
        switch (type) {
            case 1:
                id = Editor.APP_GUIDE_CONFIG_ID;
                break;
            case 2:
                id = Editor.STUDIO_GUIDE_CONFIG_ID;
                break;
            case 3:
                id = Editor.APP_GUIDE_SET_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    public void createModuleEditor() {
        if (workbench.checkEditor(Editor.Module_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.Module_EDITOR_ID).create();
    }

    public void changeToolProperties(final Component c) {
        AbstractView.createToolProperties(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return null;
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("menu.settings");
            }

            @Override
            public Page getPage() {
                return c.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }

    public void logout(Component c) {
        LoginLogListener.loginOutLog();//记录用户退出
        StudioApp.execute(c, new LogoutCmd(true));
        Themes.setTheme(Executions.getCurrent(), "atrovirens_c");
        Executions.sendRedirect("");
    }

    public void changeUserPassword(Component c) {
        Executions.createComponents("~./include/normal/password.zul", null, null);
    }

    /*@Override
    public void setDisabled(String name, boolean disabled) {
        Disable c = actionComponents.get(name);
        if (c != null)
            c.setDisabled(disabled);
    }*/

    @Override
    public void setDisabled(String name, boolean disabled) {

    }

    @Override
    public void setMenuitem(int state) {
        actionComponents.get("header_menuitem_edit_undo").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_redo").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_delete").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_selectAll").setDisabled(state < 2);

        actionComponents.get("header_menuitem_edit_find").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_findNext").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_findPrev").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_replace").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_replaceAll").setDisabled(state < 2);
        actionComponents.get("header_menuitem_edit_assist").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_comment").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_todo").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_left").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_right").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_format").setDisabled(state < 2);
        actionComponents.get("header_menuitem_code_formatSelected").setDisabled(state < 2);
        actionComponents.get("header_toolbarbutton_formatBtn").setDisabled(state < 2);
        actionComponents.get("header_toolbarbutton_widgetWinBtn").setDisabled(state < 2);
        actionComponents.get("header_toolbarbutton_selectEchartBtn").setDisabled(state < 2);
    }

    private void setIntro(Component root) {
        introJs = (IntroJs) root.query("introJs");//初始化引导控件对象
        introJs.addEventListener(IntroJs.ON_NEXT, new EventListener<NextEvent>() {//点击下一步按钮(包括结束)便会进入此监听
            @Override
            public void onEvent(NextEvent nextEvent) throws Exception {
                Operation operation = nextEvent.getOperation();
                int operationStep = operation.getOperationStep();//操作的第几步
                Component component = operation.getOperationElement();//该操作对应的控件对象
                List<Action> actionList = operation.getActionList();//要操作的动作集合(里面包含要操作的对象，事件，值)；

                for (Action action : actionList) {
                    String actionEvent = action.getActionEvent();//获得动作事件（onClick等）
                    String actionValue = action.getActionValue();//获得值
                    Component actionComp = action.getActionComponent();//获得动作控件的对象

                    /*A comp = (A) action.getActionComponent();
                        if (Strings.isBlank(actionEvent) == false){
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }*/
                    CompType compType = CompType.CompOnlyClick;
                    if (actionComp == null) {
                        System.out.println(actionComp);
                    }
                    if (actionComp.getClass().equals(Label.class)) {
                        compType = CompType.CompClickAndValue;
                        Label comp = (Label) actionComp;
                        comp.setValue(actionValue);///设置值
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }
                    } else if (actionComp.getClass().equals(Textbox.class)) {
                        compType = CompType.CompClickAndValue;
                        Textbox comp = (Textbox) actionComp;
                        comp.setValue(actionValue);
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }
                    } else if (actionComp.getClass().equals(Bandbox.class) || actionComp.getClass().equals(BandboxExt.class)) {
                        compType = CompType.CompSpecial;
                        Bandbox comp = (Bandbox) actionComp;
                        comp.setValue(actionValue);
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onOpen")) {
                            Events.postEvent(new OpenEvent(Events.ON_OPEN, comp, true, null, actionValue));
                            comp.open();
                        }
                    } else if (actionComp.getClass().equals(Combobox.class) || actionComp.getClass().equals(ComboboxExt.class)) {
                        compType = CompType.CompSpecial;
                        ///有时候返回的是+Ext的对象
                        Combobox comp = (Combobox) actionComp;

                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen"))
                                comp.open();
                            else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                Comboitem comboitem = comp.getItemAtIndex(Integer.valueOf(actionValue) - 1);
                                comp.setSelectedItem(comboitem);
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Menu.class)) {
                        compType = CompType.CompSpecial;
                        Menu comp = (Menu) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen")) {
                                if (!comp.getParent().getClass().equals(Menupopup.class)) {
                                    comp.getMenupopup().setZIndex(1800);
                                    comp.open();
                                } else {
                                    comp.getMenupopup().setZIndex(1801);
                                    comp.getMenupopup().open(comp, "absolute");
                                }
                            } else if (actionEvent.equals("onClose")) {
                                comp.getMenupopup().close();
                            }
                        }
                    } else if (actionComp.getClass().equals(Tab.class)) {
                        compType = CompType.CompSpecial;
                        Tab comp = (Tab) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onSelect")) {
                            comp.setSelected(true);
                        }
                    } else if (actionComp.getClass().equals(Checkbox.class)) {
                        compType = CompType.CompSpecial;
                        Checkbox comp = (Checkbox) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onCheck")) {
                            comp.setChecked(true);
                        }
                    } else if (actionComp.getClass().equals(Tree.class)) {
                        compType = CompType.CompSpecial;
                        Tree comp = (Tree) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onCheck")) {

                        }
                    } else if (actionComp.getClass().equals(Toolbarbutton.class)) {
                        compType = CompType.CompSpecial;
                        Toolbarbutton comp = (Toolbarbutton) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen")) {
                                String positionStr = null;
                                if (comp.getPopup().split(",").length > 1) {
                                    positionStr = comp.getPopup().split(",")[1].trim();
                                }
                                Menupopup menupopup = (Menupopup) (comp.getNextSibling());
                                //Menupopup menupopup = ((Menupopup) comp.getPage().getFellowIfAny(comp.getPopup().split(",")[0]));
                                if (positionStr != null) {
                                    menupopup.open(comp, positionStr);
                                } else {
                                    menupopup.open(comp);
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Menupopup.class)) {
                        compType = CompType.CompSpecial;
                        Menupopup comp = (Menupopup) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onClose")) {
                                comp.close();
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Listbox.class) || actionComp.getClass().equals(ListboxExt.class)) {
                        compType = CompType.CompSpecial;
                        Listbox comp = (Listbox) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_DOUBLE_CLICK) && Strings.isBlank(actionValue) == false) {
                                Listitem listitem = comp.getItems().get(Integer.valueOf(actionValue) - 1);
                                Events.postEvent(new Event(Events.ON_DOUBLE_CLICK, listitem));
                            } else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                String[] actions = actionValue.split(",");
                                if (actionValue.split(",").length > 0) {
                                    if (actions.length == 1 && actions[0].equals("all")) {
                                        comp.selectAll();
                                    } else if (actions.length > 0) {
                                        Set<Listitem> listitems = new HashSet<>();
                                        for (String line : actions) {
                                            if (StringUtil.isInteger(line)) {
                                                Listitem listitem = comp.getItems().get(Integer.valueOf(actionValue) - 1);
                                                listitems.add(listitem);
                                            }
                                            comp.setSelectedItems(listitems);
                                        }
                                    } else {
                                        Events.postEvent(new Event(actionEvent, actionComp));
                                    }
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Grid.class) || actionComp.getClass().equals(GridExt.class)) {
                        compType = CompType.CompSpecial;
                        Grid comp = (Grid) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_DOUBLE_CLICK) && Strings.isBlank(actionValue) == false) {
                                Row row = (Row) comp.getRows().getChildren().get(Integer.valueOf(actionValue) - 1);
                                Events.postEvent(new Event(Events.ON_DOUBLE_CLICK, row));
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Window.class)) {
                        compType = CompType.CompSpecial;
                        Window comp = (Window) actionComp;
                        if (!Strings.isBlank(actionValue)) {
                            comp.setZindex(Integer.valueOf(actionValue));
                        }
                    } else if (actionComp.getClass().equals(Menuitem.class)) {
                        compType = CompType.CompSpecial;
                        Menuitem comp = (Menuitem) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_CHECK) && Strings.isBlank(actionValue) == false &&
                                    (actionValue.equals("true") || actionValue.equals("false"))) {
                                if (actionValue.equals("true"))
                                    comp.setChecked(true);
                                else
                                    comp.setChecked(false);
                                Events.postEvent(new Event(Events.ON_CLICK, actionComp));
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Tabbox.class)) {
                        compType = CompType.CompSpecial;
                        Tabbox comp = (Tabbox) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_CLOSE) && Strings.isBlank(actionValue) == false) {
                                if (actionValue.equals("all"))
                                    closeAllTabs(comp);
                                else if (StringUtil.isInteger(actionValue) == true) {
                                    Tab tab = (Tab) comp.getTabs().getChildren().get(Integer.valueOf(actionValue) - 1);
                                    tab.setAutoClose(true);
                                    tab.close();
                                }
                            } else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                if (StringUtil.isInteger(actionValue) == true) {
                                    comp.setSelectedIndex(Integer.valueOf(actionValue) - 1);
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    }
                    if (compType == CompType.CompOnlyClick) {
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, actionComp));///模拟事件
                        }
                    }
                }
            }
        });
    }

    public void bindComponents(Component root) {
        ((Menuitem) root.getFellow("header_menuitem_help_forum")).setHref(StudioApp.getHelpConfig("help.baseUrl.forum"));
        ((Menuitem) root.getFellow("header_menuitem_help_guide")).setHref(StudioApp.getHelpConfig("help.baseUrl.guide"));
        ((Menuitem) root.getFellow("header_menuitem_help_reference")).setHref(StudioApp.getHelpConfig("help.baseUrl.reference"));
        ((Menuitem) root.getFellow("header_menuitem_help_component")).setHref(StudioApp.getHelpConfig("help.baseUrl.component"));
        ((Menuitem) root.getFellow("header_menuitem_help_logic")).setHref(StudioApp.getHelpConfig("help.baseUrl.logic"));
        if (!Contexts.getUser().isAuthorized("create")) {
            ((Button) root.getFellow("header_toolbarbutton_createEditor")).setDisabled(true);
        }
        setIntro(root);

        guideMenu = (Menu) root.getFellow("header_menuitem_help_showGuide");
        refreshGuide();

        LoginUserVo user = Contexts.getUser();
        Iterator<Component> itr = root.queryAll("menuitem").iterator();
        while (itr.hasNext()) {
            Menuitem mi = (Menuitem) itr.next();
            if (!Strings.isBlank(mi.getValue()))
                mi.setDisabled(!user.isAuthorized(mi.getValue()));
        }
        for (Component c : root.getSpaceOwner().getFellows()) {
            if (c instanceof Disable) {
                actionComponents.put(c.getId(), (Disable) c);
            } else if (c.getId().equals("heartbeat")) {
                // 设置心跳
                if (StudioApp.me().getConfigAsBoolean("app.sessionKeepAlive")) {
                    Timer timer = (Timer) c;
                    final int interval = StudioApp.me().getConfigAsInt(
                            "app.sessionValidationInterval") * 1000;
                    timer.setDelay(interval);
                    timer.setRepeats(true);
                    c.addEventListener(Events.ON_TIMER,
                            new EventListener<Event>() {
                                @Override
                                public void onEvent(Event event)
                                        throws Exception {
                                    LoginUserVo user = Contexts.getUser();
                                    if (user != null)
                                        user.setLastAccessTime(new Date());
                                }
                            });
                    timer.setRunning(true);
                }
            } else if (c.getId().equals("header_menupopup_view_codeditor")) {
                String theme = Contexts.getUser().getEditorTheme();
                if (Strings.isBlank(theme) || theme.equals("default"))
                    ((Menuitem) c.getFirstChild()).setChecked(true);
                else {
                    itr = c.queryAll("menuitem").iterator();
                    while (itr.hasNext()) {
                        Menuitem mi = (Menuitem) itr.next();
                        if (theme.equals(mi.getLabel())) {
                            mi.setChecked(true);
                            break;
                        }
                    }
                }
            }
        }
        setMenuitem(0);
    }

    public void refreshGuide() {
        guideMenu.getChildren().clear();
        List<GuideConfigVo> configVos = StudioApp.execute(new GetGuideConfigCmd(GuideConfigEditor.GuideConfigType.GuideConfigStudio));
        paths = GetGuideConfigCmd.allTree(configVos);
        if (paths.size() == 0)
            guideMenu.setVisible(false);
        else {
            guideMenu.setVisible(true);
            Menupopup menupopup = new Menupopup();
            for (int index = 0; index < paths.size(); index++) {
                Menuitem menuitem = new Menuitem(paths.get(index).get(0).getGuideName());
                menuitem.setImage("~./images/groups.png");
                menuitem.setId("guideItem" + Integer.valueOf(index));
                menuitem.addEventListener(Events.ON_CLICK, this);
                menuitem.setParent(menupopup);
            }
            menupopup.setParent(guideMenu);
            menupopup.addEventListener(Events.ON_OPEN, new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    for (int index = 0; index < paths.size(); index++) {
                        Clients.evalJavaScript("fullscreen(" + index + ")");
                    }
                }
            });
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget().getId().contains("guideItem")) {
            Thread.sleep(2000);
            Menuitem menu = (Menuitem) event.getTarget();
            Integer index = Integer.valueOf(menu.getId().replace("guideItem", ""));
            List<GuideConfigVo> guideConfigVoList = paths.get(index);
            List<Step> stepList = new ArrayList<>();
            for (GuideConfigVo configVo :
                    guideConfigVoList) {
                stepList.addAll(configVo.getStepList());
            }
            Options options = new Options();
            options.setSteps(stepList);
            introJs.setOptionsObj(options);
            //introJs.start();
            Messagebox.show("演示需要初始化页面,请确认保存,是否初始化?",
                    Labels.getLabel("message.title.confirm"), Messagebox.YES
                            | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            if (event.getName().equals(Messagebox.ON_YES)) {
                                List<Tab> tabs = ((WorkbenchControllerImpl) workbench).getTabbox().getTabs().getChildren();
                                closeAllTabs(((WorkbenchControllerImpl) workbench).getTabbox());
                                introJs.start();
                            }
                        }
                    });
        }
    }

    private void closeAllTabs(Tabbox tabbox) {
        List<Tab> tabs = tabbox.getTabs().getChildren();
        List<Tab> copyTabs = new ArrayList<>();
        for (Tab tab : tabs) {
            copyTabs.add(tab);
        }
        for (Tab tab : copyTabs) {
            if (!tab.getLabel().equals(Labels.getLabel("app.welcome"))) {
                tab.setAutoClose(true);
                tab.close();
            }
        }
    }

    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

}
