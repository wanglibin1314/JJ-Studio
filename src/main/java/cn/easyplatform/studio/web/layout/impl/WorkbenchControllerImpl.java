/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetAndLockEntityCmd;
import cn.easyplatform.studio.cmd.identity.SetThemeCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.vos.GetEntityVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.web.action.CreateAction;
import cn.easyplatform.studio.web.controller.MainLayoutController;
import cn.easyplatform.studio.web.controller.admin.AdminMainController;
import cn.easyplatform.studio.web.editors.*;
import cn.easyplatform.studio.web.editors.entity.ListEntityEditor;
import cn.easyplatform.studio.web.editors.entity.PageEntityEditor;
import cn.easyplatform.studio.web.layout.FunctionController;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.NavbarController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.SubType;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.util.*;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WorkbenchControllerImpl implements WorkbenchController,
        EventListener<Event> {

    private HeaderController header;

    private NavbarController entityBar;

    private NavbarController entityWindowBar;

    private HeaderController shortcutKeyBar;

    private FunctionController functionBar;

    private MainLayoutController mainLayoutController;

    private AdminMainController adminMainController;

    private Page page;

    private Tabbox tabbox;

    /**
     * @param headerController
     * @param entityController
     */
    public WorkbenchControllerImpl(HeaderController headerController, NavbarController entityController,
                                   NavbarController entityWindowController, Page page,
                                   HeaderController shortcutKeyBar, MainLayoutController mainLayoutController,
                                   FunctionController functionBar) {
        this.header = headerController;
        this.entityBar = entityController;
        this.entityWindowBar = entityWindowController;
        this.shortcutKeyBar = shortcutKeyBar;
        this.functionBar = functionBar;
        this.header.setWorkbench(this);
        this.entityBar.setWorkbench(this);
        this.entityWindowBar.setWorkbench(this);
        this.shortcutKeyBar.setWorkbench(this);
        this.functionBar.setWorkbench(this);
        this.mainLayoutController = mainLayoutController;
        this.page = page;
    }

    /**
     * admin
     *
     * @param headerController
     * @param entityController
     */
    public WorkbenchControllerImpl(HeaderController headerController, NavbarController entityController,
                                   NavbarController entityWindowController, Page page,
                                   HeaderController shortcutKeyBar, AdminMainController adminMainController,
                                   FunctionController functionBar) {
        this.header = headerController;
        this.entityBar = entityController;
        this.entityWindowBar = entityWindowController;
        this.shortcutKeyBar = shortcutKeyBar;
        this.functionBar = functionBar;
        this.header.setWorkbench(this);
        this.entityBar.setWorkbench(this);
        this.entityWindowBar.setWorkbench(this);
        this.shortcutKeyBar.setWorkbench(this);
        this.functionBar.setWorkbench(this);
        this.adminMainController = adminMainController;
        this.page = page;
    }

    public void setPerspectives(Tabbox tabbox) {
        this.tabbox = tabbox;
        this.tabbox.addEventListener(Events.ON_SELECT, this);
        Component epcontextmenu = null;
        //判断是否为admin登录
        if (LoginUserVo.UserType.DEV.getName().equals(Contexts.getUser().getRoleId())) {
            epcontextmenu = tabbox.getFellow("normal_main_epcontextmenu");
            List<Component> comps = epcontextmenu.getChildren();
            for (Component c : comps) {
                if (c instanceof Menuitem)
                    c.addEventListener(Events.ON_CLICK, this);
            }
            epcontextmenu.addEventListener(Events.ON_OPEN, this);
        }

        if (LoginUserVo.UserType.DEV.getName().equals(Contexts.getUser().getRoleId())) {
            if (this.checkEditor(Editor.DEFAULT_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(this, Editor.DEFAULT_EDITOR_ID)
                        .create();
        }
    }

    @Override
    public void onCtrlKeys(int keyCode) {
        if (keyCode == 49) {// 新增
            shortcutKeyBar.setDisabled("shortcutKey_div_create",true);
            new CreateAction(this, new CreateAction.OnCreateListener() {
                @Override
                public void closeWindow() {
                    shortcutKeyBar.setDisabled("shortcutKey_div_create",false);
                }
            }).on();
        } else if (keyCode == 50) {// 保存
            save();
        } else if (keyCode == 51) {// 保存所有
            saveAll();
        } else if (keyCode < 10) {//自由定义10个弹出窗口
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("code", keyCode);
            Executions.createComponents("~./include/entitybox.zul", null, data);
        } else if (keyCode == 54) {//弹出控件栏
            if (!((ShortcutKeyControllerImpl) shortcutKeyBar).getMenuitem().get("shortcutKey_div_widget").getStyle().contains("background")) {
                openwidgetWin();
            }
        } else if (keyCode == 87) {//格式化
            if (!((ShortcutKeyControllerImpl) shortcutKeyBar).getMenuitem().get("shortcutKey_div_code").getStyle().contains("background")) {
                formatAll();
            }
        } else if (keyCode == 81) {//弹出控件栏
            if (!((ShortcutKeyControllerImpl) shortcutKeyBar).getMenuitem().get("shortcutKey_div_eChart").getStyle().contains("background")) {
                openEchartWin();
            }
        } else {// 根据打开参数弹出对应的窗口
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null)
                WebUtils.showInfo(Labels.getLabel("editor.open.error"));
            if (editor != null)
                editor.onCtrlKeys(keyCode);
        }
    }

    public void openwidgetWin() {//打开控件栏
        Editor editor = tabbox.getSelectedTab().getValue();
        if (editor == null)
            WebUtils.showInfo(Labels.getLabel("editor.open.error"));
        if (editor != null) {
            if (editor instanceof PageEntityEditor || editor instanceof ListEntityEditor) {
                Component c = Executions.createComponents("~./include/widgetTemp.zul", null, null);
                Window win = (Window) c;
                Iterator<Component> itr = c.queryAll("toolbarbutton")
                        .iterator();
                while (itr.hasNext()) {
                    Button widget = (Button) itr.next();
                    widget.setDraggable(EntityType.TABLE.getName());
                    widget.setAttribute("t", "");
                }
            }
        }
    }

    public void openEchartWin() {
        AbstractView.createEchartView(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return "";
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return "图表模板";
            }

            @Override
            public Page getPage() {
                return tabbox.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }, "drag").doOverlapped();
        /*AbstractView.createSelectEchartView(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return "";
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return "图表模板";
            }

            @Override
            public Page getPage() {
                return tabbox.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }, "drag").doOverlapped();*/
    }

    @Override
    public <T extends BaseEntity> void createEditor(T entity, char code,
                                                    Object... args) {
        EntityEditor<T> editor = SimpleEditorFactory.createEditor(this, entity,
                code);
        editor.create(args);
    }

    @Override
    public <T extends BaseEntity> void createEditor(T entity, char code,
                                                    CreateAction.OnSuccessListener listener, Object... args) {
        AbstractEntityEditor editor = (AbstractEntityEditor) SimpleEditorFactory.createEditor(this, entity,
                code);
        editor.successListener = listener;
        editor.create(args);
    }

    @Override
    public void setDisabled(String name, boolean state) {
        if (name.equals("save")) {
            if (!Contexts.getUser().isAuthorized("save"))
                return;
            shortcutKeyBar.setDisabled("shortcutKey_div_save", state);
            if (state) {
                boolean isDirty = false;
                List<Tab> tabs = tabbox.getTabs().getChildren();
                for (Tab tab : tabs) {
                    Object val = tab.getValue();
                    if (val != null && val instanceof Savable) {
                        Savable editor = (Savable) val;
                        isDirty = editor.isDirty();
                        if (isDirty)
                            break;
                    }
                }
                shortcutKeyBar.setDisabled("shortcutKey_div_saveAll", !isDirty);
            }
        } else {
            if (name.equals("saveAll") && !Contexts.getUser().isAuthorized("saveAll"))
                return;
            shortcutKeyBar.setDisabled(name, state);
        }
    }

    @Override
    public HeaderController getShortcutKeyBar() {
        return shortcutKeyBar;
    }

    @Override
    public FunctionController getFunctionBar() {
        return functionBar;
    }

    @Override
    public NavbarController getEntityBar() {
        return entityBar;
    }

    @Override
    public NavbarController getEntityWindowBar() {
        return entityWindowBar;
    }

    @Override
    public Page getPage() {
        return page;
    }

    @Override
    public MainLayoutController getMainLayout() {
        return mainLayoutController;
    }

    @Override
    public HeaderController getHeader() {
        return header;
    }

    @Override
    public void addEditor(Tab tab, Tabpanel tabpanel) {
        tab.setContext("normal_main_epcontextmenu");
        tabbox.getTabs().appendChild(tab);
        tabbox.getTabpanels().appendChild(tabpanel);
    }

    @Override
    public Editor checkEditor(String id) {
        List<Tab> tabs = tabbox.getTabs().getChildren();
        for (Tab tab : tabs) {
            Editor editor = tab.getValue();
            if (editor != null) {
                if (id.equals(editor.getId())) {
                    tab.setSelected(true);
                    return editor;
                }
            }
        }
        return null;
    }

    public void openEntity(String entityId) {
        if (checkEditor(entityId) == null) {
            GetEntityVo ge = StudioApp.execute(new GetAndLockEntityCmd(entityId));
            if (ge != null) {
                char code = 'U';
                if (!Strings.isBlank(ge.getLockUser())) {
                    code = 'R';
                    Messagebox.show(Labels.getLabel("enitiy.locked",
                            new String[]{ge.getLockUser()}), Labels
                                    .getLabel("message.title.error"),
                            Messagebox.OK, Messagebox.INFORMATION);
                }
                createEditor(ge.getEntity(), code);
            }
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Tab) {
            Editor editor = ((Tab) event.getTarget()).getValue();
            //判断session中的user是否为空，在进行判断是否为admin
            if (Contexts.getUser() != null) {
                /*if (Contexts.getUser().getType() != 1) {
                    if (editor != null && editor instanceof Savable) {
                        Savable e = (Savable) editor;
                        setDisabled("save", !e.isDirty());
                        if (e instanceof CodeEditor) {
                            header.setMenuitem(2);
                            shortcutKeyBar.setMenuitem(2);
                        } else {
                            header.setMenuitem(1);
                            shortcutKeyBar.setMenuitem(1);
                        }
                    } else {
                        setDisabled("save", true);
                        header.setMenuitem(0);
                        shortcutKeyBar.setMenuitem(0);
                    }
                }*/
            }
            } else if (event.getTarget() instanceof Menupopup) {
                OpenEvent oe = (OpenEvent) event;
                if (oe.getReference() != null) {
                    Tab tab = (Tab) oe.getReference();
                    Editor editor = tab.getValue();
                    if (editor != null) {
                        List<Component> comps = oe.getTarget().getChildren();
                        if (editor instanceof EntityEditor<?>) {
                            for (int i = 3; i < comps.size(); i++)
                                comps.get(i).setVisible(true);
                            EntityEditor<?> ee = (EntityEditor<?>) editor;
                            LoginUserVo loginUserVo = Contexts.getUser();
                            if (ee.getEntity().getSubType().equals(SubType.JOB.getName())) {
                                ((Menuitem) comps.get(4)).setDisabled(!loginUserVo.isAuthorized("jobAdd"));
                                ((Menuitem) comps.get(5)).setDisabled(!loginUserVo.isAuthorized("jobDelete"));
                            } else {
                                ((Menuitem) comps.get(4)).setDisabled(!loginUserVo.isAuthorized(ee.getEntity().getType().toLowerCase() + "Add"));
                                ((Menuitem) comps.get(5)).setDisabled(!loginUserVo.isAuthorized(ee.getEntity().getType().toLowerCase() + "Delete"));
                            }
                            ((Menuitem) comps.get(7)).setDisabled(false);
                        } else {
                            for (int i = 4; i < comps.size(); i++) {
                                Component c = comps.get(i);
                                if (c instanceof Menuitem)
                                    ((Menuitem) c).setDisabled(true);
                            }
                        }
                    }
                    oe.getTarget().setAttribute("ref", oe.getReference());
                }
            } else if (event.getTarget() instanceof Menuitem) {
                Tab tab = (Tab) event.getTarget().getParent().getAttribute("ref");
                List<Tab> tabs = tabbox.getTabs().getChildren();
                Editor editor = tab.getValue();
                int idx = event.getTarget().getParent().getChildren().indexOf(event.getTarget());
                switch (idx) {
                    case 0://close
                        editor.close();
                        break;
                    case 1://closeOthers
                        List<Editor> tmp = new ArrayList<>();
                        for (Tab t : tabs) {
                            if (t != tab && t.getValue() != null) {
                                editor = t.getValue();
                                tmp.add(editor);
                            }
                        }
                        for (Editor e : tmp)
                            e.close();
                        break;
                    case 2://closeAll
                        tmp = new ArrayList<>();
                        for (Tab t : tabs) {
                            if (t.getValue() != null) {
                                editor = t.getValue();
                                tmp.add(editor);
                            }
                        }
                        for (Editor e : tmp)
                            e.close();
                        break;
                    case 4://copy
                        StudioUtil.showInputDialog(this, page, editor.getId(), true);
                        break;
                    case 5://delete
                        ((EntityEditor<?>) editor).delete();
                        break;
                    case 7://version
                        EntityEditor<?> ee = (EntityEditor<?>) editor;
                        BaseEntity entity = ee.getEntity();
                        EntityVo vo = new EntityVo(entity.getId(), entity.getName(), entity.getDescription(), entity.getType(), entity.getSubType());
                        StudioUtil.showHistory(this, vo);
                }
            }
        }

        public Tabbox getTabbox () {
            return tabbox;
        }

        /**
         * 设置代码编辑器风格
         *
         * @param
         */
        public void setTheme (String theme){
            StudioApp.execute(new SetThemeCmd(theme, true));
            List<Tab> tabs = tabbox.getTabs().getChildren();
            for (Tab tab : tabs) {
                Object val = tab.getValue();
                if (val != null && val instanceof EntityEditor<?>) {
                    EntityEditor<?> editor = (EntityEditor<?>) val;
                    editor.setTheme(theme);
                }
            }
        }
        /*public void setTheme (Menuitem m){
            String theme = m.getLabel();
            if (m.getPreviousSibling() == null)
                theme = "default";
            Iterator<Component> itr = m.getParent().queryAll("menuitem").iterator();
            while (itr.hasNext()) {
                Menuitem c = (Menuitem) itr.next();
                if (c != m)
                    c.setChecked(false);
            }
            StudioApp.execute(new SetThemeCmd(theme, true));
            List<Tab> tabs = tabbox.getTabs().getChildren();
            for (Tab tab : tabs) {
                Object val = tab.getValue();
                if (val != null && val instanceof EntityEditor<?>) {
                    EntityEditor<?> editor = (EntityEditor<?>) val;
                    editor.setTheme(theme);
                }
            }
        }*/

        // //////////////////////工具栏按钮事件//////////////////////////

        public void save () {
            Object val = tabbox.getSelectedTab().getValue();
            if (val != null && val instanceof Savable) {
                Savable editor = (Savable) val;
                editor.save();
            }
        }

        public void saveAll () {
            List<Tab> tabs = tabbox.getTabs().getChildren();
            for (Tab tab : tabs) {
                Object val = tab.getValue();
                if (val != null && val instanceof Savable) {
                    Savable editor = (Savable) val;
                    editor.save();
                }
            }
        }

        // //////////////////////编辑器菜单事件//////////////////////////
        public void format () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.format();
        }

        public void formatAll () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.formatAll();
        }

        public void deleteLine () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.deleteLine();
        }

        public void undo () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.undo();
        }

        public void redo () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.redo();
        }

        public void selectAll () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.selectAll();
        }

        public void indentMore () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.indentMore();
        }

        public void indentLess () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.indentLess();
        }

        public void find () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.find();
        }

        public void findNext () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.findNext();
        }

        public void findPrev () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.findPrev();
        }

        public void replace () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.replace();
        }

        public void replaceAll () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            CodeEditor e = (CodeEditor) editor;
            e.replaceAll();
        }

        public void insertTodo () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            ((CodeEditor) editor).insertTodo();
        }

        public void insertComments () {
            Editor editor = tabbox.getSelectedTab().getValue();
            if (editor == null || !(editor instanceof CodeEditor))
                return;
            ((CodeEditor) editor).insertComments();
        }
    }
