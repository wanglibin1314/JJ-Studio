package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.entities.beans.api.ApiBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.cmd.help.GetGuideConfigCmd;
import cn.easyplatform.studio.cmd.identity.LogoutCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.AppServiceFileUtils;
import cn.easyplatform.studio.utils.StringToDoc;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.QueryResultVo;
import cn.easyplatform.studio.web.action.MobileExport.AndroidExportAction;
import cn.easyplatform.studio.web.action.MobileExport.WXExportAction;
import cn.easyplatform.studio.web.action.MobileExport.iOSExportAction;
import cn.easyplatform.studio.web.controller.admin.LoginLogListener;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.editors.SimpleEditorFactory;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;
import cn.easyplatform.studio.web.layout.FunctionController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.web.ext.introJs.*;
import cn.easyplatform.web.ext.zul.BandboxExt;
import cn.easyplatform.web.ext.zul.ComboboxExt;
import cn.easyplatform.web.ext.zul.GridExt;
import cn.easyplatform.web.ext.zul.ListboxExt;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.*;
import org.zkoss.zul.theme.Themes;

import java.util.*;

public class FunctionControllerImpl  implements FunctionController,
        EventListener<Event> {

    private IntroJs introJs;
    private Vlayout funcVlayout;
    private Scrollview secondFuncScrollview;
    private Scrollview thirdFuncScrollview;
    private WorkbenchController workbench;

    private List<List<GuideConfigVo>> paths;
    public OnClickListener clickListener;

    public interface OnClickListener {
        void onShowEntity();
    }

    public void bindComponents(Component root) {
        IdSpace is = root.getSpaceOwner();
        funcVlayout = (Vlayout) is.getFellow("functionBar_vlayout_func");
        secondFuncScrollview = (Scrollview) is.getFellow("functionBar_scrollview_secondFunc");
        secondFuncScrollview.setVisible(false);
        thirdFuncScrollview = (Scrollview) is.getFellow("functionBar_scrollview_thirdFunc");
        thirdFuncScrollview.setVisible(false);
        is.getFellow("functionBar_vlayout_entity").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_entity").addEventListener(Events.ON_CLICK, this);
        is.getFellow("functionBar_vlayout_authorization").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_authorization").addEventListener(Events.ON_MOUSE_OUT, this);
        is.getFellow("functionBar_vlayout_authorization").addEventListener(Events.ON_CLICK, this);
        is.getFellow("functionBar_vlayout_version").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_version").addEventListener(Events.ON_MOUSE_OUT, this);
        is.getFellow("functionBar_vlayout_version").addEventListener(Events.ON_CLICK, this);
        is.getFellow("functionBar_vlayout_database").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_database").addEventListener(Events.ON_MOUSE_OUT, this);
        is.getFellow("functionBar_vlayout_database").addEventListener(Events.ON_CLICK, this);
        is.getFellow("functionBar_vlayout_api").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_api").addEventListener(Events.ON_MOUSE_OUT, this);
        is.getFellow("functionBar_vlayout_api").addEventListener(Events.ON_CLICK, this);
        is.getFellow("functionBar_vlayout_other").addEventListener(Events.ON_MOUSE_OVER, this);
        is.getFellow("functionBar_vlayout_other").addEventListener(Events.ON_MOUSE_OUT, this);
        is.getFellow("functionBar_vlayout_other").addEventListener(Events.ON_CLICK, this);
        root.addEventListener(Events.ON_CLICK, this);
        introJs = (IntroJs) is.getFellow("functionBar_introJs_intro");
        setIntro(root);
    }

    public void changeUserPassword(Component c) {
        Executions.createComponents("~./include/normal/password.zul", null, null);
    }

    public void logout(Component c) {
        LoginLogListener.loginOutLog();//记录用户退出
        StudioApp.execute(c, new LogoutCmd(true));
        Themes.setTheme(Executions.getCurrent(), "atrovirens_c");
        Executions.sendRedirect("");
    }

    @Override
    public void refreshGuide() {
        List<GuideConfigVo> configVos = StudioApp.execute(new GetGuideConfigCmd(GuideConfigEditor.GuideConfigType.GuideConfigStudio));
        paths = GetGuideConfigCmd.allTree(configVos);
    }

    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    @Override
    public void setMenuitem(int state) {

    }

    @Override
    public void onEvent(Event event) throws Exception {
        List<String> clickIDList = new ArrayList<>();
        clickIDList.add("functionBar_vlayout_authorization");
        clickIDList.add("functionBar_vlayout_version");
        clickIDList.add("functionBar_vlayout_database");
        clickIDList.add("functionBar_vlayout_api");
        clickIDList.add("functionBar_vlayout_other");
        if (clickIDList.contains(event.getTarget().getId())) {
            if (event.getName().equals(Events.ON_MOUSE_OVER)) {
                secondFuncScrollview.setVisible(true);
                thirdFuncScrollview.setVisible(false);
                Clients.evalJavaScript("funBarSecondShow()");
                SetSecondFunction(event.getTarget().getId());
            }/* else {
                secondFuncScrollview.setVisible(false);
                thirdFuncScrollview.setVisible(false);
            }*/
        }/* else if (event.getTarget().getId().equals("functionBar_vlayout_module")) {
            if (clickListener != null)
                clickListener.onShowModule();
            closeFunctionBar();
        } */else if (event.getTarget().getId().equals("functionBar_hlayout_main")) {
            closeFunctionBar();
        }  else if (event.getTarget().getId().equals("functionBar_vlayout_entity")) {
            if (event.getName().equals(Events.ON_MOUSE_OVER)) {
                secondFuncScrollview.setVisible(false);
                thirdFuncScrollview.setVisible(false);
            } else if (event.getName().equals(Events.ON_CLICK)) {
                secondFuncScrollview.setVisible(false);
                thirdFuncScrollview.setVisible(false);
                closeFunctionBar();
                if (clickListener != null)
                    clickListener.onShowEntity();
            }
        }
        else if (event.getTarget() instanceof Hlayout) {
            if (event.getTarget().getId().equals("menu.org")) {
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.role")) {
                createRoleEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.group")) {
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.user")) {
                createUserEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.menu")) {
                createMenuEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.access")) {
                createAccessEditor();
                closeFunctionBar();
            }else if (event.getTarget().getId().equals("button.import")) {
                createImportEditor(6);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("button.export")) {
                createExportEditor(6);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import")) {
                createImportHistEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export")) {
                createExportHistEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.hist")) {
                createVersionManageEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.table.refactor")) {
                createTableEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.db.view")) {
                createDbViewEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.sql.wizard")) {
                createSqlWizardEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("entity.api.apitest")) {
                apiTest();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("entity.api.doc.download")) {
                apiDoc();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("entity.api.doc.use.rar")) {
                apiRAR();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.project.pc.login")) {//模板配置
                createPageTemplate(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.project.pc.main")) {
                createPageTemplate(2);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.project.mobile.login")) {
                createPageTemplate(3);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.project.mobile.main")) {
                createPageTemplate(4);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.settings")) {
                changeProperties(event.getTarget());
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.main.entity")) {
                createImportEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.dictionary")) {
                createImportEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.label")) {
                createImportEditor(2);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.inform")) {
                createImportEditor(3);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.menu")) {
                createImportEditor(4);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.import.file")) {
                createImportEditor(5);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.main.entity")) {
                createExportEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.dictionary")) {
                createExportEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.label")) {
                createExportEditor(2);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.inform")) {
                createExportEditor(3);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.menu")) {
                createExportEditor(4);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.repo.export.file")) {
                createExportEditor(5);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.studio.user")) {
                createUserEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.studio.role")) {
                createRoleEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.appfile")) {//应用文件管理
                createFileEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.i18n")) {
                createI18nEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("editor.product.set")) {
                createModuleEditor();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.product")) {
                createUploadProduct();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.tools.settings")) {
                changeToolProperties();
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.tools.mobile.iOS")) {
                createMobileExportEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.tools.mobile.Android")) {
                createMobileExportEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.tools.mobile.MiniProgram")) {
                createMobileExportEditor(2);
                closeFunctionBar();
            }else if (event.getTarget().getId().equals("menu.login.log")) {
                createAdminManageEditor(0);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.online.user.management")) {
                createAdminManageEditor(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.operation.log")) {
                createAdminManageEditor(2);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.application.file.management")) {
                createAdminManageEditor(3);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.the.template.configuration")) {
                createAdminManageEditor(4);
                closeFunctionBar();
            }else if (event.getTarget().getId().equals("app.guide.config")) {
                createGuideConfig(1);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.app.guide.widgetID")) {
                createGuideConfig(3);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("studio.guide.config")) {
                createGuideConfig(2);
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.logic")) {
                Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.logic"), "_black");
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.component")) {
                Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.component"), "_black");
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.guide")) {
                Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.guide"), "_black");
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.reference")) {
                Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.reference"), "_black");
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.forum")) {
                Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.forum"), "_black");
                closeFunctionBar();
            } else if (event.getTarget().getId().equals("menu.about")) {
                createGuideConfig(4);
                closeFunctionBar();
            } else if (event.getTarget().getId().contains("guideItem")) {
                closeFunctionBar();
                Thread.sleep(2000);
                Integer index = Integer.valueOf(event.getTarget().getId().replace("guideItem", ""));
                List<GuideConfigVo> guideConfigVoList = paths.get(index);
                List<Step> stepList = new ArrayList<>();
                for (GuideConfigVo configVo :
                        guideConfigVoList) {
                    stepList.addAll(configVo.getStepList());
                }
                Options options = new Options();
                options.setSteps(stepList);
                introJs.setOptionsObj(options);
                //introJs.start();
                Messagebox.show("演示需要初始化页面,请确认保存,是否初始化?",
                        Labels.getLabel("message.title.confirm"), Messagebox.YES
                                | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (event.getName().equals(Messagebox.ON_YES)) {
                                    List<Tab> tabs = ((WorkbenchControllerImpl) workbench).getTabbox().getTabs().getChildren();
                                    closeAllTabs(((WorkbenchControllerImpl) workbench).getTabbox());
                                    introJs.start();
                                }
                            }
                        });
            } else {
                if (event.getName().equals(Events.ON_MOUSE_OVER)) {
                    thirdFuncScrollview.setVisible(true);
                    Clients.evalJavaScript("funBarThirdShow()");
                    SetThirdFunction(event.getTarget().getId());
                }/* else {
                    thirdFuncScrollview.setVisible(false);
                }*/
            }
        }
    }
    //data
    public void createRoleEditor(int type) {
        String editorType = type == 0 ? Editor.ROLE_EDITOR_ID : Editor.STUDIO_ROLE_EDITOR_ID;
        if (workbench.checkEditor(editorType) == null)
            SimpleEditorFactory.createEditor(workbench, editorType)
                    .create();
    }
    public void createUserEditor(int type) {
        String editorType = type == 0 ? Editor.USER_EDITOR_ID : Editor.STUDIO_USER_EDITOR_ID;
        if (workbench.checkEditor(editorType) == null)
            SimpleEditorFactory.createEditor(workbench, editorType)
                    .create();
    }
    private void createMenuEditor() {
        if (workbench.checkEditor(Editor.MENU_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.MENU_EDITOR_ID)
                    .create();
    }
    private void createAccessEditor() {
        if (workbench.checkEditor(Editor.ACCESS_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.ACCESS_EDITOR_ID)
                    .create();
    }
    public void createImportEditor(int type) {
        String id = Editor.ROOTTASK_IMPORT_REPO_ID;
        switch (type) {
            case 0:
                id = Editor.ROOTTASK_IMPORT_REPO_ID;
                break;
            case 1:
                id = Editor.DICT_IMPORT_REPO_ID;
                break;
            case 2:
                id = Editor.LABEL_IMPORT_REPO_ID;
                break;
            case 3:
                id = Editor.INFORM_IMPORT_REPO_ID;
                break;
            case 4:
                id = Editor.MENU_IMPORT_REPO_ID;
                break;
            case 5:
                id = Editor.FILE_IMPORT_REPO_ID;
                break;
            case 6:
                id = Editor.IMPORT_REPO_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }
    public void createExportEditor(int type) {
        String id = Editor.ROOTTASK_EXPORT_REPO_ID;
        switch (type) {
            case 0:
                id = Editor.ROOTTASK_EXPORT_REPO_ID;
                break;
            case 1:
                id = Editor.DICT_EXPORT_REPO_ID;
                break;
            case 2:
                id = Editor.LABEL_EXPORT_REPO_ID;
                break;
            case 3:
                id = Editor.INFORM_EXPORT_REPO_ID;
                break;
            case 4:
                id = Editor.MENU_EXPORT_REPO_ID;
                break;
            case 5:
                id = Editor.FILE_EXPORT_REPO_ID;
                break;
            case 6:
                id = Editor.EXPORT_REPO_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    public void createAdminManageEditor(int type) {
        String id = Editor.Login_Log_ID;
        switch (type) {
            case 0:
                id = Editor.Login_Log_ID;
                break;
            case 1:
                id = Editor.Online_User_Management_ID;
                break;
            case 2:
                id = Editor.Operation_Log_ID;
                break;
            case 3:
                id = Editor.FILE_EDITOR_ID;
                break;
            case 4:
                id = Editor.Template_Configuration_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    private void createImportHistEditor() {
        if (workbench.checkEditor(Editor.IMPORT_HIST_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.IMPORT_HIST_REPO_ID)
                    .create();
    }
    private void createExportHistEditor() {
        if (workbench.checkEditor(Editor.EXPORT_HIST_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.EXPORT_HIST_REPO_ID)
                    .create();
    }
    private void createVersionManageEditor() {
        if (workbench.checkEditor(Editor.MANAGE_REPO_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.MANAGE_REPO_ID)
                    .create();
    }

    private void createTableEditor() {
        if (workbench.checkEditor(Editor.TABLE_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.TABLE_EDITOR_ID)
                    .create();
    }
    private void createDbViewEditor() {
        if (workbench.checkEditor(Editor.DB_VIEW_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.DB_VIEW_EDITOR_ID).create();
    }
    private void createSqlWizardEditor() {
        if (workbench.checkEditor(Editor.SQL_WIZARD_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.SQL_WIZARD_EDITOR_ID).create();
    }
    private void apiTest() {
        AbstractView.createApiTestView(new EditorCallback<ApiBean>() {
            @Override
            public ApiBean getValue() {
                return null;
            }

            @Override
            public void setValue(ApiBean value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("entity.api.apitest");
            }

            @Override
            public Page getPage() {
                return secondFuncScrollview.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }, "more").doHighlighted();
    }
    private void apiDoc() {
        try {
            QueryResultVo result = StudioApp.execute(null, new QueryCmd("Api", "*", "", 0));
            //如果不存在,提示用户添加
            if (result.getEntities() == null) {
                WebUtils.showError("未创建API");
                return;
            }
            Filedownload.save(StringToDoc.apiToDoc(result).getBytes("UTF-8"), "application/msword", "EP接口文档");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void apiRAR() {
        try {
            Filedownload.save("~./word/api/APIFile.zip", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void createPageTemplate(int type) {
        if (type == 1) {
            if (workbench.checkEditor(Editor.DESKTOP_LOGIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.DESKTOP_LOGIN_EDITOR_ID)
                        .create();
        } else if (type == 2) {
            if (workbench.checkEditor(Editor.DESKTOP_MAIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.DESKTOP_MAIN_EDITOR_ID)
                        .create();
        } else if (type == 3) {
            if (workbench.checkEditor(Editor.MOBILE_LOGIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.MOBILE_LOGIN_EDITOR_ID)
                        .create();
        } else if (type == 4) {
            if (workbench.checkEditor(Editor.MOBILE_MAIN_EDITOR_ID) == null)
                SimpleEditorFactory.createEditor(workbench, Editor.MOBILE_MAIN_EDITOR_ID)
                        .create();
        }
    }
    public void changeProperties(final Component component) {
        AbstractView.createPropertiesView(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return null;
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("menu.settings");
            }

            @Override
            public Page getPage() {
                return component.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }
    private void createFileEditor() {
        if (new AppServiceFileUtils().webContainerIsLegal() == false) {
            WebUtils.showInfo(Labels.getLabel("editor.appPath.error"));
            return;
        }
        if (workbench.checkEditor(Editor.FILE_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.FILE_EDITOR_ID)
                    .create();
    }
    private void createI18nEditor() {
        if (workbench.checkEditor(Editor.I18n_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.I18n_EDITOR_ID).create();
    }
    private void createModuleEditor() {
        if (workbench.checkEditor(Editor.Module_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench,
                    Editor.Module_EDITOR_ID).create();
    }
    public void createUploadProduct() {
        if (workbench.checkEditor(Editor.PRODUCT_EDITOR_ID) == null)
            SimpleEditorFactory.createEditor(workbench, Editor.PRODUCT_EDITOR_ID)
                    .create();
    }
    private void changeToolProperties() {
        AbstractView.createToolProperties(new EditorCallback<String>() {
            @Override
            public String getValue() {
                return null;
            }

            @Override
            public void setValue(String value) {

            }

            @Override
            public String getTitle() {
                return Labels.getLabel("menu.settings");
            }

            @Override
            public Page getPage() {
                return secondFuncScrollview.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }
    public void createMobileExportEditor(int type) {
        String editorType = Editor.iOS_EXPORT_REPO_ID;
        switch (type) {
            case 0:
                new iOSExportAction(workbench).on();
                break;
            case 1:
                new AndroidExportAction(workbench).on();
                break;
            case 2:
                new WXExportAction(workbench).on();
                break;
        }
    }
    private void createGuideConfig(int type) {
        String id = Editor.APP_GUIDE_CONFIG_ID;
        switch (type) {
            case 1:
                id = Editor.APP_GUIDE_CONFIG_ID;
                break;
            case 2:
                id = Editor.STUDIO_GUIDE_CONFIG_ID;
                break;
            case 3:
                id = Editor.APP_GUIDE_SET_ID;
                break;
            case 4:
                id = Editor.STUDIO_GUIDE_ABOUT_ID;
                break;
        }
        if (workbench.checkEditor(id) == null)
            SimpleEditorFactory.createEditor(workbench, id)
                    .create();
    }

    //UI
    private void closeFunctionBar() {
        secondFuncScrollview.setVisible(false);
        thirdFuncScrollview.setVisible(false);
        workbench.getMainLayout().showFunc(false);
    }
    private void SetSecondFunction(String funcID) {
        String[] aNameList = null;
        String[] labelNameList = null;
        Boolean[] ableList = null;
        Boolean isOtherCell = false;
        LoginUserVo user = Contexts.getUser();
        if (funcID.equals("functionBar_vlayout_authorization")) {
            aNameList = new String[]{"icon-xiangmu-shouquan-jigouguanli", "icon-xiangmu-shouquan-jiaoseguanli",
                    "icon-xiangmu-shouquan-fenzuguanli", "icon-xiangmu-shouquan-yonghuguanli", "icon-caidanguanli", "icon-shouquanbiaoshi"};
            labelNameList = new String[]{"menu.org", "menu.role", "menu.group", "menu.user", "menu.menu", "menu.access"};
            ableList = new Boolean[]{true, true, true, true, true, true};
            secondFuncScrollview.setStyle("top: 176px;");//38 138
        } else if (funcID.equals("functionBar_vlayout_version")) {
            aNameList = new String[]{"icon-daoru", "icon-daochu", "icon-daorulishi", "icon-daochulishi", "icon-banbenguanli"};
            labelNameList = new String[]{"button.import", "button.export", "menu.repo.import", "menu.repo.export", "menu.repo.hist"};
            ableList = new Boolean[]{user.isAuthorized("entityImport"), user.isAuthorized("entityExport"),
                    user.isAuthorized("repoImport"), user.isAuthorized("repoExport"), user.isAuthorized("repoHist")};
            secondFuncScrollview.setStyle("top: 272px;");//138+170
        } else if (funcID.equals("functionBar_vlayout_database")) {
            aNameList = new String[]{"icon-gongju-canshubiaoguanli", "icon-gongju-shujukuliulanqi", "icon-gongju-SQLshengchengxiangdao"};
            labelNameList = new String[]{"menu.table.refactor", "menu.db.view", "menu.sql.wizard"};
            ableList = new Boolean[]{user.isAuthorized("tableRefactor"), true,
                    user.isAuthorized("sqlWizard")};
            secondFuncScrollview.setStyle("top: 364px;");//138+226
        } else if (funcID.equals("functionBar_vlayout_api")) {
            aNameList = new String[]{"icon-gongju-APIceshi", "icon-gongju-APIwendangxiazai", "icon-APIshiyongshoucexiazai"};
            labelNameList = new String[]{"entity.api.apitest", "entity.api.doc.download", "entity.api.doc.use.rar"};
            ableList = new Boolean[]{user.isAuthorized("apiTest"), user.isAuthorized("apiDoc"),
                    user.isAuthorized("apiRAR")};
            secondFuncScrollview.setStyle("top: 458px;");//320+138
        } else if (funcID.equals("functionBar_vlayout_other")) {
            isOtherCell = true;
            refreshGuide();
            if (paths.size() == 0) {
                aNameList = new String[]{"icon-zhuye-", "icon-daoru", "icon-daochu", "icon-quanxian-", "icon-gongju-",
                        "icon-gongju-daochuyidongduanxiangmu", "icon-xitongbangzhu-"};
                labelNameList = new String[]{"menu.project", "menu.import", "menu.export", "menu.studio.authorization",
                        "menu.tools", "menu.tools.mobile", "menu.help"};
                ableList = new Boolean[]{true, true, true, true, true, true, true};
            } else {
                aNameList = new String[]{"icon-zhuye-", "icon-daoru", "icon-daochu", "icon-quanxian-", "icon-gongju-",
                        "icon-gongju-daochuyidongduanxiangmu", "icon-bangzhu-houduanyindao", "icon-xitongbangzhu-"};
                labelNameList = new String[]{"menu.project", "menu.import", "menu.export", "menu.studio.authorization",
                        "menu.tools", "menu.tools.mobile", "studio.guide.title", "menu.help"};
                ableList = new Boolean[]{true, true, true, true, true, true, true, true};
            }
            secondFuncScrollview.setStyle("top: 328px;");
        }
        secondFuncScrollview.getChildren().clear();
        setFuncCell(Arrays.asList(aNameList), Arrays.asList(labelNameList), Arrays.asList(ableList),
                Arrays.asList(labelNameList), true, isOtherCell);
    }
    private void SetThirdFunction(String funcID) {
        String[] aNameList = null;
        String[] labelNameList = null;
        Boolean[] ableList = null;
        String topString = null;
        LoginUserVo user = Contexts.getUser();
        if (funcID.equals("studio.guide.title")) {
            List<String> aList = new ArrayList<>();
            List<String> nameList = new ArrayList<>();
            List<Boolean> ableArrayList = new ArrayList<>();
            List<String> idList = new ArrayList<>();
            for (int index = 0; index < paths.size(); index++) {
                aList.add("icon-bangzhu-houduanyindao");
                nameList.add(paths.get(index).get(0).getGuideName());
                ableArrayList.add(true);
                idList.add("guideItem" + Integer.valueOf(index));
            }
            thirdFuncScrollview.getChildren().clear();
            setFuncCell(aList, nameList, ableArrayList, idList, false, false);
            if (aList.size() > 0 && aList.size() <= 2) {
                thirdFuncScrollview.setStyle("top: 556px;overflow-y: none;");
                thirdFuncScrollview.setHeight(null);
            }
            else if (aList.size() > 15) {
                thirdFuncScrollview.setStyle("top: 76px;overflow-y: scroll;");
                thirdFuncScrollview.setHeight("584px");//584
            }
            else {
                int height = 664 - 38 * aList.size() - 28;
                thirdFuncScrollview.setStyle("top: " + height + "px;overflow-y: none;");
                thirdFuncScrollview.setHeight(null);
            }
        } else {
            if (funcID.equals("menu.project")) {
                topString = "top: 328px;";
                //icons-QQ
                //icon-xiangmu-diannaoduandengluyemian
                aNameList = new String[]{"icon-xiangmu-diannaoduandengluyemian", "icon-diannaoduanzhuyemian",
                        "icon-xiangmu-shoujiduandengluyemian", "icon-shoujiduanzhuyemian", "icon-gongju-peizhi"};
                labelNameList = new String[]{"menu.project.pc.login", "menu.project.pc.main", "menu.project.mobile.login",
                        "menu.project.mobile.main", "menu.settings"};
                ableList = new Boolean[]{user.isAuthorized("desktopLoginPage"), user.isAuthorized("desktopMainPage"),
                        user.isAuthorized("mobileLoginPage"), user.isAuthorized("mobileMainPage"),
                        user.isAuthorized("projectSettings")};
            } else if (funcID.equals("menu.import")) {
                topString = "top: 366px;";
                aNameList = new String[]{"icon-xiangmu-daoru-gengongnengdaoru", "icon-xiangmu-daoru-zidiandaoru",
                        "icon-xiangmu-daoru-duoguoyuyandaoru", "icon-xiangmu-daoru-xiaoxidaoru", "icon-xiangmu-daoru-caidandaoru",
                        "icon-xiangmu-daoru-wenjiandaoru"};
                labelNameList = new String[]{"menu.repo.import.main.entity", "menu.repo.import.dictionary", "menu.repo.import.label",
                        "menu.repo.import.inform", "menu.repo.import.menu", "menu.repo.import.file"};
                ableList = new Boolean[]{user.isAuthorized("importMainEntity"), user.isAuthorized("importDictionary"),
                        user.isAuthorized("importLabel"), user.isAuthorized("importInform"),
                        user.isAuthorized("importMenu"), user.isAuthorized("importFile")};
            } else if (funcID.equals("menu.export")) {
                topString = "top: 404px;";
                aNameList = new String[]{"icon-xiangmu-daoru-gengongnengdaoru", "icon-xiangmu-daoru-zidiandaoru",
                        "icon-xiangmu-daoru-duoguoyuyandaoru", "icon-xiangmu-daoru-xiaoxidaoru", "icon-xiangmu-daoru-caidandaoru",
                        "icon-xiangmu-daoru-wenjiandaoru"};
                labelNameList = new String[]{"menu.repo.export.main.entity", "menu.repo.export.dictionary", "menu.repo.export.label",
                        "menu.repo.export.inform", "menu.repo.export.menu", "menu.repo.export.file"};
                ableList = new Boolean[]{user.isAuthorized("exportMainEntity"), user.isAuthorized("exportDictionary"),
                        user.isAuthorized("exportLabel"), user.isAuthorized("exportInform"),
                        user.isAuthorized("exportMenu"), user.isAuthorized("exportFile")};
            } else if (funcID.equals("menu.studio.authorization")) {
                topString = "top: 442px;";
                aNameList = new String[]{"icon-xiangmu-shouquan-yonghuguanli", "icon-xiangmu-shouquan-jiaoseguanli"};
                labelNameList = new String[]{"menu.studio.user", "menu.studio.role"};
                ableList = new Boolean[]{true, true};
            } else if (funcID.equals("menu.tools")) {
                topString = "top: 442px;";
                aNameList = new String[]{"icon-gongju-yingyongwenjianguanli", "icon-xiangmu-daoru-duoguoyuyandaoru",
                        "icon-gongju-chanpinmokuaishezhi", "icon-bangzhu-fabuchanpin", "icon-gongju-peizhi"};
                labelNameList = new String[]{"menu.appfile", "menu.i18n", "editor.product.set", "menu.product", "menu.tools.settings"};
                ableList = new Boolean[]{user.isAuthorized("appExplorer"), user.isAuthorized("i18n"),
                        user.isAuthorized("module"), user.isAuthorized("releaseProduct"), user.isAuthorized("preference")};
            } else if (funcID.equals("menu.tools.mobile")) {
                topString = "top: 518px;";
                aNameList = new String[]{"icon-gongju-daochuyidongduanxiangmu-daochuiosxiangmu",
                        "icon-gongju-daochuyidongduanxiangmu-daochuanzhuoxiangmu",
                        "icon-gongju-daochuyidongduanxiangmu-daochuweixinxiaochengxu"};
                labelNameList = new String[]{"menu.tools.mobile.iOS", "menu.tools.mobile.Android", "menu.tools.mobile.MiniProgram"};
                ableList = new Boolean[]{user.isAuthorized("mobile_iOS"), user.isAuthorized("mobile_Android"),
                        user.isAuthorized("mobile_MiniProgram")};
            } else if (funcID.equals("menu.help")) {
                topString = "top: 290px;";
                aNameList = new String[]{"icon-bangzhu-qianduanyindaoshezhi", "icon-bangzhu-qianduankongjianIDshezhi",
                        "icon-bangzhu-houduanyindao", "icon-bangzhu-hanshushuoming",
                        "icon-bangzhu-zujianshuoming", "icon-bangzhu-yonghuzhinan", "icon-bangzhu-cankaoshouce",
                        "icon-bangzhu-kaifaluntan", "icon-bangzhu-guanyu"};
                labelNameList = new String[]{"app.guide.config", "menu.app.guide.widgetID", "studio.guide.config",
                        "menu.logic", "menu.component", "menu.guide", "menu.reference", "menu.forum",
                        "menu.about"};
                ableList = new Boolean[]{user.isAuthorized("appGuideConfig"), user.isAuthorized("appGuideWidgetID"),
                        user.isAuthorized("studioGuideConfig"), true, true, true, true, true, true};
            }
            thirdFuncScrollview.getChildren().clear();
            setFuncCell(Arrays.asList(aNameList), Arrays.asList(labelNameList), Arrays.asList(ableList), Arrays.asList(labelNameList),
                    false , false);
            thirdFuncScrollview.setStyle(topString);
            thirdFuncScrollview.setHeight(null);
        }
    }
    private void setFuncCell(List<String> aNameList, List<String> labelNameList, List<Boolean> ableList, List<String> idList,
                             Boolean isSecondFunc, Boolean isOtherCell) {
        Div emptyDiv = new Div();
        emptyDiv.setHeight("14px");
        emptyDiv.setHflex("1");
        if (isSecondFunc == true)
            secondFuncScrollview.appendChild(emptyDiv);
        else
            thirdFuncScrollview.appendChild(emptyDiv);
        for (int index = 0; index < aNameList.size(); index++) {
            Div areaDiv = new Div();
            areaDiv.setHeight("38px");
            areaDiv.setHflex("1");
            if (isSecondFunc == true)
                secondFuncScrollview.appendChild(areaDiv);
            else
                thirdFuncScrollview.appendChild(areaDiv);

            Hlayout hlayout = new Hlayout();
            hlayout.setVflex("1");
            hlayout.setHflex("1");
            hlayout.setSpacing("0");
            hlayout.setId(idList.get(index));
            hlayout.setZclass("ui-div-center-parent");
            if (ableList.get(index) == true) {
                if (isSecondFunc == true)
                    hlayout.setSclass("ui-functionBar-secondFunc-layout");
                else
                    hlayout.setSclass("ui-functionBar-thirdFunc-layout");
                if (isOtherCell) {
                    hlayout.addEventListener(Events.ON_MOUSE_OVER, this);
                    hlayout.addEventListener(Events.ON_MOUSE_OUT, this);
                } else
                    hlayout.addEventListener(Events.ON_CLICK, this);
            } else {
                if (isSecondFunc == true)
                    hlayout.setSclass("ui-functionBar-secondFunc-layout-disable");
                else
                    hlayout.setSclass("ui-functionBar-thirdFunc-layout-disable");
            }

            areaDiv.appendChild(hlayout);

            Div aEmptyDiv = new Div();
            aEmptyDiv.setVflex("1");
            aEmptyDiv.setWidth("14px");
            hlayout.appendChild(aEmptyDiv);

            A iconA = new A();
            iconA.setSclass("icon font_family ".concat(aNameList.get(index)).concat(" ui-functionBar-a"));
            iconA.setStyle("font-size: 18px;");
            hlayout.appendChild(iconA);

            Div bEmptyDiv = new Div();
            bEmptyDiv.setVflex("1");
            bEmptyDiv.setWidth("10px");
            hlayout.appendChild(bEmptyDiv);

            String name = Labels.getLabel(labelNameList.get(index));
            if (Strings.isBlank(Labels.getLabel(labelNameList.get(index)))) {
                name = labelNameList.get(index);
            }
            Label label = new Label(name);
            label.setStyle("display: block;font-size: 14px;");
            label.setSclass("ui-functionBar-label");
            label.setHflex("1");
            label.setHeight("18px");
            hlayout.appendChild(label);

            if (isOtherCell == true) {
                A nextA = new A();
                nextA.setSclass("icon font_family icon-xiayiji ui-functionBar-a");
                nextA.setStyle("font-size: 14px;");
                hlayout.appendChild(nextA);
            }

            Div secondDiv = new Div();
            secondDiv.setWidth("14px");
            secondDiv.setVflex("1");
            hlayout.appendChild(secondDiv);
        }
        Div lastEmptyDiv = new Div();
        lastEmptyDiv.setHeight("14px");
        lastEmptyDiv.setHflex("1");
        if (isSecondFunc == true) {
            secondFuncScrollview.appendChild(lastEmptyDiv);
        }
        else
            thirdFuncScrollview.appendChild(lastEmptyDiv);
    }
    //introjs
    private void setIntro(Component root) {
        introJs = (IntroJs) root.query("introJs");//初始化引导控件对象
        introJs.addEventListener(IntroJs.ON_NEXT, new EventListener<NextEvent>() {//点击下一步按钮(包括结束)便会进入此监听
            @Override
            public void onEvent(NextEvent nextEvent) throws Exception {
                Operation operation = nextEvent.getOperation();
                int operationStep = operation.getOperationStep();//操作的第几步
                Component component = operation.getOperationElement();//该操作对应的控件对象
                List<Action> actionList = operation.getActionList();//要操作的动作集合(里面包含要操作的对象，事件，值)；

                for (Action action : actionList) {
                    String actionEvent = action.getActionEvent();//获得动作事件（onClick等）
                    String actionValue = action.getActionValue();//获得值
                    Component actionComp = action.getActionComponent();//获得动作控件的对象

                    /*A comp = (A) action.getActionComponent();
                        if (Strings.isBlank(actionEvent) == false){
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }*/
                    HeaderControllerImpl.CompType compType = HeaderControllerImpl.CompType.CompOnlyClick;
                    if (actionComp == null) {
                        System.out.println(actionComp);
                    }
                    if (actionComp.getClass().equals(Label.class)) {
                        compType = HeaderControllerImpl.CompType.CompClickAndValue;
                        Label comp = (Label) actionComp;
                        comp.setValue(actionValue);///设置值
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }
                    } else if (actionComp.getClass().equals(Textbox.class)) {
                        compType = HeaderControllerImpl.CompType.CompClickAndValue;
                        Textbox comp = (Textbox) actionComp;
                        comp.setValue(actionValue);
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, comp));///模拟事件
                        }
                    } else if (actionComp.getClass().equals(Bandbox.class) || actionComp.getClass().equals(BandboxExt.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Bandbox comp = (Bandbox) actionComp;
                        comp.setValue(actionValue);
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onOpen")) {
                            Events.postEvent(new OpenEvent(Events.ON_OPEN, comp, true, null, actionValue));
                            comp.open();
                        }
                    } else if (actionComp.getClass().equals(Combobox.class) || actionComp.getClass().equals(ComboboxExt.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        ///有时候返回的是+Ext的对象
                        Combobox comp = (Combobox) actionComp;

                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen"))
                                comp.open();
                            else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                Comboitem comboitem = comp.getItemAtIndex(Integer.valueOf(actionValue) - 1);
                                comp.setSelectedItem(comboitem);
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Menu.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Menu comp = (Menu) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen")) {
                                if (!comp.getParent().getClass().equals(Menupopup.class)) {
                                    comp.getMenupopup().setZIndex(1800);
                                    comp.open();
                                } else {
                                    comp.getMenupopup().setZIndex(1801);
                                    comp.getMenupopup().open(comp, "absolute");
                                }
                            } else if (actionEvent.equals("onClose")) {
                                comp.getMenupopup().close();
                            }
                        }
                    } else if (actionComp.getClass().equals(Tab.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Tab comp = (Tab) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onSelect")) {
                            comp.setSelected(true);
                        }
                    } else if (actionComp.getClass().equals(Checkbox.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Checkbox comp = (Checkbox) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onCheck")) {
                            comp.setChecked(true);
                        }
                    } else if (actionComp.getClass().equals(Tree.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Tree comp = (Tree) actionComp;
                        if (Strings.isBlank(actionEvent) == false && actionEvent.equals("onCheck")) {

                        }
                    } else if (actionComp.getClass().equals(Toolbarbutton.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Toolbarbutton comp = (Toolbarbutton) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onOpen")) {
                                String positionStr = null;
                                if (comp.getPopup().split(",").length > 1) {
                                    positionStr = comp.getPopup().split(",")[1].trim();
                                }
                                Menupopup menupopup = (Menupopup) (comp.getNextSibling());
                                //Menupopup menupopup = ((Menupopup) comp.getPage().getFellowIfAny(comp.getPopup().split(",")[0]));
                                if (positionStr != null) {
                                    menupopup.open(comp, positionStr);
                                } else {
                                    menupopup.open(comp);
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Menupopup.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Menupopup comp = (Menupopup) actionComp;
                        if (Strings.isBlank(actionEvent) == false) {
                            if (actionEvent.equals("onClose")) {
                                comp.close();
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Listbox.class) || actionComp.getClass().equals(ListboxExt.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Listbox comp = (Listbox) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_DOUBLE_CLICK) && Strings.isBlank(actionValue) == false) {
                                Listitem listitem = comp.getItems().get(Integer.valueOf(actionValue) - 1);
                                Events.postEvent(new Event(Events.ON_DOUBLE_CLICK, listitem));
                            } else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                String[] actions = actionValue.split(",");
                                if (actionValue.split(",").length > 0) {
                                    if (actions.length == 1 && actions[0].equals("all")) {
                                        comp.selectAll();
                                    } else if (actions.length > 0) {
                                        Set<Listitem> listitems = new HashSet<>();
                                        for (String line : actions) {
                                            if (StringUtil.isInteger(line)) {
                                                Listitem listitem = comp.getItems().get(Integer.valueOf(actionValue) - 1);
                                                listitems.add(listitem);
                                            }
                                            comp.setSelectedItems(listitems);
                                        }
                                    } else {
                                        Events.postEvent(new Event(actionEvent, actionComp));
                                    }
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Grid.class) || actionComp.getClass().equals(GridExt.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Grid comp = (Grid) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_DOUBLE_CLICK) && Strings.isBlank(actionValue) == false) {
                                Row row = (Row) comp.getRows().getChildren().get(Integer.valueOf(actionValue) - 1);
                                Events.postEvent(new Event(Events.ON_DOUBLE_CLICK, row));
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Window.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Window comp = (Window) actionComp;
                        if (!Strings.isBlank(actionValue)) {
                            comp.setZindex(Integer.valueOf(actionValue));
                        }
                    } else if (actionComp.getClass().equals(Menuitem.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Menuitem comp = (Menuitem) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_CHECK) && Strings.isBlank(actionValue) == false &&
                                    (actionValue.equals("true") || actionValue.equals("false"))) {
                                if (actionValue.equals("true"))
                                    comp.setChecked(true);
                                else
                                    comp.setChecked(false);
                                Events.postEvent(new Event(Events.ON_CLICK, actionComp));
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    } else if (actionComp.getClass().equals(Tabbox.class)) {
                        compType = HeaderControllerImpl.CompType.CompSpecial;
                        Tabbox comp = (Tabbox) actionComp;
                        if (!Strings.isBlank(actionEvent)) {
                            if (actionEvent.equals(Events.ON_CLOSE) && Strings.isBlank(actionValue) == false) {
                                if (actionValue.equals("all"))
                                    closeAllTabs(comp);
                                else if (StringUtil.isInteger(actionValue) == true) {
                                    Tab tab = (Tab) comp.getTabs().getChildren().get(Integer.valueOf(actionValue) - 1);
                                    tab.setAutoClose(true);
                                    tab.close();
                                }
                            } else if (actionEvent.equals(Events.ON_SELECT) && Strings.isBlank(actionValue) == false) {
                                if (StringUtil.isInteger(actionValue) == true) {
                                    comp.setSelectedIndex(Integer.valueOf(actionValue) - 1);
                                }
                            } else
                                Events.postEvent(new Event(actionEvent, actionComp));
                        }
                    }
                    if (compType == HeaderControllerImpl.CompType.CompOnlyClick) {
                        if (Strings.isBlank(actionEvent) == false) {
                            Events.postEvent(new Event(actionEvent, actionComp));///模拟事件
                        }
                    }
                }
            }
        });
    }
    private void closeAllTabs(Tabbox tabbox) {
        List<Tab> tabs = tabbox.getTabs().getChildren();
        List<Tab> copyTabs = new ArrayList<>();
        for (Tab tab : tabs) {
            copyTabs.add(tab);
        }
        for (Tab tab : copyTabs) {
            if (!tab.getLabel().equals(Labels.getLabel("app.welcome"))) {
                tab.setAutoClose(true);
                tab.close();
            }
        }
    }
}
