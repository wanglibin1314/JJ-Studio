/**
 * Copyright www.epclouds.com ©2016
 */
package cn.easyplatform.studio.web.servlet;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.AppTemplateUtil;
import cn.easyplatform.studio.utils.FileUtil;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">陈云亮</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/12 14:41
 * @Modified By:
 */
@WebServlet(name = "templateServlet", value = "/template/*")
public class TemplateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doPost(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ProjectBean env = (ProjectBean) req.getSession().getAttribute(Contexts.EP_PROJECT);
        String projectName = (String)req.getSession().getAttribute("projectName");
        if (env == null) {
            resp.sendError(401);
        } else {
            String url = req.getRequestURI().substring(
                    req.getContextPath().length() + 10);
            String[] data = url.split("/");
            if (data.length < 3) {
                resp.sendError(400);
            } else {
                /*if (!"d0".equals(data[0]) && !"d1".equals(data[0]) && !"m0".equals(data[0]) && !"m1".equals(data[0])) {
                    resp.sendError(403);
                    return;
                }*/
                String path = StudioApp.me().getConfig("app.template.path");
                if (Strings.isBlank(path))
                    path = new AppTemplateUtil().getAppTemplatePath(projectName);
                /*path = path + "/" + env.getAppContext();*/
                File file = new File(path + "/" + url);
                if (!file.exists() || !file.isFile()) {
                    resp.sendError(404);
                    return;
                }
                String md5 = file.lastModified() + "";
                if (md5.equals(req.getHeader("if-none-match"))) {
                    resp.addHeader("ETag", file.lastModified() + "");
                    resp.sendError(304);
                } else {
                    byte[] content = FileUtils.readFileToByteArray(file);
                    String contentType = FileUtil.getFileContentType(file.getPath());
                    resp.setContentType(contentType + ";charset=utf-8");
                    //resp.addHeader("Cache-Control", "No-cache");
                    resp.addHeader("ETag", file.lastModified() + "");
                    resp.setContentLength(content.length);
                    resp.addHeader("content-type", contentType);
                    OutputStream os = resp.getOutputStream();
                    os.write(content);
                    os.flush();
                    os.close();
                }
            }
        }
    }

}
