/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.servlet;

import cn.easyplatform.studio.context.Lifecycle;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.util.SessionCleanup;
import org.zkoss.zk.ui.util.SessionInit;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebSessionListener implements SessionInit, SessionCleanup {

	@Override
	public void cleanup(Session sess) throws Exception {
		Lifecycle.endSession(sess);
	}

	@Override
	public void init(Session sess, Object request) throws Exception {
		Lifecycle.beginSession(sess);
	}

}
