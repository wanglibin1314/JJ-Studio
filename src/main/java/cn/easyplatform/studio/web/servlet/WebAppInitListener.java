/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.servlet;

import cn.easyplatform.studio.context.Lifecycle;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.util.WebAppCleanup;
import org.zkoss.zk.ui.util.WebAppInit;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebAppInitListener implements WebAppInit, WebAppCleanup {

	@Override
	public void cleanup(WebApp wapp) throws Exception {
		Lifecycle.endApp();
	}

	@Override
	public void init(WebApp wapp) throws Exception {
		Lifecycle.beginApp(wapp);
	}

}
