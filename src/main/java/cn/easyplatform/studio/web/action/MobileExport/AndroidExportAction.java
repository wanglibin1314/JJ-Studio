package cn.easyplatform.studio.web.action.MobileExport;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.utils.apkrepackage.APKInfo;
import cn.easyplatform.studio.utils.apkrepackage.APKSignatureService;
import cn.easyplatform.studio.vos.MobileConfVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.io.File;
import java.io.FileNotFoundException;


public class AndroidExportAction extends MobileAction implements EventListener<Event>,APKSignatureService.OnStatusListener {

    /**
     * @param workbench
     */
    public AndroidExportAction(WorkbenchController workbench) {
        super(workbench);

    }

    @Override
    public void on() {
        Component c = Executions.createComponents(
                "~./include/mobileExport/AndroidExport.zul", null, null);
        window = (Window)c.query("window");
        bundleTextbox = (Textbox)c.getFellow("bundleid");
        bundleTextbox.addEventListener(Events.ON_CHANGE,this);
        displayNameTextbox = (Textbox)c.getFellow("displayname");
        displayNameTextbox.addEventListener(Events.ON_CHANGE,this);
        versionTextbox = (Textbox)c.getFellow("versionnum");
        versionTextbox.addEventListener(Events.ON_CHANGE,this);
        urlTextbox = (Textbox)c.getFellow("urlTextbox");
        urlTextbox.addEventListener(Events.ON_CHANGE,this);
        iconDiv = (Div)c.getFellow("iconDiv");
        iconImg = (Image)c.getFellow("iconShow");
        iconBtn = (Button)c.getFellow("iconBtn");
        iconBtn.addEventListener(Events.ON_UPLOAD,this);
        bigPortraitDiv = (Div)c.getFellow("launchDiv");
        launchBigPortraitImg = (Image)c.getFellow("launchShow");
        launchBigPortraitBtn = (Button)c.getFellow("launchBtn");
        launchBigPortraitBtn.addEventListener(Events.ON_UPLOAD,this);
        finishBtn = (Button)c.getFellow("finishBtn");
        finishBtn.addEventListener(Events.ON_CLICK,this);

        helpBtn = (Button)c.getFellow("helpBtn");
        helpBtn.addEventListener(Events.ON_CLICK,this);

        setDefaultConf(MobileConfVo.ANDROIDCONF);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Component c = event.getTarget();
        if (c == launchBigPortraitBtn || c == iconBtn)
        {
            isChangeContent = true;
            verifyImage(event);
        }
        else if (c == displayNameTextbox || c == bundleTextbox || c == versionTextbox || c == urlTextbox)
        {
            isChangeContent = true;
        }
        else if (c == finishBtn)
        {
            if (uploaToDatabase(MobileConfVo.ANDROIDCONF))
                exportProduct();
        }
        else if (c == helpBtn)
        {
            //下载
            try {
                //Filedownload.save("~./mobileProduct/android_help.docx",null);
                String path = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/android_help.docx");
                Filedownload.save(new File(path),null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void exportProduct()
    {
        String apkPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/apkSigned/app-release.apk");
        APKInfo apkInfo = new APKInfo(apkPath, vo.getDisplayName(), vo.getBundleId(), "66",
                vo.getMobileVersion(), "", StudioApp.getServletContext().getRealPath(vo.getIcon()) ,
                "", "", "", "", StudioApp.getServletContext().getRealPath(vo.getLaunch()),
                vo.getUrl());
        String sinedApkName = APKSignatureService.getInstance().signature(apkInfo,AndroidExportAction.this);
        //下载
        try {
            Filedownload.save(new File(sinedApkName),null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        window.onClose();
    }

    @Override
    public void onfinish() {
        System.out.println("onfinish");
    }
}
