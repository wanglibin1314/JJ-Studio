package cn.easyplatform.studio.web.action.MobileExport;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.ZipUtil;
import cn.easyplatform.studio.vos.MobileConfVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class WXExportAction extends MobileAction implements EventListener<Event> {
    /**
     * @param workbench
     */
    public WXExportAction(WorkbenchController workbench) {
        super(workbench);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Component c = event.getTarget();
        if (c == finishBtn)
        {
            if (uploaToDatabase(MobileConfVo.WXCONF))
                exportProduct();
        }
        else if (c == bundleTextbox || c == displayNameTextbox || c == urlTextbox)
        {
            isChangeContent = true;
        }
        else if (c == helpBtn)
        {
            //下载
            try {
                String path = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/MiniProgram_help.docx");
                Filedownload.save(new File(path),null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void on() {
        Component c = Executions.createComponents(
                "~./include/mobileExport/WXExport.zul", null, null);
        window = (Window)c.query("window");
        bundleTextbox = (Textbox)c.getFellow("appid");
        bundleTextbox.addEventListener(Events.ON_CHANGE,this);
        displayNameTextbox = (Textbox)c.getFellow("displayname");
        displayNameTextbox.addEventListener(Events.ON_CHANGE,this);
        urlTextbox = (Textbox)c.getFellow("urlTextbox");
        urlTextbox.addEventListener(Events.ON_CHANGE,this);
        finishBtn = (Button)c.getFellow("finishBtn");
        finishBtn.addEventListener(Events.ON_CLICK,this);

        helpBtn = (Button)c.getFellow("helpBtn");
        helpBtn.addEventListener(Events.ON_CLICK,this);

        setDefaultConf(MobileConfVo.WXCONF);
    }

    private void exportProduct()
    {
        //appid projectname
        String jsonPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epshow/project.config.json");
        String jsonResult = null;
        try {
            jsonResult = FileUtil.getStreamWithFile(jsonPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = JSON.parseObject(jsonResult);
        jsonObject.put("appid",vo.getBundleId());
        jsonObject.put("projectname",vo.getDisplayName());
        jsonResult = jsonObject.toJSONString();
        FileUtil.setStreamWithFile(jsonResult,jsonPath);

        String contentPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epshow/pages/index/index.wxml");
        String content = "<web-view src=\"" + vo.getUrl() + "\"></web-view>";
        FileUtil.setStreamWithFile(content,contentPath);

        //copy路径
        File destDir = new File(StudioApp.getServletContext().getRealPath(
                FileUtil.getCachePath() + "/" + Contexts.getProject().getName() + "/"));
        if (!destDir.exists()) {
            try {
                FileUtils.forceMkdir(destDir);//在根创建了文件夹
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String oldFile = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epshow");
        String newFile = StudioApp.getServletContext().getRealPath(FileUtil.getCachePath() +
                "/" + Contexts.getProject().getName() + "/" + vo.getDisplayName());
        try {
            FileUtil.copyDir(oldFile,newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //zip
        String zipPath = StudioApp.getServletContext().getRealPath(FileUtil.getCachePath() +
                "/" + Contexts.getProject().getName() + "/" + vo.getDisplayName() + ".zip");
        try {
            ZipUtil.compress(newFile,zipPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //下载
        try {
            Filedownload.save(new File(zipPath),null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        window.onClose();
    }
}
