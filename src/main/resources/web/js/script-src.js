var epstudio = {

	selectWidget : function(r,e) {
		var fromEditor = typeof e === "string",root = jq(zk.Widget.$(r)),target = fromEditor? zk.Widget.$(e):e;
		zAu.cmd0.clearWrongValue(root.find('input[type="text"]'));
		root.find('[class~=epstudio-selected]').removeClass("epstudio-selected");
		if(target){
			if(target.$instanceof(zul.layout.LayoutRegion)){
				jq(target.$n().firstChild).addClass("epstudio-selected");
			}else{
				jq(target).addClass("epstudio-selected");
			}
			zAu.cmd0.scrollIntoView(target.uuid);
		}
	},

	selectEditorWidget : function(wgt) {
		var n, b = wgt.getTree;
		n = b ? jq(wgt.getTree()) : jq(wgt.parent.parent.$n());
		n.find("[class~=epstudio-selected]").removeClass("epstudio-selected");
		jq(wgt.$n().firstChild).addClass("epstudio-selected");
	},

	selectGridGroup : function(wgt) {
		if (wgt.isOpen()) {
			var n = jq(wgt.parent.$n());
			jq.each(n.find("@group[class~=z-group]"), function(i, g) {
				var w = zk.Widget.$(g);
				if (w != wgt && w.isRealVisible())
					w.setOpen(false);
			});
		}
	},

	prepareEntityToolbox : function(id) {
		var toolbox = jq('$' + id);
		var wgt = zk.Widget.$(toolbox);
		toolbox.resizable({
			maxHeight : 580,
			minHeight : 200,
			stop : function(event, ui) {
				wgt.setHeight(Math.round(ui.size.height) + "px");
			}
		});
		toolbox.find('.panel-title').prepend(
				'<i class="z-icon-list-alt" style="margin-right:5px"></i>');
		toolbox
				.find('.panel-heading .btn-group')
				.append(
						'<i class="toolbox-roll z-icon-caret-up"></i><i class="toolbox-close z-icon-times" style="padding-left:10px">');
		toolbox.find('.toolbox-close').click(function() {
			zAu.send(new zk.Event(wgt, 'onClose'));
			wgt.detach();
		});
		toolbox.find('.toolbox-roll').click(function() {
			var rollbtn = toolbox.find(".toolbox-roll");
			if (wgt.isOpen()) {
				toolbox.resizable('disable');
				wgt.setHeight();
				wgt.setOpen(false);
				rollbtn.removeClass("z-icon-caret-up");
				rollbtn.addClass("z-icon-caret-down");
			} else {
				toolbox.resizable('enable');
				wgt.setOpen(true);
				rollbtn.removeClass("z-icon-caret-down");
				rollbtn.addClass("z-icon-caret-up");
			}
		});
	},
    meterUpdate : function (e,desc) {
        var score = epstudio.strengthMeasure(e.value),
            meter = $("$meter"),
            meterWidget = zk.Widget.$(meter);
        switch (score) {
            case 1:
            case 2:
                meterWidget.setSclass("meter meter-red");
                break;
            case 3:
            case 4:
                meterWidget.setSclass("meter meter-orange");
                break;
            case 5:
            case 6:
                meterWidget.setSclass("meter meter-green");
                break;
            default:
                meterWidget.setSclass("meter");
        }
        zk.Widget.$($(".meter-inner")).setWidth(score * meter.width() / desc.length + "px");
        zk.Widget.$("$msg").setValue(desc[score]);
    },
    strengthMeasure  : function(text) {
        var score = 0;
        if (text.length > 0)
            score++;
        if (text.length > 6)
            score++;
        if ((text.match(/[a-z]/)) && (text.match(/[A-Z]/)))
            score++;
        if (text.match(/\d+/))
            score++;
        if (text.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))
            score++;
        if (text.length > 12)
            score++;
        if (text.length == 0)
            score = 0;
        return score;
    }
};