/**
 * Copyright www.epclouds.com ©2016
 */
package cn.easyplatform.studio;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">陈云亮</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/4/19 13:40
 * @Modified By:
 */
public final class Version {
    /**
     * project version
     */
    public static final String VERSION = "${project.version}";
    /**
     * SCM(git) revision
     */
    public static final String SCM_REVISION = "${buildNumber}";
    /**
     * SCM branch
     */
    public static final String SCM_BRANCH = "${scmBranch}";
    /**
     * build timestamp
     */
    public static final String TIMESTAMP = "${buildtimestamp}";
}
