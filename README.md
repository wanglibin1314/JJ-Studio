# 吉吉云开发-开发端
J2Paas 是一个低代码开发平台，只需要少量的代码，甚至不需要代码，就可以进行软件系统开发
## 一、技术特点
### 平台应用架构
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/1.png)

### 平台技术架构：
J2Paas采用分布式服务架构，WEB 服务与应用服务分离的原则，基于微服务框架构建，支持集成 consul、zookeeper 等配置服务组件，提供集群环境的服务发现及治理能力，支持动态自定义负载均衡、跨机房流量调整等高级服务调度能力，基于高并发、高负载场景进行优化，保障生产环境下 RPC 服务高可用。
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/2.png)

## 二、JJ开发工具功能特点
### 1、云端开发，一键生成多端应用
云开发，云应用，无需部署开发环境
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/3.png)
云协同，云管理，提高团队协同效率
一次开发，可同步生成Ios/Android/小程序等多端应用
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/4.png)

### 2、敏捷灵活，开发快人一步
0代码、拖拽式、组件化开发，像搭积木一样简单
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/5.png)
所见即所得，边开发边检查结果
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/6.png)
可视化设计，前端页面可视、流程设计可视、任务调度可视
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/7.png)
海量模板一键调用，节省从零开发的过程，同时支持二开
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/8.png)

### 3、控件化向导化，开发更简单
丰富的页面控件，只需简单的拖拉拽
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/9.png)
开发向导化，手把手教你操作
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/10.png)
齐全的帮助文档及操作指引，入门简单上手快
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/11.png)

### 4、模块化平台化，数据设计更简单
模块化图表设计，支持柱状图、饼状图、漏斗图等各式图表
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/12.png)
数据管理平台化，一目了然
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/13.png)
报表设计Excel化，开发就像做表格一样简单
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/14.png)

### 5、成果更安全，作品转生态
开发成果属于开发者，保护个人知识产权
成果可复用可继承，随时根据企业业务调整快速更新
成果市场化价值化，可发布至平台软件众包市场
![](https://gitee.com/jijiyun_development/JJ-Studio/raw/master/img/15.png)
## 三、JJ Studio可以开发什么软件
企业管理软件：ERP、MIS、CRM、WMS、OA、HR、物流快递管理等

Web应用软件后台：企业Web网站、Web商城、Web管理系统等

小程序：微信小程序、百度小程序、支付宝小程序

APP：企业内部管理APP、企业外部客户使用的消费端APP等